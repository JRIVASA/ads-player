﻿namespace isADS
{
    partial class FrmConfig_Language
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.LangPanel = new System.Windows.Forms.Panel();
            this.Next = new System.Windows.Forms.Button();
            this.Back = new System.Windows.Forms.Button();
            this.SaveLang = new System.Windows.Forms.Button();
            this.lblLanguage = new System.Windows.Forms.Label();
            this.Languages = new System.Windows.Forms.ComboBox();
            this.LangPanel.SuspendLayout();
            this.SuspendLayout();
            // 
            // LangPanel
            // 
            this.LangPanel.Controls.Add(this.Next);
            this.LangPanel.Controls.Add(this.Back);
            this.LangPanel.Controls.Add(this.SaveLang);
            this.LangPanel.Controls.Add(this.lblLanguage);
            this.LangPanel.Controls.Add(this.Languages);
            this.LangPanel.Dock = System.Windows.Forms.DockStyle.Top;
            this.LangPanel.Location = new System.Drawing.Point(0, 0);
            this.LangPanel.Name = "LangPanel";
            this.LangPanel.Size = new System.Drawing.Size(457, 103);
            this.LangPanel.TabIndex = 0;
            this.LangPanel.Paint += new System.Windows.Forms.PaintEventHandler(this.LangPanel_Paint);
            // 
            // Next
            // 
            this.Next.Font = new System.Drawing.Font("Verdana", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Next.Location = new System.Drawing.Point(219, 68);
            this.Next.Name = "Next";
            this.Next.Size = new System.Drawing.Size(98, 22);
            this.Next.TabIndex = 2;
            this.Next.Tag = "";
            this.Next.Text = "Next";
            this.Next.UseVisualStyleBackColor = true;
            this.Next.Click += new System.EventHandler(this.Next_Click);
            // 
            // Back
            // 
            this.Back.Font = new System.Drawing.Font("Verdana", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Back.Location = new System.Drawing.Point(115, 68);
            this.Back.Name = "Back";
            this.Back.Size = new System.Drawing.Size(98, 22);
            this.Back.TabIndex = 3;
            this.Back.Tag = "";
            this.Back.Text = "Back";
            this.Back.UseVisualStyleBackColor = true;
            this.Back.Click += new System.EventHandler(this.Back_Click);
            // 
            // SaveLang
            // 
            this.SaveLang.Font = new System.Drawing.Font("Verdana", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.SaveLang.Location = new System.Drawing.Point(347, 29);
            this.SaveLang.Name = "SaveLang";
            this.SaveLang.Size = new System.Drawing.Size(98, 22);
            this.SaveLang.TabIndex = 1;
            this.SaveLang.Tag = "";
            this.SaveLang.Text = "Save";
            this.SaveLang.UseVisualStyleBackColor = true;
            this.SaveLang.Click += new System.EventHandler(this.SaveLang_Click);
            // 
            // lblLanguage
            // 
            this.lblLanguage.AutoSize = true;
            this.lblLanguage.Font = new System.Drawing.Font("Verdana", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblLanguage.Location = new System.Drawing.Point(13, 32);
            this.lblLanguage.Name = "lblLanguage";
            this.lblLanguage.Size = new System.Drawing.Size(77, 16);
            this.lblLanguage.TabIndex = 1;
            this.lblLanguage.Text = "Language:";
            // 
            // Languages
            // 
            this.Languages.Font = new System.Drawing.Font("Verdana", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Languages.FormattingEnabled = true;
            this.Languages.Items.AddRange(new object[] {
            "Operating System (Default)",
            "Spanish (Spanish)",
            "Spanish Venezuela (Spanish-VE)",
            "English (English)"});
            this.Languages.Location = new System.Drawing.Point(96, 30);
            this.Languages.Name = "Languages";
            this.Languages.Size = new System.Drawing.Size(245, 22);
            this.Languages.TabIndex = 0;
            // 
            // FrmConfig_Language
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(457, 102);
            this.Controls.Add(this.LangPanel);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "FrmConfig_Language";
            this.ShowInTaskbar = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.Manual;
            this.Text = "FrmConfig";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.FrmConfig_Language_FormClosing);
            this.Load += new System.EventHandler(this.FrmConfig_Language_Load);
            this.LangPanel.ResumeLayout(false);
            this.LangPanel.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Panel LangPanel;
        private System.Windows.Forms.Label lblLanguage;
        private System.Windows.Forms.ComboBox Languages;
        private System.Windows.Forms.Button SaveLang;
        private System.Windows.Forms.Button Next;
        private System.Windows.Forms.Button Back;
    }
}