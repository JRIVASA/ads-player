﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Data.SqlClient;
using isADS.Clases;

namespace isADS
{
    public partial class FrmConfig_Volume : Form
    {
        private Boolean ApplicationInit = false;
        private Boolean OnPurposeActivation = false;
        private ToolTip CurrentVolume = new ToolTip();

        private Boolean IgnoreDefaultActivation()
        {
            OnPurposeActivation = true; return OnPurposeActivation;
        }

        public FrmConfig_Volume()
        {
            InitializeComponent();
        }

        public FrmConfig_Volume(Boolean AppInit)
        {
            InitializeComponent();
            this.ApplicationInit = AppInit;
        }

        //private Form PreviousForm = null;
        private Form NextForm = null;

        private void FrmConfig_Volume_Load(object sender, EventArgs e)
        {
            this.components = new System.ComponentModel.Container();
            this.DoubleBuffered = true;

            this.Left = (System.Windows.Forms.Screen.PrimaryScreen.WorkingArea.Width / 2) - (this.Width / 2);
            this.Top = Convert.ToInt32(System.Windows.Forms.Screen.PrimaryScreen.WorkingArea.Height * 0.10);

            TrackApplicationVolume.SetRange(0, 100);
            TrackApplicationVolume.Value = Properties.Settings.Default.ApplicationVolume;

            if (this.components == null)
            {
                this.components = new System.ComponentModel.Container();
            }
            this.components.Add(CurrentVolume);

            this.ApplyLanguage();            
            this.BringToFront();
        }

        private void ApplyLanguage()
        {

            this.Text = LanguageResources.GetText("FrmConfig.Title");
            
            this.lblVolume.Text = LanguageResources.GetText("FrmConfig.lblVolume");

            this.CloseBtn.Text = LanguageResources.GetText("FrmConfig.Finish");

        }

        private void FrmConfig_Volume_FormClosing(object sender, FormClosingEventArgs e)
        {
            if (FrmConfig.ApplicationInit && NextForm == null)
            {
                //IgnoreDefaultActivation();
                //MessageBox.Show(LanguageResources.GetText("FrmConfig.FirstUseConfigurationWarning"));
                //e.Cancel = true;
            }
            else
            {
                this.Visible = false; if (NextForm != null) NextForm.ShowDialog();
            }            
        }

        private void CloseBtn_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void LangPanel_Paint(object sender, PaintEventArgs e)
        {

        }

        private void TrackApplicationVolume_Scroll(object sender, EventArgs e)
        {
            try { 
                Audio.Volume = TrackApplicationVolume.Value;
                Properties.Settings.Default.ApplicationVolume = TrackApplicationVolume.Value;
                Properties.Settings.Default.Save();
                CurrentVolume.SetToolTip(TrackApplicationVolume, Properties.Settings.Default.ApplicationVolume.ToString());
            } catch (Exception Any) { Console.WriteLine(Any.Message); }
        }

    }
}
