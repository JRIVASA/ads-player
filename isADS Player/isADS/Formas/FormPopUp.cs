﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using isADS.Clases;
using Microsoft.Win32;

namespace isADS
{
    public partial class FormPopUp : Form    
    {

        Boolean Block = false;
        Boolean OnPurposeActivation = false;

        public Action<Object> MessageClickAction = ((Object Any) => { });
        public Action<Object> IconClickAction = ((Object Any) => { });
        public String TextMsg = String.Empty;
        public Font TextMsgFont = null;
        public Boolean KeepTopMost = false;

        private Boolean IgnoreDefaultActivation()
        {
            OnPurposeActivation = true; return OnPurposeActivation;
        }

        public FormPopUp()
        {
            InitializeComponent();         
        }

        protected override CreateParams CreateParams
        {
            get
            {
                CreateParams cp = null;
                cp = base.CreateParams;
                cp.ExStyle |= 0x80;  // Turn on WS_EX_TOOLWINDOW
                return cp;
            }
        }

        private void FormPopUp_Load(object sender, EventArgs e)
        {

            //this.TopMost = true;

            //Console.WriteLine( System.Threading.Thread.CurrentThread.CurrentUICulture.EnglishName);
            //Console.WriteLine(System.Threading.Thread.CurrentThread.CurrentCulture.EnglishName);

            this.DoubleBuffered = true;

            Microsoft.Win32.SystemEvents.DisplaySettingsChanging += SystemEvents_DisplaySettingsChanging;
            Microsoft.Win32.SystemEvents.DisplaySettingsChanged += SystemEvents_DisplaySettingsChanged;

            //this.Width = Screen.PrimaryScreen.WorkingArea.Width;

            //this.Height = Convert.ToInt32((double)Screen.PrimaryScreen.WorkingArea.Height * (double)(0.3));  // 200;
            this.Top = 0; //(Screen.PrimaryScreen.WorkingArea.Height / 2) - (this.Height / 2);
            this.Left = (-1 * this.Width); //this.Width;           

            this.Visible = true;

            //float ScalingSize = (float) Math.Round(((((double)Screen.PrimaryScreen.WorkingArea.Width * (double)(0.64)) + ((double)Screen.PrimaryScreen.WorkingArea.Height * (double)(0.36))) / (double)161.7), 0);

            this.Message.Text = this.TextMsg;
            this.Message.Font = ( TextMsgFont != null ? TextMsgFont : new Font(this.Message.Font.FontFamily, 12, FontStyle.Bold) );

            this.ApplyLanguage();

            this.BringToFront();
                     
        }

        void SystemEvents_DisplaySettingsChanged(object sender, EventArgs e)
        {
            
        }

        void SystemEvents_DisplaySettingsChanging(object sender, EventArgs e)
        {
            Microsoft.Win32.SystemEvents.DisplaySettingsChanging -= SystemEvents_DisplaySettingsChanging;
            Microsoft.Win32.SystemEvents.DisplaySettingsChanged -= SystemEvents_DisplaySettingsChanged;

            //try
            //{
            //    Block = true;

            //    if (Application.OpenForms["FrmConfig_ScreenSelect"] != null) { Application.OpenForms["FrmConfig_ScreenSelect"].Dispose(); }
            //    if (Application.OpenForms["FrmConfig_LocationSelect"] != null) { Application.OpenForms["FrmConfig_LocationSelect"].Dispose(); }
            //    if (Application.OpenForms["FormInput"] != null) { Application.OpenForms["FormInput"].Dispose(); }

            //    this.Dispose();
            //}
            //catch (Exception) { }
        }

        private void StartMoving()
        {
            while ((this.Left + this.Width) <= (Screen.PrimaryScreen.WorkingArea.Width + this.Width))            
            {
                Application.DoEvents();
                if (Block) { return; }
                this.Left = this.Left + 1;
                if (KeepTopMost) this.TopMost = true;
                //System.Threading.Thread.Sleep(new System.TimeSpan(10000000));
                System.Threading.Thread.Sleep(1);
                this.Refresh();                
            }
            this.MessageClickAction = null;
            this.IconClickAction = null;
            this.Close();
        }

        private void FormPopUp_Activated(object sender, EventArgs e)
        {
            StartMoving();
        }

        private void Message_Click(object sender, EventArgs e)
        {

            if (MessageClickAction != null) MessageClickAction.Invoke(null);            

            //if (
            //    (this.Left <= (this.Width * -1) + (this.Width * 0.4)) 
            //    ||
            //    (this.Left >= (this.Width * 0.6))
            //   ) { 
            //        return; 
            //     }

            //if (Block) {
            //    return;
            //}

            //Boolean Did = false;
            //do
            //{
            //    FormInput Input = new FormInput();

            //    Input.Text = "Stellar ADS360";
            //    Input.ActionTrue = LanguageResources.GetText("ActivationWindowChooseDEMO"); Input.ButtonTrue.Text = Input.ActionTrue;
            //    Input.ActionFalse = LanguageResources.GetText("ActivationWindowChooseActivate"); Input.ButtonFalse.Text = Input.ActionFalse;
            //    Input.Message.Text = LanguageResources.GetText("ActivationWindowChoiceMessageLine1") + 
            //        Functions.NewLine() + Functions.NewLine() +
            //        LanguageResources.GetText("ActivationWindowChoiceMessageLine2");
            //    Input.Message.ReadOnly = true;

            //    Block = true;

            //    IgnoreDefaultActivation();
            //    Input.ShowDialog(this);
                
            //    if (Input.Action.Equals(Input.ActionTrue))
            //    {
            //        // Continue DEMO
            //        break;
            //    }
            //    else if (Input.Action.Equals(Input.ActionFalse))
            //    {
            //        // OLD ActivationUI
            //        //FormActivation Activate = new FormActivation();                    
            //        //Activate.Text = "Stellar isADS";
            //        //Activate.InputKey.Text = "";
            //        //Activate.ShowDialog();

            //        //if (Activate.InputKey.Text == "")
            //        //{
            //        //    Activate.Close();
            //        //    Input.Close();
            //        //    break;
            //        //}

            //        //Variables.DEMO = !(new Functions().Activate(Activate.InputKey.Text)); // Activate.InputKey.Text.Length <= 10;

            //        //Activate.Close();

            //        IgnoreDefaultActivation();
            //        FrmConfig_LocationSelect Tmp = new FrmConfig_LocationSelect();
            //        Tmp.ShowDialog();
            //        Tmp.Dispose();

            //        Variables.DEMO = !(new Functions().isActivated());

            //        if (!Variables.DEMO) { Variables.myProgramDataFolder = Functions.getFilesFolder(); Functions.Sync(); }
                       
            //    }
            //    else if (Input.Action.Equals(Input.ActionCancel))
            //    {
            //        // Continue DEMO
            //        break;
            //    }

            //    Input.Close();

            //    Did = true;

            //} while (!Did);

            //Block = !Variables.DEMO;

            //if (Block) { this.Close(); } else { StartMoving(); }

        }

        private void FormPopUp_Deactivate(object sender, EventArgs e)
        {  
        //   if (((Form)sender).CanFocus) { ((Form)sender).BringToFront(); }
        }

        private void FormPopUp_Resize(object sender, EventArgs e)
        {
            // Console.WriteLine("X");
            
            this.Size = this.MinimumSize;
            this.WindowState = FormWindowState.Normal;
        }

        private void ImgIcon_Click(object sender, EventArgs e)
        {
            if (IconClickAction != null) IconClickAction.Invoke(null);
        }

    }    

}
