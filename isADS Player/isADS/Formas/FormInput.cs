﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using isADS.Clases;

namespace isADS
{
    public partial class FormInput : Form
    {

        public String Action = "";
        public String ActionTrue;
        public String ActionFalse;
        public String ActionCancel = "Cancelled";

        public FormInput()
        {
            InitializeComponent();
        }

        private void ButtonTrue_Click(object sender, EventArgs e)
        {
            Action = ActionTrue;
            this.Close();
        }

        private void ButtonFalse_Click(object sender, EventArgs e)
        {
            Action = ActionFalse;
            this.Close();
        }

        private void FormInput_FormClosing(object sender, FormClosingEventArgs e)
        {
            if (this.Visible)
            {
                if (Action == "") { Action = "Cancel"; }
                e.Cancel = true;
                this.Hide();
            }
            else
            {
                e.Cancel = false;
                this.Dispose();
            }
        }

        private void Message_TextChanged(object sender, EventArgs e)
        {

        }

        private void FormInput_Load(object sender, EventArgs e)
        {

        }

        private void FormInput_Activated(object sender, EventArgs e)
        {
            Functions.setTimeOut(3250, delegate(Object myTimer, System.Timers.ElapsedEventArgs ev)
            {
                try { 
                this.BeginInvoke((Action)(() => {
                    try
                    {
                        this.Message.SelectionStart = this.Message.Text.Length;
                        this.Message.ScrollToCaret();  
                    }
                    catch (Exception) {}                                 
                }));
                } catch (Exception Any) { Console.WriteLine(Any.Message); } // Most Probably, the user already closed the form before this code got executed.
                ((System.Timers.Timer)myTimer).Dispose();
            });
        }
    }
}
