﻿namespace isADS
{
    partial class FrmConfig_LocalDB
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.DBAccessPanel = new System.Windows.Forms.Panel();
            this.Next = new System.Windows.Forms.Button();
            this.Back = new System.Windows.Forms.Button();
            this.lblWinAuthText = new System.Windows.Forms.Label();
            this.SQLPassword = new System.Windows.Forms.TextBox();
            this.lblSQLPassword = new System.Windows.Forms.Label();
            this.lblSQLUser = new System.Windows.Forms.Label();
            this.SQLUser = new System.Windows.Forms.TextBox();
            this.SQLCredentials = new System.Windows.Forms.RadioButton();
            this.SQLWindowsAuthentication = new System.Windows.Forms.RadioButton();
            this.lblDatabaseAccess = new System.Windows.Forms.Label();
            this.DBAccessPanel.SuspendLayout();
            this.SuspendLayout();
            // 
            // DBAccessPanel
            // 
            this.DBAccessPanel.Controls.Add(this.Next);
            this.DBAccessPanel.Controls.Add(this.Back);
            this.DBAccessPanel.Controls.Add(this.lblWinAuthText);
            this.DBAccessPanel.Controls.Add(this.SQLPassword);
            this.DBAccessPanel.Controls.Add(this.lblSQLPassword);
            this.DBAccessPanel.Controls.Add(this.lblSQLUser);
            this.DBAccessPanel.Controls.Add(this.SQLUser);
            this.DBAccessPanel.Controls.Add(this.SQLCredentials);
            this.DBAccessPanel.Controls.Add(this.SQLWindowsAuthentication);
            this.DBAccessPanel.Controls.Add(this.lblDatabaseAccess);
            this.DBAccessPanel.Dock = System.Windows.Forms.DockStyle.Top;
            this.DBAccessPanel.Location = new System.Drawing.Point(0, 0);
            this.DBAccessPanel.Name = "DBAccessPanel";
            this.DBAccessPanel.Size = new System.Drawing.Size(457, 103);
            this.DBAccessPanel.TabIndex = 2;
            // 
            // Next
            // 
            this.Next.Font = new System.Drawing.Font("Verdana", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Next.Location = new System.Drawing.Point(226, 69);
            this.Next.Name = "Next";
            this.Next.Size = new System.Drawing.Size(98, 22);
            this.Next.TabIndex = 5;
            this.Next.Tag = "";
            this.Next.Text = "Next";
            this.Next.UseVisualStyleBackColor = true;
            this.Next.Click += new System.EventHandler(this.Next_Click);
            // 
            // Back
            // 
            this.Back.Font = new System.Drawing.Font("Verdana", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Back.Location = new System.Drawing.Point(122, 69);
            this.Back.Name = "Back";
            this.Back.Size = new System.Drawing.Size(98, 22);
            this.Back.TabIndex = 6;
            this.Back.Tag = "";
            this.Back.Text = "Back";
            this.Back.UseVisualStyleBackColor = true;
            this.Back.Click += new System.EventHandler(this.Back_Click);
            // 
            // lblWinAuthText
            // 
            this.lblWinAuthText.AutoSize = true;
            this.lblWinAuthText.Font = new System.Drawing.Font("Verdana", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblWinAuthText.Location = new System.Drawing.Point(65, 55);
            this.lblWinAuthText.Name = "lblWinAuthText";
            this.lblWinAuthText.Size = new System.Drawing.Size(324, 16);
            this.lblWinAuthText.TabIndex = 8;
            this.lblWinAuthText.Text = "Text to show for Windows Authentication Mode";
            // 
            // SQLPassword
            // 
            this.SQLPassword.Font = new System.Drawing.Font("Verdana", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.SQLPassword.Location = new System.Drawing.Point(347, 38);
            this.SQLPassword.Name = "SQLPassword";
            this.SQLPassword.PasswordChar = '*';
            this.SQLPassword.Size = new System.Drawing.Size(97, 22);
            this.SQLPassword.TabIndex = 4;
            this.SQLPassword.UseSystemPasswordChar = true;
            this.SQLPassword.TextChanged += new System.EventHandler(this.SQLPassword_TextChanged);
            // 
            // lblSQLPassword
            // 
            this.lblSQLPassword.AutoSize = true;
            this.lblSQLPassword.Font = new System.Drawing.Font("Verdana", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblSQLPassword.Location = new System.Drawing.Point(258, 39);
            this.lblSQLPassword.Name = "lblSQLPassword";
            this.lblSQLPassword.Size = new System.Drawing.Size(76, 16);
            this.lblSQLPassword.TabIndex = 6;
            this.lblSQLPassword.Text = "Password:";
            this.lblSQLPassword.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // lblSQLUser
            // 
            this.lblSQLUser.AutoSize = true;
            this.lblSQLUser.Font = new System.Drawing.Font("Verdana", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblSQLUser.Location = new System.Drawing.Point(13, 39);
            this.lblSQLUser.Name = "lblSQLUser";
            this.lblSQLUser.Size = new System.Drawing.Size(82, 16);
            this.lblSQLUser.TabIndex = 5;
            this.lblSQLUser.Text = "User Login:";
            this.lblSQLUser.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // SQLUser
            // 
            this.SQLUser.Font = new System.Drawing.Font("Verdana", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.SQLUser.Location = new System.Drawing.Point(155, 37);
            this.SQLUser.Name = "SQLUser";
            this.SQLUser.Size = new System.Drawing.Size(90, 22);
            this.SQLUser.TabIndex = 3;
            this.SQLUser.TextChanged += new System.EventHandler(this.SQLUser_TextChanged);
            // 
            // SQLCredentials
            // 
            this.SQLCredentials.AutoSize = true;
            this.SQLCredentials.Font = new System.Drawing.Font("Verdana", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.SQLCredentials.Location = new System.Drawing.Point(347, 14);
            this.SQLCredentials.Name = "SQLCredentials";
            this.SQLCredentials.Size = new System.Drawing.Size(97, 18);
            this.SQLCredentials.TabIndex = 2;
            this.SQLCredentials.Text = "Credentials";
            this.SQLCredentials.UseVisualStyleBackColor = true;
            this.SQLCredentials.CheckedChanged += new System.EventHandler(this.SQLCredentials_CheckedChanged);
            // 
            // SQLWindowsAuthentication
            // 
            this.SQLWindowsAuthentication.AutoSize = true;
            this.SQLWindowsAuthentication.Checked = true;
            this.SQLWindowsAuthentication.Font = new System.Drawing.Font("Verdana", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.SQLWindowsAuthentication.Location = new System.Drawing.Point(146, 14);
            this.SQLWindowsAuthentication.Name = "SQLWindowsAuthentication";
            this.SQLWindowsAuthentication.Size = new System.Drawing.Size(178, 18);
            this.SQLWindowsAuthentication.TabIndex = 1;
            this.SQLWindowsAuthentication.TabStop = true;
            this.SQLWindowsAuthentication.Text = "Windows Authentication";
            this.SQLWindowsAuthentication.UseVisualStyleBackColor = true;
            this.SQLWindowsAuthentication.CheckedChanged += new System.EventHandler(this.SQLWindowsAuthentication_CheckedChanged);
            // 
            // lblDatabaseAccess
            // 
            this.lblDatabaseAccess.AutoSize = true;
            this.lblDatabaseAccess.Font = new System.Drawing.Font("Verdana", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblDatabaseAccess.Location = new System.Drawing.Point(12, 14);
            this.lblDatabaseAccess.Name = "lblDatabaseAccess";
            this.lblDatabaseAccess.Size = new System.Drawing.Size(128, 16);
            this.lblDatabaseAccess.TabIndex = 1;
            this.lblDatabaseAccess.Text = "Database Access:";
            this.lblDatabaseAccess.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // FrmConfig_LocalDB
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(457, 103);
            this.Controls.Add(this.DBAccessPanel);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "FrmConfig_LocalDB";
            this.ShowInTaskbar = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.Manual;
            this.Text = "FrmConfig";
            this.Activated += new System.EventHandler(this.FrmConfig_LocalDB_Activated);
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.FrmConfig_LocalDB_FormClosing);
            this.Load += new System.EventHandler(this.FrmConfig_LocalDB_Load);
            this.DBAccessPanel.ResumeLayout(false);
            this.DBAccessPanel.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Panel DBAccessPanel;
        private System.Windows.Forms.TextBox SQLPassword;
        private System.Windows.Forms.Label lblSQLPassword;
        private System.Windows.Forms.Label lblSQLUser;
        private System.Windows.Forms.TextBox SQLUser;
        private System.Windows.Forms.RadioButton SQLCredentials;
        private System.Windows.Forms.RadioButton SQLWindowsAuthentication;
        private System.Windows.Forms.Label lblDatabaseAccess;
        private System.Windows.Forms.Label lblWinAuthText;
        private System.Windows.Forms.Button Next;
        private System.Windows.Forms.Button Back;
    }
}