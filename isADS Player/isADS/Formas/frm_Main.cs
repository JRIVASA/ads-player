﻿using isADS.Clases;
using MyConfig = isADS.Properties.Settings;
using MyResources = isADS.Properties.Resources;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Reflection;
using System.Runtime.InteropServices;

namespace isADS
{

    public partial class frm_Main : Form
    {       

        private int myTemplate;
        private String actualSchedule = "0";
        private String currentPlannedSchedule = "0";
        private int currentAdCount = 0;
        private int lastLoadedSecuence = 0;      

        private int numDisplayedSchedules = 0;
        private int TemplateBorderWidth = 0;
        private Color TemplateBorderColor = Color.Empty;

        private int currentSecuenceTime; 
        private int currentScheduleTime;

        private int scheduleTime;
        private int inactivityThreshold;

        private Boolean CampaignNotFound = false;
        //private Boolean isPlannedSchedule;
        private Boolean isReady;
        private Boolean firstInstance;
        //private Boolean CalledByCode = false;        
        private String EffectDescription = String.Empty;
        private String OrganizationID = String.Empty;

        private int numForm = 1;

        private System.Collections.Generic.Dictionary<int, int> currentSecuenceOrder;
        private System.Collections.Generic.Dictionary<int, object> currentAd;

        private List<WMPLib.IWMPMedia> PlayMedia = new List<WMPLib.IWMPMedia>();        
        private List<WMPLib.IWMPPlaylist> PlayList = new List<WMPLib.IWMPPlaylist>();
        private List<WMPLib.IWMPSettings> PlaySettings = new List<WMPLib.IWMPSettings>();
        private List<WMPLib.IWMPControls> PlayControls = new List<WMPLib.IWMPControls>();
        //List<AxHost.State> PlayCOMState = new List<AxHost.State>(); 

        //private frm_Main frmPreLoad; 
        private LinkLabel WebPage;
        private NavigationParams BrowseParams = null;
        private enum EffectsMethod : int { Steps = 1, Percent = 2 }
        
        /*
        private int SWP_NOSIZE = 0x1;
        private int SWP_NOMOVE = 0x2;
        private int SWP_NOACTIVATE = 0x10;
        
        [DllImport("user32.dll", CharSet = CharSet.Auto)]
        private static extern void SetWindowPos(IntPtr hWnd, IntPtr hWndInsertAfter,
        int X, int Y, int cx, int cy, int uFlags);

        private void SetAsBottomMost(Form Window){
            IntPtr hWnd = Window.Handle;

            SetWindowPos(hWnd, new IntPtr(1), 0,0,0,0, SWP_NOSIZE | SWP_NOMOVE | SWP_NOACTIVATE);
        }
        */

        delegate void ShowActivationForm_delegate(Form Frm);

        private void CustomInitializeComponent() // Tired of Design Mode Code Regeneration dropping my own code.
        {
            // 
            // frm_Main
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center;
            this.ControlBox = false;
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Name = "frm_Main" + numForm.ToString();
            this.ShowIcon = false;
            this.ShowInTaskbar = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.Manual;
            this.Location = new Point(0, 0);
            this.Padding = new Padding(0);
            this.Size = new Size(Screen.PrimaryScreen.Bounds.Width, Screen.PrimaryScreen.Bounds.Height);
            this.Text = "Form" + numForm.ToString();
            this.Load += new System.EventHandler(this.frm_Main_Load);
            this.Click += new System.EventHandler(this.frm_Main_Click);
            this.ResumeLayout(false);

            this.tableLayoutPanel1 = new System.Windows.Forms.TableLayoutPanel();
            this.SuspendLayout();
            // 
            // tableLayoutPanel1
            // 
            //this.tableLayoutPanel1.BackgroundImage = MyResources.LogoBigwise;
            //this.tableLayoutPanel1.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center;
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 200F));
            this.tableLayoutPanel1.Location = new Point(0, 0);
            this.tableLayoutPanel1.Name = "tableLayoutPanel1";
            this.tableLayoutPanel1.Size = new System.Drawing.Size(200, 100);
            this.tableLayoutPanel1.TabIndex = 0;
            this.Controls.Add(this.tableLayoutPanel1);

        }

        public frm_Main(Boolean Shell)
        {
            CustomInitializeComponent();

            this.Name = "frmShell";
            this.Text = "frmShell";
            this.BackColor = Color.Black;

            // this.SendToBack();

            // SetAsBottomMost(this); 
        }

        public frm_Main()
        {   
            CustomInitializeComponent();

            //Variables.DefaultOSCultureName = System.Threading.Thread.CurrentThread.CurrentCulture.Name;

            //Console.WriteLine(Variables.DefaultOSCultureName);

            //System.Threading.Thread.CurrentThread.CurrentCulture = new System.Globalization.CultureInfo(Variables.DefaultOSCultureName);
            //System.Threading.Thread.CurrentThread.CurrentUICulture = System.Threading.Thread.CurrentThread.CurrentCulture;            

            this.isReady = true; firstInstance = true; this.Visible = false;

            Variables.mySystemDrive = getSystemDrive();
            Variables.myAppDrive = getAppDrive();
            Variables.myProgramDataFolder = Functions.getFilesFolder(); // getDataFolder();

            //QuickSettings.Set("Test", "Test2", "Test3");
            //String A = QuickSettings.Get("Test", "Test2", "undefined");

            this.BackColor = Color.Black;

            this.components = new System.ComponentModel.Container();

            Timer preLoadTime = new Timer();
            preLoadTime.Tag = "preLoadTime";
            preLoadTime.Interval = 1;
            preLoadTime.Tick += new System.EventHandler(this.preLoad);
            
            frm_Main frmShell = new frm_Main(true);
            frmShell.Show();
            frmShell.AddOwnedForm(this);

            this.components.Add(preLoadTime);
            preLoadTime.Enabled = true;
        }

        public frm_Main(String currentPlannedSchedule, int numForm, int numDisplayedSchedules)
        {
            Form frmShell = Application.OpenForms["frmShell"];
            if (frmShell != null) frmShell.AddOwnedForm(this);

            this.numForm = numForm + 1;            
            CustomInitializeComponent();
            this.Location = new Point(-10000, -10000); // To avoid flickering when showing up the campaign.

            this.currentPlannedSchedule = currentPlannedSchedule;
            this.numDisplayedSchedules = numDisplayedSchedules;
            this.isReady = false; firstInstance = false;
            this.Visible = true;

            startNextSchedule();
        }

        public frm_Main(String currentPlannedSchedule, int numForm, int numDisplayedSchedules, NavigationParams BrowseParams)
        {
            Form frmShell = Application.OpenForms["frmShell"];
            if (frmShell != null) frmShell.AddOwnedForm(this);

            this.numForm = numForm + 1;
            CustomInitializeComponent();
            this.Location = new Point(-10000, -10000); // To avoid flickering when showing up the campaign.

            this.currentPlannedSchedule = currentPlannedSchedule;
            this.numDisplayedSchedules = numDisplayedSchedules;
            this.isReady = false; firstInstance = false;
            this.Visible = true;

            this.BrowseParams = BrowseParams;

            startNextSchedule();
        }

        protected override CreateParams CreateParams
        {
            get
            {
                var cp = base.CreateParams;
                cp.ExStyle |= 0x80;  // Turn on WS_EX_TOOLWINDOW
                return cp;
            }
        }

        private String getSystemDrive()
        {
            try
            {
                String tmpDrive;
                tmpDrive = System.Environment.GetEnvironmentVariable("SystemDrive");
                return tmpDrive;
            }
            catch(Exception tmpEx)
            {
                Console.WriteLine(tmpEx.Message);
                return "C:";                
            }
        }

        private String getAppDrive()
        {
            try
            {
                String tmpDrive;
                tmpDrive = Application.ExecutablePath.Left(2);
                return tmpDrive;
            }
            catch (Exception tmpEx)
            {
                Console.WriteLine(tmpEx.Message);
                return "C:";
            }
        }

        //private String getDataFolder()
        //{
        //    try
        //    {
        //        String tmpDrive;

        //        tmpDrive = System.Environment.GetEnvironmentVariable("ProgramData") +
        //        "\\" +
        //        MyConfig.Default.ProgramDataDir +
        //        MyConfig.Default.ApplicationName +
        //        "\\" +
        //        MyConfig.Default.CampaignFilesDir;

        //        return tmpDrive;                
        //    }
        //    catch (Exception tmpEx)
        //    {
        //        Console.WriteLine(tmpEx.Message);

        //        String tmpDrive =
        //        Assembly.GetExecutingAssembly().Location.Replace(
        //            Assembly.GetExecutingAssembly().GetName().Name + ".exe", String.Empty
        //        ) + 
        //        MyConfig.Default.CampaignFilesDir;
                                
        //        return tmpDrive;
        //    }

        //}

        public void startNextSchedule()
        {
            if (BrowseParams != null) { PrepareBrowsingSession(); BrowseParams = null; return; }

            if (firstInstance && !MyConfig.Default.LastCampaignDisplayed.isUndefined())
                actualSchedule = MyConfig.Default.LastCampaignDisplayed;
            else
                actualSchedule = currentPlannedSchedule;

            actualSchedule = new Functions().getNextCampaign(actualSchedule);
            
            if ((actualSchedule != currentPlannedSchedule))
            {
                currentPlannedSchedule = actualSchedule;
                //isPlannedSchedule = true;
            }
            else
            {
                // No more planned activities. Another Feature: Get some random schedule...
                // This method should consider getting random schedules with lower lifetime
                // than the time left until an upcoming planned schedule
                // or if there are none, show some static default screen and wait
                // until the next planned schedule is executed.

                //isPlannedSchedule = false;
                actualSchedule = "0"; // actualSchedule = someGetRandomScheduleMethod();

                if (actualSchedule == "0") // Schedule Not Found. Set Default if enabled else wait.
                {
                    actualSchedule = new Functions().getNextCampaign(actualSchedule);
                    currentPlannedSchedule = actualSchedule;

                    if (actualSchedule == "0") 
                    {
                        CampaignNotFound = true;

                        //this.BackgroundImageLayout = ImageLayout.Center;
                        //this.BackgroundImage = (System.Drawing.Image) LanguageResources.GetImage("LogoBigwise");

                        // System.Threading.Thread.Sleep(30000); // Environment.Exit(0); 
                        // On not found campaign waits 30 seconds to check again.
                    }
                    
                    //return;
                    // Not possible to define a random schedule.
                    // Show a static default screen / template instead;
                    // ...
                    // return;
                }

            }

            //try { 
            //this.OrganizationID = Functions.getParameterizedCommand(
            //    "SELECT TOP (1) ORGANIZATION_ID FROM MEDIAPLAYER",
            //    new Functions().DBConnection.OpenGet(), null).ExecuteScalar().ToString();
            //} catch (Exception) { this.OrganizationID = String.Empty; }
            
            myTemplate = Convert.ToInt32(new Functions().getCampaignTemplate(actualSchedule, out currentAdCount, out TemplateBorderWidth, out TemplateBorderColor));
            ////currentAdCount = new Functions().getAdCount(myTemplate);
            
            prepareComponents();
        }
            
        public void prepareComponents()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frm_Main));

            currentSecuenceOrder = new Dictionary <int, int> ();
            currentAd = new Dictionary <int, object> ();

            if (this.tableLayoutPanel1 != null)
            {
                foreach (Control Cnt in this.tableLayoutPanel1.Controls)
                { this.tableLayoutPanel1.Controls.Remove(Cnt); Control CntRef = Cnt; CntRef = null; }
            }

            this.Controls.Clear();

            this.components = new System.ComponentModel.Container();

            this.DoubleBuffered = true;

#region
            //
            {
                this.tableLayoutPanel1 = new System.Windows.Forms.TableLayoutPanel();
                this.tableLayoutPanel1.SuspendLayout();
                this.SuspendLayout();
                this.tableLayoutPanel1.Controls.Clear();

                this.tableLayoutPanel1.Anchor = ((System.Windows.Forms.AnchorStyles)
                ((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                | System.Windows.Forms.AnchorStyles.Left) | System.Windows.Forms.AnchorStyles.Right)));
                
                this.tableLayoutPanel1.ColumnCount = 2;

                if ((myTemplate == 5) || (myTemplate == 6))
                {
                    this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 80F));
                    this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 20F));
                }
                else if ((myTemplate == 7) || (myTemplate == 8))
                {
                    this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 20F));
                    this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 80F));
                }
                else
                {
                    this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
                    this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
                }

                this.tableLayoutPanel1.Location = new System.Drawing.Point(0, 0);
                this.tableLayoutPanel1.Name = "tableLayoutPanel1";
                this.tableLayoutPanel1.RowCount = 2;

                // Set Border - Method 1;

                //int nextEffect = new Random().Next(7);

                //switch (nextEffect)
                //{
                //    case 0:
                //        TemplateBorderColor = Color.Black;
                //        break;
                //    case 1:
                //        TemplateBorderColor = Color.Yellow;
                //        break;
                //    case 2:
                //        TemplateBorderColor = Color.Blue;
                //        break;
                //    case 3:
                //        TemplateBorderColor = Color.Red;
                //        break;
                //    case 4:
                //        TemplateBorderColor = Color.Green;
                //        break;
                //    case 5:
                //        TemplateBorderColor = Color.Brown;
                //        break;
                //    case 6:
                //        TemplateBorderColor = Color.White;
                //        break;
                //}

                
                //this.tableLayoutPanel1.BackColor = TemplateBorderColor; // Might Produce an ugly color transition... Doing it now with a timeout at the end of startSchedule();
                this.tableLayoutPanel1.BackColor = Color.Black;

                this.tableLayoutPanel1.CellBorderStyle = TableLayoutPanelCellBorderStyle.None;
                this.tableLayoutPanel1.BorderStyle = BorderStyle.None;
                this.tableLayoutPanel1.Margin = new Padding(0, 0, 0, 0);
                //this.tableLayoutPanel1.Margin = new Padding(25, 25, 25, 25);
                this.tableLayoutPanel1.Padding = new Padding(0, 0, 0, 0);
                //this.tableLayoutPanel1.Padding = new Padding(25, 25, 25, 25);
                this.tableLayoutPanel1.AutoSize = false;
                this.tableLayoutPanel1.MouseUp += LayoutPanel_MouseUp;
                this.tableLayoutPanel1.MouseDown += LayoutPanel_MouseDown;

                if (CampaignNotFound)
                {
                    this.tableLayoutPanel1.BackgroundImageLayout = ImageLayout.Center;
                    this.tableLayoutPanel1.BackgroundImage = MyResources.Logo_Stellar_ADS360;

                    WebPage = new LinkLabel();

                    // 
                    // WebPage
                    // 

                    float ScalingSize = (float)Math.Round(((((double)Screen.PrimaryScreen.WorkingArea.Width * (double)(0.64)) + ((double)Screen.PrimaryScreen.WorkingArea.Height * (double)(0.36))) / (double)53.90), 0); // 9.75F;

                    WebPage.AutoSize = true;
                    WebPage.Font = new System.Drawing.Font("Microsoft Sans Serif", ScalingSize, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
                    WebPage.Name = "WebPage";
                    //WebPage.Size = new System.Drawing.Size(296, 16);
                    WebPage.Text = "www.StellarADS360.com";
                    WebPage.LinkColor = Color.FromArgb(232, 126, 36); // Color.FromArgb(72, 164, 71);// Color.PaleGreen;
                    WebPage.ActiveLinkColor = WebPage.LinkColor;
                    WebPage.VisitedLinkColor = WebPage.LinkColor;
                    WebPage.BackColor = Color.Black;                    

                    WebPage.Location = new System.Drawing.Point(
                        Convert.ToInt32((this.Width / 2) - (TextRenderer.MeasureText(WebPage.Text, WebPage.Font).Width / 2)), //Convert.ToInt32(((Screen.PrimaryScreen.WorkingArea.Width / 2) - (WebPage.Width * (ScalingSize * 1.25 / 8) / 2))),
                        Convert.ToInt32(this.Height * 0.85)
                    );                    

                    LinkLabel.Link ProductLink = new LinkLabel.Link();
                    ProductLink.LinkData = "Http://" + WebPage.Text;
                    WebPage.Links.Add(ProductLink);

                    WebPage.LinkClicked += new System.Windows.Forms.LinkLabelLinkClickedEventHandler(
                        (Sender, Args) => {
                            try
                            {
                                System.Diagnostics.Process.Start(Args.Link.LinkData.ToString());
                            }
                            catch (Exception)
                            {
                                MessageBox.Show(LanguageResources.GetText("OpenLinkError"));
                            }                        
                        }); // new System.Windows.Forms.LinkLabelLinkClickedEventHandler(this.WebPage_LinkClicked);
                    WebPage.MouseUp += LayoutPanel_MouseUp;
                    WebPage.MouseDown += LayoutPanel_MouseDown;

                    this.Controls.Add(WebPage);
                }                    

                if ((myTemplate == 5) || (myTemplate == 6) || (myTemplate == 7) || (myTemplate == 8))
                {
                    this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 80F));
                    this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 20F));
                }
                else
                {
                    this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
                    this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
                }

                //
                 // ********* Descomentar para visualizar el tableLayout en el Design Tab. ************
                 //
                //this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
                //this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));

                this.tableLayoutPanel1.Size = this.Size; // new System.Drawing.Size(640, 480);
                this.tableLayoutPanel1.TabIndex = 4;
                this.tableLayoutPanel1.Dock = DockStyle.Fill;

                //ApplyBorder(this.tableLayoutPanel1, 5); // Method 2, Apply Border on each CellPainting. Too much Flickering...

                this.Controls.Add(this.tableLayoutPanel1);

                this.tableLayoutPanel1.ResumeLayout(false);
                this.ResumeLayout(false);

            }
            //
#endregion
            

            Timer myScheduleTime = new Timer();
            Timer myNextScheduleTimer = new Timer();           
            Timer mySecuenceTimeElapsed = new Timer();
            Timer tmpUserActivity = new Timer();
            
            myScheduleTime.Enabled = false;
            myScheduleTime.Tag = "myScheduleTime";
            myScheduleTime.Tick += new System.EventHandler(this.scheduleTimeIsUp);

            this.components.Add(myScheduleTime);

            myNextScheduleTimer.Enabled = false;
            myNextScheduleTimer.Tag = "myNextScheduleTimer";
            myNextScheduleTimer.Tick += new System.EventHandler(this.prepareNextSchedule);

            this.components.Add(myNextScheduleTimer);

            mySecuenceTimeElapsed.Enabled = false;
            mySecuenceTimeElapsed.Tag = "mySecuenceTimeElapsed";
            mySecuenceTimeElapsed.Tick += new System.EventHandler(this.keepTrack);
            mySecuenceTimeElapsed.Interval = 1000;

            this.components.Add(mySecuenceTimeElapsed);

            tmpUserActivity.Enabled = false;
            tmpUserActivity.Tag = "tmpUserActivity";
            tmpUserActivity.Tick += new System.EventHandler(this.resumeSecuence);

            this.components.Add(tmpUserActivity);

            #region DemoTryHard

            if (Variables.DEMO)
            {
                Timer tmpDemoReminder = new Timer();
                tmpDemoReminder.Enabled = false;
                tmpDemoReminder.Tag = "tmpDemoReminder";
                tmpDemoReminder.Interval = 1250;

                EventHandler DemoEvent = null;

                DemoEvent = delegate(Object DemoReminder, EventArgs DemoArgs)
                {
                    if (Variables.DEMO) { frm_Main_Click(this, null); }
                    else
                    { ((Timer)DemoReminder).Enabled = false; ((Timer)DemoReminder).Tick -= DemoEvent; }
                };

                tmpDemoReminder.Tick += DemoEvent;

                this.components.Add(tmpDemoReminder);

                tmpDemoReminder.Enabled = false;
            }

            #endregion

            //this.tableLayoutPanel1.SuspendLayout();

            for (int i = 1; i <= currentAdCount; i++)
            {

                currentSecuenceOrder.Add(i, 0);
                currentAd.Add(i, null);

                System.Windows.Forms.Timer myTime = new System.Windows.Forms.Timer();               
                AxWMPLib.AxWindowsMediaPlayer myWmp = new AxWMPLib.AxWindowsMediaPlayer();
                //System.Windows.Forms.Timer myWmpErrorCheckTimer = new System.Windows.Forms.Timer();
                System.Windows.Forms.WebBrowser myWeb = new System.Windows.Forms.WebBrowser();
                System.Windows.Forms.PictureBox myPics = new System.Windows.Forms.PictureBox();

                myTime.Enabled = false;
                myTime.Interval = 1;
                myTime.Tag = "myTime" + i;
                myTime.Tick += new System.EventHandler(this.timeIsUp);
                
                this.components.Add(myTime);

                //myWmpErrorCheckTimer.Enabled = false;
                //myWmpErrorCheckTimer.Interval = 2000;
                //myWmpErrorCheckTimer.Tag = "myWmpErrorCheckTimer" + i;
                //myWmpErrorCheckTimer.Tick += new System.EventHandler(this.checkWmpState);

                //this.components.Add(myWmpErrorCheckTimer);

                myWmp.Enabled = true;
                myWmp.Name = "myWmp" + i;
                //myWmp.OcxState = ((System.Windows.Forms.AxHost.State)(resources.GetObject("wmp.OcxState")));

                PlayMedia.Add(null);
                PlaySettings.Add(null);
                PlayControls.Add(null);
                PlayList.Add(null);
                //PlayCOMState.Add(myWmp.OcxState);

                myWmp.Location = new System.Drawing.Point(0, 240);
                myWmp.Size = new System.Drawing.Size(320, 240);
                
                myWmp.Dock = DockStyle.None;
                myWmp.Margin = new Padding(0, 0, 0, 0);
                myWmp.Padding = new Padding(0, 0, 0, 0);
                myWmp.Anchor = fullControlAnchors();                

                myWmp.ErrorEvent += new System.EventHandler(this.playerError);
                //myWmp.Warning += myWmp_Warning;
                //myWmp.NewStream += myWmp_NewStream;
                //myWmp.MediaError += myWmp_MediaError;
                //myWmp.Disconnect += myWmp_Disconnect;
                myWmp.PlayStateChange += myWmp_PlayStateChange;
                // myWmp.CurrentItemChange += new AxWMPLib._WMPOCXEvents_CurrentItemChangeEventHandler(this.mediaChange);
                myWmp.ClickEvent += new AxWMPLib._WMPOCXEvents_ClickEventHandler(this.videoClick);
                //myWmp.DoubleClickEvent += new AxWMPLib._WMPOCXEvents_DoubleClickEventHandler(this.videoDoubleClick);
                myWmp.MouseDownEvent += new AxWMPLib._WMPOCXEvents_MouseDownEventHandler(this.videoMouseDown);
                myWmp.MouseUpEvent += new AxWMPLib._WMPOCXEvents_MouseUpEventHandler(this.videoMouseUp);

                //Application.AddMessageFilter(new IgnoreMouseDoubleClickMessageFilter(this.tableLayoutPanel1, myWmp));

                myPics.Name = "myPics" + i;
                myPics.Dock = DockStyle.None;
                myPics.Margin = new Padding(0, 0, 0, 0);
                myPics.Padding = new Padding(0, 0, 0, 0);
                myPics.Anchor = fullControlAnchors();
                myPics.SizeMode = PictureBoxSizeMode.StretchImage;
                myPics.Click += new System.EventHandler(this.pictureClick);
                myPics.MouseDown += new MouseEventHandler(this.pictureMouseDown); //myPics.DoubleClick += new System.EventHandler(this.pictureDoubleClick);
                myPics.MouseUp += new MouseEventHandler(this.pictureMouseUp);

                myWeb.Name = "myWeb" + i;
                myWeb.Dock = DockStyle.None;
                myWeb.Margin = new Padding(0, 0, 0, 0);
                myWeb.Anchor = fullControlAnchors();
                myWeb.IsWebBrowserContextMenuEnabled = false;
                myWeb.ScrollBarsEnabled = false;
                myWeb.ScriptErrorsSuppressed = true;

                myWeb.DocumentCompleted += docReady;
                    //new System.Windows.Forms.WebBrowserDocumentCompletedEventHandler(this.docReady);
                myWeb.NewWindow += webNewWindow; //new System.ComponentModel.CancelEventHandler(this.webNewWindow);

                //myWeb.Visible = false;
                //myWmp.Visible = false;
                //myPics.Visible = false;

                //These are added to the TableLayout Container first to serve as placeholders
                //to prepare the Layout as defined in the current Template
                this.tableLayoutPanel1.Controls.Add(myPics);
                //These are added to the Form Container as Resources so they don't affect
                //the current Template Layout
                this.Controls.Add(myWeb);
                this.Controls.Add(myWmp);

            }

            //this.tableLayoutPanel1.ResumeLayout(true);            

            setTemplateLayout();

            scheduleTime = (new Functions().getCampaignLifeTime(actualSchedule)) * 1000;

            resources.ReleaseAllResources();
            resources = null;

            if (this.isReady)
            {       
                startSchedule();
            }
            else
            {                
                this.isReady = true;
            }
        }

        void myWmp_PlayStateChange(object sender, AxWMPLib._WMPOCXEvents_PlayStateChangeEvent e)
        {
            if ((sender as AxWMPLib.AxWindowsMediaPlayer).playState == WMPLib.WMPPlayState.wmppsPlaying)
            if (((sender as AxWMPLib.AxWindowsMediaPlayer).currentMedia.imageSourceHeight == 0) &&
            ((sender as AxWMPLib.AxWindowsMediaPlayer).currentMedia.imageSourceWidth == 0))
            {
                //Console.WriteLine(((AxWMPLib.AxWindowsMediaPlayer)sender).status.ToString());
                //Functions.RestartApp();

                //Update: Forced Termination might result in random crashes.
                //Let's instead clean resources, and leave program in a ready state before exit.

                if (!Program.RestartRequired)
                { 
                    Program.RestartRequired = true;
                    Functions.setTimeOut(1000, delegate(Object myTimer, System.Timers.ElapsedEventArgs ev)
                    {
                        try
                        {
                            this.BeginInvoke((Action)(() =>
                            {
                                Program.RestartRequired = true;
                                Timer myScheduleTime = ((Timer)this.components.Components.OfType<IComponent>().Where(ti => ti.GetType().FullName == "System.Windows.Forms.Timer")
                                    .ToList().Where(ti => ((Timer)ti).Tag != null).ToList().Find(ti => ((Timer)ti).Tag.ToString() == "myScheduleTime"));
                                myScheduleTime.Enabled = false;
                                myScheduleTime.Interval = 1;
                                myScheduleTime.Enabled = true;
                            }));
                        }
                        catch (Exception Any) { 
                            Console.WriteLine(Any.Message);
                            scheduleTimeIsUp(null, null);
                        }
                        ((System.Timers.Timer)myTimer).Dispose();
                    });
                }
            }
        }

        void myWmp_Disconnect(object sender, AxWMPLib._WMPOCXEvents_DisconnectEvent e)
        {
            Console.WriteLine(e);
        }

        void myWmp_MediaError(object sender, AxWMPLib._WMPOCXEvents_MediaErrorEvent e)
        {
            Console.WriteLine(e);
        }

        void myWmp_NewStream(object sender, EventArgs e)
        {
            Console.WriteLine(e);
        }

        void myWmp_Warning(object sender, AxWMPLib._WMPOCXEvents_WarningEvent e)
        {
            Console.WriteLine(e);
        }

        public void PrepareBrowsingSession()
        {

            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frm_Main));

            if (this.tableLayoutPanel1 != null)
            {
                foreach (Control Cnt in this.tableLayoutPanel1.Controls)
                { this.tableLayoutPanel1.Controls.Remove(Cnt); Control CntRef = Cnt; CntRef = null; }
            }

            this.Controls.Clear();

            this.components = new System.ComponentModel.Container();

            this.DoubleBuffered = true;

            #region
            //
            {
                this.tableLayoutPanel1 = new System.Windows.Forms.TableLayoutPanel();
                this.tableLayoutPanel1.SuspendLayout();
                this.SuspendLayout();
                this.tableLayoutPanel1.Controls.Clear();

                this.tableLayoutPanel1.Anchor = ((System.Windows.Forms.AnchorStyles)
                ((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                | System.Windows.Forms.AnchorStyles.Left) | System.Windows.Forms.AnchorStyles.Right)));

                this.tableLayoutPanel1.ColumnCount = 1;
                this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
                this.tableLayoutPanel1.RowCount = 1;
                this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
                
                this.tableLayoutPanel1.Location = new System.Drawing.Point(0, 0);
                this.tableLayoutPanel1.Name = "tableLayoutPanel1";                

                // Set Border - Method 1;

                //int nextEffect = new Random().Next(7);

                //switch (nextEffect)
                //{
                //    case 0:
                //        TemplateBorderColor = Color.Black;
                //        break;
                //    case 1:
                //        TemplateBorderColor = Color.Yellow;
                //        break;
                //    case 2:
                //        TemplateBorderColor = Color.Blue;
                //        break;
                //    case 3:
                //        TemplateBorderColor = Color.Red;
                //        break;
                //    case 4:
                //        TemplateBorderColor = Color.Green;
                //        break;
                //    case 5:
                //        TemplateBorderColor = Color.Brown;
                //        break;
                //    case 6:
                //        TemplateBorderColor = Color.White;
                //        break;
                //}

                //this.tableLayoutPanel1.BackColor = TemplateBorderColor; // Might Produce an ugly color transition... Doing it now with a timeout at the end of startSchedule();
                this.tableLayoutPanel1.BackColor = Color.Black;

                this.tableLayoutPanel1.CellBorderStyle = TableLayoutPanelCellBorderStyle.None;
                this.tableLayoutPanel1.BorderStyle = BorderStyle.None;
                this.tableLayoutPanel1.Margin = new Padding(0, 0, 0, 0);
                //this.tableLayoutPanel1.Margin = new Padding(25, 25, 25, 25);
                this.tableLayoutPanel1.Padding = new Padding(0, 0, 0, 0);
                //this.tableLayoutPanel1.Padding = new Padding(25, 25, 25, 25);
                this.tableLayoutPanel1.AutoSize = false;
                this.tableLayoutPanel1.MouseUp += LayoutPanel_MouseUp;
                this.tableLayoutPanel1.MouseDown += LayoutPanel_MouseDown;

                //if (CampaignNotFound)
                //{
                //    this.tableLayoutPanel1.BackgroundImageLayout = ImageLayout.Center;
                //    this.tableLayoutPanel1.BackgroundImage = MyResources.Logo_Stellar_ADS360;

                //    WebPage = new LinkLabel();

                //    // 
                //    // WebPage
                //    // 

                //    float ScalingSize = (float)Math.Round(((((double)Screen.PrimaryScreen.WorkingArea.Width * (double)(0.64)) + ((double)Screen.PrimaryScreen.WorkingArea.Height * (double)(0.36))) / (double)53.90), 0); // 9.75F;

                //    WebPage.AutoSize = true;
                //    WebPage.Font = new System.Drawing.Font("Microsoft Sans Serif", ScalingSize, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
                //    WebPage.Name = "WebPage";
                //    //WebPage.Size = new System.Drawing.Size(296, 16);
                //    WebPage.Text = "isADS.miStellar.com";
                //    WebPage.LinkColor = Color.FromArgb(232, 126, 36); // Color.FromArgb(72, 164, 71);// Color.PaleGreen;
                //    WebPage.ActiveLinkColor = WebPage.LinkColor;
                //    WebPage.VisitedLinkColor = WebPage.LinkColor;
                //    WebPage.BackColor = Color.Black;

                //    WebPage.Location = new System.Drawing.Point(
                //        Convert.ToInt32((this.Width / 2) - (TextRenderer.MeasureText(WebPage.Text, WebPage.Font).Width / 2)), //Convert.ToInt32(((Screen.PrimaryScreen.WorkingArea.Width / 2) - (WebPage.Width * (ScalingSize * 1.25 / 8) / 2))),
                //        Convert.ToInt32(this.Height * 0.85)
                //    );

                //    LinkLabel.Link ProductLink = new LinkLabel.Link();
                //    ProductLink.LinkData = "Http://" + WebPage.Text;
                //    WebPage.Links.Add(ProductLink);

                //    WebPage.LinkClicked += new System.Windows.Forms.LinkLabelLinkClickedEventHandler(
                //        (Sender, Args) =>
                //        {
                //            try
                //            {
                //                System.Diagnostics.Process.Start(Args.Link.LinkData.ToString());
                //            }
                //            catch (Exception)
                //            {
                //                MessageBox.Show(LanguageResources.GetText("OpenLinkError"));
                //            }
                //        }); // new System.Windows.Forms.LinkLabelLinkClickedEventHandler(this.WebPage_LinkClicked);
                //    WebPage.MouseUp += LayoutPanel_MouseUp;
                //    WebPage.MouseDown += LayoutPanel_MouseDown;

                //    this.Controls.Add(WebPage);

                //}               

                this.tableLayoutPanel1.Size = this.Size; // new System.Drawing.Size(640, 480);
                this.tableLayoutPanel1.TabIndex = 4;
                this.tableLayoutPanel1.Dock = DockStyle.Fill;

                this.Controls.Add(this.tableLayoutPanel1);

                this.tableLayoutPanel1.ResumeLayout(false);
                this.ResumeLayout(false);

            }
            //
            #endregion


            Timer myScheduleTime = new Timer();
            Timer myNextScheduleTimer = new Timer();
            Timer mySecuenceTimeElapsed = new Timer();
            Timer tmpUserActivity = new Timer();

            myScheduleTime.Enabled = false;
            myScheduleTime.Tag = "myScheduleTime";
            myScheduleTime.Tick += new System.EventHandler(this.scheduleTimeIsUp);

            this.components.Add(myScheduleTime);

            myNextScheduleTimer.Enabled = false;
            myNextScheduleTimer.Tag = "myNextScheduleTimer";
            myNextScheduleTimer.Tick += new System.EventHandler(this.prepareNextSchedule);

            this.components.Add(myNextScheduleTimer);

            mySecuenceTimeElapsed.Enabled = false;
            mySecuenceTimeElapsed.Tag = "mySecuenceTimeElapsed";
            mySecuenceTimeElapsed.Tick += new System.EventHandler(this.keepTrack);
            mySecuenceTimeElapsed.Interval = 1000;

            this.components.Add(mySecuenceTimeElapsed);

            tmpUserActivity.Enabled = false;
            tmpUserActivity.Tag = "tmpUserActivity";
            tmpUserActivity.Tick += new System.EventHandler(this.resumeSecuence);

            this.components.Add(tmpUserActivity);

            #region DemoTryHard

            if (Variables.DEMO)
            {
                Timer tmpDemoReminder = new Timer();
                tmpDemoReminder.Enabled = false;
                tmpDemoReminder.Tag = "tmpDemoReminder";
                tmpDemoReminder.Interval = 1250;

                EventHandler DemoEvent = null;

                DemoEvent = delegate(Object DemoReminder, EventArgs DemoArgs)
                {
                    if (Variables.DEMO) { frm_Main_Click(this, null); }
                    else
                    { ((Timer)DemoReminder).Enabled = false; ((Timer)DemoReminder).Tick -= DemoEvent; }
                };

                tmpDemoReminder.Tick += DemoEvent;

                this.components.Add(tmpDemoReminder);

                tmpDemoReminder.Enabled = false;
            }

            #endregion

            //this.tableLayoutPanel1.SuspendLayout();

            System.Windows.Forms.WebBrowser myWeb = new System.Windows.Forms.WebBrowser();                
               
            myWeb.Name = "myWeb";
            myWeb.Dock = DockStyle.None;
            myWeb.Margin = new Padding(0, 0, 0, 0);
            myWeb.Anchor = fullControlAnchors();
            myWeb.IsWebBrowserContextMenuEnabled = BrowseParams.EnableContextMenu;
            myWeb.ScriptErrorsSuppressed = true;
            myWeb.ScrollBarsEnabled = false;

            //Console.WriteLine(myWeb.Version);

            myWeb.DocumentCompleted += docReady;
            //new System.Windows.Forms.WebBrowserDocumentCompletedEventHandler(this.docReady);
            myWeb.NewWindow += webNewWindow; //new System.ComponentModel.CancelEventHandler(this.webNewWindow);

            this.tableLayoutPanel1.Controls.Add(myWeb);
         
            //this.tableLayoutPanel1.ResumeLayout(true);                        

            // FullScreen
            /*  ____________
            *  |            |
            *  |____________|
            */

            myTemplate = 1;

            tableLayoutPanel1.SetCellPosition(myWeb, new TableLayoutPanelCellPosition(0, 0));

            actualSchedule = BrowseParams.CurrentCampaignID;

            String AdType = String.Empty;
            Boolean controlException = false;
            String comment = String.Empty;            

            ////
            
            try
            {                 
                AdType = "Link";                            

                myWeb.Tag = BrowseParams.Link;

                myWeb.Navigate(BrowseParams.Link);  // myWeb.Url = new System.UriBuilder(BrowseParams.Link).Uri;

                Console.WriteLine(myWeb.Tag.ToString());        
            }
            catch (OutOfMemoryException tmpEx)
            {
                controlException = true;

                comment = "The application was unable to load the Requested Link: [" + '"' + BrowseParams.Link + '"' + "]. ";

                comment += "Additional error reporting information: [" + tmpEx.Message + "]"; 
            }
            catch (System.IO.FileNotFoundException tmpEx)
            {
                controlException = true;

                comment = "The application was unable to load the Requested Link: [" + '"' + BrowseParams.Link + '"' + "]. ";
                comment += "Additional error reporting information: [" + "File not found or invalid file path: " + '"' + tmpEx.Message + '"' + "]";
            }
            catch (ArgumentNullException tmpEx)
            {
                controlException = true;

                comment = "The application was unable to load the Requested Link: [" + '"' + BrowseParams.Link + '"' + "]. ";
                comment += "Additional error reporting information: [" + tmpEx.Message + "]";
            }
            catch (Exception tmpEx)
            {
                controlException = true;

                comment = "The application was unable to load the Requested Link: [" + '"' + BrowseParams.Link + '"' + "]. ";

                comment += "Additional error reporting information: [" + tmpEx.Message + "]";
            }

            Ads ClsAds = new Ads();

            if (controlException)
            {
                ClsAds.saveAdClick(actualSchedule, BrowseParams.CallerAdID, AdType, 0, 0, comment);

                //Timer myScheduleTime;

                //myScheduleTime = ((Timer)this.components.Components.OfType<IComponent>().Where(ti => ti.GetType().FullName == "System.Windows.Forms.Timer")
                //.ToList().Where(ti => ((Timer)ti).Tag != null).ToList().Find(ti => ((Timer)ti).Tag.ToString() == "myScheduleTime"));

                //try { myScheduleTime.Interval -= (int)((new Functions().nextAd_DisplayTime(nextAd.id, pSecuence, currentSecuenceOrder[pSecuence], actualSchedule)) * 1000); }
                //catch (Exception tmpEx) { myScheduleTime.Interval = 1; } // Invalid Negative or Zero Interval protection 

                // inactivityThreshold = 0; // Testing...
            }

            ClsAds.saveAdClick(actualSchedule, BrowseParams.CallerAdID, AdType, 0, 1, "Requested Link: [" + '"' + BrowseParams.Link + '"' + "]");

            ClsAds = null;

            inactivityThreshold = BrowseParams.InteractionTime;
                                             
            ////

            scheduleTime = (BrowseParams.DisplayTime * 1000);

            if (BrowseParams.NextCampaign == NavigationParams.NextCampaignToShow.Current) { currentPlannedSchedule = (Int64.Parse(BrowseParams.CurrentCampaignID) - 1).ToString(); } // Next Campaign to look for would return the Current One. }

            resources.ReleaseAllResources();
            resources = null;

            this.isReady = true;
           
        }

        void LayoutPanel_MouseDown(object sender, MouseEventArgs e)
        {
            frm_Main_Click(null, null);
        }

        void LayoutPanel_MouseUp(object sender, MouseEventArgs e)
        {
            frm_Main_Click(null, null);
        }

        private void preLoad(object sender, EventArgs e)
        {
            ((Timer)sender).Enabled = false;

            //FrmConfig.ApplicationInit = false;
            //FrmConfig_OrganizationDetails Y = new FrmConfig_OrganizationDetails();
            //Y.ShowDialog();

            FrmConfig.ApplicationInit = true;

            FrmConfig Config = new FrmConfig();
            //FrmConfig_Language Config = new FrmConfig_Language(); // FrmConfig_Language Config = new FrmConfig_Language();
            Config.ShowDialog();

            Config.Dispose();

//            //
              
//            DateTime Ini = DateTime.Now;

//            String FileSerial = DataEncryption.EncryptText("FILEHASH", Functions.EncryptionKey);

//            DateTime End = DateTime.Now;

//            var Time = End.Subtract(Ini);
            
//            Console.WriteLine(Time);

//            Ini = DateTime.Now;

//            String ContentSerial = new BIGWISE.ProductStatus.Register().GetActivationKey("FILEHASH", Functions.EncryptionKey, 337);

//            End = DateTime.Now;

//            Time = End.Subtract(Ini);

//            Console.WriteLine(Time);

//            //

//            //

//            Ini = DateTime.Now;

//            System.IO.FileInfo fn = new System.IO.FileInfo("D:\\Peliculas\\test\\test.avi");

//            FileSerial = (fn.Name + "|" + fn.Length.ToString());

//            FileSerial = DataEncryption.EncryptText(FileSerial, Functions.EncryptionKey);

////            using (System.IO.FileStream stream = System.IO.File.OpenRead("D:\\Peliculas\\test\\test.avi")) // D:\\Peliculas\\test\\test.avi // C:\\img\\222.wmv
////{
////    System.Security.Cryptography.MD5Cng sha = new System.Security.Cryptography.MD5Cng();
////    byte[] hash = sha.ComputeHash(stream);
    
////    string result = Functions.ByteToHex2(hash);

////    End = DateTime.Now;

////    Time = End.Subtract(Ini);

////    Console.WriteLine(Time);

////}

//            End = DateTime.Now;

//            Time = End.Subtract(Ini);

//            Console.WriteLine(Time);

//            //

            if (MyConfig.Default.SyncToServer) { Functions.Sync(); System.Environment.Exit(0); }
            if (Program.Args.Contains("/SyncToServer")) {
                MyConfig.Default.SyncToServer = true;
                MyConfig.Default.Save();
                Functions.Sync();
                System.Environment.Exit(0); 
            }

            FrmConfig.ApplicationInit = false;

            Functions f = new Functions();

            ((Timer)sender).Enabled = false;

            Variables.ProductSerial = Functions.GetSerial();

            //if (Variables.DebugMode) System.Windows.Forms.MessageBox.Show("S: " + Variables.ProductSerial);

            // String Key = new BIGWISE.ProductStatus.Register().GetActivationKey("RIVAS CORP", "F4EB-B517-35FB-72E8-AF10-6369-6335-98A7", Variables.ProductCode);

            Variables.DEMO = !f.isActivated();

            while (Variables.DEMO)
            {

                FormInput Input = new FormInput();

                Input.Text = "Stellar ADS360";
                Input.ActionTrue = LanguageResources.GetText("ActivationWindowChooseDEMO"); Input.ButtonTrue.Text = Input.ActionTrue;
                Input.ActionFalse = LanguageResources.GetText("ActivationWindowChooseActivate"); Input.ButtonFalse.Text = Input.ActionFalse;
                Input.Message.Text = LanguageResources.GetText("ActivationWindowChoiceMessageLine1")
                    + System.Environment.NewLine + System.Environment.NewLine +
                    LanguageResources.GetText("ActivationWindowChoiceMessageLine2");
                Input.Message.ReadOnly = true;
                Input.ShowDialog();

                if (Input.Action.Equals(Input.ActionTrue))
                {
                    // Continue DEMO
                    break;
                }
                else if (Input.Action.Equals(Input.ActionFalse))
                {
                    // OLD ActivationUI
                    //FormActivation Activate = new FormActivation();
                    //Activate.Text = "Stellar isADS";
                    //Activate.InputKey.Text = "";
                    //Activate.ShowDialog();

                    //if (Activate.InputKey.Text == "") {
                    //    Activate.Close();
                    //    Input.Close();
                    //    break; 
                    //}

                    //Variables.DEMO = !f.Activate(Activate.InputKey.Text);

                    //Activate.Close();

                    //if (!Variables.DEMO) { MessageBox.Show(LanguageResources.GetText("ActivationProcessSuccess")); }
                    //else { MessageBox.Show(LanguageResources.GetText("ActivationProcessFailed")); }

                    FrmConfig_LocationSelect Tmp = new FrmConfig_LocationSelect();
                    Tmp.ShowDialog();

                    Variables.DEMO = !f.isActivated();                    

                }
                else if (Input.Action.Equals(Input.ActionCancel))
                {
                    // Continue DEMO
                    break;
                }

                Input.Close();

            }

            if (!Variables.DEMO)
            {
                Variables.myProgramDataFolder = Functions.getFilesFolder();
                try
                {
                    String FilesFolder = Variables.myProgramDataFolder;
                    if (!(System.IO.Directory.Exists(FilesFolder)))
                        System.IO.Directory.CreateDirectory(FilesFolder);
                }
                catch (Exception Any)
                {
                    Console.WriteLine(Any.Message);
                }
                Functions.Sync();
            }                
            else
            {
                MyConfig.Default.MediaPlayerID = "DEMO";
                MyConfig.Default.PropertyValues["MediaPlayerID"].IsDirty = false;
            }            

            numDisplayedSchedules = 3;

            startNextSchedule();

            //startSchedule();
        }

        public void startSchedule()
        {

            Timer myScheduleTime = ((Timer)this.components.Components.OfType<IComponent>().Where(ti => ti.GetType().FullName == "System.Windows.Forms.Timer")
            .ToList().Where(ti => ((Timer)ti).Tag != null).ToList().Find(ti => ((Timer)ti).Tag.ToString() == "myScheduleTime"));

            Timer myNextScheduleTimer = ((Timer)this.components.Components.OfType<IComponent>().Where(ti => ti.GetType().FullName == "System.Windows.Forms.Timer")
            .ToList().Where(ti => ((Timer)ti).Tag != null).ToList().Find(ti => ((Timer)ti).Tag.ToString() == "myNextScheduleTimer"));

            Timer mySecuenceTimeElapsed = ((Timer)this.components.Components.OfType<IComponent>().Where(ti => ti.GetType().FullName == "System.Windows.Forms.Timer")
            .ToList().Where(ti => ((Timer)ti).Tag != null).ToList().Find(ti => ((Timer)ti).Tag.ToString() == "mySecuenceTimeElapsed"));

            //Form frmShell = Application.OpenForms["frmShell"];

            int nextEffect = 0;

            if  (!firstInstance) {

                this.Visible = true;

                //this.BeginInvoke(
                //    (Action)(() =>
                //    {

                        nextEffect = new Random().Next(5);

                        switch (nextEffect)
                        {
                            case 0:
                                this.Location = new Point(0, 0);
                                this.Width = Screen.PrimaryScreen.Bounds.Width;
                                this.Height = Screen.PrimaryScreen.Bounds.Height;
                                break;
                            case 1:
                                this.Location = new Point(Screen.PrimaryScreen.Bounds.Width, 0);
                                this.Width = Screen.PrimaryScreen.Bounds.Width;
                                break;
                            case 2:
                                this.Location = new Point(Screen.PrimaryScreen.Bounds.Width * -1, 0);
                                this.Width = Screen.PrimaryScreen.Bounds.Width;
                                break;
                            //GrowInRight
                            //case 2:
                            //    this.Width = 2;
                            //    this.Location = new Point(0, 0);
                            //    break;
                            case 3:
                                this.Location = new Point(0, Screen.PrimaryScreen.Bounds.Height * -1);
                                this.Height = Screen.PrimaryScreen.Bounds.Height;
                                break;
                            case 4:
                                this.Location = new Point(0, Screen.PrimaryScreen.Bounds.Height);
                                this.Height = Screen.PrimaryScreen.Bounds.Height;
                                break;
                            //GrowInUp
                            //case 4:
                            //    this.Height = 2;
                            //    this.Location = new Point(0, 0);
                            //    break;
                        }

                //switch (EffectDescription)
                //{
                //    case "None":
                //        this.Location = new Point(0, 0);
                //        this.Width = Screen.PrimaryScreen.Bounds.Width;
                //        this.Height = Screen.PrimaryScreen.Bounds.Height;
                //        break;
                //    case "SlideLeft":
                //        this.Location = new Point(Screen.PrimaryScreen.Bounds.Width, 0);
                //        this.Width = Screen.PrimaryScreen.Bounds.Width;
                //        break;
                //    case "SlideRight":
                //        this.Width = 2;
                //        this.Location = new Point(0, 0);
                //        break;
                //    case "SlideDown":
                //        this.Location = new Point(0, Screen.PrimaryScreen.Bounds.Height);
                //        this.Height = Screen.PrimaryScreen.Bounds.Height;
                //        break;
                //    case "SlideUp":
                //        this.Height = 2;
                //        this.Location = new Point(0, 0);
                //        break;
                //}

                    //})
                    //);

                //this.Opacity = 1;

                //this.Width = 2;
                //this.Location = new Point(0, 0);
                //this.Location = new Point(Screen.PrimaryScreen.Bounds.Width, 0);
                //this.Width = Screen.PrimaryScreen.Bounds.Width;
            }

            try { Audio.Volume = MyConfig.Default.ApplicationVolume; }
            catch (Exception Any) { Console.WriteLine(Any.Message); }

            for (int i = 1; i <= currentAdCount; i++)
            {
                startSecuence(i);
            }                        

            for (int i = 1; i <= currentAdCount; i++)
            {
                ((Timer)this.components.Components.OfType<IComponent>().Where(ti => ti.GetType().FullName == "System.Windows.Forms.Timer")
                .ToList().Where(ti => ((Timer)ti).Tag != null).ToList().Find(ti => "myTime" + ((Timer)ti).Tag.ToString().Right(1) == "myTime" + i.ToString())
                ).Enabled = true;
            }

            myScheduleTime.Interval = this.scheduleTime;

            if (scheduleTime > 5000)
            {
                myNextScheduleTimer.Interval = scheduleTime - 5000;
            }
            else
            {
                myNextScheduleTimer.Interval = 500;
            }

            myNextScheduleTimer.Enabled = true;

            myScheduleTime.Enabled = true;

            if (mySecuenceTimeElapsed != null) { mySecuenceTimeElapsed.Enabled = true; }

            if (!firstInstance) // (this.Width <= 150) // (this.Location == new Point(Screen.PrimaryScreen.Bounds.Width,0)) //(this.Width <= 150)
            {

                //this.BeginInvoke(
                //    (Action)(() =>
                //    {

                        switch (nextEffect)
                        {
                            case 0:
                                //this.AnimationDrawContentLeftToRight(1000);                        
                                break;
                            case 1:
                                //this.AnimationDrawContentBottomToTop(1000);
                                slideInLeft(EffectsMethod.Steps, 9, this);
                                break;
                            case 2:
                                //this.AnimationExpandContent(1000);
                                slideInRight(EffectsMethod.Percent, 11, this);
                                break;
                            case 3:
                                //this.AnimationSlideContentRightToLeft(1000);
                                slideInDown(EffectsMethod.Percent, 11, this);
                                break;
                            case 4:
                                //this.AnimationSlideContentTopToBottom(1000);
                                slideInUp(EffectsMethod.Percent, 14, this);
                                break;
                        }          

                        //switch (EffectDescription)
                        //{
                        //    case "None":
                        //        //this.AnimationDrawContentLeftToRight(1000);                        
                        //        break;
                        //    case "SlideLeft":
                        //        slideInLeft(EffectsMethod.Steps, 7, this);
                        //        break;
                        //    case "SlideRight":
                        //        slideInRight(EffectsMethod.Percent, 11, this);
                        //        break;
                        //    case "SlideDown":
                        //        slideInDown(EffectsMethod.Percent, 11, this);
                        //        break;
                        //    case "SlideUp":
                        //        slideInUp(EffectsMethod.Percent, 14, this);
                        //        break;
                        //}

                    //})
                    //);

            }

            this.Show();

            //Color TemplateBorderColor = Color.White;

            //nextEffect = new Random().Next(7);

            //switch (nextEffect)
            //{
            //    case 0:
            //        TemplateBorderColor = Color.Black;
            //        break;
            //    case 1:
            //        TemplateBorderColor = Color.Yellow;
            //        break;
            //    case 2:
            //        TemplateBorderColor = Color.Blue;
            //        break;
            //    case 3:
            //        TemplateBorderColor = Color.Red;
            //        break;
            //    case 4:
            //        TemplateBorderColor = Color.Green;
            //        break;
            //    case 5:
            //        TemplateBorderColor = Color.Brown;
            //        break;
            //    case 6:
            //        TemplateBorderColor = Color.White;
            //        break;
            //}

            //tableLayoutPanel1.BackColor = TemplateBorderColor;

            if ((numDisplayedSchedules >= 3) && (Variables.DEMO))
            {

                Form AnnoyingForm = Application.OpenForms["AnnoyingForm"]; 
               
                if (AnnoyingForm == null) 
                {
                    numDisplayedSchedules = 1;

                    System.Threading.ThreadPool.QueueUserWorkItem(new System.Threading.WaitCallback((Data) => { 
                        ShowActivationRequest(); 
                    }));
                    
                    //System.Threading.Thread ParallelAction = new System.Threading.Thread(ShowActivationRequest);                                        

                    // //Variables.SecondThread = new System.Threading.Thread(ShowActivationRequest);

                    //ParallelAction.Start();                
                }
                
            }
            else { numDisplayedSchedules++; }

            if (Variables.CurrentAppSession_CampaingsAndSessionsDisplayed + 1 >= Int64.MaxValue)
                Variables.CurrentAppSession_CampaingsAndSessionsDisplayed = 0; // Avoid Exception;

            Variables.CurrentAppSession_CampaingsAndSessionsDisplayed += 1;

            if (MyConfig.Default.TotalCampaingsAndSessionsDisplayed + 1 >= Int64.MaxValue)
                MyConfig.Default.TotalCampaingsAndSessionsDisplayed = 0; // Avoid Exception;

            MyConfig.Default.TotalCampaingsAndSessionsDisplayed += 1;
            MyConfig.Default.Save();

            try
            {
                if (MyConfig.Default.SoftwareUpdate_CheckForUpdates)
                {
                    Functions f = new Functions();

                    Form UpdateAlert = Application.OpenForms["FormPopUp"];

                    if (UpdateAlert == null)
                    {
                        if (Functions.CheckForUpdates(f.DBConnection, 10))
                        {

                            Console.WriteLine((MyConfig.Default.TotalCampaingsAndSessionsDisplayed % MyConfig.Default.SoftwareUpdate_CampaignsInterval));

                            if ((MyConfig.Default.TotalCampaingsAndSessionsDisplayed % MyConfig.Default.SoftwareUpdate_CampaignsInterval) == 0)
                            {

                                //// Test Restart Code.

                                //////Functions.RestartApp();

                                ////It's Working. Update: Forced Termination might result in random crashes.
                                ////Let's instead clean resources, and leave program in a ready state before exit.                              

                                //Functions.setTimeOut(1000, delegate(Object myTimer, System.Timers.ElapsedEventArgs ev)
                                //{
                                //    try
                                //    {
                                //        this.BeginInvoke((Action)(() => {
                                //            Program.RestartRequired = true;
                                //            myScheduleTime.Enabled = false;
                                //            myScheduleTime.Interval = 1;
                                //            myScheduleTime.Enabled = true; 
                                //        }));
                                //    }
                                //    catch (Exception Any) { Console.WriteLine(Any.Message); }
                                //    ((System.Timers.Timer)myTimer).Dispose();
                                //});

                                //throw new System.Exception("EndCheck");

                                System.Threading.ThreadPool.QueueUserWorkItem(new System.Threading.WaitCallback((Data) =>
                                {
                                    FormPopUp tmpUpdateAlert = new FormPopUp();                                    

                                    tmpUpdateAlert.TextMsg = LanguageResources.GetText("FrmConfig_Updates.Message");
                                    //tmpUpdateAlert.TextMsgFont = new Font(...);

                                    tmpUpdateAlert.MessageClickAction = ((Object Any) =>
                                    {
                                        //System.Diagnostics.Process.Start("http://www.stellarads360.com"); // Users could open the Browser and just leave it there. :/.
                                        var Msg = new Message();
                                        this.ProcessCmdKey(ref Msg, Keys.F5);

                                        tmpUpdateAlert.BeginInvoke((Action)(() =>
                                        {
                                            if (tmpUpdateAlert.QuitFocusControl.CanFocus) tmpUpdateAlert.QuitFocusControl.Focus();
                                        }));
                                    });

                                    tmpUpdateAlert.IconClickAction = ((Object Any) =>
                                    {
                                        var Msg = new Message();
                                        this.ProcessCmdKey(ref Msg, Keys.F5);

                                        tmpUpdateAlert.BeginInvoke((Action)(() =>
                                        {
                                            if (tmpUpdateAlert.QuitFocusControl.CanFocus) tmpUpdateAlert.QuitFocusControl.Focus();
                                        }));
                                    });

                                    tmpUpdateAlert.KeepTopMost = true;

                                    tmpUpdateAlert.ShowDialog();

                                    tmpUpdateAlert.Dispose();
                                }));

                                //new System.Threading.Thread(() =>
                                //{
                                //    FormPopUp tmpUpdateAlert = new FormPopUp();
                                //    tmpUpdateAlert.ShowDialog();
                                //}).Start();

                            }
                        }
                    }
                }
            } catch (Exception Any) 
            { Console.WriteLine(Any.Message); }

            if (Variables.DEMO)
            {
                Timer tmpDemoReminder = ((Timer)this.components.Components.OfType<IComponent>().Where(ti => ti.GetType().FullName == "System.Windows.Forms.Timer")
                    .ToList().Where(ti => ((Timer)ti).Tag != null).ToList().Find(ti => ((Timer)ti).Tag.ToString() == "tmpDemoReminder"));
                if (tmpDemoReminder != null) tmpDemoReminder.Enabled = true;
            }
            else 
            { 
                this.BringToFront(); 
            }

            // To enable BorderColor on Template.

            //Functions.setTimeOut(25, delegate(Object myTimer, System.Timers.ElapsedEventArgs ev)
            //{
            //    try { this.BeginInvoke((Action)(() => { this.tableLayoutPanel1.BackColor = TemplateBorderColor; })); }
            //    catch (Exception) { }
            //    ((System.Timers.Timer)myTimer).Dispose();
            //});

        }

        private void ShowActivationRequest()
        {            
            AnnoyingForm Activation = new AnnoyingForm();
            Activation.ShowDialog();
        }

        private AnchorStyles fullControlAnchors()
        {
            return ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
            | System.Windows.Forms.AnchorStyles.Left)
            | System.Windows.Forms.AnchorStyles.Right)));
        }

        /// <summary>
        /// Establece el template a utilizar.
        /// </summary>
        /// <param name="template">Código del template a utilizar..</param>
        private void setTemplateLayout()
        {

            //AxWMPLib.AxWindowsMediaPlayer myWmp;
            //PictureBox myPics;
            //WebBrowser myWebrowser;
            Control placeHolder;

            switch (myTemplate)
            {
                // FullScreen
                /*  ____________
                *  |            |
                *  |____________|
                */
                case 1:
                {

                    placeHolder = tableLayoutPanel1.Controls.Find("myPics1", true).ElementAt(0);
                    tableLayoutPanel1.SetCellPosition(placeHolder, new TableLayoutPanelCellPosition(0, 0));
                    
                    tableLayoutPanel1.SetColumnSpan(placeHolder, 2);
                    tableLayoutPanel1.SetRowSpan(placeHolder, 2);
                    
                    tableLayoutPanel1.RowStyles[0].Height = 100F;
                    tableLayoutPanel1.RowStyles[1].Height = 0F;
                    tableLayoutPanel1.ColumnStyles[0].Width = 0F;
                    tableLayoutPanel1.ColumnStyles[1].Width = 0F;

                    break;
                }
                // 3-Div: 25%, 25%, 50%;
                /*  ____________
                *  |__1__|__2___|
                *  |_____3______|
                */
                case 2:
                {
                    placeHolder = tableLayoutPanel1.Controls.Find("myPics1", true).ElementAt(0);
                    tableLayoutPanel1.SetCellPosition(placeHolder, new TableLayoutPanelCellPosition(0, 0));
                    placeHolder = tableLayoutPanel1.Controls.Find("myPics2", true).ElementAt(0);
                    tableLayoutPanel1.SetCellPosition(placeHolder, new TableLayoutPanelCellPosition(1, 0));
                    placeHolder = tableLayoutPanel1.Controls.Find("myPics3", true).ElementAt(0);
                    tableLayoutPanel1.SetCellPosition(placeHolder, new TableLayoutPanelCellPosition(0, 1));
                    
                    tableLayoutPanel1.SetColumnSpan(placeHolder, 2);
                    
                    tableLayoutPanel1.RowStyles[0].Height = 50F;
                    tableLayoutPanel1.RowStyles[1].Height = 50F;
                    tableLayoutPanel1.ColumnStyles[0].Width = 50F;
                    tableLayoutPanel1.ColumnStyles[1].Width = 50F;

                    break;
                }
                // Vertical Split: 50%, 50%
                /*  ____________
                *  |  1  |  2   |
                *  |_____|______|
                */
                case 3:
                {
                    placeHolder = tableLayoutPanel1.Controls.Find("myPics1", true).ElementAt(0);
                    tableLayoutPanel1.SetCellPosition(placeHolder, new TableLayoutPanelCellPosition(0, 0));
                    tableLayoutPanel1.SetRowSpan(placeHolder, 2);

                    placeHolder = tableLayoutPanel1.Controls.Find("myPics2", true).ElementAt(0);
                    tableLayoutPanel1.SetCellPosition(placeHolder, new TableLayoutPanelCellPosition(1, 0));
                    tableLayoutPanel1.SetRowSpan(placeHolder, 2);

                    tableLayoutPanel1.RowStyles[0].Height = 100F;
                    tableLayoutPanel1.RowStyles[1].Height = 0F;
                    tableLayoutPanel1.ColumnStyles[0].Width = 50F;
                    tableLayoutPanel1.ColumnStyles[1].Width = 50F;

                    break;
                }
                // Horizontal Split: 50%, 50%
                /*  ____________
                *  |_____1______|
                *  |_____2______|
                */
                case 4:
                {
                    placeHolder = tableLayoutPanel1.Controls.Find("myPics1", true).ElementAt(0);
                    tableLayoutPanel1.SetCellPosition(placeHolder, new TableLayoutPanelCellPosition(0, 0));
                    tableLayoutPanel1.SetColumnSpan(placeHolder, 2);

                    placeHolder = tableLayoutPanel1.Controls.Find("myPics2", true).ElementAt(0);
                    tableLayoutPanel1.SetCellPosition(placeHolder, new TableLayoutPanelCellPosition(0, 1));
                    tableLayoutPanel1.SetColumnSpan(placeHolder, 2);

                    tableLayoutPanel1.ColumnStyles[0].Width = 100F;
                    tableLayoutPanel1.ColumnStyles[1].Width = 0F;
                    tableLayoutPanel1.RowStyles[0].Height = 50F;
                    tableLayoutPanel1.RowStyles[1].Height = 50F;

                    break;
                }
                // Template 5: 20x70(%), 80x70(%), 100x30(%)
                /*  ____________
                *  | 1|         |
                *  |__|__2______|
                *  |_____3______|
                */
                case 5:
                {
                    placeHolder = tableLayoutPanel1.Controls.Find("myPics1", true).ElementAt(0);
                    tableLayoutPanel1.SetCellPosition(placeHolder, new TableLayoutPanelCellPosition(0, 0));
                    placeHolder = tableLayoutPanel1.Controls.Find("myPics2", true).ElementAt(0);
                    tableLayoutPanel1.SetCellPosition(placeHolder, new TableLayoutPanelCellPosition(1, 0));
                    placeHolder = tableLayoutPanel1.Controls.Find("myPics3", true).ElementAt(0);
                    tableLayoutPanel1.SetCellPosition(placeHolder, new TableLayoutPanelCellPosition(0, 1));

                    tableLayoutPanel1.SetColumnSpan(placeHolder, 2);

                    tableLayoutPanel1.ColumnStyles[0].Width = 20F;
                    tableLayoutPanel1.ColumnStyles[1].Width = 80F;
                    tableLayoutPanel1.RowStyles[0].Height = 80F;
                    tableLayoutPanel1.RowStyles[1].Height = 20F;

                    break;
                }
                // Template 6: 80x70(%), 20x70(%), 100x30(%)
                /*  ____________
                *  |         |2 |
                *  |_____1___|__|
                *  |_____3______|
                */
                case 6:
                {
                    placeHolder = tableLayoutPanel1.Controls.Find("myPics1", true).ElementAt(0);
                    tableLayoutPanel1.SetCellPosition(placeHolder, new TableLayoutPanelCellPosition(0, 0));

                    placeHolder = tableLayoutPanel1.Controls.Find("myPics2", true).ElementAt(0);
                    tableLayoutPanel1.SetCellPosition(placeHolder, new TableLayoutPanelCellPosition(1, 0));

                    placeHolder = tableLayoutPanel1.Controls.Find("myPics3", true).ElementAt(0);
                    tableLayoutPanel1.SetCellPosition(placeHolder, new TableLayoutPanelCellPosition(0, 1));

                    tableLayoutPanel1.SetColumnSpan(placeHolder, 2);

                    tableLayoutPanel1.ColumnStyles[0].Width = 80F;
                    tableLayoutPanel1.ColumnStyles[1].Width = 20F;
                    tableLayoutPanel1.RowStyles[0].Height = 80F;
                    tableLayoutPanel1.RowStyles[1].Height = 20F;

                    break;
                }
                // Template 7: 20x100(%), 80x70(%), 70x30(%)
                /*  ____________
                *  | 1|         |
                *  |  |__2______|
                *  |__|__3______|
                */
                case 7:
                {
                    placeHolder = tableLayoutPanel1.Controls.Find("myPics1", true).ElementAt(0);
                    tableLayoutPanel1.SetCellPosition(placeHolder, new TableLayoutPanelCellPosition(0, 0));

                    tableLayoutPanel1.SetRowSpan(placeHolder, 2);

                    placeHolder = tableLayoutPanel1.Controls.Find("myPics2", true).ElementAt(0);
                    tableLayoutPanel1.SetCellPosition(placeHolder, new TableLayoutPanelCellPosition(1, 0));
                    placeHolder = tableLayoutPanel1.Controls.Find("myPics3", true).ElementAt(0);
                    tableLayoutPanel1.SetCellPosition(placeHolder, new TableLayoutPanelCellPosition(1, 1));

                    tableLayoutPanel1.ColumnStyles[0].Width = 20F;
                    tableLayoutPanel1.ColumnStyles[1].Width = 80F;
                    tableLayoutPanel1.RowStyles[0].Height = 80F;
                    tableLayoutPanel1.RowStyles[1].Height = 20F;

                    break;
                }
                // Template 8: 80x70(%), 20x100(%), 70x30(%)
                /*  ____________
                *  |         |2 |
                *  |_____1___|  |
                *  |_____3___|__|
                */
                case 8:
                {
                    placeHolder = tableLayoutPanel1.Controls.Find("myPics1", true).ElementAt(0);
                    tableLayoutPanel1.SetCellPosition(placeHolder, new TableLayoutPanelCellPosition(0, 0));

                    placeHolder = tableLayoutPanel1.Controls.Find("myPics2", true).ElementAt(0);
                    tableLayoutPanel1.SetCellPosition(placeHolder, new TableLayoutPanelCellPosition(1, 0));

                    tableLayoutPanel1.SetRowSpan(placeHolder, 2);

                    placeHolder = tableLayoutPanel1.Controls.Find("myPics3", true).ElementAt(0);
                    tableLayoutPanel1.SetCellPosition(placeHolder, new TableLayoutPanelCellPosition(0, 1));

                    tableLayoutPanel1.ColumnStyles[0].Width = 80F;
                    tableLayoutPanel1.ColumnStyles[1].Width = 20F;
                    tableLayoutPanel1.RowStyles[0].Height = 80F;
                    tableLayoutPanel1.RowStyles[1].Height = 20F;

                    break;
                }                
            }
        }

        private void videoClick(object sender, AxWMPLib._WMPOCXEvents_ClickEvent e)
        {
            try
            {

                this.SimpleClickEffect();

                int Secuence = int.Parse(((Control)sender).Name.Right(1));

                (currentAd[Secuence] as AdInfo).AdBaseData.saveAdClick(actualSchedule.ToString(), (currentAd[Secuence] as AdInfo).AdBaseData.ID, "Video", 1, 0);

                if (!(currentAd[Secuence] as AdInfo).DisplaySettings.ContentLink.isUndefined())
                {
                    Form frmShell = Application.OpenForms["frmShell"];
                    frm_Main frmPreLoad = (frm_Main)frmShell.OwnedForms.ToList().Find(frm => (frm.Name == ("frm_Main" + (numForm + 1).ToString())));
                    if (frmPreLoad != null) { frmPreLoad.Dispose(); }

                    this.BrowseParams = new NavigationParams()
                    {
                        Link = (currentAd[Secuence] as AdInfo).DisplaySettings.ContentLink,
                        EnableContextMenu = true,
                        DisplayTime = (currentAd[Secuence] as AdInfo).DisplaySettings.DisplayTime,
                        InteractionTime = ((currentAd[Secuence] as AdInfo).DisplaySettings.InteractionTime > 10 ? (currentAd[Secuence] as AdInfo).DisplaySettings.InteractionTime : 10),
                        NextCampaign = NavigationParams.NextCampaignToShow.Next,
                        CurrentCampaignID = actualSchedule,
                        CallerAdID = (currentAd[Secuence] as AdInfo).AdBaseData.ID
                    };

                    this.scheduleTimeIsUp(null, null);

                    return;
                }

                runInteractionTime();

            } catch { }
        }

        private void videoMouseDown(object sender, AxWMPLib._WMPOCXEvents_MouseDownEvent e)
        {
            frm_Main_Click(null, null);
        }
        private void videoMouseUp(object sender, AxWMPLib._WMPOCXEvents_MouseUpEvent e)
        {
            frm_Main_Click(null, null);
        }

        //private void videoDoubleClick(object sender, AxWMPLib._WMPOCXEvents_DoubleClickEvent e)
        //{
        //    videoClick(sender, null);
        //}

        private void playerError(object sender, EventArgs e)
        {
            try
            {

                int Secuence = int.Parse(((Control)sender).Name.Right(1));

                //AxWMPLib.AxWindowsMediaPlayer myWmp = ((AxWMPLib.AxWindowsMediaPlayer)
                //this.Controls.Find("MyWmp" + Secuence, true)[0]);

                string comment;

                //Console.WriteLine(myWmp.currentMedia.sourceURL);

                if (!System.IO.File.Exists(PlayMedia[Secuence - 1].sourceURL)) //System.IO.File.Exists(myWmp.currentMedia.sourceURL)
                {
                    comment = "The application was unable to play / show this content. ";
                    comment += "Additional error reporting information: [" + "File not found or invalid file path: " + "\"" + PlayMedia[Secuence - 1].sourceURL + "\"" + "]";
                }
                else
                {
                    comment = "The application was unable to play / show this content. It is recommended " +
                    "to check for proper file naming, avoiding special characters, and also make sure that " +
                    "the content does not require extra dependencies or is in a non-standard format. ";
                    comment += "Additional error reporting information: [" + "Invalid video file or non-supported file format" + "]";
                }

                (currentAd[Secuence] as AdInfo).AdBaseData.saveAdClick(actualSchedule.ToString(), (currentAd[Secuence] as AdInfo).AdBaseData.ID, "Video", 0, -1, comment);

                // Timer myTime; Timer myScheduleTime;

                //myScheduleTime = ((Timer)this.components.Components.OfType<IComponent>().Where(ti => ti.GetType().FullName == "System.Windows.Forms.Timer")
                //.ToList().Where(ti => ((Timer)ti).Tag != null).ToList().Find(ti => ((Timer)ti).Tag.ToString() == "myScheduleTime"));

                //try
                //{
                //myTime = ((Timer)this.components.Components.OfType<IComponent>().Where(ti => ti.GetType().FullName == "System.Windows.Forms.Timer")
                //.ToList().Where(ti => ((Timer)ti).Tag != null).ToList().Find(ti => "myTime" + ((Timer)ti).Tag.ToString().Right(1) == "myTime" + Secuence.ToString()));

                //myScheduleTime.Interval -= (int)(myTime.Interval);
                //}
                //catch (Exception tmpEx) {
                //Console.WriteLine(tmpEx.Message);    
                //myScheduleTime.Interval = 1000; return; } // Invalid Negative or Zero Interval protection 

                inactivityThreshold = 0;

                ((Timer)this.components.Components.OfType<IComponent>().Where(ti => ti.GetType().FullName == "System.Windows.Forms.Timer")
                .ToList().Where(ti => ((Timer)ti).Tag != null).ToList().Find(ti => "myTime" + ((Timer)ti).Tag.ToString().Right(1) == "myTime" + Secuence.ToString())
                ).Interval = 1;

                // Console.WriteLine(((AxWMPLib.AxWindowsMediaPlayer)sender).Error.get_Item(0).errorDescription);

            } catch { }

        }

        private void checkWmpState(object sender, EventArgs e)
        {          
            int Secuence = int.Parse(((Timer)sender).Tag.ToString().Right(1)); 

            AxWMPLib.AxWindowsMediaPlayer myWmp;
            
            myWmp = ((AxWMPLib.AxWindowsMediaPlayer)this.Controls.Find("MyWmp" + Secuence, true)[0]);

            if (myWmp != null)
            {

            ((Timer)sender).Enabled = false;

            if  (myWmp.Error.errorCount == 0)
                {
                    (currentAd[Secuence] as AdInfo).AdBaseData.saveAdClick(actualSchedule.ToString(), (currentAd[Secuence] as AdInfo).AdBaseData.ID, "Video", 0, 1);
                }
                else
                {
                    string comment;

                    if (!System.IO.File.Exists(myWmp.currentMedia.sourceURL))
                    {
                        comment = "The application was unable to play / show this content. ";
                        comment += "Additional error reporting information: [" + "File not Found" + "].";
                    }
                    else
                    {
                        comment = "The application was unable to play / show this content. It is recommended " +
                        "to check for proper file naming, avoiding special characters, and also make sure that " +
                        "the content does not require extra dependencies or is in a non-standard format. ";
                        comment += "Additional error reporting information: [" + "Invalid video file or non-supported file format" + "].";
                    }

                    (currentAd[Secuence] as AdInfo).AdBaseData.saveAdClick(actualSchedule.ToString(), (currentAd[Secuence] as AdInfo).AdBaseData.ID, "Video", 0, 0, comment);

                    Timer myScheduleTime;

                    myScheduleTime = ((Timer)this.components.Components.OfType<IComponent>().Where(ti => ti.GetType().FullName == "System.Windows.Forms.Timer")
                    .ToList().Where(ti => ((Timer)ti).Tag != null).ToList().Find(ti => ((Timer)ti).Tag.ToString() == "myScheduleTime"));

                    try { myScheduleTime.Interval -= (int)(((Timer)this.components.Components.OfType<IComponent>().Where(ti => ti.GetType().FullName == "System.Windows.Forms.Timer")
                    .ToList().Where(ti => ((Timer)ti).Tag != null).ToList().Find(ti => "myTime" + ((Timer)ti).Tag.ToString().Right(1) == "myTime" + Secuence.ToString())
                    ).Interval); }

                    catch (Exception) { myScheduleTime.Interval = 1000; return; } // Invalid Negative or Zero Interval protection 

                    ((Timer)this.components.Components.OfType<IComponent>().Where(ti => ti.GetType().FullName == "System.Windows.Forms.Timer")
                    .ToList().Where(ti => ((Timer)ti).Tag != null).ToList().Find(ti => "myTime" + ((Timer)ti).Tag.ToString().Right(1) == "myTime" + Secuence.ToString())
                    ).Interval = 250;

                    inactivityThreshold = 0;
                }
            }

        }

        private void mediaChange(object sender, AxWMPLib._WMPOCXEvents_CurrentItemChangeEvent e)
        {
            //Ads ad = new Ads();
            
            //string adId = ad.getAdVideo(new Functions().DBConnection, 
            //((AxWMPLib.AxWindowsMediaPlayer)sender).currentMedia.name);

            //ad.saveAdClick(actualSchedule.ToString(), Int32.Parse(adId), "Video", 1, 0);
        }

        private void pictureClick(object sender, EventArgs e)
        {
            try
            {

                this.SimpleClickEffect();

                int Secuence = int.Parse(((Control)sender).Name.Right(1));

                (currentAd[Secuence] as AdInfo).AdBaseData.saveAdClick(actualSchedule.ToString(), (currentAd[Secuence] as AdInfo).AdBaseData.ID, "Image", 1, 0);

                if (!(currentAd[Secuence] as AdInfo).DisplaySettings.ContentLink.isUndefined())
                // new System.Diagnostics.Process() { StartInfo = new System.Diagnostics.ProcessStartInfo((currentAd[Secuence] as AdInfo).DisplaySettings.ContentLink) }.Start(); // Test
                {
                    Form frmShell = Application.OpenForms["frmShell"];
                    frm_Main frmPreLoad = (frm_Main)frmShell.OwnedForms.ToList().Find(frm => (frm.Name == ("frm_Main" + (numForm + 1).ToString())));
                    if (frmPreLoad != null) { frmPreLoad.Dispose(); }

                    this.BrowseParams = new NavigationParams()
                    {
                        Link = (currentAd[Secuence] as AdInfo).DisplaySettings.ContentLink,
                        EnableContextMenu = true,
                        DisplayTime = (currentAd[Secuence] as AdInfo).DisplaySettings.DisplayTime,
                        InteractionTime = ((currentAd[Secuence] as AdInfo).DisplaySettings.InteractionTime > 10 ? (currentAd[Secuence] as AdInfo).DisplaySettings.InteractionTime : 10),
                        NextCampaign = NavigationParams.NextCampaignToShow.Next,
                        CurrentCampaignID = actualSchedule,
                        CallerAdID = (currentAd[Secuence] as AdInfo).AdBaseData.ID
                    };

                    this.scheduleTimeIsUp(null, null);

                    return;
                }

                runInteractionTime();

            } catch { }
        }

        private void pictureMouseDown(object sender, MouseEventArgs e)
        {
            frm_Main_Click(null, null);
        }

        private void pictureMouseUp(object sender, MouseEventArgs e)
        {
            frm_Main_Click(null, null);
        }

        //private void pictureDoubleClick(object sender, EventArgs e)
        //{
        //    pictureClick(sender, null);  
        //}

        private void docReady(object sender, WebBrowserDocumentCompletedEventArgs e)
        {
            HtmlDocument Doc = (sender as WebBrowser).Document;

            if (Doc != null)
            {               
                Doc.Window.Name = (sender as WebBrowser).Name;
                Doc.Click += webClick;
                Doc.MouseDown += webMouseDown;
                Doc.MouseUp += webMouseUp;
                HtmlElement ActiveElement = Doc.ActiveElement;
                ActiveElement.KeyPress += webInput;

                //if (((System.Windows.Forms.WebBrowser)sender).Document.Url.AbsolutePath.Equals("/dnserrordiagoff.htm"))
                //{
                //    webError((sender as WebBrowser));
                //}
            }
        }

        private void webError(WebBrowser myWeb)
        {
            try
            {

                int Secuence = int.Parse(myWeb.Name.Right(1));

                string comment;

                Console.WriteLine(myWeb.Document.Url.Fragment.Replace("#file:///", ""));

                Uri Url = new Uri(myWeb.Document.Url.Fragment.Replace("#file:///", ""));

                if (!System.IO.File.Exists(myWeb.Document.Url.Fragment.Replace("#file:///", "")))
                {
                    comment = "The application was unable to play / show this content. ";
                    comment += "Additional error reporting information: [" + "File not found or invalid URL: " + "\"" + myWeb.Document.Url.PathAndQuery + "\"" + "]";
                }
                else
                {
                    comment = "The application was unable to play / show this content. ";
                    comment += "Additional error reporting information: [" + "File not found or invalid URL: " + "\"" + myWeb.Document.Url.PathAndQuery + "\"" + "]";
                }

                (currentAd[Secuence] as AdInfo).AdBaseData.saveAdClick(actualSchedule.ToString(), (currentAd[Secuence] as AdInfo).AdBaseData.ID, "App", 0, -1, comment);

                // Timer myTime; Timer myScheduleTime;

                //myScheduleTime = ((Timer)this.components.Components.OfType<IComponent>().Where(ti => ti.GetType().FullName == "System.Windows.Forms.Timer")
                //.ToList().Where(ti => ((Timer)ti).Tag != null).ToList().Find(ti => ((Timer)ti).Tag.ToString() == "myScheduleTime"));

                //try
                //{
                //myTime = ((Timer)this.components.Components.OfType<IComponent>().Where(ti => ti.GetType().FullName == "System.Windows.Forms.Timer")
                //.ToList().Where(ti => ((Timer)ti).Tag != null).ToList().Find(ti => "myTime" + ((Timer)ti).Tag.ToString().Right(1) == "myTime" + Secuence.ToString()));

                //myScheduleTime.Interval -= (int)(myTime.Interval);
                //}
                //catch (Exception tmpEx) {
                //Console.WriteLine(tmpEx.Message);    
                //myScheduleTime.Interval = 1000; return; } // Invalid Negative or Zero Interval protection             

                inactivityThreshold = 0;

                ((Timer)this.components.Components.OfType<IComponent>().Where(ti => ti.GetType().FullName == "System.Windows.Forms.Timer")
                .ToList().Where(ti => ((Timer)ti).Tag != null).ToList().Find(ti => "myTime" + ((Timer)ti).Tag.ToString().Right(1) == "myTime" + Secuence.ToString())
                ).Interval = 1;

            } catch { }

        }

        private void webInput(object sender, HtmlElementEventArgs e)
        {
            // int Secuence = int.Parse(((HtmlElement)sender).Document.Window.Name.Right(1));

            // ((Ads)currentAd[Secuence]).saveAdClick(actualSchedule.ToString(), ((Ads)currentAd[Secuence]).id, "App", 1, 0);

            runInteractionTime();
        }

        private void webClick(object sender, HtmlElementEventArgs e)
        {
            try
            {

                this.SimpleClickEffect();

                if (currentAdCount > 0)
                {
                    int Secuence = int.Parse(((HtmlDocument)sender).Window.Name.Right(1));

                    (currentAd[Secuence] as AdInfo).AdBaseData.saveAdClick(actualSchedule.ToString(), (currentAd[Secuence] as AdInfo).AdBaseData.ID, "App", 1, 0);
                }

                runInteractionTime();

            } catch { }
        }

        private void webMouseDown(object sender, HtmlElementEventArgs e)
        {
            frm_Main_Click(null, null);
        }

        private void webMouseUp(object sender, HtmlElementEventArgs e)
        {
            frm_Main_Click(null, null);
        }

        private void webDoubleClick(object sender, HtmlElementEventArgs e)
        {
            webClick(sender, e);
        }

        private void webNewWindow(object sender, CancelEventArgs e)
        {
            WebBrowser IE = (sender as WebBrowser);

            if (IE.StatusText.StartsWith("http://", StringComparison.Ordinal))
            {
                e.Cancel = true;
                try { IE.Navigate(IE.StatusText); } catch { }
            }                        
        }

        private void startSecuence(int pSecuence)
        {
            int Secuence = pSecuence;
            int returnOrder = currentSecuenceOrder[Secuence];
            String nextAdId = new Functions().nextSecuenceAd(actualSchedule, Secuence, ref returnOrder);
            currentSecuenceOrder[Secuence] = returnOrder;

            showNextAd(Secuence, nextAdId);

            lastLoadedSecuence = Secuence;
        }

        private void keepTrack(object sender, EventArgs e){
            this.currentSecuenceTime++;
            this.currentScheduleTime++;
        }

        private void resumeSecuence(object sender, EventArgs e)
        {   
            Timer tmpUserActivity, myScheduleTime, mySecuenceTime = null;
            
            tmpUserActivity = ((Timer)sender);

            tmpUserActivity.Enabled = false;

            myScheduleTime = ((Timer)this.components.Components.OfType<IComponent>().Where(ti => ti.GetType().FullName == "System.Windows.Forms.Timer")
            .ToList().Where(ti => ((Timer)ti).Tag != null).ToList().Find(ti => ((Timer)ti).Tag.ToString() == "myScheduleTime"));

            Timer mySecuenceTimeElapsed = ((Timer)this.components.Components.OfType<IComponent>().Where(ti => ti.GetType().FullName == "System.Windows.Forms.Timer")
            .ToList().Where(ti => ((Timer)ti).Tag != null).ToList().Find(ti => ((Timer)ti).Tag.ToString() == "mySecuenceTimeElapsed"));

            try{ myScheduleTime.Interval -= (int) (currentScheduleTime * 1000); } catch(Exception) { myScheduleTime.Interval = 1000; } // Invalid Negative or Zero Interval protection 

            if (currentAdCount > 0) 
            { 
                mySecuenceTime = ((Timer)this.components.Components.OfType<IComponent>().Where(ti => ti.GetType().FullName == "System.Windows.Forms.Timer")
                .ToList().Where(ti => ((Timer)ti).Tag != null).ToList().Find(ti => ((Timer)ti).Tag.ToString().Right(1) == "1"));
            
                try{ mySecuenceTime.Interval -= (int) (currentSecuenceTime * 1000); } catch(Exception) { mySecuenceTime.Interval = 1000; } // Invalid Negative or Zero Interval protection                
            }
            
            currentSecuenceTime = 0; currentScheduleTime = 0;
            mySecuenceTimeElapsed.Interval = 1000;

            mySecuenceTimeElapsed.Enabled = true;
            if (mySecuenceTime != null) mySecuenceTime.Enabled = true;
            myScheduleTime.Enabled = true;

        }

        protected override bool ProcessCmdKey(ref Message msg, Keys keyData)
        {
            if (keyData == Keys.F1)
            {               

                //MessageBox.Show("You pressed the F1 key");

                Form Config = Application.OpenForms["FrmConfig_Language"];
                if (Config == null)
                {
                    System.Threading.Thread ParallelConfig = new System.Threading.Thread(ShowConfig);
                    ParallelConfig.SetApartmentState(System.Threading.ApartmentState.STA);
                    ParallelConfig.Start();
                }
                else
                {
                    Config.BeginInvoke(
                        (Action)(() =>
                        {
                            Config.BringToFront();
                            Config.TopMost = true;
                        })
                        );
                }
                
                return true;    // indicate that you handled this keystroke
            }
            else if (keyData == Keys.F2)
            {
                //MessageBox.Show("You pressed the F2 key");

                Form Config = Application.OpenForms["FrmConfig_Volume"];
                if (Config == null)
                {
                    System.Threading.Thread ParallelConfig = new System.Threading.Thread(ShowVolume);
                    ParallelConfig.Start();
                }
                else
                {
                    Config.BeginInvoke(
                        (Action)(() =>
                        {
                            Config.BringToFront();
                            Config.TopMost = true;
                        })
                        );
                }

                return true;    // indicate that you handled this keystroke            
            }
            else if (keyData == Keys.F3)
            {
                //MessageBox.Show("You pressed the F3 key");

                Form Config = Application.OpenForms["FrmConfig_SyncInterval"];
                if (Config == null)
                {
                    System.Threading.Thread ParallelConfig = new System.Threading.Thread(ShowSyncInterval);
                    ParallelConfig.Start();
                }
                else
                {
                    Config.BeginInvoke(
                        (Action)(() =>
                        {
                            Config.BringToFront();
                            Config.TopMost = true;
                        })
                        );
                }

                return true;    // indicate that you handled this keystroke                      
            }
            else if (keyData == Keys.F5)
            {
                //MessageBox.Show("You pressed the F5 key");

                Form Config = Application.OpenForms["FrmConfig_Updates"];
                if (Config == null)
                {
                    System.Threading.Thread ParallelConfig = new System.Threading.Thread(ShowUpdateConfig);
                    ParallelConfig.Start();
                }
                else
                {
                    Config.BeginInvoke(
                        (Action)(() =>
                        {
                            Config.BringToFront();
                            Config.TopMost = true;                                                        
                        })
                        );
                }

                return true;    // indicate that you handled this keystroke                      
            }
            else if (keyData == Keys.F12)
            {
                //DialogResult Resp = MessageBox.Show("Are you sure you wish to close the application?", "Stellar ADS360", MessageBoxButtons.OKCancel, MessageBoxIcon.Question, MessageBoxDefaultButton.Button2);                
                //if (Resp == System.Windows.Forms.DialogResult.OK)
                //{                    
                //    System.Environment.ExitCode = 0;
                //    System.Environment.Exit(System.Environment.ExitCode);
                //}

                FormInput Input = new FormInput();

                Input.StartPosition = FormStartPosition.Manual;
                Input.Location = new Point(
                    (Int32)( ((Double) this.Width / 2) - ((Double) Input.Width / 2) ),
                    (Int32)( ((Double) this.Height * 0.10) ) 
                );
                Input.TopMost = true;

                Input.Text = "Stellar ADS360";
                Input.ActionTrue = LanguageResources.GetText("FrmConfig.ConfirmDBSettingsChange"); Input.ButtonTrue.Text = Input.ActionTrue;
                Input.ActionFalse = LanguageResources.GetText("FrmConfig.Cancel"); Input.ButtonFalse.Text = Input.ActionFalse;
                Input.Message.Text = Functions.NewLine() + LanguageResources.GetText("Application.ExitQuestion");
                Input.Message.ScrollBars = ScrollBars.None;
                Input.Message.ReadOnly = true;

                Form frmShell = Application.OpenForms["frmShell"];
                if (frmShell != null)
                    Input.ShowDialog(frmShell);
                else
                    Input.ShowDialog();

                if (Input.Action.Equals(Input.ActionTrue))
                {
                    System.Environment.ExitCode = 0;
                    System.Environment.Exit(System.Environment.ExitCode);                    
                }                

                Input.Close();

            }

            // Call the base class
            return base.ProcessCmdKey(ref msg, keyData);
        }       

        private void ShowConfig()
        {
            //Form Config = Application.OpenForms["FrmConfig_Language"];
            //if (Config == null)
            //{
                //FrmConfig_Language.ApplicationInit = false;
                FrmConfig_Language Config = new FrmConfig_Language();
                Config.TopMost = true;
                Config.ShowDialog();                
                Functions.Sync();
            //}
            //else { Config.BeginInvoke(
            //(Action)(() =>
            //{
            //    Config.BringToFront();
            //})                
            //    ); }
        }

        private void ShowVolume()
        {
            FrmConfig_Volume Config = new FrmConfig_Volume();
            Config.TopMost = true;
            Config.ShowDialog();            
        }

        private void ShowSyncInterval()
        {
            FrmConfig_SyncInterval Config = new FrmConfig_SyncInterval();
            Config.TopMost = true;
            Config.ShowDialog();
        }

        private void ShowUpdateConfig()
        {
            FrmConfig_Updates Config = new FrmConfig_Updates();
            Config.TopMost = true;
            Config.ShowDialog();
        }

        private void runInteractionTime()
        {
            if ((inactivityThreshold > 0) && (myTemplate == 1))
            {
                Timer tmpUserActivity = ((Timer)this.components.Components.OfType<IComponent>().Where(ti => ti.GetType().FullName == "System.Windows.Forms.Timer")
                .ToList().Where(ti => ((Timer)ti).Tag != null).ToList().Find(ti => ((Timer)ti).Tag.ToString() == "tmpUserActivity"));

                Timer mySecuenceTimeElapsed = ((Timer)this.components.Components.OfType<IComponent>().Where(ti => ti.GetType().FullName == "System.Windows.Forms.Timer")
                .ToList().Where(ti => ((Timer)ti).Tag != null).ToList().Find(ti => ((Timer)ti).Tag.ToString() == "mySecuenceTimeElapsed"));

                mySecuenceTimeElapsed.Enabled = false;

                tmpUserActivity.Enabled = false;
                tmpUserActivity.Interval = (inactivityThreshold * 1000);
                tmpUserActivity.Enabled = true;                

                ((Timer)this.components.Components.OfType<IComponent>().Where(ti => ti.GetType().FullName == "System.Windows.Forms.Timer")
                .ToList().Where(ti => ((Timer)ti).Tag != null).ToList().Find(ti => ((Timer)ti).Tag.ToString() == "myScheduleTime")).Enabled = false;

                try { 

                    ((Timer)this.components.Components.OfType<IComponent>().Where(ti => ti.GetType().FullName == "System.Windows.Forms.Timer")
                    .ToList().Where(ti => ((Timer)ti).Tag != null).ToList().Find(ti => ((Timer)ti).Tag.ToString().Right(1) == "1")).Enabled = false;

                } catch { } // Will cause exception if it is a Linked Browsing Session. And besides, Interaction Time is not implemented for Multi-Secuence Campaigns.
            }
        }

        private void timeIsUp(object sender, EventArgs e)
        {
            int Secuence = int.Parse(((Timer)sender).Tag.ToString().Right(1));            
            int returnOrder = currentSecuenceOrder[Secuence];
            String nextAdId = new Functions().nextSecuenceAd(actualSchedule, Secuence, ref returnOrder);
            currentSecuenceOrder[Secuence] = returnOrder;

            if (nextAdId != "0")
            {
                ((Timer)sender).Enabled = false;
                showNextAd(Secuence, nextAdId);
                ((Timer)sender).Enabled = true;
            }
            else
            {
                ((Timer)sender).Enabled = false; 
            }

            lastLoadedSecuence = Secuence;

            //if ((this.Opacity == 0) && (Secuence == lastLoadedSecuence)) { 
               //FadeInForm(this, 10); 
            //}
        }

        private void scheduleTimeIsUp(object sender, EventArgs e)
        {            

            Timer myScheduleTime = ((Timer)this.components.Components.OfType<IComponent>().Where(ti => ti.GetType().FullName == "System.Windows.Forms.Timer")
                .ToList().Where(ti => ((Timer)ti).Tag != null).ToList().Find(ti => ((Timer)ti).Tag.ToString() == "myScheduleTime"));

            Timer myNextScheduleTimer = ((Timer)this.components.Components.OfType<IComponent>().Where(ti => ti.GetType().FullName == "System.Windows.Forms.Timer")
                 .ToList().Where(ti => ((Timer)ti).Tag != null).ToList().Find(ti => ((Timer)ti).Tag.ToString() == "myNextScheduleTimer"));

            myScheduleTime.Enabled = false;

            myNextScheduleTimer.Enabled = false;

            Form frmShell = Application.OpenForms["frmShell"];
            
            /*
            frm_Main frmPreLoad = (frm_Main) frmShell.OwnedForms.ToList().Find(frm => (frm.Name == ("frm_Main" + (numForm + 1).ToString())));

            if (frmPreLoad == null)
            {                
                prepareNextSchedule(myNextScheduleTimer, null);
                frmPreLoad = (frm_Main) frmShell.OwnedForms.ToList().Find(frm => (frm.Name == ("frm_Main" + (numForm + 1).ToString())));                
            }*/

            if (((frm_Main)frmShell.OwnedForms.ToList().Find(frm => (frm.Name == ("frm_Main" + (numForm + 1).ToString())))) == null)
            {
                prepareNextSchedule(myNextScheduleTimer, null);
            }
            
            //int nextEffect = 0;

            //if (!CampaignNotFound) nextEffect = new Random().Next(5);            

            //switch (nextEffect)
            //{
            //    case 0:
            //        this.Visible = false;
            //        frmPreLoad.EffectDescription = "None";
            //        break;
            //    case 1:
            //        slideOutLeft(EffectsMethod.Steps, 7, this);
            //        frmPreLoad.EffectDescription = "None"; //SlideLeft
            //        break;
            //    case 2:
            //        slideOutRight(EffectsMethod.Percent, 11, this);
            //        frmPreLoad.EffectDescription = "None";
            //        break;
            //    case 3: 
            //        slideOutDown(EffectsMethod.Percent, 8, this);
            //        frmPreLoad.EffectDescription = "None";
            //        break;
            //    case 4:
            //        slideOutUp(EffectsMethod.Percent, 4, this);
            //        frmPreLoad.EffectDescription = "None";
            //        break;
            //}

            //slideOutLeft(EffectsMethod.Steps, 7, this);
            //slideOutLeft(EffectsMethod.Percent, 14, this);
            //slideOutRight(EffectsMethod.Percent, 11, this);

            //Form OwnedFrm = this.OwnedForms[0];

            //this.RemoveOwnedForm(OwnedFrm);

            //frmPreLoad.AddOwnedForm(OwnedFrm); 

            //frmPreLoad.Opacity = 0.01;

            MyConfig.Default.LastCampaignDisplayed = currentPlannedSchedule;
            MyConfig.Default.Save();

            TimeSpan Diff = DateTime.Now - Program.StartupTime; Boolean Restart = false;

            //if (Diff.TotalMinutes > 120) 
            if (Diff > (TimeSpan.Parse(MyConfig.Default.ResetTimeSpan)))
                Restart = true;

            if (!(Restart || Program.RestartRequired))
            { 
                //frmPreLoad.startSchedule();
                ((frm_Main)frmShell.OwnedForms.ToList().Find(frm => (frm.Name == ("frm_Main" + (numForm + 1).ToString())))).startSchedule();
            } else
            {
                frmShell.BringToFront();
            }

            //Form AnnoyingForm = Application.OpenForms["AnnoyingForm"];
            //if (AnnoyingForm != null)
            //{
            //    frmPreLoad.BeginInvoke((Action)(() => { frmPreLoad.AddOwnedForm(AnnoyingForm); }), null);
            //}

            Control TmpCtl = null;            
            
            if (this.tableLayoutPanel1.Controls.Find("myWeb", false).Length == 1) // is This a Browsing Session?
            {
                // Terminate Browsing Session...

                myScheduleTime.Dispose();

                myNextScheduleTimer.Dispose();                

                WebBrowser Web = (WebBrowser) this.tableLayoutPanel1.Controls.Find("myWeb", false)[0];

                this.tableLayoutPanel1.Controls.Remove(Web);

                this.tableLayoutPanel1.Dispose(); this.tableLayoutPanel1 = null;

                Web.NewWindow -= webNewWindow;
                Web.DocumentCompleted -= docReady;

                HtmlDocument Doc = Web.Document;

                if (Doc != null)
                {
                    Doc.Click -= webClick;
                    Doc.MouseDown -= webMouseDown;
                    Doc.MouseUp -= webMouseUp;
                    HtmlElement ActiveElement = Doc.ActiveElement;
                    ActiveElement.KeyPress -= webInput;

                    Marshal.FinalReleaseComObject(ActiveElement.DomElement);
                    //Object Body = Doc.Body.DomElement;
                    //Marshal.FinalReleaseComObject(Body); 
                    //Test Later...
                    Marshal.FinalReleaseComObject(Doc.DomDocument);
                }

                PlayMedia.Clear(); PlayMedia = null;
                PlayList.Clear(); PlayList = null;
                PlaySettings.Clear(); PlaySettings = null;
                PlayControls.Clear(); PlayControls = null;

                if (this.components != null) this.components.Dispose();

                if (frmShell != null) frmShell.RemoveOwnedForm(this);

                this.Close();
                this.Dispose(true);

                System.Runtime.GCSettings.LargeObjectHeapCompactionMode = System.Runtime.GCLargeObjectHeapCompactionMode.CompactOnce;

                GC.Collect();
                GC.WaitForPendingFinalizers();

                return;

            }

            // Otherwise it is a Regular Campaign, so lets try to release ALL resources.

            if (!this.CampaignNotFound) Variables.CurrentAppSession_FinishedCampaigns += 1;

            if (MyConfig.Default.TotalCampaignsFinished + 1 >= Int64.MaxValue)
                MyConfig.Default.TotalCampaignsFinished = 0;

            MyConfig.Default.TotalCampaignsFinished += 1;
            MyConfig.Default.Save();

            for (int i = 1; i <= currentAdCount; i++)
            {
                ((Timer)this.components.Components.OfType<IComponent>().Where(ti => ti.GetType().FullName == "System.Windows.Forms.Timer")
                .ToList().Where(ti => ((Timer)ti).Tag != null).ToList().Find(ti => "myTime" + ((Timer)ti).Tag.ToString().Right(1) == "myTime" + i.ToString())
                ).Enabled = false;

                ((Timer)this.components.Components.OfType<IComponent>().Where(ti => ti.GetType().FullName == "System.Windows.Forms.Timer")
                .ToList().Where(ti => ((Timer)ti).Tag != null).ToList().Find(ti => "myTime" + ((Timer)ti).Tag.ToString().Right(1) == "myTime" + i.ToString())
                ).Dispose();

                TmpCtl = getActiveControl(i);

                switch (TmpCtl.GetType().Name)
                {
                    case "AxWindowsMediaPlayer":
                        {                            
                            //((AxWMPLib.AxWindowsMediaPlayer)TmpCtl).currentMedia = null;
                            //((AxWMPLib.AxWindowsMediaPlayer)TmpCtl).mediaCollection.getAll().clear();                                                                                    
                            

                            //((AxWMPLib.AxWindowsMediaPlayer)TmpCtl).currentMedia = null;
                            //((AxWMPLib.AxWindowsMediaPlayer)TmpCtl).playlistCollection.remove(((AxWMPLib.AxWindowsMediaPlayer)TmpCtl).currentPlaylist);
                            //((AxWMPLib.AxWindowsMediaPlayer)TmpCtl).currentPlaylist = null;
                            //System.Runtime.InteropServices.Marshal.ReleaseComObject(((AxWMPLib.AxWindowsMediaPlayer)TmpCtl));

                            PlayControls[i - 1].stop();
                            PlayList[i - 1].removeItem(PlayMedia[i - 1]);                            
                            PlayList[i - 1].clear();
                            
                            (TmpCtl as AxWMPLib.AxWindowsMediaPlayer).Tag = null;
                            (TmpCtl as AxWMPLib.AxWindowsMediaPlayer).ErrorEvent -= playerError;
                            (TmpCtl as AxWMPLib.AxWindowsMediaPlayer).ClickEvent -= videoClick;
                            (TmpCtl as AxWMPLib.AxWindowsMediaPlayer).MouseUpEvent -= videoMouseUp;
                            (TmpCtl as AxWMPLib.AxWindowsMediaPlayer).MouseDownEvent -= videoMouseDown;

                            Marshal.FinalReleaseComObject(PlayMedia[i - 1]);
                            Marshal.FinalReleaseComObject(PlaySettings[i - 1]);
                            Marshal.FinalReleaseComObject(PlayList[i - 1]);
                            Marshal.FinalReleaseComObject(PlayControls[i - 1]);
                            // Marshal.FinalReleaseComObject(PlayCOMState[i]);

                            (TmpCtl as AxWMPLib.AxWindowsMediaPlayer).URL = null;
                            (TmpCtl as AxWMPLib.AxWindowsMediaPlayer).close(); 

                            break;
                        }
                    case "PictureBox":
                        {
                            if ((TmpCtl as PictureBox).Image != null)
                                (TmpCtl as PictureBox).Image.Dispose();
                            (TmpCtl as PictureBox).Image = null;
                            break;
                        }
                    case "WebBrowser":
                        {
                            try
                            {
                                (TmpCtl as WebBrowser).NewWindow -= webNewWindow;
                                (TmpCtl as WebBrowser).DocumentCompleted -= docReady;

                                HtmlDocument Doc = (TmpCtl as WebBrowser).Document;

                                if (Doc != null)
                                {                                    
                                    Doc.Click -= webClick;
                                    Doc.MouseDown -= webMouseDown;
                                    Doc.MouseUp -= webMouseUp;
                                    HtmlElement ActiveElement = Doc.ActiveElement;
                                    
                                    ActiveElement.KeyPress -= webInput;

                                    Marshal.FinalReleaseComObject(ActiveElement.DomElement);
                                    //Object Body = Doc.Body.DomElement;
                                    //Marshal.FinalReleaseComObject(Body); 
                                    //Test Later...
                                    Marshal.FinalReleaseComObject(Doc.DomDocument);
                                }

                                /*
                                ((WebBrowser)TmpCtl).Stop();
                                ((WebBrowser)TmpCtl).Url = null;
                                */                                
                            }
                            catch (Exception) { }
                            break;
                        }

                }

                if (TmpCtl != null) 
                { tableLayoutPanel1.Controls.Remove(TmpCtl); TmpCtl.Dispose(); } //TmpCtl = null; }

            }

            myScheduleTime.Dispose();

            myNextScheduleTimer.Dispose();

            ((Timer)this.components.Components.OfType<IComponent>().Where(ti => ti.GetType().FullName == "System.Windows.Forms.Timer")
            .ToList().Where(ti => ((Timer)ti).Tag != null).ToList().Find(ti => ((Timer)ti).Tag.ToString() == "tmpUserActivity")).Dispose();

            ((Timer)this.components.Components.OfType<IComponent>().Where(ti => ti.GetType().FullName == "System.Windows.Forms.Timer")
            .ToList().Where(ti => ((Timer)ti).Tag != null).ToList().Find(ti => ((Timer)ti).Tag.ToString() == "mySecuenceTimeElapsed")).Dispose();

            this.tableLayoutPanel1.Dispose(); this.tableLayoutPanel1 = null;            

            while (this.Controls.Count > 0) { 

                foreach (Control Cnt in this.Controls)
                {
                    TmpCtl = Cnt;

                    if (CampaignNotFound)
                    {
                        this.Controls.Remove(TmpCtl);
                        TmpCtl.Dispose();                        
                        continue;
                    }                                        

                    int Secuence = Int32.Parse(TmpCtl.Name.Right(1));

                    switch (TmpCtl.GetType().Name)
                    {
                        case "AxWindowsMediaPlayer":
                            {   /*
                                ((AxWMPLib.AxWindowsMediaPlayer)TmpCtl).Ctlcontrols.stop();
                                ((AxWMPLib.AxWindowsMediaPlayer)TmpCtl).mediaCollection.getAll().clear();
                                //((AxWMPLib.AxWindowsMediaPlayer)TmpCtl).currentMedia = null;
                                ((AxWMPLib.AxWindowsMediaPlayer)TmpCtl).currentPlaylist.clear();
                                //((AxWMPLib.AxWindowsMediaPlayer)TmpCtl).currentPlaylist = null;
                                //System.Runtime.InteropServices.Marshal.ReleaseComObject(((AxWMPLib.AxWindowsMediaPlayer)TmpCtl));
                                */

                                (TmpCtl as AxWMPLib.AxWindowsMediaPlayer).Tag = null;
                                (TmpCtl as AxWMPLib.AxWindowsMediaPlayer).ErrorEvent -= playerError;
                                (TmpCtl as AxWMPLib.AxWindowsMediaPlayer).ClickEvent -= videoClick;
                                (TmpCtl as AxWMPLib.AxWindowsMediaPlayer).MouseUpEvent -= videoMouseUp;
                                (TmpCtl as AxWMPLib.AxWindowsMediaPlayer).MouseDownEvent -= videoMouseDown;

                                if (PlayControls[Secuence - 1] != null)
                                {

                                    PlayControls[Secuence - 1].stop();
                                    PlayList[Secuence - 1].removeItem(PlayMedia[Secuence - 1]);
                                    PlayList[Secuence - 1].clear();

                                    Marshal.FinalReleaseComObject(PlayMedia[Secuence - 1]);
                                    Marshal.FinalReleaseComObject(PlaySettings[Secuence - 1]);
                                    Marshal.FinalReleaseComObject(PlayList[Secuence - 1]);
                                    Marshal.FinalReleaseComObject(PlayControls[Secuence - 1]);
                                    //Marshal.FinalReleaseComObject(PlayCOMState[Secuence - 1]);

                                }

                                (TmpCtl as AxWMPLib.AxWindowsMediaPlayer).URL = null;
                                (TmpCtl as AxWMPLib.AxWindowsMediaPlayer).close();                                                              

                                break;
                            }
                        case "PictureBox":
                            {
                                if ((TmpCtl as PictureBox).Image != null)
                                    (TmpCtl as PictureBox).Image.Dispose();
                                ((PictureBox)TmpCtl).Image = null;
                                break;
                            }
                        case "WebBrowser":
                            {
                                (TmpCtl as WebBrowser).NewWindow -= webNewWindow;
                                (TmpCtl as WebBrowser).DocumentCompleted -= docReady;

                                HtmlDocument Doc = (TmpCtl as WebBrowser).Document;

                                if (Doc != null)
                                {
                                    Doc.Click -= webClick;
                                    Doc.MouseDown -= webMouseDown;
                                    Doc.MouseUp -= webMouseUp;
                                    HtmlElement ActiveElement = Doc.ActiveElement;
                                    ActiveElement.KeyPress -= webInput;

                                    Marshal.FinalReleaseComObject(ActiveElement.DomElement);
                                    //Object Body = Doc.Body.DomElement;
                                    //Marshal.FinalReleaseComObject(Body); 
                                    //Test Later...
                                    Marshal.FinalReleaseComObject(Doc.DomDocument);                                    
                                }

                                //((WebBrowser)TmpCtl).Stop();
                                //((WebBrowser)TmpCtl).Url = null;
                                break;
                            }
                    }

                    this.Controls.Remove(TmpCtl);
                    TmpCtl.Dispose();
                    //TmpCtl = null;

                }

            }

            AdInfo TmpInfo;
            foreach (Object Tmp in currentAd.Values) { TmpInfo = (Tmp as AdInfo); if (TmpInfo != null) { TmpInfo.AdBaseData = null; TmpInfo.DisplaySettings = null; TmpInfo = null; } }            

            currentAd.Clear(); currentAd = null;
            currentSecuenceOrder.Clear(); currentSecuenceOrder = null;

            PlayMedia.Clear(); PlayMedia = null;
            PlayList.Clear(); PlayList = null;
            PlaySettings.Clear(); PlaySettings = null;
            PlayControls.Clear(); PlayControls = null;

            //Console.WriteLine(this.Visible); //this.Visible = false;
            //this.Location = new Point(-500, -500);

            if (this.components != null) this.components.Dispose();

            if (!firstInstance)
            {                
                if (frmShell != null) frmShell.RemoveOwnedForm(this);
                
                this.Close();
                this.Dispose(true);

                //foreach (Form frm in Application.OpenForms)
                //{
                    //Console.WriteLine(frm.Name);
                //}                
            }
            else
            {
                this.Visible = false;                
            }

            // Restart after a while to avoid Memory Failures / Slow Performance.

            if (Restart || Program.RestartRequired)
            {
                Functions.RestartApp();
            }

            System.Runtime.GCSettings.LargeObjectHeapCompactionMode = System.Runtime.GCLargeObjectHeapCompactionMode.CompactOnce;

            GC.Collect();
            GC.WaitForPendingFinalizers();

            // GC.Collect();

            //frmPreLoad.startSchedule();         
            
        }

        private void prepareNextSchedule(object sender, EventArgs e)
        {
            //Form frmShell = Application.OpenForms["frmShell"];
            //frm_Main frmPreLoad = (frm_Main) frmShell.OwnedForms.ToList().Find(frm => (frm.Name == ("frm_Main" + (numForm + 1).ToString())));            

            if (this.BrowseParams != null)
            {
                //frmPreLoad = 
                new frm_Main(this.currentPlannedSchedule, numForm, numDisplayedSchedules, NavigationParams.Copy(this.BrowseParams));
                this.BrowseParams = null;
            }
            else
            {   //frmPreLoad = 
                new frm_Main(this.currentPlannedSchedule, numForm, numDisplayedSchedules); }

            ((Timer)sender).Enabled = false;
        }

        private Control getActiveControl(int pSecuence)
        {
            Control ActiveCtrl = null;

            switch (myTemplate)
            {
                // FullScreen
                case 1:
                    {
                        ActiveCtrl = tableLayoutPanel1.GetControlFromPosition(0, 0);
                        break;
                    }
                // 3-Div
                case 2:
                case 5:
                case 6:
                case 7:
                    {
                        switch (pSecuence)
                        {
                            case 1:
                                {
                                    ActiveCtrl = tableLayoutPanel1.GetControlFromPosition(0, 0);
                                    break;
                                }
                            case 2:
                                {
                                    ActiveCtrl = tableLayoutPanel1.GetControlFromPosition(1, 0);
                                    break;
                                }
                            case 3:
                                {
                                    ActiveCtrl = tableLayoutPanel1.GetControlFromPosition(1, 1);
                                    break;
                                }
                        }
                        break;
                    }
                // Vertical Split: 50%, 50%
                case 3:
                    {
                        switch (pSecuence)
                        {
                            case 1:
                                {
                                    ActiveCtrl = tableLayoutPanel1.GetControlFromPosition(0, 0);
                                    break;
                                }
                            case 2:
                                {
                                    ActiveCtrl = tableLayoutPanel1.GetControlFromPosition(1, 0);
                                    break;
                                }
                        }
                        break;
                    }
                // Horizontal Split: 50%, 50%
                case 4:
                    {
                        switch (pSecuence)
                        {
                            case 1:
                                {
                                    ActiveCtrl = tableLayoutPanel1.GetControlFromPosition(0, 0);
                                    break;
                                }
                            case 2:
                                {
                                    ActiveCtrl = tableLayoutPanel1.GetControlFromPosition(0, 1);
                                    break;
                                }
                        }
                        break;
                    }
                // 3-Div
                case 8:
                    {
                        switch (pSecuence)
                        {
                            case 1:
                                {
                                    ActiveCtrl = tableLayoutPanel1.GetControlFromPosition(0, 0);
                                    break;
                                }
                            case 2:
                                {
                                    ActiveCtrl = tableLayoutPanel1.GetControlFromPosition(1, 0);
                                    break;
                                }
                            case 3:
                                {
                                    ActiveCtrl = tableLayoutPanel1.GetControlFromPosition(0, 1);
                                    break;
                                }
                        }
                        break;
                    }
            }

            return ActiveCtrl;
        }

        private void ApplyBorder(Control FrmCtrl, int pSecuence, int pWidth)
        {
            int WidthPerControl = Convert.ToInt32(pWidth / 2);

            switch (myTemplate)
            {
                // FullScreen
                /*  ____________
                *  |            |
                *  |____________|
                */
                case 1:
                    {
                        break; // No Inner Border.
                    }
                // 3-Div: 25%, 25%, 50%;
                /*  ____________
                *  |__1__|__2___|
                *  |_____3______|
                */
                case 2:
                    {

                        switch (pSecuence)
                        {
                            case 1:
                                {
                                    FrmCtrl.Margin = new Padding(0, 0, WidthPerControl, WidthPerControl);
                                    break;
                                }
                            case 2:
                                {
                                    FrmCtrl.Margin = new Padding(WidthPerControl, 0, 0, WidthPerControl);
                                    break;
                                }
                            case 3:
                                {
                                    FrmCtrl.Margin = new Padding(0, WidthPerControl, 0, 0);
                                    break;
                                }
                        }

                        break;

                    }
                // Vertical Split: 50%, 50%
                /*  ____________
                *  |  1  |  2   |
                *  |_____|______|
                */
                case 3:
                    {

                        switch (pSecuence)
                        {
                            case 1:
                                {
                                    FrmCtrl.Margin = new Padding(0, 0, WidthPerControl, 0);
                                    break;
                                }
                            case 2:
                                {
                                    FrmCtrl.Margin = new Padding(WidthPerControl, 0, 0, 0);
                                    break;
                                }
                        }

                        break;

                    }
                // Horizontal Split: 50%, 50%
                /*  ____________
                *  |_____1______|
                *  |_____2______|
                */
                case 4:
                    {

                        switch (pSecuence)
                        {
                            case 1:
                                {
                                    FrmCtrl.Margin = new Padding(0, 0, 0, WidthPerControl);
                                    break;
                                }
                            case 2:
                                {
                                    FrmCtrl.Margin = new Padding(0, WidthPerControl, 0, 0);
                                    break;
                                }
                        }

                        break;

                    }
                // Template 5: 20x70(%), 80x70(%), 100x30(%)
                /*  ____________
                *  | 1|         |
                *  |__|__2______|
                *  |_____3______|
                */
                case 5:
                    {

                        switch (pSecuence)
                        {
                            case 1:
                                {
                                    FrmCtrl.Margin = new Padding(0, 0, WidthPerControl, WidthPerControl);
                                    break;
                                }
                            case 2:
                                {
                                    FrmCtrl.Margin = new Padding(WidthPerControl, 0, 0, WidthPerControl);
                                    break;
                                }
                            case 3:
                                {
                                    FrmCtrl.Margin = new Padding(0, WidthPerControl, 0, 0);
                                    break;
                                }
                        }

                        break;

                    }
                // Template 6: 80x70(%), 20x70(%), 100x30(%)
                /*  ____________
                *  |         |2 |
                *  |_____1___|__|
                *  |_____3______|
                */
                case 6:
                    {

                        switch (pSecuence)
                        {
                            case 1:
                                {
                                    FrmCtrl.Margin = new Padding(0, 0, WidthPerControl, WidthPerControl);
                                    break;
                                }
                            case 2:
                                {
                                    FrmCtrl.Margin = new Padding(WidthPerControl, 0, 0, WidthPerControl);
                                    break;
                                }
                            case 3:
                                {
                                    FrmCtrl.Margin = new Padding(0, WidthPerControl, 0, 0);
                                    break;
                                }
                        }

                        break;

                    }
                // Template 7: 20x100(%), 80x70(%), 70x30(%)
                /*  ____________
                *  | 1|         |
                *  |  |__2______|
                *  |__|__3______|
                */
                case 7:
                    {

                        switch (pSecuence)
                        {
                            case 1:
                                {
                                    FrmCtrl.Margin = new Padding(0, 0, WidthPerControl, 0);
                                    break;
                                }
                            case 2:
                                {
                                    FrmCtrl.Margin = new Padding(WidthPerControl, 0, 0, WidthPerControl);
                                    break;
                                }
                            case 3:
                                {
                                    FrmCtrl.Margin = new Padding(WidthPerControl, WidthPerControl, 0, 0);
                                    break;
                                }
                        }

                        break;

                    }
                // Template 8: 80x70(%), 20x100(%), 70x30(%)
                /*  ____________
                *  |         |2 |
                *  |_____1___|  |
                *  |_____3___|__|
                */
                case 8:
                    {

                        switch (pSecuence)
                        {
                            case 1:
                                {
                                    FrmCtrl.Margin = new Padding(0, 0, WidthPerControl, WidthPerControl);
                                    break;
                                }
                            case 2:
                                {
                                    FrmCtrl.Margin = new Padding(WidthPerControl, 0, 0, 0);
                                    break;
                                }
                            case 3:
                                {
                                    FrmCtrl.Margin = new Padding(0, WidthPerControl, WidthPerControl, 0);
                                    break;
                                }
                        }

                        break;

                    }                 
            }
        }

        private void ApplyBorder(Control FrmCtrl, int pSecuence, int pWidth, Color BorderColor)
        {
            int WidthPerControl = Convert.ToInt32(pWidth / 2);            

            switch (myTemplate)
            {
                // FullScreen
                /*  ____________
                *  |            |
                *  |____________|
                */
                case 1:
                    {
                        break; // No Inner Border.
                    }
                // 3-Div: 25%, 25%, 50%;
                /*  ____________
                *  |__1__|__2___|
                *  |_____3______|
                */
                case 2:
                    {

                        //CalledByCode = true;

                        switch (pSecuence)
                        {
                            case 1:
                                {
                                    FrmCtrl.Margin = new Padding(0, 0, WidthPerControl, WidthPerControl);                                    

                                    break;
                                }
                            case 2:
                                {
                                    FrmCtrl.Margin = new Padding(WidthPerControl, 0, 0, WidthPerControl);

                                    break;
                                }
                            case 3:
                                {
                                    FrmCtrl.Margin = new Padding(0, WidthPerControl, 0, 0);

                                    break;
                                }
                        }

                        //tableLayoutPanel1.Invalidate(true);

                        break;

                    }
                // Vertical Split: 50%, 50%
                /*  ____________
                *  |  1  |  2   |
                *  |_____|______|
                */
                case 3:
                    {

                        switch (pSecuence)
                        {
                            case 1:
                                {
                                    FrmCtrl.Margin = new Padding(0, 0, WidthPerControl, 0);
                                    break;
                                }
                            case 2:
                                {
                                    FrmCtrl.Margin = new Padding(WidthPerControl, 0, 0, 0);
                                    break;
                                }
                        }

                        break;

                    }
                // Horizontal Split: 50%, 50%
                /*  ____________
                *  |_____1______|
                *  |_____2______|
                */
                case 4:
                    {

                        switch (pSecuence)
                        {
                            case 1:
                                {
                                    FrmCtrl.Margin = new Padding(0, 0, 0, WidthPerControl);
                                    break;
                                }
                            case 2:
                                {
                                    FrmCtrl.Margin = new Padding(0, WidthPerControl, 0, 0);
                                    break;
                                }
                        }

                        break;

                    }
                // Template 5: 20x70(%), 80x70(%), 100x30(%)
                /*  ____________
                *  | 1|         |
                *  |__|__2______|
                *  |_____3______|
                */
                case 5:
                    {

                        switch (pSecuence)
                        {
                            case 1:
                                {
                                    FrmCtrl.Margin = new Padding(0, 0, WidthPerControl, WidthPerControl);
                                    break;
                                }
                            case 2:
                                {
                                    FrmCtrl.Margin = new Padding(WidthPerControl, 0, 0, WidthPerControl);
                                    break;
                                }
                            case 3:
                                {
                                    FrmCtrl.Margin = new Padding(0, WidthPerControl, 0, 0);
                                    break;
                                }
                        }

                        break;

                    }
                // Template 6: 80x70(%), 20x70(%), 100x30(%)
                /*  ____________
                *  |         |2 |
                *  |_____1___|__|
                *  |_____3______|
                */
                case 6:
                    {

                        switch (pSecuence)
                        {
                            case 1:
                                {
                                    FrmCtrl.Margin = new Padding(0, 0, WidthPerControl, WidthPerControl);
                                    break;
                                }
                            case 2:
                                {
                                    FrmCtrl.Margin = new Padding(WidthPerControl, 0, 0, WidthPerControl);
                                    break;
                                }
                            case 3:
                                {
                                    FrmCtrl.Margin = new Padding(0, WidthPerControl, 0, 0);
                                    break;
                                }
                        }

                        break;

                    }
                // Template 7: 20x100(%), 80x70(%), 70x30(%)
                /*  ____________
                *  | 1|         |
                *  |  |__2______|
                *  |__|__3______|
                */
                case 7:
                    {

                        switch (pSecuence)
                        {
                            case 1:
                                {
                                    FrmCtrl.Margin = new Padding(0, 0, WidthPerControl, 0);
                                    break;
                                }
                            case 2:
                                {
                                    FrmCtrl.Margin = new Padding(WidthPerControl, 0, 0, WidthPerControl);
                                    break;
                                }
                            case 3:
                                {
                                    FrmCtrl.Margin = new Padding(WidthPerControl, WidthPerControl, 0, 0);
                                    break;
                                }
                        }

                        break;

                    }
                // Template 8: 80x70(%), 20x100(%), 70x30(%)
                /*  ____________
                *  |         |2 |
                *  |_____1___|  |
                *  |_____3___|__|
                */
                case 8:
                    {

                        switch (pSecuence)
                        {
                            case 1:
                                {
                                    FrmCtrl.Margin = new Padding(0, 0, WidthPerControl, WidthPerControl);
                                    break;
                                }
                            case 2:
                                {
                                    FrmCtrl.Margin = new Padding(WidthPerControl, 0, 0, 0);
                                    break;
                                }
                            case 3:
                                {
                                    FrmCtrl.Margin = new Padding(0, WidthPerControl, WidthPerControl, 0);
                                    break;
                                }
                        }

                        break;

                    }
            }
        }

        private void ApplyBorder(Control FrmCtrl, int pWidth)
        {
            int WidthPerControl = Convert.ToInt32(pWidth / 2);

            switch (myTemplate)
            {
                // FullScreen
                /*  ____________
                *  |            |
                *  |____________|
                */
                case 1:
                    {
                        break; // No Inner Border.
                    }
                // 3-Div: 25%, 25%, 50%;
                /*  ____________
                *  |__1__|__2___|
                *  |_____3______|
                */
                case 2:
                    {
                        (FrmCtrl as TableLayoutPanel).CellPaint += (Sender, Args) => {

                            //if (CalledByCode) { 

                                if (Args.Row == 0 && Args.Column == 0) {
                                    ControlPaint.DrawBorder(Args.Graphics, Args.CellBounds,
                                    Color.Yellow, 0, ButtonBorderStyle.Solid,
                                    Color.Yellow, 0, ButtonBorderStyle.Solid,
                                    Color.Yellow, WidthPerControl, ButtonBorderStyle.Solid,
                                    Color.Yellow, WidthPerControl, ButtonBorderStyle.Solid); 
                                }

                                if (Args.Row == 0 && Args.Column == 1)
                                {                                
                                    ControlPaint.DrawBorder(Args.Graphics, Args.CellBounds,
                                    Color.Yellow, WidthPerControl, ButtonBorderStyle.Solid,
                                    Color.Yellow, 0, ButtonBorderStyle.Solid,
                                    Color.Yellow, 0, ButtonBorderStyle.Solid,
                                    Color.Yellow, WidthPerControl, ButtonBorderStyle.Solid);
                                }

                                if (Args.Row == 1 && Args.Column == 0)
                                {
                                    ControlPaint.DrawBorder(Args.Graphics, Args.CellBounds,
                                    Color.Yellow, 0, ButtonBorderStyle.Solid,
                                    Color.Yellow, WidthPerControl, ButtonBorderStyle.Solid,
                                    Color.Yellow, 0, ButtonBorderStyle.Solid,
                                    Color.Yellow, 0, ButtonBorderStyle.Solid);
                                }

                                if (Args.Row == 1 && Args.Column == 1)
                                {
                                    ControlPaint.DrawBorder(Args.Graphics, Args.CellBounds,
                                    Color.Yellow, 0, ButtonBorderStyle.Solid,
                                    Color.Yellow, WidthPerControl, ButtonBorderStyle.Solid,
                                    Color.Yellow, 0, ButtonBorderStyle.Solid,
                                    Color.Yellow, 0, ButtonBorderStyle.Solid);
                                }

                                //CalledByCode = false;

                            //}
                            //else
                            //{
                            //    Console.WriteLine(Args.CellBounds);
                            //}

                        };

                        break;

                    }
                // Vertical Split: 50%, 50%
                /*  ____________
                *  |  1  |  2   |
                *  |_____|______|
                */
                case 3:
                    {



                        break;

                    }
                // Horizontal Split: 50%, 50%
                /*  ____________
                *  |_____1______|
                *  |_____2______|
                */
                case 4:
                    {



                        break;

                    }
                // Template 5: 20x70(%), 80x70(%), 100x30(%)
                /*  ____________
                *  | 1|         |
                *  |__|__2______|
                *  |_____3______|
                */
                case 5:
                    {



                        break;

                    }
                // Template 6: 80x70(%), 20x70(%), 100x30(%)
                /*  ____________
                *  |         |2 |
                *  |_____1___|__|
                *  |_____3______|
                */
                case 6:
                    {



                        break;

                    }
                // Template 7: 20x100(%), 80x70(%), 70x30(%)
                /*  ____________
                *  | 1|         |
                *  |  |__2______|
                *  |__|__3______|
                */
                case 7:
                    {



                        break;

                    }
                // Template 8: 80x70(%), 20x100(%), 70x30(%)
                /*  ____________
                *  |         |2 |
                *  |_____1___|  |
                *  |_____3___|__|
                */
                case 8:
                    {



                        break;

                    }
            }
        }

        private Boolean showNextAd(int pSecuence, String pNextAdId)
        {

            Boolean Succeed = false; Boolean controlException = false; Boolean AdInfoNotFound = false; String comment = String.Empty;
            String adType = String.Empty; CurrentAdSettings AdSettings = null;

            Ads nextAd = new Ads(pNextAdId); 
            
            if (nextAd.ID.isUndefined()) { AdInfoNotFound = true; goto ErrorHandling; }

            AdSettings = CurrentAdSettings.FromParams(nextAd.ID, pSecuence, currentSecuenceOrder[pSecuence], actualSchedule);

            if (AdSettings == null) { AdInfoNotFound = true; goto ErrorHandling; }

            currentAd[pSecuence] = new AdInfo() { AdBaseData = nextAd, DisplaySettings = AdSettings };            

            Control ActiveCtrl = null; Control FrmCtrl = null;

            ActiveCtrl = getActiveControl(pSecuence);

            // Console.WriteLine(ActiveCtrl.GetType().Name);

            switch (ActiveCtrl.GetType().Name)
            {
                case "AxWindowsMediaPlayer":
                    {
                        //((AxWMPLib.AxWindowsMediaPlayer)ActiveCtrl).Ctlcontrols.stop();                            
                        PlayControls[pSecuence - 1].stop();
                        break;
                    }
                /*case "WebBrowser":
                    {
                        ((WebBrowser)ActiveCtrl).Stop();
                        break;
                    }
                */
            }

            tableLayoutPanel1.Controls.Remove(ActiveCtrl);
            ActiveCtrl.Visible = false;

            this.Controls.Add(ActiveCtrl);

            try
            {

                if (nextAd.isValid())
                {
                    switch (nextAd.Type)
                    {
                        case 0:
                        {
                            adType = "Image";

                            FrmCtrl = this.Controls.Find("MyPics" + pSecuence, true)[0];

                            ((PictureBox)FrmCtrl).Image = Image.FromFile(
                                Variables.myProgramDataFolder + (nextAd.AdvertiserID.isUndefined() ? String.Empty : nextAd.AdvertiserID + @"\") + (pNextAdId.isUndefined() ? String.Empty : pNextAdId + @"\") + nextAd.FileName
                                //Variables.myProgramDataFolder + nextAd.FileName
                            );

                            break;
                        }
                        case 1:
                        {
                            adType = "Video";

                            FrmCtrl = this.Controls.Find("MyWmp" + pSecuence, true)[0];

                            //WMPLib.IWMPMedia media;

                            //((AxWMPLib.AxWindowsMediaPlayer)FrmCtrl).CreateControl();                                
                            /*
                             * 
                            if (((AxWMPLib.AxWindowsMediaPlayer)FrmCtrl).currentPlaylist.count != 0)
                            {
                                ((AxWMPLib.AxWindowsMediaPlayer)FrmCtrl).currentPlaylist.clear();
                            }

                            //media = ((AxWMPLib.AxWindowsMediaPlayer)FrmCtrl).newMedia(
                            //Variables.myProgramDataFolder + pNextAdId + @"\" + nextAd.fileName);

                            // ((AxWMPLib.AxWindowsMediaPlayer)FrmCtrl).settings.enableErrorDialogs = false;                                                               

                            ((AxWMPLib.AxWindowsMediaPlayer)FrmCtrl).enableContextMenu = false;
                            //((AxWMPLib.AxWindowsMediaPlayer)FrmCtrl).enableContextMenu = true;                            
                            ((AxWMPLib.AxWindowsMediaPlayer)FrmCtrl).Ctlenabled = false;
                            ((AxWMPLib.AxWindowsMediaPlayer)FrmCtrl).uiMode = "none";
                            ((AxWMPLib.AxWindowsMediaPlayer)FrmCtrl).stretchToFit = true;
                            ((AxWMPLib.AxWindowsMediaPlayer)FrmCtrl).settings.setMode("loop", true);

                            ((AxWMPLib.AxWindowsMediaPlayer)FrmCtrl).Tag = (
                            Variables.myProgramDataFolder + (nextAd.AdvertiserID.isUndefined() ? String.Empty : nextAd.AdvertiserID + @"\") + (pNextAdId.isUndefined() ? String.Empty : pNextAdId + @"\") + nextAd.FileName
                                //Variables.myProgramDataFolder + nextAd.FileName)                                    
                            );

                            if (!System.IO.File.Exists(((AxWMPLib.AxWindowsMediaPlayer)FrmCtrl).Tag.ToString()))
                            {
                                throw new System.IO.FileNotFoundException(((AxWMPLib.AxWindowsMediaPlayer)FrmCtrl).Tag.ToString());
                            }

                            ((AxWMPLib.AxWindowsMediaPlayer)FrmCtrl).currentPlaylist.appendItem(
                            ((AxWMPLib.AxWindowsMediaPlayer)FrmCtrl).newMedia(((AxWMPLib.AxWindowsMediaPlayer)FrmCtrl).Tag.ToString())); // media                                

                            ((AxWMPLib.AxWindowsMediaPlayer)FrmCtrl).Ctlcontrols.play();

                            // Console.WriteLine(((AxWMPLib.AxWindowsMediaPlayer)FrmCtrl).status.ToString());
                            
                             */                            

                            if (PlayList[pSecuence - 1] == null){
                                PlayList[pSecuence - 1] = (FrmCtrl as AxWMPLib.AxWindowsMediaPlayer).currentPlaylist;
                                PlaySettings[pSecuence - 1] = (FrmCtrl as AxWMPLib.AxWindowsMediaPlayer).settings;
                                PlaySettings[pSecuence - 1].enableErrorDialogs = true;
                                PlayControls[pSecuence - 1] = (FrmCtrl as AxWMPLib.AxWindowsMediaPlayer).Ctlcontrols;
                            }

                            if (PlayList[pSecuence - 1].count != 0)
                            {
                                PlayList[pSecuence - 1].removeItem(PlayMedia[pSecuence - 1]);
                                Marshal.FinalReleaseComObject(PlayMedia[pSecuence - 1]);
                                PlayMedia[pSecuence - 1] = null;
                                PlayList[pSecuence - 1].clear();
                            }

                            //media = ((AxWMPLib.AxWindowsMediaPlayer)FrmCtrl).newMedia(
                            //Variables.myProgramDataFolder + pNextAdId + @"\" + nextAd.fileName);

                            // ((AxWMPLib.AxWindowsMediaPlayer)FrmCtrl).settings.enableErrorDialogs = false;                                                               
                            
                            (FrmCtrl as AxWMPLib.AxWindowsMediaPlayer).enableContextMenu = false;
                            //(FrmCtrl as AxWMPLib.AxWindowsMediaPlayer).enableContextMenu = true;                            
                            (FrmCtrl as AxWMPLib.AxWindowsMediaPlayer).Ctlenabled = false;
                            (FrmCtrl as AxWMPLib.AxWindowsMediaPlayer).uiMode = "none";
                            (FrmCtrl as AxWMPLib.AxWindowsMediaPlayer).stretchToFit = true;
                            PlaySettings[pSecuence - 1].setMode("loop", true);

                            (FrmCtrl as AxWMPLib.AxWindowsMediaPlayer).Tag = (
                            Variables.myProgramDataFolder + (nextAd.AdvertiserID.isUndefined() ? String.Empty : nextAd.AdvertiserID + @"\") + (pNextAdId.isUndefined() ? String.Empty : pNextAdId + @"\") + nextAd.FileName
                                //Variables.myProgramDataFolder + nextAd.FileName)                                    
                            );

                            if (!System.IO.File.Exists((FrmCtrl as AxWMPLib.AxWindowsMediaPlayer).Tag.ToString()))
                            {
                                throw new System.IO.FileNotFoundException((FrmCtrl as AxWMPLib.AxWindowsMediaPlayer).Tag.ToString());
                            }

                            PlayMedia[pSecuence - 1] = (FrmCtrl as AxWMPLib.AxWindowsMediaPlayer).newMedia(
                                (FrmCtrl as AxWMPLib.AxWindowsMediaPlayer).Tag.ToString()
                            );

                            PlayList[pSecuence - 1].appendItem(PlayMedia[pSecuence - 1]);

                            PlayControls[pSecuence - 1].play();                                                                                   

                            break;
                        }
                        case 2:
                        {
                            adType = "Url";

                            FrmCtrl = this.Controls.Find("MyWeb" + pSecuence, true)[0];

                            String FileNameWithoutExtension = nextAd.FileName.Substring(0, nextAd.FileName.Length - nextAd.Extension.Length);

                            // String JustToCheckPath = (Variables.myProgramDataFolder + pNextAdId + @"\" + FileNameWithoutExtension + "\\" + "index.html");

                            ((WebBrowser)FrmCtrl).Tag = (
                                (Variables.myProgramDataFolder + (nextAd.AdvertiserID.isUndefined() ? String.Empty : nextAd.AdvertiserID + @"\") + (pNextAdId.isUndefined() ? String.Empty : pNextAdId + @"\") + FileNameWithoutExtension + "\\" + "index.html"));
                            //(Variables.myProgramDataFolder + FileNameWithoutExtension + "\\" + "index.html"));

                            //if (!System.IO.File.Exists(((WebBrowser)FrmCtrl).Tag.ToString())) throw new System.IO.FileNotFoundException(((WebBrowser)FrmCtrl).Tag.ToString()); 
                            if (!System.IO.File.Exists(((WebBrowser)FrmCtrl).Tag.ToString()))
                            {
                                throw new ArgumentNullException(String.Empty, "Invalid or unable to display url: " +
                                "\"" + ((WebBrowser)FrmCtrl).Tag.ToString() + "\"");
                            }

                            ((WebBrowser)FrmCtrl).Url = new System.Uri(//"C:/13/index.html", System.UriKind.Absolute);                                            
                            ((WebBrowser)FrmCtrl).Tag.ToString(), System.UriKind.Absolute);

                            Console.WriteLine(((WebBrowser)FrmCtrl).Tag.ToString());

                            break;
                        }
                        case 3:
                        {
                            adType = "Link";

                            FrmCtrl = this.Controls.Find("MyWeb" + pSecuence, true)[0];

                            String TmpUrl = nextAd.FileName;

                            ((WebBrowser)FrmCtrl).Tag = TmpUrl;

                            ((WebBrowser)FrmCtrl).Url = new System.Uri(
                            ((WebBrowser)FrmCtrl).Tag.ToString(), System.UriKind.RelativeOrAbsolute);

                            Console.WriteLine(((WebBrowser)FrmCtrl).Tag.ToString());

                            break;
                        }
                    }                                   
                }
                else
                {

                    FrmCtrl = this.Controls.Find("MyPics" + pSecuence, true)[0];

                    if ((FrmCtrl as PictureBox).Image != null) 
                        (FrmCtrl as PictureBox).Image.Dispose();
                    (FrmCtrl as PictureBox).Image = null;
                    (FrmCtrl as PictureBox).BackColor = Color.Black;

                    switch (nextAd.Type){
                        case 0:
                        {
                            adType = "Image";
                            break;
                        }
                        case 1:
                        {
                            adType = "Video";
                            break;
                        }
                        case 2:
                        {
                            adType = "App";
                            break;
                        }
                    }

                    throw new Exception("Invalid Content / File Serial");

                }

            }
            catch (OutOfMemoryException)
            {
                controlException = true;

                comment = "The application was unable to play / show this content. It is recommended " +
                "to check for proper file naming, avoiding special characters, and also make sure that " +
                "the content does not require extra dependencies or is in a non-standard format. ";

                comment += "Additional error reporting information: [" + "Invalid image file format or non-supported pixel format" + "]"; // tmpEx.Message.ToString() - Out of Memory does not provide enough info.
            }
            catch (System.IO.FileNotFoundException tmpEx)
            {
                controlException = true;

                comment = "The application was unable to play / show this content. ";
                comment += "Additional error reporting information: [" + "File not found or invalid file path: " + '"' + tmpEx.Message + '"' + "]"; 
            }
            catch (ArgumentNullException tmpEx)
            {
                controlException = true;

                comment = "The application was unable to play / show this content. ";                    
                comment += "Additional error reporting information: [" + tmpEx.Message + "]";
            }                    
            catch (Exception tmpEx)
            {
                controlException = true;

                comment = "The application was unable to play / show this content. It is recommended " +
                "to check for proper file naming, avoiding special characters, and also make sure that " +
                "the content does not require extra dependencies or is in a non-standard format. ";
                    
                comment += "Additional error reporting information: [" + tmpEx.Message + "]";
            }

            this.Controls.Remove(FrmCtrl);

            FrmCtrl.Visible = true;

            tableLayoutPanel1.Controls.Add(FrmCtrl);

            ApplyBorder(FrmCtrl, pSecuence, TemplateBorderWidth); // Method 1, Just let show the Panel BackColor.                

            switch (myTemplate)
            {
                // FullScreen
                /*  ____________
                *  |            |
                *  |____________|
                */
                case 1:
                {

                    tableLayoutPanel1.SetCellPosition(FrmCtrl, new TableLayoutPanelCellPosition(0, 0));

                    tableLayoutPanel1.SetColumnSpan(FrmCtrl, 2);
                    tableLayoutPanel1.SetRowSpan(FrmCtrl, 2);

                    break;

                }
                // 3-Div: 25%, 25%, 50%;
                /*  ____________
                *  |__1__|__2___|
                *  |_____3______|
                */
                case 2:
                {

                    switch (pSecuence)
                    {
                        case 1:
                            {
                                tableLayoutPanel1.SetCellPosition(FrmCtrl, new TableLayoutPanelCellPosition(0, 0));
                                break;
                            }
                        case 2:
                            {
                                tableLayoutPanel1.SetCellPosition(FrmCtrl, new TableLayoutPanelCellPosition(1, 0));
                                break;
                            }
                        case 3:
                            {
                                tableLayoutPanel1.SetCellPosition(FrmCtrl, new TableLayoutPanelCellPosition(0, 1));
                                tableLayoutPanel1.SetColumnSpan(FrmCtrl, 2);
                                break;
                            }
                    }

                    break;

                }
                // Vertical Split: 50%, 50%
                /*  ____________
                *  |  1  |  2   |
                *  |_____|______|
                */
                case 3:
                {

                    switch (pSecuence)
                    {
                        case 1:
                            {
                                tableLayoutPanel1.SetCellPosition(FrmCtrl, new TableLayoutPanelCellPosition(0, 0));
                                break;
                            }
                        case 2:
                            {
                                tableLayoutPanel1.SetCellPosition(FrmCtrl, new TableLayoutPanelCellPosition(1, 0));
                                break;
                            }
                    }

                    tableLayoutPanel1.SetRowSpan(FrmCtrl, 2);

                    break;

                }
                // Horizontal Split: 50%, 50%
                /*  ____________
                *  |_____1______|
                *  |_____2______|
                */
                case 4:
                {

                    switch (pSecuence)
                    {
                        case 1:
                            {
                                tableLayoutPanel1.SetCellPosition(FrmCtrl, new TableLayoutPanelCellPosition(0, 0));
                                break;
                            }
                        case 2:
                            {
                                tableLayoutPanel1.SetCellPosition(FrmCtrl, new TableLayoutPanelCellPosition(1, 0));
                                break;
                            }
                    }

                    tableLayoutPanel1.SetColumnSpan(FrmCtrl, 2);

                    break;

                }
                // Template 5: 20x70(%), 80x70(%), 100x30(%)
                /*  ____________
                *  | 1|         |
                *  |__|__2______|
                *  |_____3______|
                */
                case 5:
                {

                    switch (pSecuence)
                    {
                        case 1:
                            {
                                tableLayoutPanel1.SetCellPosition(FrmCtrl, new TableLayoutPanelCellPosition(0, 0));
                                break;
                            }
                        case 2:
                            {
                                tableLayoutPanel1.SetCellPosition(FrmCtrl, new TableLayoutPanelCellPosition(1, 0));
                                break;
                            }
                        case 3:
                            {
                                tableLayoutPanel1.SetCellPosition(FrmCtrl, new TableLayoutPanelCellPosition(0, 1));
                                tableLayoutPanel1.SetColumnSpan(FrmCtrl, 2);
                                break;
                            }
                    }

                    break;

                }
                // Template 6: 80x70(%), 20x70(%), 100x30(%)
                /*  ____________
                *  |         |2 |
                *  |_____1___|__|
                *  |_____3______|
                */
                case 6:
                {

                    switch (pSecuence)
                    {
                        case 1:
                            {
                                tableLayoutPanel1.SetCellPosition(FrmCtrl, new TableLayoutPanelCellPosition(0, 0));
                                break;
                            }
                        case 2:
                            {
                                tableLayoutPanel1.SetCellPosition(FrmCtrl, new TableLayoutPanelCellPosition(1, 0));
                                break;
                            }
                        case 3:
                            {
                                tableLayoutPanel1.SetCellPosition(FrmCtrl, new TableLayoutPanelCellPosition(0, 1));
                                tableLayoutPanel1.SetColumnSpan(FrmCtrl, 2);
                                break;                                        
                            }
                    }

                    break;

                }
                // Template 7: 20x100(%), 80x70(%), 70x30(%)
                /*  ____________
                *  | 1|         |
                *  |  |__2______|
                *  |__|__3______|
                */
                case 7:
                {

                    switch (pSecuence)
                    {
                        case 1:
                            {
                                tableLayoutPanel1.SetCellPosition(FrmCtrl, new TableLayoutPanelCellPosition(0, 0));
                                tableLayoutPanel1.SetRowSpan(FrmCtrl, 2);
                                break;
                            }
                        case 2:
                            {
                                tableLayoutPanel1.SetCellPosition(FrmCtrl, new TableLayoutPanelCellPosition(1, 0));                                        
                                break;
                            }
                        case 3:
                            {
                                tableLayoutPanel1.SetCellPosition(FrmCtrl, new TableLayoutPanelCellPosition(1, 1));
                                break;
                            }
                    }

                    break;

                }
                // Template 8: 80x70(%), 20x100(%), 70x30(%)
                /*  ____________
                *  |         |2 |
                *  |_____1___|  |
                *  |_____3___|__|
                */
                case 8:
                {

                    switch (pSecuence)
                    {
                        case 1:
                            {
                                tableLayoutPanel1.SetCellPosition(FrmCtrl, new TableLayoutPanelCellPosition(0, 0));
                                break;
                            }
                        case 2:
                            {
                                tableLayoutPanel1.SetCellPosition(FrmCtrl, new TableLayoutPanelCellPosition(1, 0));
                                tableLayoutPanel1.SetRowSpan(FrmCtrl, 2);
                                break;
                            }
                        case 3:
                            {
                                tableLayoutPanel1.SetCellPosition(FrmCtrl, new TableLayoutPanelCellPosition(0, 1));
                                break;
                            }
                    }

                    break;

                }
            }
            
            //FrmCtrl.Visible = true;

            ErrorHandling:    
       
            if (controlException || AdInfoNotFound)
            {
                if (controlException) nextAd.saveAdClick(actualSchedule.ToString(), pNextAdId, adType, 0, 0, comment);            

                ((Timer)this.components.Components.OfType<IComponent>().Where(ti => ti.GetType().FullName == "System.Windows.Forms.Timer")
                .ToList().Where(ti => ((Timer)ti).Tag != null).ToList().Find(ti => "myTime" + ((Timer)ti).Tag.ToString().Right(1) == "myTime" + pSecuence.ToString())
                ).Interval = 1;

                //Timer myScheduleTime;

                //myScheduleTime = ((Timer)this.components.Components.OfType<IComponent>().Where(ti => ti.GetType().FullName == "System.Windows.Forms.Timer")
                //.ToList().Where(ti => ((Timer)ti).Tag != null).ToList().Find(ti => ((Timer)ti).Tag.ToString() == "myScheduleTime"));

                //try { myScheduleTime.Interval -= (int)((new Functions().nextAd_DisplayTime(nextAd.id, pSecuence, currentSecuenceOrder[pSecuence], actualSchedule)) * 1000); }
                //catch (Exception tmpEx) { myScheduleTime.Interval = 1; } // Invalid Negative or Zero Interval protection 

                inactivityThreshold = 0;

                Succeed = false;

                return Succeed;
            }

            //if (nextAd.type != 1)
            //{
                nextAd.saveAdClick(actualSchedule, pNextAdId, adType, 0, 1);
            //}
            //else
            //{
            //    Timer myWmpErrorCheckTimer =
            //    ((Timer)this.components.Components.OfType<IComponent>().Where(ti => ti.GetType().FullName == "System.Windows.Forms.Timer")
            //    .ToList().Where(ti => ((Timer)ti).Tag != null).ToList().Find(ti => "myWmpErrorCheckTimer" + ((Timer)ti).Tag.ToString().Right(1) == "myWmpErrorCheckTimer" + pSecuence.ToString()));

            //    myWmpErrorCheckTimer.Interval = 4000;
            //    myWmpErrorCheckTimer.Enabled = true;
            //    myWmpErrorCheckTimer.Start();
            //}

            if (myTemplate == 1) {
                inactivityThreshold = AdSettings.InteractionTime; // (new Functions().nextAd_InteractionTime(nextAd.ID, pSecuence, currentSecuenceOrder[pSecuence], actualSchedule));
                
                if (currentSecuenceOrder[pSecuence] != 1) {
                    //currentScheduleTime += (int)
                    //(((Timer)this.components.Components.OfType<IComponent>().Where(ti => ti.GetType().FullName == "System.Windows.Forms.Timer")
                    //.ToList().Where(ti => ((Timer)ti).Tag != null).ToList().Find(ti => "myTime" + ((Timer)ti).Tag.ToString().Right(1) == "myTime" + pSecuence.ToString())
                    //).Interval / 1000);
                    currentSecuenceTime = 0;
                }                            
            }else{
                inactivityThreshold = 0;
            }

            ((Timer)this.components.Components.OfType<IComponent>().Where(ti => ti.GetType().FullName == "System.Windows.Forms.Timer")
            .ToList().Where(ti => ((Timer)ti).Tag != null).ToList().Find(ti => "myTime" + ((Timer)ti).Tag.ToString().Right(1) == "myTime" + pSecuence.ToString())
            ).Interval = (AdSettings.DisplayTime * 1000); // (new Functions().nextAd_DisplayTime(nextAd.ID, pSecuence, currentSecuenceOrder[pSecuence], actualSchedule)) * 1000;

            //ApplyBorder(FrmCtrl, pSecuence, 5, Color.Yellow); // Method 3, Let's apply Border Manually for each Control.

            Succeed = true;

            return Succeed;
        }

        private void webBrowser_NewWindow(object sender, CancelEventArgs e)
        {
            e.Cancel = true;
        }

        private void frm_Main_Load(object sender, EventArgs e)
        {

        }

        private void ShowActivationForm(Form Frm)
        {

            if (Application.OpenForms["FormInput"] != null || Application.OpenForms["FrmConfig_ScreenSelect"] != null || Application.OpenForms["FrmConfig_LocationSelect"] != null)
                Frm.TopMost = false;
            else
            {
                Frm.BringToFront();
                Frm.TopMost = true;
            }

            //if (Application.OpenForms["FormInput"] != null)
            //{
            //    Application.OpenForms["FormInput"].BringToFront();
            //}

            //if (Application.OpenForms["FormActivation"] != null)
            //{
            //    Application.OpenForms["FormActivation"].BringToFront();
            //}

            //if (Variables.SecondThread != null) { 

            //Form AnnoyingForm = Application.OpenForms["AnnoyingForm"];

            //if (AnnoyingForm != null)
            //{
            //    AnnoyingForm.BringToFront();
            //}

            //}
        }       

        public void SafelyShowActivationForm()
        {
            if (Variables.DEMO)
            { 
                Form AnnoyingForm = Application.OpenForms["AnnoyingForm"];

                if (AnnoyingForm != null)
                {
                    if (AnnoyingForm.InvokeRequired)
                    {
                        ShowActivationForm_delegate d = new ShowActivationForm_delegate(ShowActivationForm);

                        //this.Invoke(d, new object[]{AnnoyingForm});
                        AnnoyingForm.BeginInvoke(d, new object[] { AnnoyingForm });
                    }
                    else
                    {
                        ShowActivationForm(AnnoyingForm);
                    }
                }
            }
        }

        public void frm_Main_Click(object sender, EventArgs e)
        {
            if (e != null) { this.SimpleClickEffect(); }            
            this.SendToBack();

            //Form frmShell = Application.OpenForms["frmShell"];
            //if (frmShell != null) frmShell.SendToBack();            
                
            SafelyShowActivationForm();            
        }

        // Form Coming In: |Screen| <-- Form ... |Form into Screen After Slide|
        private static void slideInLeft(EffectsMethod pMethod, int pIncrements, System.Windows.Forms.Form pForm)
        {
            switch (pMethod)
            {
                case EffectsMethod.Steps:
                    {
                        int StepVal = (int)(Screen.PrimaryScreen.Bounds.Width / pIncrements);
                        while (pForm.Left != 0)
                        {
                            if ((pForm.Left - StepVal) <= 0)
                            { pForm.Left = 0; }
                            else
                            { pForm.Left -= StepVal; }

                            pForm.Refresh();

                            System.Threading.Thread.Sleep(1);
                        }
                        break;
                    }
                case EffectsMethod.Percent:
                    {
                        int StepVal = (int)(Screen.PrimaryScreen.Bounds.Width * pIncrements / 100);
                        while (pForm.Left != 0)
                        {
                            if ((pForm.Left - StepVal) <= 0)
                            { pForm.Left = 0; }
                            else
                            { pForm.Left -= StepVal; }

                            pForm.Refresh();

                            System.Threading.Thread.Sleep(1);
                        }
                        break;
                    }
            }
        }

        // Form Going Out: |Form into Screen| ... Form After Slide <-- |Screen|

        private static void slideOutLeft(EffectsMethod pMethod, int pIncrements, System.Windows.Forms.Form pForm)
        {
            switch (pMethod)
            {
                case EffectsMethod.Steps:
                    {
                        int StepVal = (int)(pForm.Width / pIncrements);
                        while ((pForm.Width > 2) || (pForm.Visible))
                        {
                            if ((pForm.Width - StepVal) <= 0)
                            {
                                pForm.Visible = false;
                                pForm.Width = 0;
                            }
                            else
                            { pForm.Width -= StepVal; }

                            pForm.Refresh();

                            System.Threading.Thread.Sleep(1);
                        }
                        break;
                    }
                case EffectsMethod.Percent:
                    {
                        int StepVal = (int)(pForm.Width * pIncrements / 100);
                        while ((pForm.Width > 2) || (pForm.Visible))
                        {
                            if ((pForm.Width - StepVal) <= 0)
                            {
                                pForm.Visible = false;
                                pForm.Width = 0;
                            }
                            else
                            { pForm.Width -= StepVal; }

                            pForm.Refresh();

                            System.Threading.Thread.Sleep(1);
                        }
                        break;
                    }
            }
        }

        // Form Coming In: Form --> |Screen| ... |Form into Screen After Slide|
        private static void slideInRight(EffectsMethod pMethod, int pIncrements, System.Windows.Forms.Form pForm)
        {
            switch (pMethod)
            {
                case EffectsMethod.Steps:
                    {
                        int StepVal = (int)(Screen.PrimaryScreen.Bounds.Width / pIncrements);
                        while (pForm.Left != 0)
                        {
                            if ((pForm.Left + StepVal) >= 0)
                            { pForm.Left = 0; }
                            else
                            { pForm.Left += StepVal; }

                            pForm.Refresh();

                            System.Threading.Thread.Sleep(1);
                        }
                        break;
                    }
                case EffectsMethod.Percent:
                    {
                        int StepVal = (int)(Screen.PrimaryScreen.Bounds.Width * pIncrements / 100);
                        while (pForm.Left != 0)
                        {
                            if ((pForm.Left + StepVal) >= 0)
                            { pForm.Left = 0; }
                            else
                            { pForm.Left += StepVal; }

                            pForm.Refresh();

                            System.Threading.Thread.Sleep(1);
                        }
                        break;
                    }
            }
        }

        // Form Going Out: |Form into Screen| ... |Screen| --> Form After Slide
        private static void slideOutRight(EffectsMethod pMethod, int pIncrements, System.Windows.Forms.Form pForm)
        {
            switch (pMethod)
            {
                case EffectsMethod.Steps:
                    {
                        int StepVal = (int)(Screen.PrimaryScreen.Bounds.Width / pIncrements);
                        while (pForm.Left != Screen.PrimaryScreen.Bounds.Width)
                        {
                            if ((Screen.PrimaryScreen.Bounds.Width - pForm.Left) <= StepVal)
                            { pForm.Left = (Screen.PrimaryScreen.Bounds.Width); }
                            else
                            { pForm.Left = (pForm.Left + StepVal); }

                            pForm.Refresh();

                            System.Threading.Thread.Sleep(1);
                        }
                        break;
                    }
                case EffectsMethod.Percent:
                    {
                        int StepVal = (int)(Screen.PrimaryScreen.Bounds.Width * pIncrements / 100);
                        while (pForm.Left != Screen.PrimaryScreen.Bounds.Width)
                        {
                            if ((Screen.PrimaryScreen.Bounds.Width - pForm.Left) <= StepVal)
                            { pForm.Left = (Screen.PrimaryScreen.Bounds.Width); }
                            else
                            { pForm.Left = (pForm.Left + StepVal); }

                            pForm.Refresh();

                            System.Threading.Thread.Sleep(1);
                        }
                        break;
                    }
            }
        }

        //                                 !      !
        // Form Coming In:                 ! Form ! 
        //                                 !      !
        //                          |                    |       |                    |
        //                          |       Screen       |  ...  |  Form into Screen  |
        //                          |                    |       |                    |
        private static void slideInDown(EffectsMethod pMethod, int pIncrements, System.Windows.Forms.Form pForm)
        {
            switch (pMethod)
            {
                case EffectsMethod.Steps:
                    {
                        int StepVal = (int)(Screen.PrimaryScreen.Bounds.Height / pIncrements);
                        while (pForm.Top != 0)
                        {
                            if ((pForm.Top + StepVal) >= 0)
                            { pForm.Top = 0; }
                            else
                            { pForm.Top += StepVal; }

                            pForm.Refresh();

                            System.Threading.Thread.Sleep(1);
                        }
                        break;
                    }
                case EffectsMethod.Percent:
                    {
                        int StepVal = (int)(Screen.PrimaryScreen.Bounds.Height * pIncrements / 100);
                        while (pForm.Top != 0)
                        {
                            if ((pForm.Top + StepVal) >= 0)
                            { pForm.Top = 0; }
                            else
                            { pForm.Top += StepVal; }

                            pForm.Refresh();

                            System.Threading.Thread.Sleep(1);
                        }
                        break;
                    }
            }
        }

        //                  |                    |       |                    |       
        //                  |  Form into Screen  |  ...  |       Screen       |  
        //                  |                    |       |                    |       
        //                                                      !      !
        // Form Going Out:                                      ! Form ! 
        //                                                      !      !
        private static void slideOutDown(EffectsMethod pMethod, int pIncrements, System.Windows.Forms.Form pForm)
        {
            switch (pMethod)
            {
                case EffectsMethod.Steps:
                    {
                        int StepVal = (int)(pForm.Height / pIncrements);
                        while ((pForm.Height > 2) || (pForm.Visible))
                        {
                            if ((pForm.Height - StepVal) <= 0)
                            {
                                pForm.Visible = false;
                                pForm.Height = 0;
                            }
                            else
                            { pForm.Height -= StepVal; }

                            pForm.Refresh();

                            System.Threading.Thread.Sleep(1);
                        }
                        break;
                    }
                case EffectsMethod.Percent:
                    {
                        int StepVal = (int)(pForm.Height * pIncrements / 100);
                        while ((pForm.Height > 2) || (pForm.Visible))
                        {
                            if ((pForm.Height - StepVal) <= 0)
                            {
                                pForm.Visible = false;
                                pForm.Height = 0;
                            }
                            else
                            { pForm.Height -= StepVal; }

                            pForm.Refresh();

                            System.Threading.Thread.Sleep(1);
                        }
                        break;
                    }
            }
        }

        //                          |                    |       |                    |
        //                          |       Screen       |  ...  |  Form into Screen  |
        //                          |                    |       |                    |
        //                                 ¡      ¡
        // Form Coming In:                 ¡ Form ¡ 
        //                                 ¡      ¡
        private static void slideInUp(EffectsMethod pMethod, int pIncrements, System.Windows.Forms.Form pForm)
        {
            switch (pMethod)
            {
                case EffectsMethod.Steps:
                    {
                        int StepVal = (int)(Screen.PrimaryScreen.Bounds.Height / pIncrements);
                        while (pForm.Top != 0)
                        {
                            if ((pForm.Top - StepVal) <= 0)
                            { pForm.Top = 0; }
                            else
                            { pForm.Top -= StepVal; }

                            pForm.Refresh();

                            System.Threading.Thread.Sleep(1);
                        }
                        break;
                    }
                case EffectsMethod.Percent:
                    {
                        int StepVal = (int)(Screen.PrimaryScreen.Bounds.Height * pIncrements / 100);
                        while (pForm.Top != 0)
                        {
                            if ((pForm.Top - StepVal) <= 0)
                            { pForm.Top = 0; }
                            else
                            { pForm.Top -= StepVal; }

                            pForm.Refresh();

                            System.Threading.Thread.Sleep(1);
                        }
                        break;
                    }
            }
        }

        //                                                      ¡      ¡
        // Form Going Out:                                      ¡ Form ¡ 
        //                                                      ¡      ¡
        //                  |                    |       |                    |       
        //                  |  Form into Screen  |  ...  |       Screen       |  
        //                  |                    |       |                    |       
        private static void slideOutUp(EffectsMethod pMethod, int pIncrements, System.Windows.Forms.Form pForm)
        {
            switch (pMethod)
            {
                case EffectsMethod.Steps:
                    {
                        int StepVal = (int)(Screen.PrimaryScreen.Bounds.Height / pIncrements);
                        while (pForm.Top != Screen.PrimaryScreen.Bounds.Height)
                        {
                            if ((Screen.PrimaryScreen.Bounds.Height - pForm.Top) <= StepVal)
                            { pForm.Top = (Screen.PrimaryScreen.Bounds.Height); }
                            else
                            { pForm.Top = (pForm.Top + StepVal); }

                            pForm.Refresh();

                            System.Threading.Thread.Sleep(1);
                        }
                        break;
                    }
                case EffectsMethod.Percent:
                    {
                        int StepVal = (int)(Screen.PrimaryScreen.Bounds.Height * pIncrements / 100);
                        while (pForm.Top != Screen.PrimaryScreen.Bounds.Height)
                        {
                            if ((Screen.PrimaryScreen.Bounds.Height - pForm.Top) <= StepVal)
                            { pForm.Top = (Screen.PrimaryScreen.Bounds.Height); }
                            else
                            { pForm.Top = (pForm.Top + StepVal); }

                            pForm.Refresh();

                            System.Threading.Thread.Sleep(1);
                        }
                        break;
                    }
            }
        }

        // Form Grows to the Right: |Form (0 Width) --> Rest of the Screen| ... |Form into Screen|
        private static void GrowInRight(EffectsMethod pMethod, int pIncrements, System.Windows.Forms.Form pForm)
        {
            switch (pMethod)
            {
                case EffectsMethod.Steps:
                    {
                        int StepVal = (int)(Screen.PrimaryScreen.Bounds.Width / pIncrements);
                        while (pForm.Width != Screen.PrimaryScreen.Bounds.Width)
                        {
                            if ((pForm.Width + StepVal) >= Screen.PrimaryScreen.Bounds.Width)
                            { pForm.Width = Screen.PrimaryScreen.Bounds.Width; }
                            else
                            { pForm.Width += StepVal; }

                            pForm.Refresh();

                            System.Threading.Thread.Sleep(1);
                        }
                        break;
                    }
                case EffectsMethod.Percent:
                    {
                        int StepVal = (int)(Screen.PrimaryScreen.Bounds.Width * pIncrements / 100);
                        while (pForm.Width != Screen.PrimaryScreen.Bounds.Width)
                        {
                            if ((pForm.Width + StepVal) >= Screen.PrimaryScreen.Bounds.Width)
                            { pForm.Width = Screen.PrimaryScreen.Bounds.Width; }
                            else
                            { pForm.Width += StepVal; }

                            pForm.Refresh();

                            System.Threading.Thread.Sleep(1);
                        }
                        break;
                    }
            }
        }

        //                          | Rest of the Screen |  ...  |                    |
        // Form Grows to the Top:   |                    |  ...  |  Form into Screen  |
        //                          |   Form (0 Height)  |  ...  |                    |
        private static void GrowInUp(EffectsMethod pMethod, int pIncrements, System.Windows.Forms.Form pForm)
        {
            switch (pMethod)
            {
                case EffectsMethod.Steps:
                    {
                        int StepVal = (int)(Screen.PrimaryScreen.Bounds.Height / pIncrements);
                        while (pForm.Height != Screen.PrimaryScreen.Bounds.Height)
                        {
                            if ((pForm.Height + StepVal) >= Screen.PrimaryScreen.Bounds.Height)
                            { pForm.Height = Screen.PrimaryScreen.Bounds.Height; }
                            else
                            { pForm.Height += StepVal; }

                            pForm.Refresh();

                            System.Threading.Thread.Sleep(1);
                        }
                        break;
                    }
                case EffectsMethod.Percent:
                    {
                        int StepVal = (int)(Screen.PrimaryScreen.Bounds.Height * pIncrements / 100);
                        while (pForm.Height != Screen.PrimaryScreen.Bounds.Height)
                        {
                            if ((pForm.Height + StepVal) >= Screen.PrimaryScreen.Bounds.Height)
                            { pForm.Height = Screen.PrimaryScreen.Bounds.Height; }
                            else
                            { pForm.Height += StepVal; }

                            pForm.Refresh();

                            System.Threading.Thread.Sleep(1);
                        }
                        break;
                    }
            }
        }

        // Transparent Form becomes Opaque (Show). 
        public static void FadeInForm(System.Windows.Forms.Form f, byte NumberOfSteps)
        {
            float StepVal = (float)(100f / NumberOfSteps);
            for (byte b = 0; b < NumberOfSteps; b++)
            {
                f.Opacity = f.Opacity + (StepVal / 100);
                f.Refresh();
                //System.Threading.Thread.Sleep(1);
            }
        }

        // Opaque Form becomes Transparent (Hide).
        public static void FadeOutForm(System.Windows.Forms.Form f, byte NumberOfSteps)
        {
            float StepVal = (float)(100f / NumberOfSteps);
            float fOpacity = 100f;
            for (byte b = 0; b <= NumberOfSteps; b++)
            {
                f.Opacity = fOpacity / 100;
                f.Refresh();
                fOpacity -= StepVal;
                System.Threading.Thread.Sleep(1);
            }
        }

    }
}
