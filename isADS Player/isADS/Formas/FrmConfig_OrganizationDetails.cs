﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Data.SqlClient;
using isADS.Clases;

namespace isADS
{
    public partial class FrmConfig_OrganizationDetails : Form
    {

        private Boolean ApplicationInit = false;
        private String OriginalOrganizationIDValue = String.Empty;
        private String OriginalSetupKeyValue = String.Empty;
        private String CurrentOrganizationIDValue = String.Empty;
        private String CurrentSetupKeyValue = String.Empty;
        private Boolean OnPurposeActivation = false;

        private Form PreviousForm = null;
        private Form NextForm = null;

        private Boolean IgnoreDefaultActivation()
        {
            OnPurposeActivation = true; return OnPurposeActivation;
        }

        public FrmConfig_OrganizationDetails()
        {
            InitializeComponent();
        }

        public FrmConfig_OrganizationDetails(Boolean AppInit)
        {
            InitializeComponent();
            this.ApplicationInit = AppInit;
        }

        private void FrmConfig_OrganizationDetails_Load(object sender, EventArgs e)
        {
            this.components = new System.ComponentModel.Container();
            this.DoubleBuffered = true;

            this.Left = (System.Windows.Forms.Screen.PrimaryScreen.WorkingArea.Width / 2) - (this.Width / 2);
            this.Top = Convert.ToInt32(System.Windows.Forms.Screen.PrimaryScreen.WorkingArea.Height * 0.10);

            OriginalOrganizationIDValue = DataEncryption.DecryptText(Properties.Settings.Default.Organization_ID, Functions.EncryptionKey); // Properties.Settings.Default.Organization_ID;
            // OriginalSetupKeyValue = DataEncryption.DecryptText(Properties.Settings.Default.Organization_SetupKey, Functions.EncryptionKey);
            
            this.OrganizationID.Text = OriginalOrganizationIDValue;
            this.SetupKey.Text = String.Empty; // this.SetupKey.Text = OriginalSetupKeyValue;

            CurrentOrganizationIDValue = OriginalOrganizationIDValue;
            CurrentSetupKeyValue = OriginalSetupKeyValue;

            //this.ClientSize = new System.Drawing.Size(414, 147);
            //this.Size = this.ClientSize;

            this.ApplyLanguage();

            this.lblOrganizationID.AutoSize = false;
            this.OrganizationID.Width = 277;
            this.OrganizationID.Font = new Font(this.OrganizationID.Font.FontFamily, 8);
            this.lblOrganizationID.Width = TextRenderer.MeasureText(lblOrganizationID.Text, lblOrganizationID.Font).Width;
            // this.lblOrganizationID.TextAlign = ContentAlignment.MiddleRight;
            this.lblOrganizationID.Left = (this.DBAccessPanel.Width / 2) - ((this.lblOrganizationID.Width + 1 + this.OrganizationID.Width) / 2); // Convert.ToInt32(this.Width * 0.05);
            this.OrganizationID.Left = this.lblOrganizationID.Left + this.lblOrganizationID.Width + 1; // (this.lblOrganizationID.Left + this.lblOrganizationID.Width)  + 5;

            this.BringToFront();
        }

        private Boolean ValidateSettings()
        {
            //Boolean CorrectSettings = true;

            //if (ApplicationInit)
            //{
            return (!this.OrganizationID.Text.isUndefined());
            //}             
        }

        private void FrmConfig_OrganizationDetails_Activated(object sender, EventArgs e)
        {
            this.OrganizationID.Focus();
            this.OrganizationID.SelectAll();
            this.OrganizationID.SelectionStart = 0;
        }

        private void FrmConfig_OrganizationDetails_FormClosing(object sender, FormClosingEventArgs e)
        {
            //if (FrmConfig.ApplicationInit && PreviousForm == null && NextForm == null)
            //{
            //    IgnoreDefaultActivation();
            //    MessageBox.Show(LanguageResources.GetText("FrmConfig.FirstUseConfigurationWarning"));
            //    e.Cancel = true;
            //}
            //else
            //{
                if (!ValidateSettings())
                {
                    e.Cancel = true;

                    IgnoreDefaultActivation();
                    MessageBox.Show(LanguageResources.GetText("FrmConfig.MsgUndefinedOrganizationID"));

                }
                else
                {

                    Boolean SettingsChange = false;

                    if (Properties.Settings.Default.Organization_ID.isUndefined() && Properties.Settings.Default.Organization_SetupKey.isUndefined())
                    {
                        SettingsChange = true;
                    }
                    else
                    {
                        SettingsChange = ((this.CurrentOrganizationIDValue != this.OriginalOrganizationIDValue)
                            || (this.CurrentSetupKeyValue != this.OriginalSetupKeyValue));

                        if (SettingsChange)
                        {
                            FormInput Input = new FormInput();

                            Input.Text = "Stellar ADS360";
                            Input.ActionTrue = LanguageResources.GetText("FrmConfig.ConfirmDBSettingsChange"); Input.ButtonTrue.Text = Input.ActionTrue;
                            Input.ActionFalse = LanguageResources.GetText("FrmConfig.DiscardDBSettingsChange"); Input.ButtonFalse.Text = Input.ActionFalse;
                            Input.Message.Text = LanguageResources.GetText("FrmConfig.OrganizationSettingsChangeWarning");

                            Input.Message.ReadOnly = true;
                            IgnoreDefaultActivation();
                            Input.ShowDialog();

                            SettingsChange = (Input.Action.Equals(Input.ActionTrue));
                        }                       
                    }

                    if (SettingsChange)
                    {
                        Properties.Settings.Default.Organization_ID = DataEncryption.EncryptText(CurrentOrganizationIDValue, Functions.EncryptionKey); // this.OrganizationID.Text;
                        // Properties.Settings.Default.Organization_SetupKey = DataEncryption.EncryptText(CurrentSetupKeyValue, Functions.EncryptionKey);
                        Properties.Settings.Default.Save();
                    }

                    this.Visible = false;
                    if (NextForm != null) NextForm.ShowDialog();
                    else if (PreviousForm != null) PreviousForm.ShowDialog();
                }
            //}
        }
      
        private void ApplyLanguage()
        {
            this.Text = LanguageResources.GetText("FrmConfig_OrganizationDetails.Title");

            this.lblOrganizationID.Text = LanguageResources.GetText("FrmConfig_OrganizationDetails.lblOrganizationID");
            this.lblSetupKey.Text = LanguageResources.GetText("FrmConfig_OrganizationDetails.lblSetupKey");

            this.Back.Text = LanguageResources.GetText("FrmConfig.GoBack");
            this.Next.Text = LanguageResources.GetText("FrmConfig.Finish");
        }             

        private void Next_Click(object sender, EventArgs e)
        {
            //NextForm = new FrmConfig_ScreenSelect(FrmConfig.ApplicationInit);
            this.Close();
        }

        private void Back_Click(object sender, EventArgs e)
        {
            if (FrmConfig.ApplicationInit)
            { NextForm = new FrmConfig_Language(FrmConfig.ApplicationInit); }
            else
            { PreviousForm = new FrmConfig_RemoteDB(FrmConfig.ApplicationInit); }

            this.Close();            
        }

        private void OrganizationID_TextChanged(object sender, EventArgs e)
        {
            CurrentOrganizationIDValue = this.OrganizationID.Text;
        }

        private void SetupKey_TextChanged(object sender, EventArgs e)
        {
            CurrentSetupKeyValue = this.SetupKey.Text;
        }

        private void OrganizationID_MaskInputRejected(object sender, MaskInputRejectedEventArgs e)
        {
            Console.WriteLine(e);
        }
    }
}
