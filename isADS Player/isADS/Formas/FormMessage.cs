﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using isADS.Clases;

namespace isADS
{
    public partial class FormMessage : Form
    {

        public Boolean AutoScrollToEnd = true;

        public FormMessage()
        {
            InitializeComponent();
        }

        private void ButtonTrue_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void FormMessage_FormClosing(object sender, FormClosingEventArgs e)
        {           
            e.Cancel = false;
            this.Dispose();         
        }

        private void Message_TextChanged(object sender, EventArgs e)
        {

        }

        private void FormMessage_Load(object sender, EventArgs e)
        {

        }

        protected override bool ProcessCmdKey(ref Message msg, Keys keyData)
        {
            if (keyData == Keys.Return || keyData == Keys.Space)
            {
                this.Close();
            }
            return true;
        }

        private void FormMessage_Activated(object sender, EventArgs e)
        {
            if (AutoScrollToEnd)
            Functions.setTimeOut(3250, delegate(Object myTimer, System.Timers.ElapsedEventArgs ev)
            {
                try { 
                this.BeginInvoke((Action)(() => {
                    try
                    {
                        this.Message.SelectionStart = this.Message.Text.Length;
                        this.Message.ScrollToCaret();  
                    }
                    catch {}                                 
                }));
                } catch (Exception Any) { Console.WriteLine(Any.Message); } // Most Probably, the user already closed the form before this code got executed.
                ((System.Timers.Timer)myTimer).Dispose();
            });
        }
    }
}
