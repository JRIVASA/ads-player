﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Data.SqlClient;
using isADS.Clases;

namespace isADS
{
    public partial class FrmConfig_LocationSelect : Form
    {

        //public LanguageResourcesMT LanguageResources = new LanguageResourcesMT();

        private Boolean ApplicationInit = false;
        private String OriginalSQLUserValue = String.Empty;
        private String OriginalSQLPassValue = String.Empty;
        private Boolean OnPurposeActivation = false;

        private Form PreviousForm = null;
        private Form NextForm = null;

        Dictionary<String, Location> Locations = null;

        private Boolean IgnoreDefaultActivation()
        {
            OnPurposeActivation = true; return OnPurposeActivation;
        }

        public FrmConfig_LocationSelect()
        {
            InitializeComponent();
        }

        public FrmConfig_LocationSelect(Boolean AppInit)
        {
            InitializeComponent();
            this.ApplicationInit = AppInit;
        }

        private void FrmConfig_LocationSelect_Load(object sender, EventArgs e)
        {                       
            this.components = new System.ComponentModel.Container();
            this.DoubleBuffered = true;

            this.Left = (System.Windows.Forms.Screen.PrimaryScreen.WorkingArea.Width / 2) - (this.Width / 2);
            this.Top = Convert.ToInt32(System.Windows.Forms.Screen.PrimaryScreen.WorkingArea.Height * 0.10);

            this.LocationIDsComboBox.Tag = this.LocationIDsComboBox.Width; // InitialWidth
            this.LocationIDsTextBox.Tag = this.LocationIDsTextBox.Width; // InitialWidth

            //this.Next.Visible = false;

            this.ApplyLanguage();            
            this.BringToFront();
        }

        private Boolean ValidateSettings()
        {
            Boolean CorrectSettings = true;

            if (FrmConfig.ApplicationInit)
            {
                CheckConfig(); // return CheckConfig();               
            }
            else
            {
                CheckConfig();
            }

            return CorrectSettings;
        }

        private Boolean CheckConfig()
        {

            String TmpConnectionString = 
                Functions.getAlternateConnectionString(
                    "A2NWPLSK14SQL-v05.shr.prod.iad2.secureserver.net", // Instancia //(Properties.Settings.Default.RemoteDB_Beta ? "isADSBeta.db.8609058.hostedresource.com" : "isADS.db.8609058.hostedresource.com"),
                    (Properties.Settings.Default.RemoteDB_Beta ? "isADSBeta" : "isADS"), // Base de Datos
                    (Properties.Settings.Default.RemoteDB_Beta ? "isADS" : "isADS"), // User Login
                    (Properties.Settings.Default.RemoteDB_Beta ? "W!s3Ads30" : "W!s3Ads30") // Pwd
                );


            if ((Variables.TmpConnection == null ? true : (Variables.TmpConnection.State != ConnectionState.Open)))
                Variables.TmpConnection = new SqlConnection(TmpConnectionString).OpenGet();

            Variables.SelectedOrganizationID = Functions.getOrganizationID(Variables.TmpConnection,
            DataEncryption.DecryptText(Properties.Settings.Default.Organization_ID, Functions.EncryptionKey), false);

            Locations = isADS.Clases.Location.getAvailableLocations(Variables.SelectedOrganizationID, Variables.TmpConnection);

            if (Locations != null)
            {
                // Show Selection.

                LocationIDsTextBox.Visible = false;
                LocationIDsComboBox.Left = LocationIDsTextBox.Left;

                LocationIDsComboBox.Width = Convert.ToInt32(this.LocationIDsComboBox.Tag) +
                    Convert.ToInt32(this.LocationIDsTextBox.Tag);

                LocationIDsComboBox.Items.Clear();
                Locations.ToList<KeyValuePair<String, Location>>().ForEach((TmpLocation) => LocationIDsComboBox.Items.Add(TmpLocation.Key));

                if (Locations.Count >= 1) { LocationIDsComboBox.SelectedItem = LocationIDsComboBox.Items[0]; }

                return true;
            }
            else
            {
                // No DB data.

                LocationIDsTextBox.Visible = false;
                LocationIDsComboBox.Left = LocationIDsTextBox.Left;

                LocationIDsComboBox.Width = Convert.ToInt32(this.LocationIDsComboBox.Tag) +
                    Convert.ToInt32(this.LocationIDsTextBox.Tag);
                
                IgnoreDefaultActivation();
                //Functions.QuickMsgBox(LanguageResources.GetText("FrmConfig.LocationSelect_ErrorMessage"));
                ShowErrorMessage();
                Back_Click(null, null);

                return false;

            }

        }

        private void ShowErrorMessage()
        {
            try
            {

                FormMessage Msg = new FormMessage();

                /*Msg.StartPosition = FormStartPosition.Manual;
                Input.Location = new Point(
                    (Int32)(((Double)this.Width / 2) - ((Double)Input.Width / 2)),
                    (Int32)(((Double)this.Height * 0.10))
                );             
                Input.TopMost = true;
                */
            
                Msg.StartPosition = FormStartPosition.CenterParent;
           
                Msg.Text = "Stellar ADS360";
                Msg.Action.Text = LanguageResources.GetText("FrmConfig.Continue");
                Msg.Message.Text = LanguageResources.GetText("FrmConfig.LocationSelect_ErrorMessage");

                Msg.Message.ScrollBars = ScrollBars.Both;
                Msg.Message.ReadOnly = true;

                //Form frmShell = Application.OpenForms["frmShell"];
                //if (frmShell != null)
                    //Msg.ShowDialog(frmShell);
                //else
                    //Msg.ShowDialog();

                Msg.ShowDialog(this);            

                Msg.Close();

            } catch (Exception Any) { Console.WriteLine(Any.Message); }
        }

        private void FrmConfig_LocationSelect_Activated(object sender, EventArgs e)
        {
            if (OnPurposeActivation) 
            { 
                OnPurposeActivation = false; return;
            }            
            ValidateSettings();
        }

        private void FrmConfig_LocationSelect_FormClosing(object sender, FormClosingEventArgs e)
        {
            //if (FrmConfig.ApplicationInit && PreviousForm == null && NextForm == null)
            //{
            //    IgnoreDefaultActivation();

            //    MessageBox.Show(LanguageResources.GetText("FrmConfig.FirstUseConfigurationWarning"));
            //    e.Cancel = true;
            //}
            //else
            //{
                //if (!ValidateSettings())
                //{
                //    e.Cancel = true;

                //    IgnoreDefaultActivation();
                //    MessageBox.Show(LanguageResources.GetText("FrmConfig.MsgUndefinedScreenID"));
                //}
                //else
                //{
                    this.Visible = false;
                    if (NextForm != null) NextForm.ShowDialog();
                    else if (PreviousForm != null) PreviousForm.ShowDialog();
                //}
            //}
        }

        private void ApplyLanguage()
        {
            this.Text = LanguageResources.GetText("FrmConfig_LocationSelect.Title");

            this.LocationDataPanel.Text = LanguageResources.GetText("FrmConfig_LocationSelect.LocationDataPanel");

            this.lblLocationID.Text = LanguageResources.GetText("FrmConfig_LocationSelect.lblLocationID");
            this.lblLocationName.Text = LanguageResources.GetText("FrmConfig_LocationSelect.lblLocationName");
            this.lblLocationAddress.Text = LanguageResources.GetText("FrmConfig_LocationSelect.lblLocationAddress");
            this.lblLocationCity.Text = LanguageResources.GetText("FrmConfig_LocationSelect.lblLocationCity");
            this.lblLocationState.Text = LanguageResources.GetText("FrmConfig_LocationSelect.lblLocationState");
            this.lblLocationCountry.Text = LanguageResources.GetText("FrmConfig_LocationSelect.lblLocationCountry");

            this.Back.Text = LanguageResources.GetText("FrmConfig.Cancel");
            this.Next.Text = LanguageResources.GetText("FrmConfig.Continue");
        }

        private void Back_Click(object sender, EventArgs e)
        {
            PreviousForm = null; this.Close();
        }

        private void Next_Click(object sender, EventArgs e)
        {
            try
            {
                Variables.SelectedLocation = Locations[LocationIDsComboBox.SelectedItem.ToString()];
            }
            catch (Exception){ }

            NextForm = new FrmConfig_ScreenSelect(FrmConfig.ApplicationInit); this.Close();
        }

        private void ChangeLocation(String ID)
        {
            try 
	        {	        
		        LocationName.Text = Locations[ID].Name;
                LocationAddress.Text = Locations[ID].Address;
                LocationCity.Text = Locations[ID].City;
                LocationState.Text = Locations[ID].State;
                LocationCountry.Text = Locations[ID].Country;
	        }
	        catch (Exception)
	        {
                
	        }
        }

        private void ScreenIDsComboBox_SelectedIndexChanged(object sender, EventArgs e)
        {
            ChangeLocation((LocationIDsComboBox.SelectedItem).ToString());
        }       
        
    }
}
