﻿namespace isADS
{
    partial class FrmConfig_Volume
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.LangPanel = new System.Windows.Forms.Panel();
            this.CloseBtn = new System.Windows.Forms.Button();
            this.lblVolume = new System.Windows.Forms.Label();
            this.TrackApplicationVolume = new System.Windows.Forms.TrackBar();
            this.LangPanel.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.TrackApplicationVolume)).BeginInit();
            this.SuspendLayout();
            // 
            // LangPanel
            // 
            this.LangPanel.Controls.Add(this.TrackApplicationVolume);
            this.LangPanel.Controls.Add(this.CloseBtn);
            this.LangPanel.Controls.Add(this.lblVolume);
            this.LangPanel.Dock = System.Windows.Forms.DockStyle.Top;
            this.LangPanel.Location = new System.Drawing.Point(0, 0);
            this.LangPanel.Name = "LangPanel";
            this.LangPanel.Size = new System.Drawing.Size(457, 103);
            this.LangPanel.TabIndex = 0;
            this.LangPanel.Paint += new System.Windows.Forms.PaintEventHandler(this.LangPanel_Paint);
            // 
            // CloseBtn
            // 
            this.CloseBtn.Font = new System.Drawing.Font("Verdana", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.CloseBtn.Location = new System.Drawing.Point(347, 29);
            this.CloseBtn.Name = "CloseBtn";
            this.CloseBtn.Size = new System.Drawing.Size(98, 22);
            this.CloseBtn.TabIndex = 1;
            this.CloseBtn.Tag = "";
            this.CloseBtn.Text = "Close";
            this.CloseBtn.UseVisualStyleBackColor = true;
            this.CloseBtn.Click += new System.EventHandler(this.CloseBtn_Click);
            // 
            // lblVolume
            // 
            this.lblVolume.AutoSize = true;
            this.lblVolume.Font = new System.Drawing.Font("Verdana", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblVolume.Location = new System.Drawing.Point(13, 32);
            this.lblVolume.Name = "lblVolume";
            this.lblVolume.Size = new System.Drawing.Size(61, 16);
            this.lblVolume.TabIndex = 1;
            this.lblVolume.Text = "Volume:";
            // 
            // TrackApplicationVolume
            // 
            this.TrackApplicationVolume.Location = new System.Drawing.Point(90, 29);
            this.TrackApplicationVolume.Maximum = 100;
            this.TrackApplicationVolume.Name = "TrackApplicationVolume";
            this.TrackApplicationVolume.Size = new System.Drawing.Size(243, 45);
            this.TrackApplicationVolume.TabIndex = 2;
            this.TrackApplicationVolume.TickFrequency = 5;
            this.TrackApplicationVolume.Scroll += new System.EventHandler(this.TrackApplicationVolume_Scroll);
            // 
            // FrmConfig_Volume
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(457, 102);
            this.Controls.Add(this.LangPanel);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "FrmConfig_Volume";
            this.ShowInTaskbar = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.Manual;
            this.Text = "FrmConfig";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.FrmConfig_Volume_FormClosing);
            this.Load += new System.EventHandler(this.FrmConfig_Volume_Load);
            this.LangPanel.ResumeLayout(false);
            this.LangPanel.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.TrackApplicationVolume)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Panel LangPanel;
        private System.Windows.Forms.Label lblVolume;
        private System.Windows.Forms.Button CloseBtn;
        private System.Windows.Forms.TrackBar TrackApplicationVolume;
    }
}