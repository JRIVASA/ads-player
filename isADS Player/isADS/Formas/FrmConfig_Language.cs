﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Data.SqlClient;
using isADS.Clases;

namespace isADS
{
    public partial class FrmConfig_Language : Form
    {
        private Boolean ApplicationInit = false;
        private Boolean OnPurposeActivation = false;

        private Boolean IgnoreDefaultActivation()
        {
            OnPurposeActivation = true; return OnPurposeActivation;
        }

        public FrmConfig_Language()
        {
            InitializeComponent();
        }

        public FrmConfig_Language(Boolean AppInit)
        {
            InitializeComponent();
            this.ApplicationInit = AppInit;
        }

        //private Form PreviousForm = null;
        private Form NextForm = null;

        private void FrmConfig_Language_Load(object sender, EventArgs e)
        {
            this.components = new System.ComponentModel.Container();
            this.DoubleBuffered = true;

            this.Left = (System.Windows.Forms.Screen.PrimaryScreen.WorkingArea.Width / 2) - (this.Width / 2);
            this.Top = Convert.ToInt32(System.Windows.Forms.Screen.PrimaryScreen.WorkingArea.Height * 0.10);

            if (FrmConfig.ApplicationInit) this.Back.Visible = false;

            this.ApplyLanguage();            
            this.BringToFront();
        }       

        private void ApplyLanguage()
        {

            this.Text = LanguageResources.GetText("FrmConfig_Language.Title");

            //this.lblLanguage.Text = this.LanguageResources.GetText("SetClipBoardText");
            this.lblLanguage.Text = LanguageResources.GetText("FrmConfig.lblLanguage");

            this.Languages.Items.Clear();

            this.Languages.Items.Add(LanguageResources.GetText("FrmConfig.Languages.OperatingSystemLanguage"));
            this.Languages.Items.Add(LanguageResources.GetText("FrmConfig.Languages.SpanishLanguage"));
            //this.Languages.Items.Add(LanguageResources.GetText("FrmConfig.Languages.SpanishLanguageVenezuelanCulture"));
            this.Languages.Items.Add(LanguageResources.GetText("FrmConfig.Languages.EnglishLanguage"));
            // Testing non-supported languages / locales.
            //this.Languages.Items.Add("ESP MEX (Spanish-MX)");
            //this.Languages.Items.Add("frances (French)");

            this.Languages.Text = LanguageResources.GetText("FrmConfig.SelectLanguage");

            this.SaveLang.Text = LanguageResources.GetText("FrmConfig.ApplyLanguage");

            this.Back.Text = LanguageResources.GetText("FrmConfig.Return");
            this.Next.Text = LanguageResources.GetText("FrmConfig.GoNext");

        }

        private void SaveLang_Click(object sender, EventArgs e)
        {
            // Save on BD and Save on App.Config

            String[] SelectedLangArray = this.Languages.Text.Split('(');

            if (SelectedLangArray.Length < 2) return;            

            String SelectedLang = SelectedLangArray[1];
            SelectedLang = SelectedLang.Substring(0, SelectedLang.Length - 1);

            if (SelectedLang.Equals("Default", StringComparison.OrdinalIgnoreCase)) 
            {
                SelectedLang = "Default"; // Variables.DefaultOSCultureName ;
            }
            else
            {
                switch (SelectedLang)
                {
                    case "English":
                        {
                            SelectedLang = "en"; break;
                        }
                    case "Spanish":
                        {
                            SelectedLang = "es"; break;
                        }
                    //case "Spanish-VE":
                    //    {
                    //        SelectedLang = "es-VE"; break;
                    //    }
                    // Testing non-supported languages / locales.
                    //case "Spanish-MX":
                    //    {
                    //        SelectedLang = "es-MX"; break;
                    //    }
                    //case "French":
                    //    {
                    //        SelectedLang = "fr"; break;
                    //    }
                    default:
                       { return; }
                }             
                
            }

            Properties.Settings.Default.Language = SelectedLang;
            Properties.Settings.Default.Save();

            Console.WriteLine(Properties.Settings.Default.Language);

            try
            {

                Functions.LocalizeApplication();

                this.ApplyLanguage();

                this.SaveLang.Text = LanguageResources.GetText("FrmConfig.AppliedLanguage");

                Functions.setTimeOut(2000, delegate(Object myTimer, System.Timers.ElapsedEventArgs ev)
                {
                    try { 
                        this.BeginInvoke((Action)(() => { this.SaveLang.Text = LanguageResources.GetText("FrmConfig.ApplyLanguage"); }));
                    } catch (Exception Any) { Console.WriteLine(Any.Message); }
                    ((System.Timers.Timer)myTimer).Dispose();
                });

            }
            catch (Exception) { }

        }

        private void Back_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void Next_Click(object sender, EventArgs e)
        {
            if (FrmConfig.ApplicationInit)
            { NextForm = new FrmConfig_OrganizationDetails(FrmConfig.ApplicationInit); }
            else
            { NextForm = new FrmConfig_LocalDB(FrmConfig.ApplicationInit); }
            
            this.Close();
        }

        private void FrmConfig_Language_FormClosing(object sender, FormClosingEventArgs e)
        {
            if (FrmConfig.ApplicationInit && NextForm == null)
            {
                IgnoreDefaultActivation();
                MessageBox.Show(LanguageResources.GetText("FrmConfig.FirstUseConfigurationWarning"));
                e.Cancel = true;
            }
            else
            {
                this.Visible = false; if (NextForm != null) NextForm.ShowDialog();
            }            
        }

        private void LangPanel_Paint(object sender, PaintEventArgs e)
        {

        }
    }
}
