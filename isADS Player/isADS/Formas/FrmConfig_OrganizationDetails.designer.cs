﻿namespace isADS
{
    partial class FrmConfig_OrganizationDetails
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.DBAccessPanel = new System.Windows.Forms.Panel();
            this.Next = new System.Windows.Forms.Button();
            this.Back = new System.Windows.Forms.Button();
            this.SetupKey = new System.Windows.Forms.TextBox();
            this.lblSetupKey = new System.Windows.Forms.Label();
            this.lblOrganizationID = new System.Windows.Forms.Label();
            this.OrganizationID = new System.Windows.Forms.MaskedTextBox();
            this.DBAccessPanel.SuspendLayout();
            this.SuspendLayout();
            // 
            // DBAccessPanel
            // 
            this.DBAccessPanel.Controls.Add(this.OrganizationID);
            this.DBAccessPanel.Controls.Add(this.Next);
            this.DBAccessPanel.Controls.Add(this.Back);
            this.DBAccessPanel.Controls.Add(this.SetupKey);
            this.DBAccessPanel.Controls.Add(this.lblSetupKey);
            this.DBAccessPanel.Controls.Add(this.lblOrganizationID);
            this.DBAccessPanel.Dock = System.Windows.Forms.DockStyle.Top;
            this.DBAccessPanel.Location = new System.Drawing.Point(0, 0);
            this.DBAccessPanel.Name = "DBAccessPanel";
            this.DBAccessPanel.Size = new System.Drawing.Size(457, 103);
            this.DBAccessPanel.TabIndex = 2;
            // 
            // Next
            // 
            this.Next.Font = new System.Drawing.Font("Verdana", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Next.Location = new System.Drawing.Point(226, 69);
            this.Next.Name = "Next";
            this.Next.Size = new System.Drawing.Size(98, 22);
            this.Next.TabIndex = 5;
            this.Next.Tag = "";
            this.Next.Text = "Next";
            this.Next.UseVisualStyleBackColor = true;
            this.Next.Click += new System.EventHandler(this.Next_Click);
            // 
            // Back
            // 
            this.Back.Font = new System.Drawing.Font("Verdana", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Back.Location = new System.Drawing.Point(122, 69);
            this.Back.Name = "Back";
            this.Back.Size = new System.Drawing.Size(98, 22);
            this.Back.TabIndex = 6;
            this.Back.Tag = "";
            this.Back.Text = "Back";
            this.Back.UseVisualStyleBackColor = true;
            this.Back.Click += new System.EventHandler(this.Back_Click);
            // 
            // SetupKey
            // 
            this.SetupKey.Font = new System.Drawing.Font("Verdana", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.SetupKey.Location = new System.Drawing.Point(357, 22);
            this.SetupKey.Name = "SetupKey";
            this.SetupKey.PasswordChar = '*';
            this.SetupKey.Size = new System.Drawing.Size(86, 22);
            this.SetupKey.TabIndex = 4;
            this.SetupKey.UseSystemPasswordChar = true;
            this.SetupKey.Visible = false;
            this.SetupKey.TextChanged += new System.EventHandler(this.SetupKey_TextChanged);
            // 
            // lblSetupKey
            // 
            this.lblSetupKey.Font = new System.Drawing.Font("Verdana", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblSetupKey.Location = new System.Drawing.Point(217, 24);
            this.lblSetupKey.Name = "lblSetupKey";
            this.lblSetupKey.Size = new System.Drawing.Size(134, 16);
            this.lblSetupKey.TabIndex = 6;
            this.lblSetupKey.Text = "Setup Key:";
            this.lblSetupKey.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.lblSetupKey.Visible = false;
            // 
            // lblOrganizationID
            // 
            this.lblOrganizationID.AutoSize = true;
            this.lblOrganizationID.Font = new System.Drawing.Font("Verdana", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblOrganizationID.ImageAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.lblOrganizationID.Location = new System.Drawing.Point(9, 23);
            this.lblOrganizationID.Name = "lblOrganizationID";
            this.lblOrganizationID.Size = new System.Drawing.Size(115, 16);
            this.lblOrganizationID.TabIndex = 5;
            this.lblOrganizationID.Text = "Organization ID:";
            this.lblOrganizationID.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // OrganizationID
            // 
            this.OrganizationID.AsciiOnly = true;
            this.OrganizationID.CutCopyMaskFormat = System.Windows.Forms.MaskFormat.IncludePromptAndLiterals;
            this.OrganizationID.Font = new System.Drawing.Font("Verdana", 9F);
            this.OrganizationID.HidePromptOnLeave = true;
            this.OrganizationID.InsertKeyMode = System.Windows.Forms.InsertKeyMode.Overwrite;
            this.OrganizationID.Location = new System.Drawing.Point(130, 21);
            this.OrganizationID.Margin = new System.Windows.Forms.Padding(0);
            this.OrganizationID.Mask = "AAAA-AAAA-AAAA-AAAA-AAAA-AAAA-AAAA-AAAA";
            this.OrganizationID.Name = "OrganizationID";
            this.OrganizationID.PromptChar = ' ';
            this.OrganizationID.ResetOnPrompt = true;
            this.OrganizationID.ResetOnSpace = true;
            this.OrganizationID.Size = new System.Drawing.Size(81, 22);
            this.OrganizationID.TabIndex = 7;
            this.OrganizationID.TextMaskFormat = System.Windows.Forms.MaskFormat.IncludePromptAndLiterals;
            this.OrganizationID.MaskInputRejected += new System.Windows.Forms.MaskInputRejectedEventHandler(this.OrganizationID_MaskInputRejected);
            this.OrganizationID.TextChanged += new System.EventHandler(this.OrganizationID_TextChanged);
            // 
            // FrmConfig_OrganizationDetails
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(457, 103);
            this.Controls.Add(this.DBAccessPanel);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "FrmConfig_OrganizationDetails";
            this.ShowInTaskbar = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.Manual;
            this.Text = "FrmConfig";
            this.Activated += new System.EventHandler(this.FrmConfig_OrganizationDetails_Activated);
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.FrmConfig_OrganizationDetails_FormClosing);
            this.Load += new System.EventHandler(this.FrmConfig_OrganizationDetails_Load);
            this.DBAccessPanel.ResumeLayout(false);
            this.DBAccessPanel.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Panel DBAccessPanel;
        private System.Windows.Forms.TextBox SetupKey;
        private System.Windows.Forms.Label lblSetupKey;
        private System.Windows.Forms.Label lblOrganizationID;
        private System.Windows.Forms.Button Next;
        private System.Windows.Forms.Button Back;
        private System.Windows.Forms.MaskedTextBox OrganizationID;
    }
}