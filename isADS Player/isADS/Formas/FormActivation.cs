﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using isADS.Clases;

namespace isADS
{
    public partial class FormActivation : Form
    {    

        public FormActivation()
        {
            InitializeComponent();
        }

        protected override CreateParams CreateParams
        {
            get
            {
                var cp = base.CreateParams;
                cp.ExStyle |= 0x80;  // Turn on WS_EX_TOOLWINDOW
                return cp;
            }
        }

        private void ButtonTrue_Click(object sender, EventArgs e)
        {
            this.Hide();
        }

        private void FormInput_FormClosing(object sender, FormClosingEventArgs e)
        {

        }

        private void ButtonCopySerial_Click(object sender, EventArgs e)
        {
            Serial.SelectAll();
            Serial.Copy();
            ButtonCopySerial.Text = LanguageResources.GetText("SetClipBoardText");

            Functions.setTimeOut(3000, delegate(Object myTimer, System.Timers.ElapsedEventArgs ev) {
                try {
                    this.BeginInvoke((Action)(() => { this.ButtonCopySerial.Text = LanguageResources.GetText("FormActivation.ButtonCopySerial"); }));
                } catch (Exception Any) { Console.WriteLine(Any.Message); }
                ((System.Timers.Timer)myTimer).Dispose();
            });

        }

        private void FormActivation_Load(object sender, EventArgs e)
        {
            LinkLabel.Link ProductLink = new LinkLabel.Link();
            ProductLink.LinkData = lblStep2Link.Text;
            lblStep2Link.Links.Add(ProductLink);
            
            lblStep4.Text += Properties.Settings.Default.MediaPlayerID;

            this.ApplyLanguage();
            ButtonCopySerial.Text = LanguageResources.GetText("WaitForSerial");

            ButtonCopySerial.Left = (this.Width / 2) - (ButtonCopySerial.Width / 2);
            lblActivationKey.Left = (this.Width / 2) - (lblActivationKey.Width / 2);
        }

        private void FormActivation_FormClosed(object sender, FormClosedEventArgs e)
        {
            if (this.Visible)
            {                
                this.Hide();
            }
            else
            {
                this.Dispose();
            }
        }

        private void ButtonFalse_Click(object sender, EventArgs e)
        {
            this.InputKey.Text = "";
            this.Hide();
        }

        private void FormActivation_Activated(object sender, EventArgs e)
        {
            Application.DoEvents();
            Serial.Text = Clases.Functions.GetSerial();
            ButtonCopySerial.Text = LanguageResources.GetText("FormActivation.ButtonCopySerial");         
        }

        private void lblStep2Link_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
        {
            try
            {
                System.Diagnostics.Process.Start(e.Link.LinkData.ToString());
            }
            catch (Exception)
            {
                Functions.QuickMsgBox(LanguageResources.GetText("OpenLinkError"));
            }
        }

        private void InputKey_TextChanged(object sender, EventArgs e)
        {

        }
    }
}
