﻿namespace isADS
{
    partial class FrmConfig_LocationSelect
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.ScreenPanel = new System.Windows.Forms.Panel();
            this.LocationDataPanel = new System.Windows.Forms.GroupBox();
            this.LocationCountry = new System.Windows.Forms.Label();
            this.lblLocationCountry = new System.Windows.Forms.Label();
            this.LocationState = new System.Windows.Forms.Label();
            this.lblLocationState = new System.Windows.Forms.Label();
            this.LocationCity = new System.Windows.Forms.Label();
            this.lblLocationCity = new System.Windows.Forms.Label();
            this.LocationAddress = new System.Windows.Forms.Label();
            this.lblLocationAddress = new System.Windows.Forms.Label();
            this.LocationName = new System.Windows.Forms.Label();
            this.lblLocationName = new System.Windows.Forms.Label();
            this.Next = new System.Windows.Forms.Button();
            this.Back = new System.Windows.Forms.Button();
            this.LocationIDsComboBox = new System.Windows.Forms.ComboBox();
            this.LocationIDsTextBox = new System.Windows.Forms.TextBox();
            this.lblLocationID = new System.Windows.Forms.Label();
            this.ScreenPanel.SuspendLayout();
            this.LocationDataPanel.SuspendLayout();
            this.SuspendLayout();
            // 
            // ScreenPanel
            // 
            this.ScreenPanel.Controls.Add(this.LocationDataPanel);
            this.ScreenPanel.Controls.Add(this.Next);
            this.ScreenPanel.Controls.Add(this.Back);
            this.ScreenPanel.Controls.Add(this.LocationIDsComboBox);
            this.ScreenPanel.Controls.Add(this.LocationIDsTextBox);
            this.ScreenPanel.Controls.Add(this.lblLocationID);
            this.ScreenPanel.Dock = System.Windows.Forms.DockStyle.Top;
            this.ScreenPanel.Location = new System.Drawing.Point(0, 0);
            this.ScreenPanel.Name = "ScreenPanel";
            this.ScreenPanel.Size = new System.Drawing.Size(457, 269);
            this.ScreenPanel.TabIndex = 1;
            // 
            // LocationDataPanel
            // 
            this.LocationDataPanel.Controls.Add(this.LocationCountry);
            this.LocationDataPanel.Controls.Add(this.lblLocationCountry);
            this.LocationDataPanel.Controls.Add(this.LocationState);
            this.LocationDataPanel.Controls.Add(this.lblLocationState);
            this.LocationDataPanel.Controls.Add(this.LocationCity);
            this.LocationDataPanel.Controls.Add(this.lblLocationCity);
            this.LocationDataPanel.Controls.Add(this.LocationAddress);
            this.LocationDataPanel.Controls.Add(this.lblLocationAddress);
            this.LocationDataPanel.Controls.Add(this.LocationName);
            this.LocationDataPanel.Controls.Add(this.lblLocationName);
            this.LocationDataPanel.Font = new System.Drawing.Font("Verdana", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LocationDataPanel.Location = new System.Drawing.Point(14, 71);
            this.LocationDataPanel.Name = "LocationDataPanel";
            this.LocationDataPanel.Size = new System.Drawing.Size(429, 153);
            this.LocationDataPanel.TabIndex = 15;
            this.LocationDataPanel.TabStop = false;
            this.LocationDataPanel.Text = "Location Data";
            // 
            // LocationCountry
            // 
            this.LocationCountry.AutoSize = true;
            this.LocationCountry.Font = new System.Drawing.Font("Verdana", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LocationCountry.Location = new System.Drawing.Point(119, 125);
            this.LocationCountry.Name = "LocationCountry";
            this.LocationCountry.Size = new System.Drawing.Size(60, 16);
            this.LocationCountry.TabIndex = 16;
            this.LocationCountry.Text = "Country";
            this.LocationCountry.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // lblLocationCountry
            // 
            this.lblLocationCountry.AutoSize = true;
            this.lblLocationCountry.Font = new System.Drawing.Font("Verdana", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblLocationCountry.Location = new System.Drawing.Point(18, 125);
            this.lblLocationCountry.Name = "lblLocationCountry";
            this.lblLocationCountry.Size = new System.Drawing.Size(66, 16);
            this.lblLocationCountry.TabIndex = 15;
            this.lblLocationCountry.Text = "Country:";
            this.lblLocationCountry.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // LocationState
            // 
            this.LocationState.AutoSize = true;
            this.LocationState.Font = new System.Drawing.Font("Verdana", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LocationState.Location = new System.Drawing.Point(118, 100);
            this.LocationState.Name = "LocationState";
            this.LocationState.Size = new System.Drawing.Size(45, 16);
            this.LocationState.TabIndex = 14;
            this.LocationState.Text = "State";
            this.LocationState.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // lblLocationState
            // 
            this.lblLocationState.AutoSize = true;
            this.lblLocationState.Font = new System.Drawing.Font("Verdana", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblLocationState.Location = new System.Drawing.Point(18, 100);
            this.lblLocationState.Name = "lblLocationState";
            this.lblLocationState.Size = new System.Drawing.Size(51, 16);
            this.lblLocationState.TabIndex = 13;
            this.lblLocationState.Text = "State:";
            this.lblLocationState.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // LocationCity
            // 
            this.LocationCity.AutoSize = true;
            this.LocationCity.Font = new System.Drawing.Font("Verdana", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LocationCity.Location = new System.Drawing.Point(119, 75);
            this.LocationCity.Name = "LocationCity";
            this.LocationCity.Size = new System.Drawing.Size(34, 16);
            this.LocationCity.TabIndex = 12;
            this.LocationCity.Text = "City";
            this.LocationCity.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // lblLocationCity
            // 
            this.lblLocationCity.AutoSize = true;
            this.lblLocationCity.Font = new System.Drawing.Font("Verdana", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblLocationCity.Location = new System.Drawing.Point(18, 75);
            this.lblLocationCity.Name = "lblLocationCity";
            this.lblLocationCity.Size = new System.Drawing.Size(40, 16);
            this.lblLocationCity.TabIndex = 11;
            this.lblLocationCity.Text = "City:";
            this.lblLocationCity.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // LocationAddress
            // 
            this.LocationAddress.AutoSize = true;
            this.LocationAddress.Font = new System.Drawing.Font("Verdana", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LocationAddress.Location = new System.Drawing.Point(119, 50);
            this.LocationAddress.Name = "LocationAddress";
            this.LocationAddress.Size = new System.Drawing.Size(60, 16);
            this.LocationAddress.TabIndex = 10;
            this.LocationAddress.Text = "Address";
            this.LocationAddress.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // lblLocationAddress
            // 
            this.lblLocationAddress.AutoSize = true;
            this.lblLocationAddress.Font = new System.Drawing.Font("Verdana", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblLocationAddress.Location = new System.Drawing.Point(18, 50);
            this.lblLocationAddress.Name = "lblLocationAddress";
            this.lblLocationAddress.Size = new System.Drawing.Size(66, 16);
            this.lblLocationAddress.TabIndex = 9;
            this.lblLocationAddress.Text = "Address:";
            this.lblLocationAddress.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // LocationName
            // 
            this.LocationName.AutoSize = true;
            this.LocationName.Font = new System.Drawing.Font("Verdana", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LocationName.Location = new System.Drawing.Point(119, 25);
            this.LocationName.Name = "LocationName";
            this.LocationName.Size = new System.Drawing.Size(44, 16);
            this.LocationName.TabIndex = 8;
            this.LocationName.Text = "Name";
            this.LocationName.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // lblLocationName
            // 
            this.lblLocationName.AutoSize = true;
            this.lblLocationName.Font = new System.Drawing.Font("Verdana", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblLocationName.Location = new System.Drawing.Point(18, 25);
            this.lblLocationName.Name = "lblLocationName";
            this.lblLocationName.Size = new System.Drawing.Size(50, 16);
            this.lblLocationName.TabIndex = 7;
            this.lblLocationName.Text = "Name:";
            this.lblLocationName.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // Next
            // 
            this.Next.Font = new System.Drawing.Font("Verdana", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Next.Location = new System.Drawing.Point(218, 234);
            this.Next.Name = "Next";
            this.Next.Size = new System.Drawing.Size(98, 22);
            this.Next.TabIndex = 4;
            this.Next.Tag = "";
            this.Next.Text = "Next";
            this.Next.UseVisualStyleBackColor = true;
            this.Next.Click += new System.EventHandler(this.Next_Click);
            // 
            // Back
            // 
            this.Back.Font = new System.Drawing.Font("Verdana", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Back.Location = new System.Drawing.Point(114, 234);
            this.Back.Name = "Back";
            this.Back.Size = new System.Drawing.Size(98, 22);
            this.Back.TabIndex = 5;
            this.Back.Tag = "";
            this.Back.Text = "Back";
            this.Back.UseVisualStyleBackColor = true;
            this.Back.Click += new System.EventHandler(this.Back_Click);
            // 
            // LocationIDsComboBox
            // 
            this.LocationIDsComboBox.BackColor = System.Drawing.SystemColors.Window;
            this.LocationIDsComboBox.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.LocationIDsComboBox.Font = new System.Drawing.Font("Verdana", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LocationIDsComboBox.FormattingEnabled = true;
            this.LocationIDsComboBox.Location = new System.Drawing.Point(256, 31);
            this.LocationIDsComboBox.MaxDropDownItems = 100;
            this.LocationIDsComboBox.Name = "LocationIDsComboBox";
            this.LocationIDsComboBox.Size = new System.Drawing.Size(121, 22);
            this.LocationIDsComboBox.TabIndex = 2;
            this.LocationIDsComboBox.SelectedIndexChanged += new System.EventHandler(this.ScreenIDsComboBox_SelectedIndexChanged);
            // 
            // LocationIDsTextBox
            // 
            this.LocationIDsTextBox.Font = new System.Drawing.Font("Verdana", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LocationIDsTextBox.Location = new System.Drawing.Point(136, 31);
            this.LocationIDsTextBox.Name = "LocationIDsTextBox";
            this.LocationIDsTextBox.Size = new System.Drawing.Size(121, 22);
            this.LocationIDsTextBox.TabIndex = 1;
            // 
            // lblLocationID
            // 
            this.lblLocationID.AutoSize = true;
            this.lblLocationID.Font = new System.Drawing.Font("Verdana", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblLocationID.Location = new System.Drawing.Point(28, 33);
            this.lblLocationID.Name = "lblLocationID";
            this.lblLocationID.Size = new System.Drawing.Size(89, 16);
            this.lblLocationID.TabIndex = 5;
            this.lblLocationID.Text = "Location ID:";
            // 
            // FrmConfig_LocationSelect
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(457, 268);
            this.Controls.Add(this.ScreenPanel);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "FrmConfig_LocationSelect";
            this.ShowInTaskbar = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.Manual;
            this.Text = "FrmConfig";
            this.Activated += new System.EventHandler(this.FrmConfig_LocationSelect_Activated);
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.FrmConfig_LocationSelect_FormClosing);
            this.Load += new System.EventHandler(this.FrmConfig_LocationSelect_Load);
            this.ScreenPanel.ResumeLayout(false);
            this.ScreenPanel.PerformLayout();
            this.LocationDataPanel.ResumeLayout(false);
            this.LocationDataPanel.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Panel ScreenPanel;
        public System.Windows.Forms.ComboBox LocationIDsComboBox;
        private System.Windows.Forms.TextBox LocationIDsTextBox;
        private System.Windows.Forms.Label lblLocationID;
        private System.Windows.Forms.Button Next;
        private System.Windows.Forms.Button Back;
        private System.Windows.Forms.GroupBox LocationDataPanel;
        private System.Windows.Forms.Label LocationCountry;
        private System.Windows.Forms.Label lblLocationCountry;
        private System.Windows.Forms.Label LocationState;
        private System.Windows.Forms.Label lblLocationState;
        private System.Windows.Forms.Label LocationCity;
        private System.Windows.Forms.Label lblLocationCity;
        private System.Windows.Forms.Label LocationAddress;
        private System.Windows.Forms.Label lblLocationAddress;
        private System.Windows.Forms.Label LocationName;
        private System.Windows.Forms.Label lblLocationName;
    }
}