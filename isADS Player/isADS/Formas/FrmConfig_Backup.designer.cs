﻿namespace isADS
{
    partial class FrmConfig_Backup
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.LangPanel = new System.Windows.Forms.Panel();
            this.SaveLang = new System.Windows.Forms.Button();
            this.lblLanguage = new System.Windows.Forms.Label();
            this.Languages = new System.Windows.Forms.ComboBox();
            this.ScreenPanel = new System.Windows.Forms.Panel();
            this.ChangeScreenID = new System.Windows.Forms.Button();
            this.ScreenIDsComboBox = new System.Windows.Forms.ComboBox();
            this.ScreenIDsTextBox = new System.Windows.Forms.TextBox();
            this.lblScreenID = new System.Windows.Forms.Label();
            this.DBAccessPanel = new System.Windows.Forms.Panel();
            this.lblWinAuthText = new System.Windows.Forms.Label();
            this.SQLPassword = new System.Windows.Forms.TextBox();
            this.lblSQLPassword = new System.Windows.Forms.Label();
            this.lblSQLUser = new System.Windows.Forms.Label();
            this.SQLUser = new System.Windows.Forms.TextBox();
            this.SQLCredentials = new System.Windows.Forms.RadioButton();
            this.SQLWindowsAuthentication = new System.Windows.Forms.RadioButton();
            this.lblDatabaseAccess = new System.Windows.Forms.Label();
            this.LangPanel.SuspendLayout();
            this.ScreenPanel.SuspendLayout();
            this.DBAccessPanel.SuspendLayout();
            this.SuspendLayout();
            // 
            // LangPanel
            // 
            this.LangPanel.Controls.Add(this.SaveLang);
            this.LangPanel.Controls.Add(this.lblLanguage);
            this.LangPanel.Controls.Add(this.Languages);
            this.LangPanel.Dock = System.Windows.Forms.DockStyle.Top;
            this.LangPanel.Location = new System.Drawing.Point(0, 0);
            this.LangPanel.Name = "LangPanel";
            this.LangPanel.Size = new System.Drawing.Size(457, 71);
            this.LangPanel.TabIndex = 0;
            // 
            // SaveLang
            // 
            this.SaveLang.Font = new System.Drawing.Font("Verdana", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.SaveLang.Location = new System.Drawing.Point(347, 29);
            this.SaveLang.Name = "SaveLang";
            this.SaveLang.Size = new System.Drawing.Size(98, 22);
            this.SaveLang.TabIndex = 2;
            this.SaveLang.Tag = "Save";
            this.SaveLang.Text = "Save";
            this.SaveLang.UseVisualStyleBackColor = true;
            this.SaveLang.Click += new System.EventHandler(this.SaveLang_Click);
            // 
            // lblLanguage
            // 
            this.lblLanguage.AutoSize = true;
            this.lblLanguage.Font = new System.Drawing.Font("Verdana", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblLanguage.Location = new System.Drawing.Point(13, 32);
            this.lblLanguage.Name = "lblLanguage";
            this.lblLanguage.Size = new System.Drawing.Size(77, 16);
            this.lblLanguage.TabIndex = 1;
            this.lblLanguage.Text = "Language:";
            // 
            // Languages
            // 
            this.Languages.Font = new System.Drawing.Font("Verdana", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Languages.FormattingEnabled = true;
            this.Languages.Items.AddRange(new object[] {
            "Operating System (Default)",
            "Spanish (Spanish)",
            "Spanish Venezuela (Spanish-VE)",
            "English (English)"});
            this.Languages.Location = new System.Drawing.Point(96, 30);
            this.Languages.Name = "Languages";
            this.Languages.Size = new System.Drawing.Size(245, 22);
            this.Languages.TabIndex = 0;
            // 
            // ScreenPanel
            // 
            this.ScreenPanel.Controls.Add(this.ChangeScreenID);
            this.ScreenPanel.Controls.Add(this.ScreenIDsComboBox);
            this.ScreenPanel.Controls.Add(this.ScreenIDsTextBox);
            this.ScreenPanel.Controls.Add(this.lblScreenID);
            this.ScreenPanel.Dock = System.Windows.Forms.DockStyle.Top;
            this.ScreenPanel.Location = new System.Drawing.Point(0, 71);
            this.ScreenPanel.Name = "ScreenPanel";
            this.ScreenPanel.Size = new System.Drawing.Size(457, 76);
            this.ScreenPanel.TabIndex = 1;
            // 
            // ChangeScreenID
            // 
            this.ChangeScreenID.Font = new System.Drawing.Font("Verdana", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ChangeScreenID.Location = new System.Drawing.Point(347, 30);
            this.ChangeScreenID.Name = "ChangeScreenID";
            this.ChangeScreenID.Size = new System.Drawing.Size(98, 22);
            this.ChangeScreenID.TabIndex = 5;
            this.ChangeScreenID.Tag = "Save";
            this.ChangeScreenID.Text = "Save";
            this.ChangeScreenID.UseVisualStyleBackColor = true;
            this.ChangeScreenID.Click += new System.EventHandler(this.ChangeScreenID_Click);
            // 
            // ScreenIDsComboBox
            // 
            this.ScreenIDsComboBox.BackColor = System.Drawing.SystemColors.Window;
            this.ScreenIDsComboBox.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.ScreenIDsComboBox.Font = new System.Drawing.Font("Verdana", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ScreenIDsComboBox.FormattingEnabled = true;
            this.ScreenIDsComboBox.Location = new System.Drawing.Point(218, 31);
            this.ScreenIDsComboBox.MaxDropDownItems = 100;
            this.ScreenIDsComboBox.Name = "ScreenIDsComboBox";
            this.ScreenIDsComboBox.Size = new System.Drawing.Size(121, 22);
            this.ScreenIDsComboBox.TabIndex = 4;
            this.ScreenIDsComboBox.MouseHover += new System.EventHandler(this.ScreenIDsComboBox_MouseHover);
            // 
            // ScreenIDsTextBox
            // 
            this.ScreenIDsTextBox.Font = new System.Drawing.Font("Verdana", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ScreenIDsTextBox.Location = new System.Drawing.Point(98, 31);
            this.ScreenIDsTextBox.Name = "ScreenIDsTextBox";
            this.ScreenIDsTextBox.Size = new System.Drawing.Size(121, 22);
            this.ScreenIDsTextBox.TabIndex = 3;
            // 
            // lblScreenID
            // 
            this.lblScreenID.AutoSize = true;
            this.lblScreenID.Font = new System.Drawing.Font("Verdana", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblScreenID.Location = new System.Drawing.Point(13, 33);
            this.lblScreenID.Name = "lblScreenID";
            this.lblScreenID.Size = new System.Drawing.Size(79, 16);
            this.lblScreenID.TabIndex = 2;
            this.lblScreenID.Text = "Screen ID:";
            // 
            // DBAccessPanel
            // 
            this.DBAccessPanel.Controls.Add(this.lblWinAuthText);
            this.DBAccessPanel.Controls.Add(this.SQLPassword);
            this.DBAccessPanel.Controls.Add(this.lblSQLPassword);
            this.DBAccessPanel.Controls.Add(this.lblSQLUser);
            this.DBAccessPanel.Controls.Add(this.SQLUser);
            this.DBAccessPanel.Controls.Add(this.SQLCredentials);
            this.DBAccessPanel.Controls.Add(this.SQLWindowsAuthentication);
            this.DBAccessPanel.Controls.Add(this.lblDatabaseAccess);
            this.DBAccessPanel.Dock = System.Windows.Forms.DockStyle.Top;
            this.DBAccessPanel.Location = new System.Drawing.Point(0, 147);
            this.DBAccessPanel.Name = "DBAccessPanel";
            this.DBAccessPanel.Size = new System.Drawing.Size(457, 71);
            this.DBAccessPanel.TabIndex = 2;
            // 
            // lblWinAuthText
            // 
            this.lblWinAuthText.AutoSize = true;
            this.lblWinAuthText.Font = new System.Drawing.Font("Verdana", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblWinAuthText.Location = new System.Drawing.Point(65, 55);
            this.lblWinAuthText.Name = "lblWinAuthText";
            this.lblWinAuthText.Size = new System.Drawing.Size(324, 16);
            this.lblWinAuthText.TabIndex = 8;
            this.lblWinAuthText.Text = "Text to show for Windows Authentication Mode";
            // 
            // SQLPassword
            // 
            this.SQLPassword.Font = new System.Drawing.Font("Verdana", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.SQLPassword.Location = new System.Drawing.Point(347, 38);
            this.SQLPassword.Name = "SQLPassword";
            this.SQLPassword.PasswordChar = '*';
            this.SQLPassword.Size = new System.Drawing.Size(97, 22);
            this.SQLPassword.TabIndex = 7;
            this.SQLPassword.UseSystemPasswordChar = true;
            this.SQLPassword.TextChanged += new System.EventHandler(this.SQLPassword_TextChanged);
            // 
            // lblSQLPassword
            // 
            this.lblSQLPassword.AutoSize = true;
            this.lblSQLPassword.Font = new System.Drawing.Font("Verdana", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblSQLPassword.Location = new System.Drawing.Point(258, 39);
            this.lblSQLPassword.Name = "lblSQLPassword";
            this.lblSQLPassword.Size = new System.Drawing.Size(76, 16);
            this.lblSQLPassword.TabIndex = 6;
            this.lblSQLPassword.Text = "Password:";
            this.lblSQLPassword.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // lblSQLUser
            // 
            this.lblSQLUser.AutoSize = true;
            this.lblSQLUser.Font = new System.Drawing.Font("Verdana", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblSQLUser.Location = new System.Drawing.Point(13, 39);
            this.lblSQLUser.Name = "lblSQLUser";
            this.lblSQLUser.Size = new System.Drawing.Size(82, 16);
            this.lblSQLUser.TabIndex = 5;
            this.lblSQLUser.Text = "User Login:";
            this.lblSQLUser.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // SQLUser
            // 
            this.SQLUser.Font = new System.Drawing.Font("Verdana", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.SQLUser.Location = new System.Drawing.Point(155, 37);
            this.SQLUser.Name = "SQLUser";
            this.SQLUser.Size = new System.Drawing.Size(90, 22);
            this.SQLUser.TabIndex = 4;
            this.SQLUser.TextChanged += new System.EventHandler(this.SQLUser_TextChanged);
            // 
            // SQLCredentials
            // 
            this.SQLCredentials.AutoSize = true;
            this.SQLCredentials.Font = new System.Drawing.Font("Verdana", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.SQLCredentials.Location = new System.Drawing.Point(347, 14);
            this.SQLCredentials.Name = "SQLCredentials";
            this.SQLCredentials.Size = new System.Drawing.Size(97, 18);
            this.SQLCredentials.TabIndex = 3;
            this.SQLCredentials.Text = "Credentials";
            this.SQLCredentials.UseVisualStyleBackColor = true;
            this.SQLCredentials.CheckedChanged += new System.EventHandler(this.SQLCredentials_CheckedChanged);
            // 
            // SQLWindowsAuthentication
            // 
            this.SQLWindowsAuthentication.AutoSize = true;
            this.SQLWindowsAuthentication.Checked = true;
            this.SQLWindowsAuthentication.Font = new System.Drawing.Font("Verdana", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.SQLWindowsAuthentication.Location = new System.Drawing.Point(146, 14);
            this.SQLWindowsAuthentication.Name = "SQLWindowsAuthentication";
            this.SQLWindowsAuthentication.Size = new System.Drawing.Size(178, 18);
            this.SQLWindowsAuthentication.TabIndex = 2;
            this.SQLWindowsAuthentication.TabStop = true;
            this.SQLWindowsAuthentication.Text = "Windows Authentication";
            this.SQLWindowsAuthentication.UseVisualStyleBackColor = true;
            this.SQLWindowsAuthentication.CheckedChanged += new System.EventHandler(this.SQLWindowsAuthentication_CheckedChanged);
            // 
            // lblDatabaseAccess
            // 
            this.lblDatabaseAccess.AutoSize = true;
            this.lblDatabaseAccess.Font = new System.Drawing.Font("Verdana", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblDatabaseAccess.Location = new System.Drawing.Point(12, 14);
            this.lblDatabaseAccess.Name = "lblDatabaseAccess";
            this.lblDatabaseAccess.Size = new System.Drawing.Size(128, 16);
            this.lblDatabaseAccess.TabIndex = 1;
            this.lblDatabaseAccess.Text = "Database Access:";
            this.lblDatabaseAccess.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // FrmConfig
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(457, 218);
            this.Controls.Add(this.DBAccessPanel);
            this.Controls.Add(this.ScreenPanel);
            this.Controls.Add(this.LangPanel);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "FrmConfig";
            this.ShowInTaskbar = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.Manual;
            this.Text = "FrmConfig";
            this.Activated += new System.EventHandler(this.FrmConfig_Backup_Activated);
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.FrmConfig_Backup_FormClosing);
            this.Load += new System.EventHandler(this.FrmConfig_Backup_Load);
            this.LangPanel.ResumeLayout(false);
            this.LangPanel.PerformLayout();
            this.ScreenPanel.ResumeLayout(false);
            this.ScreenPanel.PerformLayout();
            this.DBAccessPanel.ResumeLayout(false);
            this.DBAccessPanel.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Panel LangPanel;
        private System.Windows.Forms.Label lblLanguage;
        private System.Windows.Forms.ComboBox Languages;
        private System.Windows.Forms.Button SaveLang;
        private System.Windows.Forms.Panel ScreenPanel;
        private System.Windows.Forms.Button ChangeScreenID;
        public System.Windows.Forms.ComboBox ScreenIDsComboBox;
        private System.Windows.Forms.TextBox ScreenIDsTextBox;
        private System.Windows.Forms.Label lblScreenID;
        private System.Windows.Forms.Panel DBAccessPanel;
        private System.Windows.Forms.TextBox SQLPassword;
        private System.Windows.Forms.Label lblSQLPassword;
        private System.Windows.Forms.Label lblSQLUser;
        private System.Windows.Forms.TextBox SQLUser;
        private System.Windows.Forms.RadioButton SQLCredentials;
        private System.Windows.Forms.RadioButton SQLWindowsAuthentication;
        private System.Windows.Forms.Label lblDatabaseAccess;
        private System.Windows.Forms.Label lblWinAuthText;
    }
}