﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Data.SqlClient;
using isADS.Clases;

namespace isADS
{
    public partial class FrmConfig_Backup : Form
    {

        //public LanguageResourcesMT LanguageResources = new LanguageResourcesMT();

        public static Boolean ApplicationInit = false;
        private Boolean ConfigInitCheck = true;
        String Action = String.Empty;
        private Boolean OriginalWindowsAuthValue = false;
        private String OriginalSQLUserValue = String.Empty;
        private String OriginalSQLPassValue = String.Empty;
        private Boolean CurrentWindowsAuthValue;
        private String CurrentSQLUserValue;
        private String CurrentSQLPassValue;
        private Boolean OnPurposeActivation = false;

        private Boolean IgnoreDefaultActivation()
        {
            OnPurposeActivation = true; return OnPurposeActivation;
        }

        public FrmConfig_Backup()
        {
            InitializeComponent();
        }

        private void FrmConfig_Backup_Load(object sender, EventArgs e)
        {
            this.components = new System.ComponentModel.Container();
            this.DoubleBuffered = true;

            this.Left = (System.Windows.Forms.Screen.PrimaryScreen.WorkingArea.Width / 2) - (this.Width / 2);
            this.Top = Convert.ToInt32(System.Windows.Forms.Screen.PrimaryScreen.WorkingArea.Height * 0.10);

            this.Text = LanguageResources.GetText("FrmConfig.Title");
            this.SaveLang.Text = LanguageResources.GetText("FrmConfig.ApplyLanguage");
            this.ChangeScreenID.Text = LanguageResources.GetText("FrmConfig.SaveConfig");

            this.ScreenIDsComboBox.Tag = this.ScreenIDsComboBox.Width; // InitialWidth
            this.ScreenIDsTextBox.Tag = this.ScreenIDsTextBox.Width; // InitialWidth

            lblWinAuthText.Left = lblSQLUser.Left; lblWinAuthText.Top = lblSQLUser.Top;

            OriginalWindowsAuthValue = Properties.Settings.Default.LocalDB_WindowsAuth;
            CurrentWindowsAuthValue = OriginalWindowsAuthValue;

            OriginalSQLUserValue = Functions.DecryptedSQLUser; // Properties.Settings.Default.SQLUser;
            OriginalSQLPassValue = Functions.DecryptedSQLPassword; // Properties.Settings.Default.SQLPass;
            CurrentSQLUserValue = OriginalSQLUserValue;
            CurrentSQLPassValue = OriginalSQLPassValue;

            if (CurrentWindowsAuthValue)
            { SQLWindowsAuthentication.Checked = true; SQLCredentials_CheckedChanged(null, null); } 
            else 
            { SQLCredentials.Checked = true; }

            //this.ClientSize = new System.Drawing.Size(414, 147);
            //this.Size = this.ClientSize;

            this.ApplyLanguage();            
            this.BringToFront();
        }

        private SqlConnection getConnection()
        {
            if (CurrentWindowsAuthValue)
            {
                return Functions.getAlternateTrustedConnection();
            }
            else
            {
                return Functions.getAlternateConnection(CurrentSQLUserValue, CurrentSQLPassValue);
            }
        }

        private Boolean ValidateSettings()
        {
            Boolean CorrectSettings = true;

            if (ApplicationInit)
            {
                if (Action == "Close") 
                { return CorrectSettings; }
                else if (Action != String.Empty)
                { 
                    return CheckConfig();
                }                
                
                if (!CheckConfig())
                {
                    this.Close();
                    CorrectSettings = false;
                }
                else
                {
                    Action = "Close"; this.Close(); return CorrectSettings;
                }
            }
            else
            {
                CheckConfig();
            }

            return CorrectSettings;
        }

        private void TriggerDatabaseCheck()
        {
            Timer myCheckDBTimer = ((Timer)this.components.Components.OfType<IComponent>().Where(ti => ti.GetType().FullName == "System.Windows.Forms.Timer")
            .ToList().Where(ti => ((Timer)ti).Tag != null).ToList().Find(ti => ((Timer)ti).Tag.ToString() == "myCheckDBTimer"));

            if (myCheckDBTimer == null)
            {
                myCheckDBTimer = new Timer();
                myCheckDBTimer.Tag = "myCheckDBTimer";
                myCheckDBTimer.Interval = 1500;
                this.components.Add(myCheckDBTimer);
                myCheckDBTimer.Tick += myCheckDBTimer_Tick;
                myCheckDBTimer.Enabled = true;
            }
            else
            {
                myCheckDBTimer.Enabled = false;
                myCheckDBTimer.Interval = 1500;
                myCheckDBTimer.Enabled = true;
            }
        }

        void myCheckDBTimer_Tick(object sender, EventArgs e)
        {
            Timer myCheckDBTimer = ((Timer)sender);

            myCheckDBTimer.Enabled = false;

            CheckDatabaseAccess();
            Functions.setTimeOut(2000, delegate(Object myTimer, System.Timers.ElapsedEventArgs ev)
            {
                this.BeginInvoke((Action)(() =>
                {
                    try
                    {
                        if (!myCheckDBTimer.Enabled)
                        {
                            this.lblDatabaseAccess.Text = LanguageResources.GetText("FrmConfig.lblDatabaseAccess");
                            this.components.Remove(myCheckDBTimer); myCheckDBTimer.Dispose(); 
                        }
                    }
                    catch (Exception) { }
                }));
                ((System.Timers.Timer)myTimer).Dispose();
            });            
        }

        private Boolean CheckDatabaseAccess()
        {
            String ErrorDescription = String.Empty;

            Boolean BD = true; // Uncomment: Boolean BD = new Functions().FrmConfig_GetScreens(this, getConnection(), out ErrorDescription);

            if (BD)
            {
                // Should be done before when assigning value to BD if its true.

                //ScreenIDsComboBox.Items.Clear();
                //ScreenIDsComboBox.Items.Add("Screen ID 1");
                //ScreenIDsComboBox.Items.Add("Screen ID 2");
                //ScreenIDsComboBox.Items.Add("1002");

                // Show Selection.

                ScreenIDsComboBox.Visible = true;
                ScreenIDsTextBox.Visible = false;
                ScreenIDsComboBox.Left = ScreenIDsTextBox.Left;

                ScreenIDsComboBox.Width = Convert.ToInt32(this.ScreenIDsComboBox.Tag) +
                    Convert.ToInt32(this.ScreenIDsTextBox.Tag);
            }
            else
            {
                // No DB data. Show Input.

                ScreenIDsComboBox.Visible = false;
                ScreenIDsTextBox.Visible = true;
                ScreenIDsTextBox.Width = Convert.ToInt32(this.ScreenIDsComboBox.Tag) +
                    Convert.ToInt32(this.ScreenIDsTextBox.Tag);
            }

            if (ErrorDescription.isUndefined()) 
            { lblDatabaseAccess.Text = LanguageResources.GetText("FrmConfig.DBAccessGranted"); }
            else
            { lblDatabaseAccess.Text = LanguageResources.GetText("FrmConfig.DBAccessError"); }

            return BD;
        }

        private Boolean CheckConfig()
        {
            String ErrorDescription = String.Empty;

            Boolean BD = true; // Uncomment: new Functions().FrmConfig_GetScreens(this, getConnection(), out ErrorDescription);

            if (BD)
            {
                // Should be done before when assigning value to BD if its true.

                //ScreenIDsComboBox.Items.Clear();
                //ScreenIDsComboBox.Items.Add("Screen ID 1");
                //ScreenIDsComboBox.Items.Add("Screen ID 2");
                //ScreenIDsComboBox.Items.Add("1002");

                // Show Selection.

                ScreenIDsTextBox.Visible = false;
                ScreenIDsComboBox.Left = ScreenIDsTextBox.Left;

                ScreenIDsComboBox.Width = Convert.ToInt32(this.ScreenIDsComboBox.Tag) +
                    Convert.ToInt32(this.ScreenIDsTextBox.Tag);
            }
            else
            {
                // No DB data. Show Input.

                ScreenIDsComboBox.Visible = false;
                ScreenIDsTextBox.Width = Convert.ToInt32(this.ScreenIDsComboBox.Tag) +
                    Convert.ToInt32(this.ScreenIDsTextBox.Tag);
            }

            if (!ErrorDescription.isUndefined())
            {
                Action = "Check:" + "DatabaseConnectionNotWorking"; return false;
            }

            String ScreenID = Properties.Settings.Default.MediaPlayerID; // "1002"; // Buscar SelectedMediaPlayerID en BD.SYSTEM_SETTINGS

            if (ScreenID.isUndefined()) { Action = "Check:" + "Undefined ScreenID"; return false; }

            if (!ValidScreenID(ScreenID)) { Action = "Check:" + "Unexisting ScreenID"; return false; }

            return true;
        }

        private Boolean ValidScreenID(String ScreenID)
        {
            // Buscar si se encuentra SELECT * FROM MEDIAPLAYER WHERE ID = @ScreenID

            Boolean Valid = false;

            if (this.ScreenIDsComboBox.Items.Count > 0)
            {
                foreach (String DBScreenID in this.ScreenIDsComboBox.Items)
                {
                    if (DBScreenID == ScreenID) { Valid = true; break; }
                }
            }
            else
            {
                Valid=true;
            }
            
            return Valid;
        }

        private void FrmConfig_Backup_Activated(object sender, EventArgs e)
        {
            if (OnPurposeActivation) 
            { 
                OnPurposeActivation = false; return;
            }            
            ValidateSettings();
        }

        private void FrmConfig_Backup_FormClosing(object sender, FormClosingEventArgs e)
        {
            if (!ValidateSettings())
            { 
                e.Cancel = true;

                    switch (Action.Split(new String[] { "Check:" }, StringSplitOptions.None)[1])
                    {
                        case "Undefined ScreenID":
                            {
                                if (ConfigInitCheck) { ConfigInitCheck = false; }
                                else
                                {
                                    IgnoreDefaultActivation();
                                    MessageBox.Show(LanguageResources.GetText("FrmConfig.MsgUndefinedScreenID"));                                    
                                }
                                break;
                            }
                        case "Unexisting ScreenID":
                            {
                                if (ConfigInitCheck) { ConfigInitCheck = false; }
                                else
                                {
                                    IgnoreDefaultActivation();
                                    MessageBox.Show(LanguageResources.GetText("FrmConfig.MsgInvalidScreenID"));
                                }
                                break;
                            }
                        case "DatabaseConnectionNotWorking":
                            {
                                if (ConfigInitCheck) { ConfigInitCheck = false; }

                                IgnoreDefaultActivation();
                                MessageBox.Show(LanguageResources.GetText("FrmConfig.MsgDatabaseConnectionNotWorking"));
                                
                                break;
                            }
                    }
                
            }
            else
            {
                Boolean DBAccessSettingsChangeWarning = false;

                if (SQLWindowsAuthentication.Checked) { 
                    DBAccessSettingsChangeWarning = (SQLWindowsAuthentication.Checked != OriginalWindowsAuthValue);
                }
                else if (SQLCredentials.Checked)
                {
                    DBAccessSettingsChangeWarning = (SQLWindowsAuthentication.Checked != OriginalWindowsAuthValue);

                    if (!DBAccessSettingsChangeWarning)
                    {
                        DBAccessSettingsChangeWarning = !SQLUser.Text.isUndefined();
                    }
                }

                    if (DBAccessSettingsChangeWarning)
                    {
                        FormInput Input = new FormInput();

                        Input.Text = "Stellar ADS360";
                        Input.ActionTrue = LanguageResources.GetText("FrmConfig.ConfirmDBSettingsChange"); Input.ButtonTrue.Text = Input.ActionTrue;
                        Input.ActionFalse = LanguageResources.GetText("FrmConfig.DiscardDBSettingsChange"); Input.ButtonFalse.Text = Input.ActionFalse;
                        Input.Message.Text = LanguageResources.GetText("FrmConfig.DBAccessSettingsChangeWarning");
                            //+ Functions.NewLine() + Functions.NewLine() +
                            //LanguageResources.GetText("ActivationWindowChoiceMessageLine2");
                        Input.Message.ReadOnly = true;
                        IgnoreDefaultActivation();
                        Input.ShowDialog();

                        DBAccessSettingsChangeWarning = (Input.Action.Equals(Input.ActionTrue));
                    }

                    if (DBAccessSettingsChangeWarning && SQLWindowsAuthentication.Checked)
                    {
                        Properties.Settings.Default.LocalDB_WindowsAuth = true;
                        Properties.Settings.Default.Save();

                        Functions.ConnectionData = Functions.getConnectionData();
                    }
                    else if (DBAccessSettingsChangeWarning && SQLCredentials.Checked)
                    {
                        Properties.Settings.Default.LocalDB_WindowsAuth = false;

                        Properties.Settings.Default.LocalDB_SQLUser = DataEncryption.EncryptText(this.SQLUser.Text, Functions.EncryptionKey); // Crypt this.SQLUser.Text;
                        Properties.Settings.Default.LocalDB_SQLPass = DataEncryption.EncryptText(this.SQLPassword.Text, Functions.EncryptionKey); // Crypt this.SQLPassword.Text;
                        Properties.Settings.Default.Save();

                        Functions.DecryptedSQLUser = CurrentSQLUserValue;
                        Functions.DecryptedSQLPassword = CurrentSQLPassValue;
                        Functions.ConnectionData = Functions.getConnectionData();
                    }
                    // else Discard Changes.
                    else 
                    { 
                        // Other Connection Settings unaffected.
                    }
            }
        }

        private void ChangeScreenID_Click(object sender, EventArgs e)
        {
            if (this.ScreenIDsComboBox.Visible)
            {
                if (this.ScreenIDsComboBox.Text.isUndefined())
                { MessageBox.Show(this, LanguageResources.GetText("FrmConfig.InvalidValue")); return; }

                Properties.Settings.Default.MediaPlayerID = this.ScreenIDsComboBox.Text;
            }
            else if (this.ScreenIDsTextBox.Visible)
            {
                if (this.ScreenIDsTextBox.Text.isUndefined()) { MessageBox.Show(this, LanguageResources.GetText("Invalid Value.")); return; }

                Properties.Settings.Default.MediaPlayerID = this.ScreenIDsTextBox.Text;
            }
            
            Properties.Settings.Default.Save();

            this.ChangeScreenID.Text = LanguageResources.GetText("FrmConfig.ConfigSaved");

            Functions.setTimeOut(2000, delegate(Object myTimer, System.Timers.ElapsedEventArgs ev)
            {
                try { 
                    this.BeginInvoke((Action)(() => { this.ChangeScreenID.Text = LanguageResources.GetText("FrmConfig.SaveConfig"); }));
                } catch (Exception Any) { Console.WriteLine(Any.Message); }
                ((System.Timers.Timer)myTimer).Dispose();
            });
            // Save on BD and Save on App.Config
        }

        private void ApplyLanguage()
        {
            //this.lblLanguage.Text = this.LanguageResources.GetText("SetClipBoardText");
            this.lblLanguage.Text = LanguageResources.GetText("FrmConfig.lblLanguage");
            this.lblScreenID.Text = LanguageResources.GetText("FrmConfig.lblScreenID");

            this.Languages.Items.Clear();

            this.Languages.Items.Add(LanguageResources.GetText("FrmConfig.Languages.OperatingSystemLanguage"));
            this.Languages.Items.Add(LanguageResources.GetText("FrmConfig.Languages.SpanishLanguage"));
            //this.Languages.Items.Add(LanguageResources.GetText("FrmConfig.Languages.SpanishLanguageVenezuelanCulture"));
            this.Languages.Items.Add(LanguageResources.GetText("FrmConfig.Languages.EnglishLanguage"));
            // Testing non-supported languages / locales.
            //this.Languages.Items.Add("ESP MEX (Spanish-MX)");
            //this.Languages.Items.Add("frances (French)");

            this.Languages.Text = LanguageResources.GetText("FrmConfig.SelectLanguage");

            this.SaveLang.Text = LanguageResources.GetText("FrmConfig.ApplyLanguage");
            this.ChangeScreenID.Text = LanguageResources.GetText("FrmConfig.SaveButton");

            this.lblDatabaseAccess.Text = LanguageResources.GetText("FrmConfig_LocalDB.lblDatabaseAccess");

            this.SQLWindowsAuthentication.Text = LanguageResources.GetText("FrmConfig.SQLWindowsAuthentication");
            this.SQLCredentials.Text = LanguageResources.GetText("FrmConfig.SQLCredentials");

            this.lblSQLUser.Text = LanguageResources.GetText("FrmConfig.lblSQLUser");
            this.lblSQLPassword.Text = LanguageResources.GetText("FrmConfig.lblSQLPassword");

            this.lblWinAuthText.Text = LanguageResources.GetText("FrmConfig.lblWinAuthText");
        }

        private void SaveLang_Click(object sender, EventArgs e)
        {
            // Save on BD and Save on App.Config

            String[] SelectedLangArray = this.Languages.Text.Split('(');

            if (SelectedLangArray.Length < 2) return;            

            String SelectedLang = SelectedLangArray[1];
            SelectedLang = SelectedLang.Substring(0, SelectedLang.Length - 1);

            if (SelectedLang.Equals("Default", StringComparison.OrdinalIgnoreCase)) 
            {
                SelectedLang = "Default"; // Variables.DefaultOSCultureName ;
            }
            else
            {
                switch (SelectedLang)
                {
                    case "English":
                        {
                            SelectedLang = "en"; break;
                        }
                    case "Spanish":
                        {
                            SelectedLang = "es"; break;
                        }
                    case "Spanish-VE":
                        {
                            SelectedLang = "es-VE"; break;
                        }
                    // Testing non-supported languages / locales.
                    //case "Spanish-MX":
                    //    {
                    //        SelectedLang = "es-MX"; break;
                    //    }
                    //case "French":
                    //    {
                    //        SelectedLang = "fr"; break;
                    //    }
                    default:
                       { return; }
                }             
                
            }

            Properties.Settings.Default.Language = SelectedLang;
            Properties.Settings.Default.Save();

            Console.WriteLine(Properties.Settings.Default.Language);

            try
            {

                //System.Threading.Thread.CurrentThread.CurrentCulture =
                //new System.Globalization.CultureInfo(Properties.Settings.Default.Language);
                //System.Threading.Thread.CurrentThread.CurrentUICulture =
                //    System.Threading.Thread.CurrentThread.CurrentCulture;

                //System.Globalization.CultureInfo.DefaultThreadCurrentCulture = System.Threading.Thread.CurrentThread.CurrentCulture;
                //System.Globalization.CultureInfo.DefaultThreadCurrentUICulture = System.Threading.Thread.CurrentThread.CurrentUICulture;

                Functions.LocalizeApplication();

                this.ApplyLanguage();

                this.SaveLang.Text = LanguageResources.GetText("FrmConfig.AppliedLanguage");

                Functions.setTimeOut(2000, delegate(Object myTimer, System.Timers.ElapsedEventArgs ev)
                {
                    try { 
                        this.BeginInvoke((Action)(() => { this.SaveLang.Text = LanguageResources.GetText("FrmConfig.ApplyLanguage"); }));
                    } catch (Exception Any) { Console.WriteLine(Any.Message); }
                    ((System.Timers.Timer)myTimer).Dispose();
                });

            }
            catch (Exception) { }   

        }

        private void ScreenIDsComboBox_MouseHover(object sender, EventArgs e)
        {
            //lblLanguage.Text = new Random().Next(1, 5).ToString();
        }

        private void DatabaseAccessSettingsChanged()
        {
            if (SQLWindowsAuthentication.Checked)
            {
                lblSQLUser.Visible = false;
                lblSQLPassword.Visible = false;
                SQLUser.Visible = false;
                SQLPassword.Visible = false;

                lblWinAuthText.Visible = true;

                CurrentWindowsAuthValue = true;
            } else if (SQLCredentials.Checked)
            {
                lblSQLUser.Visible = true;
                lblSQLPassword.Visible = true;
                SQLUser.Visible = true;
                SQLPassword.Visible = true;

                lblWinAuthText.Visible = false;

                CurrentWindowsAuthValue = false;
                if (!this.SQLUser.Text.isUndefined()) { 
                    this.CurrentSQLUserValue = this.SQLUser.Text;
                    this.CurrentSQLPassValue = this.SQLPassword.Text;
                    Functions.ConnectionData = Functions.getConnectionData();
                }
            }

            TriggerDatabaseCheck(); // CheckDatabaseAccess();

        }

        private void SQLWindowsAuthentication_CheckedChanged(object sender, EventArgs e)
        {
            DatabaseAccessSettingsChanged();
        }

        private void SQLCredentials_CheckedChanged(object sender, EventArgs e)
        {
            DatabaseAccessSettingsChanged();
        }

        private void SQLUser_TextChanged(object sender, EventArgs e)
        {
            DatabaseAccessSettingsChanged();
        }

        private void SQLPassword_TextChanged(object sender, EventArgs e)
        {
            DatabaseAccessSettingsChanged();
        }
    }
}
