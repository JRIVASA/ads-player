﻿namespace isADS
{
    partial class FrmConfig_OptionalSettings
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.LangPanel = new System.Windows.Forms.Panel();
            this.Next = new System.Windows.Forms.Button();
            this.Back = new System.Windows.Forms.Button();
            this.EnableDefaultCampaign = new System.Windows.Forms.CheckBox();
            this.CampaignID = new System.Windows.Forms.TextBox();
            this.lblCampaignID = new System.Windows.Forms.Label();
            this.LangPanel.SuspendLayout();
            this.SuspendLayout();
            // 
            // LangPanel
            // 
            this.LangPanel.Controls.Add(this.CampaignID);
            this.LangPanel.Controls.Add(this.lblCampaignID);
            this.LangPanel.Controls.Add(this.EnableDefaultCampaign);
            this.LangPanel.Controls.Add(this.Next);
            this.LangPanel.Controls.Add(this.Back);
            this.LangPanel.Dock = System.Windows.Forms.DockStyle.Top;
            this.LangPanel.Location = new System.Drawing.Point(0, 0);
            this.LangPanel.Name = "LangPanel";
            this.LangPanel.Size = new System.Drawing.Size(457, 103);
            this.LangPanel.TabIndex = 0;
            // 
            // Next
            // 
            this.Next.Font = new System.Drawing.Font("Verdana", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Next.Location = new System.Drawing.Point(219, 68);
            this.Next.Name = "Next";
            this.Next.Size = new System.Drawing.Size(98, 22);
            this.Next.TabIndex = 2;
            this.Next.Tag = "";
            this.Next.Text = "Next";
            this.Next.UseVisualStyleBackColor = true;
            this.Next.Click += new System.EventHandler(this.Next_Click);
            // 
            // Back
            // 
            this.Back.Font = new System.Drawing.Font("Verdana", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Back.Location = new System.Drawing.Point(115, 68);
            this.Back.Name = "Back";
            this.Back.Size = new System.Drawing.Size(98, 22);
            this.Back.TabIndex = 3;
            this.Back.Tag = "";
            this.Back.Text = "Back";
            this.Back.UseVisualStyleBackColor = true;
            this.Back.Click += new System.EventHandler(this.Back_Click);
            // 
            // EnableDefaultCampaign
            // 
            this.EnableDefaultCampaign.AutoSize = true;
            this.EnableDefaultCampaign.FlatAppearance.CheckedBackColor = System.Drawing.SystemColors.Control;
            this.EnableDefaultCampaign.Font = new System.Drawing.Font("Verdana", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.EnableDefaultCampaign.Location = new System.Drawing.Point(12, 30);
            this.EnableDefaultCampaign.Name = "EnableDefaultCampaign";
            this.EnableDefaultCampaign.Size = new System.Drawing.Size(190, 20);
            this.EnableDefaultCampaign.TabIndex = 4;
            this.EnableDefaultCampaign.Text = "Enable Default Campaign";
            this.EnableDefaultCampaign.UseVisualStyleBackColor = true;
            this.EnableDefaultCampaign.CheckedChanged += new System.EventHandler(this.EnableDefaultCampaign_CheckedChanged);
            // 
            // CampaignID
            // 
            this.CampaignID.Font = new System.Drawing.Font("Verdana", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.CampaignID.Location = new System.Drawing.Point(352, 29);
            this.CampaignID.Name = "CampaignID";
            this.CampaignID.Size = new System.Drawing.Size(93, 22);
            this.CampaignID.TabIndex = 6;
            this.CampaignID.TextChanged += new System.EventHandler(this.CampaignID_TextChanged);
            // 
            // lblCampaignID
            // 
            this.lblCampaignID.AutoSize = true;
            this.lblCampaignID.Font = new System.Drawing.Font("Verdana", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblCampaignID.Location = new System.Drawing.Point(250, 31);
            this.lblCampaignID.Name = "lblCampaignID";
            this.lblCampaignID.Size = new System.Drawing.Size(96, 16);
            this.lblCampaignID.TabIndex = 7;
            this.lblCampaignID.Text = "Campaign ID:";
            // 
            // FrmConfig_OptionalSettings
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(457, 102);
            this.Controls.Add(this.LangPanel);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "FrmConfig_OptionalSettings";
            this.ShowInTaskbar = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.Manual;
            this.Text = "FrmConfig";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.FrmConfig_OptionalSettings_FormClosing);
            this.Load += new System.EventHandler(this.FrmConfig_OptionalSettings_Load);
            this.LangPanel.ResumeLayout(false);
            this.LangPanel.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Panel LangPanel;
        private System.Windows.Forms.Button Next;
        private System.Windows.Forms.Button Back;
        private System.Windows.Forms.CheckBox EnableDefaultCampaign;
        private System.Windows.Forms.TextBox CampaignID;
        private System.Windows.Forms.Label lblCampaignID;
    }
}