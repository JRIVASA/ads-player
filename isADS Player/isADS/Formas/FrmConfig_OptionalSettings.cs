﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Data.SqlClient;
using isADS.Clases;

namespace isADS
{
    public partial class FrmConfig_OptionalSettings : Form
    {
        private Boolean ApplicationInit = false;
        private Boolean OnPurposeActivation = false;
        private Boolean OriginalEnableDefaultCampaignValue;
        private String OriginalDefaultCampaignIDValue;

        private Boolean IgnoreDefaultActivation()
        {
            OnPurposeActivation = true; return OnPurposeActivation;
        }

        public FrmConfig_OptionalSettings()
        {
            InitializeComponent();
        }

        public FrmConfig_OptionalSettings(Boolean AppInit)
        {
            InitializeComponent();
            this.ApplicationInit = AppInit;
        }

        private Form PreviousForm = null;
        private Form NextForm = null;

        private void FrmConfig_OptionalSettings_Load(object sender, EventArgs e)
        {
            this.components = new System.ComponentModel.Container();
            this.DoubleBuffered = true;

            this.Left = (System.Windows.Forms.Screen.PrimaryScreen.WorkingArea.Width / 2) - (this.Width / 2);
            this.Top = Convert.ToInt32(System.Windows.Forms.Screen.PrimaryScreen.WorkingArea.Height * 0.10);

            if (FrmConfig.ApplicationInit) this.Back.Visible = false;

            OriginalEnableDefaultCampaignValue = Properties.Settings.Default.EnableDefaultCampaign;
            OriginalDefaultCampaignIDValue = Properties.Settings.Default.DefaultCampaignID;

            if (OriginalEnableDefaultCampaignValue)
            {
                this.EnableDefaultCampaign.Checked = true;
                this.CampaignID.Text = OriginalDefaultCampaignIDValue;
            }

            this.ApplyLanguage();            
            this.BringToFront();
        }       

        private void ApplyLanguage()
        {

            this.Text = LanguageResources.GetText("FrmConfig_Language.Title");

            this.EnableDefaultCampaign.Text = LanguageResources.GetText("FrmConfig.EnableDefaultCampaign");
            this.lblCampaignID.Text = LanguageResources.GetText("FrmConfig.lblCampaignID");

            this.Back.Text = LanguageResources.GetText("FrmConfig.GoBack");
            this.Next.Text = LanguageResources.GetText("FrmConfig.Finish");

        }

        private void Back_Click(object sender, EventArgs e)
        {
            PreviousForm = new FrmConfig_ScreenSelect(FrmConfig.ApplicationInit); this.Close();
        }

        private void Next_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private Boolean ValidateSettings()
        {
            if (EnableDefaultCampaign.Checked) return !this.CampaignID.Text.isUndefined();
            else return true;
        }

        private void FrmConfig_OptionalSettings_FormClosing(object sender, FormClosingEventArgs e)
        {
            if (FrmConfig.ApplicationInit && NextForm == null)
            {
                IgnoreDefaultActivation();
                MessageBox.Show(LanguageResources.GetText("FrmConfig.FirstUseConfigurationWarning"));
                e.Cancel = true;
            }
            else
            {
                if (!(ValidateSettings()))
                {
                    e.Cancel = true;

                    IgnoreDefaultActivation();
                    MessageBox.Show(LanguageResources.GetText("FrmConfig.MsgDatabaseConnectionNotWorking"));
                    
                }
                else
                { 
                    if (this.EnableDefaultCampaign.Checked != OriginalEnableDefaultCampaignValue)
                    {
                        if (OriginalEnableDefaultCampaignValue)
                        {
                            Properties.Settings.Default.EnableDefaultCampaign = true;
                            Properties.Settings.Default.DefaultCampaignID = this.CampaignID.Text;
                            Properties.Settings.Default.Save();
                        }
                        else
                        {
                            Properties.Settings.Default.EnableDefaultCampaign = false;
                            Properties.Settings.Default.Save();
                        }
                    }
                }

                this.Visible = false; if (NextForm != null) NextForm.ShowDialog();
            }            
        }

        private void EnableDefaultCampaign_CheckedChanged(object sender, EventArgs e)
        {
            if (this.EnableDefaultCampaign.Checked)
            {
                this.lblCampaignID.Visible = true;
                this.CampaignID.Visible = true;
            }
            else
            {
                this.lblCampaignID.Visible = false;
                this.CampaignID.Visible = false;
            }
        }

        private void CampaignID_TextChanged(object sender, EventArgs e)
        {

        }
    }
}
