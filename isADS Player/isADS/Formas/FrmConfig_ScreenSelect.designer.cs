﻿namespace isADS
{
    partial class FrmConfig_ScreenSelect
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.ScreenPanel = new System.Windows.Forms.Panel();
            this.ScreenDataPanel = new System.Windows.Forms.GroupBox();
            this.ScreenUbicationLine2 = new System.Windows.Forms.Label();
            this.ScreenUbicationLine1 = new System.Windows.Forms.Label();
            this.lblScreenUbication = new System.Windows.Forms.Label();
            this.ScreenModel = new System.Windows.Forms.Label();
            this.lblScreenModel = new System.Windows.Forms.Label();
            this.ScreenBrand = new System.Windows.Forms.Label();
            this.lblScreenBrand = new System.Windows.Forms.Label();
            this.ScreenDescription = new System.Windows.Forms.Label();
            this.lblScreenDescription = new System.Windows.Forms.Label();
            this.Next = new System.Windows.Forms.Button();
            this.Back = new System.Windows.Forms.Button();
            this.ScreenIDsComboBox = new System.Windows.Forms.ComboBox();
            this.ScreenIDsTextBox = new System.Windows.Forms.TextBox();
            this.lblScreenID = new System.Windows.Forms.Label();
            this.ScreenPanel.SuspendLayout();
            this.ScreenDataPanel.SuspendLayout();
            this.SuspendLayout();
            // 
            // ScreenPanel
            // 
            this.ScreenPanel.Controls.Add(this.ScreenDataPanel);
            this.ScreenPanel.Controls.Add(this.Next);
            this.ScreenPanel.Controls.Add(this.Back);
            this.ScreenPanel.Controls.Add(this.ScreenIDsComboBox);
            this.ScreenPanel.Controls.Add(this.ScreenIDsTextBox);
            this.ScreenPanel.Controls.Add(this.lblScreenID);
            this.ScreenPanel.Dock = System.Windows.Forms.DockStyle.Top;
            this.ScreenPanel.Location = new System.Drawing.Point(0, 0);
            this.ScreenPanel.Name = "ScreenPanel";
            this.ScreenPanel.Size = new System.Drawing.Size(457, 273);
            this.ScreenPanel.TabIndex = 1;
            // 
            // ScreenDataPanel
            // 
            this.ScreenDataPanel.Controls.Add(this.ScreenUbicationLine2);
            this.ScreenDataPanel.Controls.Add(this.ScreenUbicationLine1);
            this.ScreenDataPanel.Controls.Add(this.lblScreenUbication);
            this.ScreenDataPanel.Controls.Add(this.ScreenModel);
            this.ScreenDataPanel.Controls.Add(this.lblScreenModel);
            this.ScreenDataPanel.Controls.Add(this.ScreenBrand);
            this.ScreenDataPanel.Controls.Add(this.lblScreenBrand);
            this.ScreenDataPanel.Controls.Add(this.ScreenDescription);
            this.ScreenDataPanel.Controls.Add(this.lblScreenDescription);
            this.ScreenDataPanel.Font = new System.Drawing.Font("Verdana", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ScreenDataPanel.Location = new System.Drawing.Point(16, 70);
            this.ScreenDataPanel.Name = "ScreenDataPanel";
            this.ScreenDataPanel.Size = new System.Drawing.Size(429, 150);
            this.ScreenDataPanel.TabIndex = 16;
            this.ScreenDataPanel.TabStop = false;
            this.ScreenDataPanel.Text = "Screen Data";
            // 
            // ScreenUbicationLine2
            // 
            this.ScreenUbicationLine2.AutoSize = true;
            this.ScreenUbicationLine2.Font = new System.Drawing.Font("Verdana", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ScreenUbicationLine2.Location = new System.Drawing.Point(18, 125);
            this.ScreenUbicationLine2.Name = "ScreenUbicationLine2";
            this.ScreenUbicationLine2.Size = new System.Drawing.Size(69, 16);
            this.ScreenUbicationLine2.TabIndex = 15;
            this.ScreenUbicationLine2.Text = "Ubication";
            this.ScreenUbicationLine2.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // ScreenUbicationLine1
            // 
            this.ScreenUbicationLine1.AutoSize = true;
            this.ScreenUbicationLine1.Font = new System.Drawing.Font("Verdana", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ScreenUbicationLine1.Location = new System.Drawing.Point(122, 100);
            this.ScreenUbicationLine1.Name = "ScreenUbicationLine1";
            this.ScreenUbicationLine1.Size = new System.Drawing.Size(69, 16);
            this.ScreenUbicationLine1.TabIndex = 14;
            this.ScreenUbicationLine1.Text = "Ubication";
            this.ScreenUbicationLine1.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // lblScreenUbication
            // 
            this.lblScreenUbication.AutoSize = true;
            this.lblScreenUbication.Font = new System.Drawing.Font("Verdana", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblScreenUbication.Location = new System.Drawing.Point(18, 100);
            this.lblScreenUbication.Name = "lblScreenUbication";
            this.lblScreenUbication.Size = new System.Drawing.Size(75, 16);
            this.lblScreenUbication.TabIndex = 13;
            this.lblScreenUbication.Text = "Ubication:";
            this.lblScreenUbication.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // ScreenModel
            // 
            this.ScreenModel.AutoSize = true;
            this.ScreenModel.Font = new System.Drawing.Font("Verdana", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ScreenModel.Location = new System.Drawing.Point(122, 75);
            this.ScreenModel.Name = "ScreenModel";
            this.ScreenModel.Size = new System.Drawing.Size(46, 16);
            this.ScreenModel.TabIndex = 12;
            this.ScreenModel.Text = "Model";
            this.ScreenModel.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // lblScreenModel
            // 
            this.lblScreenModel.AutoSize = true;
            this.lblScreenModel.Font = new System.Drawing.Font("Verdana", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblScreenModel.Location = new System.Drawing.Point(18, 75);
            this.lblScreenModel.Name = "lblScreenModel";
            this.lblScreenModel.Size = new System.Drawing.Size(52, 16);
            this.lblScreenModel.TabIndex = 11;
            this.lblScreenModel.Text = "Model:";
            this.lblScreenModel.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // ScreenBrand
            // 
            this.ScreenBrand.AutoSize = true;
            this.ScreenBrand.Font = new System.Drawing.Font("Verdana", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ScreenBrand.Location = new System.Drawing.Point(122, 50);
            this.ScreenBrand.Name = "ScreenBrand";
            this.ScreenBrand.Size = new System.Drawing.Size(45, 16);
            this.ScreenBrand.TabIndex = 10;
            this.ScreenBrand.Text = "Brand";
            this.ScreenBrand.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // lblScreenBrand
            // 
            this.lblScreenBrand.AutoSize = true;
            this.lblScreenBrand.Font = new System.Drawing.Font("Verdana", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblScreenBrand.Location = new System.Drawing.Point(18, 50);
            this.lblScreenBrand.Name = "lblScreenBrand";
            this.lblScreenBrand.Size = new System.Drawing.Size(51, 16);
            this.lblScreenBrand.TabIndex = 9;
            this.lblScreenBrand.Text = "Brand:";
            this.lblScreenBrand.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // ScreenDescription
            // 
            this.ScreenDescription.AutoSize = true;
            this.ScreenDescription.Font = new System.Drawing.Font("Verdana", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ScreenDescription.Location = new System.Drawing.Point(122, 25);
            this.ScreenDescription.Name = "ScreenDescription";
            this.ScreenDescription.Size = new System.Drawing.Size(81, 16);
            this.ScreenDescription.TabIndex = 8;
            this.ScreenDescription.Text = "Description";
            this.ScreenDescription.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // lblScreenDescription
            // 
            this.lblScreenDescription.AutoSize = true;
            this.lblScreenDescription.Font = new System.Drawing.Font("Verdana", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblScreenDescription.Location = new System.Drawing.Point(18, 25);
            this.lblScreenDescription.Name = "lblScreenDescription";
            this.lblScreenDescription.Size = new System.Drawing.Size(87, 16);
            this.lblScreenDescription.TabIndex = 7;
            this.lblScreenDescription.Text = "Description:";
            this.lblScreenDescription.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // Next
            // 
            this.Next.Font = new System.Drawing.Font("Verdana", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Next.Location = new System.Drawing.Point(218, 239);
            this.Next.Name = "Next";
            this.Next.Size = new System.Drawing.Size(98, 22);
            this.Next.TabIndex = 4;
            this.Next.Tag = "";
            this.Next.Text = "Next";
            this.Next.UseVisualStyleBackColor = true;
            this.Next.Click += new System.EventHandler(this.Next_Click);
            // 
            // Back
            // 
            this.Back.Font = new System.Drawing.Font("Verdana", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Back.Location = new System.Drawing.Point(114, 239);
            this.Back.Name = "Back";
            this.Back.Size = new System.Drawing.Size(98, 22);
            this.Back.TabIndex = 5;
            this.Back.Tag = "";
            this.Back.Text = "Back";
            this.Back.UseVisualStyleBackColor = true;
            this.Back.Click += new System.EventHandler(this.Back_Click);
            // 
            // ScreenIDsComboBox
            // 
            this.ScreenIDsComboBox.BackColor = System.Drawing.SystemColors.Window;
            this.ScreenIDsComboBox.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.ScreenIDsComboBox.Font = new System.Drawing.Font("Verdana", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ScreenIDsComboBox.FormattingEnabled = true;
            this.ScreenIDsComboBox.Location = new System.Drawing.Point(234, 31);
            this.ScreenIDsComboBox.MaxDropDownItems = 100;
            this.ScreenIDsComboBox.Name = "ScreenIDsComboBox";
            this.ScreenIDsComboBox.Size = new System.Drawing.Size(121, 22);
            this.ScreenIDsComboBox.TabIndex = 2;
            this.ScreenIDsComboBox.SelectedIndexChanged += new System.EventHandler(this.ScreenIDsComboBox_SelectedIndexChanged);
            // 
            // ScreenIDsTextBox
            // 
            this.ScreenIDsTextBox.Font = new System.Drawing.Font("Verdana", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ScreenIDsTextBox.Location = new System.Drawing.Point(114, 31);
            this.ScreenIDsTextBox.Name = "ScreenIDsTextBox";
            this.ScreenIDsTextBox.Size = new System.Drawing.Size(121, 22);
            this.ScreenIDsTextBox.TabIndex = 1;
            // 
            // lblScreenID
            // 
            this.lblScreenID.AutoSize = true;
            this.lblScreenID.Font = new System.Drawing.Font("Verdana", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblScreenID.Location = new System.Drawing.Point(24, 33);
            this.lblScreenID.Name = "lblScreenID";
            this.lblScreenID.Size = new System.Drawing.Size(79, 16);
            this.lblScreenID.TabIndex = 5;
            this.lblScreenID.Text = "Screen ID:";
            // 
            // FrmConfig_ScreenSelect
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(457, 273);
            this.Controls.Add(this.ScreenPanel);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "FrmConfig_ScreenSelect";
            this.ShowInTaskbar = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.Manual;
            this.Text = "FrmConfig";
            this.Activated += new System.EventHandler(this.FrmConfig_ScreenSelect_Activated);
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.FrmConfig_ScreenSelect_FormClosing);
            this.Load += new System.EventHandler(this.FrmConfig_ScreenSelect_Load);
            this.ScreenPanel.ResumeLayout(false);
            this.ScreenPanel.PerformLayout();
            this.ScreenDataPanel.ResumeLayout(false);
            this.ScreenDataPanel.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Panel ScreenPanel;
        public System.Windows.Forms.ComboBox ScreenIDsComboBox;
        private System.Windows.Forms.TextBox ScreenIDsTextBox;
        private System.Windows.Forms.Label lblScreenID;
        private System.Windows.Forms.Button Next;
        private System.Windows.Forms.Button Back;
        private System.Windows.Forms.GroupBox ScreenDataPanel;
        private System.Windows.Forms.Label ScreenUbicationLine2;
        private System.Windows.Forms.Label ScreenUbicationLine1;
        private System.Windows.Forms.Label lblScreenUbication;
        private System.Windows.Forms.Label ScreenModel;
        private System.Windows.Forms.Label lblScreenModel;
        private System.Windows.Forms.Label ScreenBrand;
        private System.Windows.Forms.Label lblScreenBrand;
        private System.Windows.Forms.Label ScreenDescription;
        private System.Windows.Forms.Label lblScreenDescription;
    }
}