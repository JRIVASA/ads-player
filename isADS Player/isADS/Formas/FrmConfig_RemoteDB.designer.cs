﻿namespace isADS
{
    partial class FrmConfig_RemoteDB
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.DBAccessPanel = new System.Windows.Forms.Panel();
            this.FilesSyncPanel = new System.Windows.Forms.GroupBox();
            this.ChangeDir = new System.Windows.Forms.Button();
            this.ShowDir = new System.Windows.Forms.Button();
            this.LocalFilesSource = new System.Windows.Forms.TextBox();
            this.lblLocalFilesSource = new System.Windows.Forms.Label();
            this.OnlineFilesSync = new System.Windows.Forms.RadioButton();
            this.LocalFilesSync = new System.Windows.Forms.RadioButton();
            this.lblOnlineConfig = new System.Windows.Forms.Button();
            this.lblCustomServer = new System.Windows.Forms.Button();
            this.DBName = new System.Windows.Forms.TextBox();
            this.lblDBName = new System.Windows.Forms.Label();
            this.lblServerInstance = new System.Windows.Forms.Label();
            this.ServerInstance = new System.Windows.Forms.TextBox();
            this.Next = new System.Windows.Forms.Button();
            this.Back = new System.Windows.Forms.Button();
            this.lblWinAuthText = new System.Windows.Forms.Label();
            this.SQLPassword = new System.Windows.Forms.TextBox();
            this.lblSQLPassword = new System.Windows.Forms.Label();
            this.lblSQLUser = new System.Windows.Forms.Label();
            this.SQLUser = new System.Windows.Forms.TextBox();
            this.SQLCredentials = new System.Windows.Forms.RadioButton();
            this.SQLWindowsAuthentication = new System.Windows.Forms.RadioButton();
            this.lblDatabaseAccess = new System.Windows.Forms.Label();
            this.DBAccessPanel.SuspendLayout();
            this.FilesSyncPanel.SuspendLayout();
            this.SuspendLayout();
            // 
            // DBAccessPanel
            // 
            this.DBAccessPanel.Controls.Add(this.FilesSyncPanel);
            this.DBAccessPanel.Controls.Add(this.lblOnlineConfig);
            this.DBAccessPanel.Controls.Add(this.lblCustomServer);
            this.DBAccessPanel.Controls.Add(this.DBName);
            this.DBAccessPanel.Controls.Add(this.lblDBName);
            this.DBAccessPanel.Controls.Add(this.lblServerInstance);
            this.DBAccessPanel.Controls.Add(this.ServerInstance);
            this.DBAccessPanel.Controls.Add(this.Next);
            this.DBAccessPanel.Controls.Add(this.Back);
            this.DBAccessPanel.Controls.Add(this.lblWinAuthText);
            this.DBAccessPanel.Controls.Add(this.SQLPassword);
            this.DBAccessPanel.Controls.Add(this.lblSQLPassword);
            this.DBAccessPanel.Controls.Add(this.lblSQLUser);
            this.DBAccessPanel.Controls.Add(this.SQLUser);
            this.DBAccessPanel.Controls.Add(this.SQLCredentials);
            this.DBAccessPanel.Controls.Add(this.SQLWindowsAuthentication);
            this.DBAccessPanel.Controls.Add(this.lblDatabaseAccess);
            this.DBAccessPanel.Dock = System.Windows.Forms.DockStyle.Top;
            this.DBAccessPanel.Location = new System.Drawing.Point(0, 0);
            this.DBAccessPanel.Name = "DBAccessPanel";
            this.DBAccessPanel.Size = new System.Drawing.Size(457, 292);
            this.DBAccessPanel.TabIndex = 2;
            // 
            // FilesSyncPanel
            // 
            this.FilesSyncPanel.Controls.Add(this.ChangeDir);
            this.FilesSyncPanel.Controls.Add(this.ShowDir);
            this.FilesSyncPanel.Controls.Add(this.LocalFilesSource);
            this.FilesSyncPanel.Controls.Add(this.lblLocalFilesSource);
            this.FilesSyncPanel.Controls.Add(this.OnlineFilesSync);
            this.FilesSyncPanel.Controls.Add(this.LocalFilesSync);
            this.FilesSyncPanel.Font = new System.Drawing.Font("Verdana", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.FilesSyncPanel.Location = new System.Drawing.Point(16, 136);
            this.FilesSyncPanel.Name = "FilesSyncPanel";
            this.FilesSyncPanel.Size = new System.Drawing.Size(429, 88);
            this.FilesSyncPanel.TabIndex = 14;
            this.FilesSyncPanel.TabStop = false;
            this.FilesSyncPanel.Text = "Files Synchronization";
            // 
            // ChangeDir
            // 
            this.ChangeDir.Font = new System.Drawing.Font("Verdana", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ChangeDir.Location = new System.Drawing.Point(314, 54);
            this.ChangeDir.Name = "ChangeDir";
            this.ChangeDir.Size = new System.Drawing.Size(33, 22);
            this.ChangeDir.TabIndex = 12;
            this.ChangeDir.Tag = "";
            this.ChangeDir.Text = "...";
            this.ChangeDir.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            this.ChangeDir.UseVisualStyleBackColor = true;
            this.ChangeDir.Click += new System.EventHandler(this.ChangeDir_Click);
            // 
            // ShowDir
            // 
            this.ShowDir.Font = new System.Drawing.Font("Verdana", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ShowDir.Location = new System.Drawing.Point(353, 54);
            this.ShowDir.Name = "ShowDir";
            this.ShowDir.Size = new System.Drawing.Size(25, 22);
            this.ShowDir.TabIndex = 11;
            this.ShowDir.Tag = "";
            this.ShowDir.Text = "+";
            this.ShowDir.UseVisualStyleBackColor = true;
            this.ShowDir.Click += new System.EventHandler(this.ShowDir_Click);
            // 
            // LocalFilesSource
            // 
            this.LocalFilesSource.Font = new System.Drawing.Font("Verdana", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LocalFilesSource.Location = new System.Drawing.Point(116, 54);
            this.LocalFilesSource.Name = "LocalFilesSource";
            this.LocalFilesSource.Size = new System.Drawing.Size(192, 22);
            this.LocalFilesSource.TabIndex = 8;
            this.LocalFilesSource.TextChanged += new System.EventHandler(this.LocalFilesSource_TextChanged);
            // 
            // lblLocalFilesSource
            // 
            this.lblLocalFilesSource.AutoSize = true;
            this.lblLocalFilesSource.Font = new System.Drawing.Font("Verdana", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblLocalFilesSource.Location = new System.Drawing.Point(24, 56);
            this.lblLocalFilesSource.Name = "lblLocalFilesSource";
            this.lblLocalFilesSource.Size = new System.Drawing.Size(94, 16);
            this.lblLocalFilesSource.TabIndex = 7;
            this.lblLocalFilesSource.Text = "Files Source:";
            this.lblLocalFilesSource.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // OnlineFilesSync
            // 
            this.OnlineFilesSync.AutoSize = true;
            this.OnlineFilesSync.Font = new System.Drawing.Font("Verdana", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.OnlineFilesSync.Location = new System.Drawing.Point(243, 25);
            this.OnlineFilesSync.Name = "OnlineFilesSync";
            this.OnlineFilesSync.Size = new System.Drawing.Size(90, 18);
            this.OnlineFilesSync.TabIndex = 6;
            this.OnlineFilesSync.Text = "Direct Link";
            this.OnlineFilesSync.UseVisualStyleBackColor = true;
            this.OnlineFilesSync.CheckedChanged += new System.EventHandler(this.OnlineFilesSync_CheckedChanged);
            // 
            // LocalFilesSync
            // 
            this.LocalFilesSync.AutoSize = true;
            this.LocalFilesSync.Checked = true;
            this.LocalFilesSync.Font = new System.Drawing.Font("Verdana", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LocalFilesSync.Location = new System.Drawing.Point(27, 25);
            this.LocalFilesSync.Name = "LocalFilesSync";
            this.LocalFilesSync.Size = new System.Drawing.Size(128, 18);
            this.LocalFilesSync.TabIndex = 5;
            this.LocalFilesSync.TabStop = true;
            this.LocalFilesSync.Text = "Local Repository";
            this.LocalFilesSync.UseVisualStyleBackColor = true;
            this.LocalFilesSync.CheckedChanged += new System.EventHandler(this.LocalFilesSync_CheckedChanged);
            // 
            // lblOnlineConfig
            // 
            this.lblOnlineConfig.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.lblOnlineConfig.Font = new System.Drawing.Font("Verdana", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblOnlineConfig.Location = new System.Drawing.Point(346, 7);
            this.lblOnlineConfig.Name = "lblOnlineConfig";
            this.lblOnlineConfig.Size = new System.Drawing.Size(98, 22);
            this.lblOnlineConfig.TabIndex = 2;
            this.lblOnlineConfig.Tag = "";
            this.lblOnlineConfig.Text = "Online";
            this.lblOnlineConfig.UseVisualStyleBackColor = true;
            this.lblOnlineConfig.Click += new System.EventHandler(this.lblOnlineConfig_Click);
            // 
            // lblCustomServer
            // 
            this.lblCustomServer.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.lblCustomServer.Font = new System.Drawing.Font("Verdana", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblCustomServer.Location = new System.Drawing.Point(146, 7);
            this.lblCustomServer.Name = "lblCustomServer";
            this.lblCustomServer.Size = new System.Drawing.Size(178, 22);
            this.lblCustomServer.TabIndex = 1;
            this.lblCustomServer.Tag = "";
            this.lblCustomServer.Text = "Custom Server";
            this.lblCustomServer.UseVisualStyleBackColor = true;
            this.lblCustomServer.Click += new System.EventHandler(this.lblCustomServer_Click);
            // 
            // DBName
            // 
            this.DBName.Font = new System.Drawing.Font("Verdana", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.DBName.Location = new System.Drawing.Point(347, 66);
            this.DBName.Name = "DBName";
            this.DBName.Size = new System.Drawing.Size(97, 22);
            this.DBName.TabIndex = 6;
            this.DBName.TextChanged += new System.EventHandler(this.DBName_TextChanged);
            // 
            // lblDBName
            // 
            this.lblDBName.AutoSize = true;
            this.lblDBName.Font = new System.Drawing.Font("Verdana", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblDBName.Location = new System.Drawing.Point(258, 67);
            this.lblDBName.Name = "lblDBName";
            this.lblDBName.Size = new System.Drawing.Size(72, 16);
            this.lblDBName.TabIndex = 13;
            this.lblDBName.Text = "DB Name:";
            this.lblDBName.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // lblServerInstance
            // 
            this.lblServerInstance.AutoSize = true;
            this.lblServerInstance.Font = new System.Drawing.Font("Verdana", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblServerInstance.Location = new System.Drawing.Point(13, 67);
            this.lblServerInstance.Name = "lblServerInstance";
            this.lblServerInstance.Size = new System.Drawing.Size(120, 16);
            this.lblServerInstance.TabIndex = 12;
            this.lblServerInstance.Text = "Server Instance:";
            this.lblServerInstance.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // ServerInstance
            // 
            this.ServerInstance.Font = new System.Drawing.Font("Verdana", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ServerInstance.Location = new System.Drawing.Point(155, 65);
            this.ServerInstance.Name = "ServerInstance";
            this.ServerInstance.Size = new System.Drawing.Size(90, 22);
            this.ServerInstance.TabIndex = 5;
            this.ServerInstance.TextChanged += new System.EventHandler(this.ServerInstance_TextChanged);
            // 
            // Next
            // 
            this.Next.Font = new System.Drawing.Font("Verdana", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Next.Location = new System.Drawing.Point(236, 240);
            this.Next.Name = "Next";
            this.Next.Size = new System.Drawing.Size(98, 22);
            this.Next.TabIndex = 9;
            this.Next.Tag = "";
            this.Next.Text = "Next";
            this.Next.UseVisualStyleBackColor = true;
            this.Next.Click += new System.EventHandler(this.Next_Click);
            // 
            // Back
            // 
            this.Back.Font = new System.Drawing.Font("Verdana", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Back.Location = new System.Drawing.Point(132, 240);
            this.Back.Name = "Back";
            this.Back.Size = new System.Drawing.Size(98, 22);
            this.Back.TabIndex = 10;
            this.Back.Tag = "";
            this.Back.Text = "Back";
            this.Back.UseVisualStyleBackColor = true;
            this.Back.Click += new System.EventHandler(this.Back_Click);
            // 
            // lblWinAuthText
            // 
            this.lblWinAuthText.AutoSize = true;
            this.lblWinAuthText.Font = new System.Drawing.Font("Verdana", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblWinAuthText.Location = new System.Drawing.Point(65, 265);
            this.lblWinAuthText.Name = "lblWinAuthText";
            this.lblWinAuthText.Size = new System.Drawing.Size(324, 16);
            this.lblWinAuthText.TabIndex = 8;
            this.lblWinAuthText.Text = "Text to show for Windows Authentication Mode";
            // 
            // SQLPassword
            // 
            this.SQLPassword.Font = new System.Drawing.Font("Verdana", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.SQLPassword.Location = new System.Drawing.Point(347, 96);
            this.SQLPassword.Name = "SQLPassword";
            this.SQLPassword.PasswordChar = '*';
            this.SQLPassword.Size = new System.Drawing.Size(97, 22);
            this.SQLPassword.TabIndex = 8;
            this.SQLPassword.UseSystemPasswordChar = true;
            this.SQLPassword.TextChanged += new System.EventHandler(this.SQLPassword_TextChanged);
            // 
            // lblSQLPassword
            // 
            this.lblSQLPassword.AutoSize = true;
            this.lblSQLPassword.Font = new System.Drawing.Font("Verdana", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblSQLPassword.Location = new System.Drawing.Point(258, 97);
            this.lblSQLPassword.Name = "lblSQLPassword";
            this.lblSQLPassword.Size = new System.Drawing.Size(76, 16);
            this.lblSQLPassword.TabIndex = 6;
            this.lblSQLPassword.Text = "Password:";
            this.lblSQLPassword.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // lblSQLUser
            // 
            this.lblSQLUser.AutoSize = true;
            this.lblSQLUser.Font = new System.Drawing.Font("Verdana", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblSQLUser.Location = new System.Drawing.Point(13, 97);
            this.lblSQLUser.Name = "lblSQLUser";
            this.lblSQLUser.Size = new System.Drawing.Size(82, 16);
            this.lblSQLUser.TabIndex = 5;
            this.lblSQLUser.Text = "User Login:";
            this.lblSQLUser.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // SQLUser
            // 
            this.SQLUser.Font = new System.Drawing.Font("Verdana", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.SQLUser.Location = new System.Drawing.Point(155, 95);
            this.SQLUser.Name = "SQLUser";
            this.SQLUser.Size = new System.Drawing.Size(90, 22);
            this.SQLUser.TabIndex = 7;
            this.SQLUser.TextChanged += new System.EventHandler(this.SQLUser_TextChanged);
            // 
            // SQLCredentials
            // 
            this.SQLCredentials.AutoSize = true;
            this.SQLCredentials.Font = new System.Drawing.Font("Verdana", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.SQLCredentials.Location = new System.Drawing.Point(347, 35);
            this.SQLCredentials.Name = "SQLCredentials";
            this.SQLCredentials.Size = new System.Drawing.Size(97, 18);
            this.SQLCredentials.TabIndex = 4;
            this.SQLCredentials.Text = "Credentials";
            this.SQLCredentials.UseVisualStyleBackColor = true;
            this.SQLCredentials.CheckedChanged += new System.EventHandler(this.SQLCredentials_CheckedChanged);
            // 
            // SQLWindowsAuthentication
            // 
            this.SQLWindowsAuthentication.AutoSize = true;
            this.SQLWindowsAuthentication.Checked = true;
            this.SQLWindowsAuthentication.Font = new System.Drawing.Font("Verdana", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.SQLWindowsAuthentication.Location = new System.Drawing.Point(146, 35);
            this.SQLWindowsAuthentication.Name = "SQLWindowsAuthentication";
            this.SQLWindowsAuthentication.Size = new System.Drawing.Size(178, 18);
            this.SQLWindowsAuthentication.TabIndex = 3;
            this.SQLWindowsAuthentication.TabStop = true;
            this.SQLWindowsAuthentication.Text = "Windows Authentication";
            this.SQLWindowsAuthentication.UseVisualStyleBackColor = true;
            this.SQLWindowsAuthentication.CheckedChanged += new System.EventHandler(this.SQLWindowsAuthentication_CheckedChanged);
            // 
            // lblDatabaseAccess
            // 
            this.lblDatabaseAccess.AutoSize = true;
            this.lblDatabaseAccess.Font = new System.Drawing.Font("Verdana", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblDatabaseAccess.Location = new System.Drawing.Point(12, 35);
            this.lblDatabaseAccess.Name = "lblDatabaseAccess";
            this.lblDatabaseAccess.Size = new System.Drawing.Size(128, 16);
            this.lblDatabaseAccess.TabIndex = 1;
            this.lblDatabaseAccess.Text = "Database Access:";
            this.lblDatabaseAccess.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // FrmConfig_RemoteDB
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(457, 290);
            this.Controls.Add(this.DBAccessPanel);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "FrmConfig_RemoteDB";
            this.ShowInTaskbar = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.Manual;
            this.Text = "FrmConfig";
            this.Activated += new System.EventHandler(this.FrmConfig_RemoteDB_Activated);
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.FrmConfig_RemoteDB_FormClosing);
            this.Load += new System.EventHandler(this.FrmConfig_RemoteDB_Load);
            this.DBAccessPanel.ResumeLayout(false);
            this.DBAccessPanel.PerformLayout();
            this.FilesSyncPanel.ResumeLayout(false);
            this.FilesSyncPanel.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Panel DBAccessPanel;
        private System.Windows.Forms.TextBox SQLPassword;
        private System.Windows.Forms.Label lblSQLPassword;
        private System.Windows.Forms.Label lblSQLUser;
        private System.Windows.Forms.TextBox SQLUser;
        private System.Windows.Forms.RadioButton SQLCredentials;
        private System.Windows.Forms.RadioButton SQLWindowsAuthentication;
        private System.Windows.Forms.Label lblDatabaseAccess;
        private System.Windows.Forms.Label lblWinAuthText;
        private System.Windows.Forms.Button Next;
        private System.Windows.Forms.Button Back;
        private System.Windows.Forms.Button lblOnlineConfig;
        private System.Windows.Forms.Button lblCustomServer;
        private System.Windows.Forms.TextBox DBName;
        private System.Windows.Forms.Label lblDBName;
        private System.Windows.Forms.Label lblServerInstance;
        private System.Windows.Forms.TextBox ServerInstance;
        private System.Windows.Forms.GroupBox FilesSyncPanel;
        private System.Windows.Forms.Button ChangeDir;
        private System.Windows.Forms.Button ShowDir;
        private System.Windows.Forms.TextBox LocalFilesSource;
        private System.Windows.Forms.Label lblLocalFilesSource;
        private System.Windows.Forms.RadioButton OnlineFilesSync;
        private System.Windows.Forms.RadioButton LocalFilesSync;
    }
}