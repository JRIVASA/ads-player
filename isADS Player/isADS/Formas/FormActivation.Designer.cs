﻿namespace isADS
{
    partial class FormActivation
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.ButtonTrue = new System.Windows.Forms.Button();
            this.InputKey = new System.Windows.Forms.TextBox();
            this.Serial = new System.Windows.Forms.TextBox();
            this.lblActivationKey = new System.Windows.Forms.Label();
            this.ButtonCopySerial = new System.Windows.Forms.Button();
            this.ButtonFalse = new System.Windows.Forms.Button();
            this.lblStep1 = new System.Windows.Forms.Label();
            this.lblStep2 = new System.Windows.Forms.Label();
            this.lblStep3 = new System.Windows.Forms.Label();
            this.lblStep4 = new System.Windows.Forms.Label();
            this.lblStep2Link = new System.Windows.Forms.LinkLabel();
            this.lblStep5 = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // ButtonTrue
            // 
            this.ButtonTrue.Font = new System.Drawing.Font("Verdana", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ButtonTrue.Location = new System.Drawing.Point(93, 355);
            this.ButtonTrue.Name = "ButtonTrue";
            this.ButtonTrue.Size = new System.Drawing.Size(122, 38);
            this.ButtonTrue.TabIndex = 0;
            this.ButtonTrue.Text = "Activate";
            this.ButtonTrue.UseVisualStyleBackColor = true;
            this.ButtonTrue.Click += new System.EventHandler(this.ButtonTrue_Click);
            // 
            // InputKey
            // 
            this.InputKey.Font = new System.Drawing.Font("Verdana", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.InputKey.Location = new System.Drawing.Point(31, 296);
            this.InputKey.Multiline = true;
            this.InputKey.Name = "InputKey";
            this.InputKey.Size = new System.Drawing.Size(416, 48);
            this.InputKey.TabIndex = 2;
            this.InputKey.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.InputKey.TextChanged += new System.EventHandler(this.InputKey_TextChanged);
            // 
            // Serial
            // 
            this.Serial.Font = new System.Drawing.Font("Verdana", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Serial.Location = new System.Drawing.Point(31, 210);
            this.Serial.Multiline = true;
            this.Serial.Name = "Serial";
            this.Serial.ReadOnly = true;
            this.Serial.Size = new System.Drawing.Size(416, 48);
            this.Serial.TabIndex = 3;
            this.Serial.TabStop = false;
            this.Serial.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // lblActivationKey
            // 
            this.lblActivationKey.AutoSize = true;
            this.lblActivationKey.Font = new System.Drawing.Font("Verdana", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblActivationKey.Location = new System.Drawing.Point(128, 277);
            this.lblActivationKey.Name = "lblActivationKey";
            this.lblActivationKey.Size = new System.Drawing.Size(203, 16);
            this.lblActivationKey.TabIndex = 5;
            this.lblActivationKey.Text = "Insert your Activation Key:";
            // 
            // ButtonCopySerial
            // 
            this.ButtonCopySerial.Font = new System.Drawing.Font("Verdana", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ButtonCopySerial.Location = new System.Drawing.Point(131, 168);
            this.ButtonCopySerial.Name = "ButtonCopySerial";
            this.ButtonCopySerial.Size = new System.Drawing.Size(196, 36);
            this.ButtonCopySerial.TabIndex = 6;
            this.ButtonCopySerial.Text = "Copy Product Serial";
            this.ButtonCopySerial.UseVisualStyleBackColor = true;
            this.ButtonCopySerial.Click += new System.EventHandler(this.ButtonCopySerial_Click);
            // 
            // ButtonFalse
            // 
            this.ButtonFalse.Font = new System.Drawing.Font("Verdana", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ButtonFalse.Location = new System.Drawing.Point(233, 355);
            this.ButtonFalse.Name = "ButtonFalse";
            this.ButtonFalse.Size = new System.Drawing.Size(122, 38);
            this.ButtonFalse.TabIndex = 7;
            this.ButtonFalse.Text = "Cancel";
            this.ButtonFalse.UseVisualStyleBackColor = true;
            this.ButtonFalse.Click += new System.EventHandler(this.ButtonFalse_Click);
            // 
            // lblStep1
            // 
            this.lblStep1.AutoSize = true;
            this.lblStep1.Font = new System.Drawing.Font("Verdana", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblStep1.Location = new System.Drawing.Point(45, 9);
            this.lblStep1.Name = "lblStep1";
            this.lblStep1.Size = new System.Drawing.Size(205, 16);
            this.lblStep1.TabIndex = 8;
            this.lblStep1.Text = "(1) Copy the Product Serial";
            // 
            // lblStep2
            // 
            this.lblStep2.AutoSize = true;
            this.lblStep2.Font = new System.Drawing.Font("Verdana", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblStep2.Location = new System.Drawing.Point(45, 37);
            this.lblStep2.Name = "lblStep2";
            this.lblStep2.Size = new System.Drawing.Size(77, 16);
            this.lblStep2.TabIndex = 9;
            this.lblStep2.Text = "(2) Go to ";
            // 
            // lblStep3
            // 
            this.lblStep3.AutoSize = true;
            this.lblStep3.Font = new System.Drawing.Font("Verdana", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblStep3.Location = new System.Drawing.Point(45, 66);
            this.lblStep3.Name = "lblStep3";
            this.lblStep3.Size = new System.Drawing.Size(302, 16);
            this.lblStep3.TabIndex = 10;
            this.lblStep3.Text = "(3) Log-in and Enter the Screens section";
            // 
            // lblStep4
            // 
            this.lblStep4.AutoSize = true;
            this.lblStep4.Font = new System.Drawing.Font("Verdana", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblStep4.Location = new System.Drawing.Point(45, 96);
            this.lblStep4.Name = "lblStep4";
            this.lblStep4.Size = new System.Drawing.Size(295, 16);
            this.lblStep4.TabIndex = 11;
            this.lblStep4.Text = "(4) Select the Screen with following ID: ";
            // 
            // lblStep2Link
            // 
            this.lblStep2Link.AutoSize = true;
            this.lblStep2Link.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblStep2Link.Location = new System.Drawing.Point(128, 37);
            this.lblStep2Link.Name = "lblStep2Link";
            this.lblStep2Link.Size = new System.Drawing.Size(296, 16);
            this.lblStep2Link.TabIndex = 12;
            this.lblStep2Link.TabStop = true;
            this.lblStep2Link.Text = "http://www.sgnge.com/isADS/Default.aspx";
            this.lblStep2Link.LinkClicked += new System.Windows.Forms.LinkLabelLinkClickedEventHandler(this.lblStep2Link_LinkClicked);
            // 
            // lblStep5
            // 
            this.lblStep5.AutoSize = true;
            this.lblStep5.Font = new System.Drawing.Font("Verdana", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblStep5.Location = new System.Drawing.Point(45, 125);
            this.lblStep5.Name = "lblStep5";
            this.lblStep5.Size = new System.Drawing.Size(353, 16);
            this.lblStep5.TabIndex = 13;
            this.lblStep5.Text = "(5) Follow the steps to Generate Activation Key";
            // 
            // FormActivation
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(474, 405);
            this.ControlBox = false;
            this.Controls.Add(this.lblStep5);
            this.Controls.Add(this.lblStep2Link);
            this.Controls.Add(this.lblStep4);
            this.Controls.Add(this.lblStep3);
            this.Controls.Add(this.lblStep2);
            this.Controls.Add(this.lblStep1);
            this.Controls.Add(this.ButtonFalse);
            this.Controls.Add(this.ButtonCopySerial);
            this.Controls.Add(this.lblActivationKey);
            this.Controls.Add(this.Serial);
            this.Controls.Add(this.InputKey);
            this.Controls.Add(this.ButtonTrue);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "FormActivation";
            this.ShowInTaskbar = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Activated += new System.EventHandler(this.FormActivation_Activated);
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.FormInput_FormClosing);
            this.FormClosed += new System.Windows.Forms.FormClosedEventHandler(this.FormActivation_FormClosed);
            this.Load += new System.EventHandler(this.FormActivation_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        public System.Windows.Forms.Button ButtonTrue;
        public System.Windows.Forms.TextBox InputKey;
        public System.Windows.Forms.TextBox Serial;
        private System.Windows.Forms.Label lblActivationKey;
        public System.Windows.Forms.Button ButtonCopySerial;
        public System.Windows.Forms.Button ButtonFalse;
        private System.Windows.Forms.Label lblStep1;
        private System.Windows.Forms.Label lblStep2;
        private System.Windows.Forms.Label lblStep3;
        private System.Windows.Forms.Label lblStep4;
        private System.Windows.Forms.LinkLabel lblStep2Link;
        private System.Windows.Forms.Label lblStep5;
    }
}