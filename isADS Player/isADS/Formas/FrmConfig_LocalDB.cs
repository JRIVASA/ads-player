﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Data.SqlClient;
using isADS.Clases;

namespace isADS
{
    public partial class FrmConfig_LocalDB : Form
    {

        private Boolean ApplicationInit = false;
        private Boolean ConfigInitCheck = true;
        String Action = String.Empty;
        private Boolean OriginalWindowsAuthValue = false;
        private String OriginalSQLUserValue = String.Empty;
        private String OriginalSQLPassValue = String.Empty;
        private Boolean CurrentWindowsAuthValue;
        private String CurrentSQLUserValue;
        private String CurrentSQLPassValue;
        private Boolean OnPurposeActivation = false;

        private Form PreviousForm = null;
        private Form NextForm = null;

        private Boolean IgnoreDefaultActivation()
        {
            OnPurposeActivation = true; return OnPurposeActivation;
        }

        public FrmConfig_LocalDB()
        {
            InitializeComponent();
        }

        public FrmConfig_LocalDB(Boolean AppInit)
        {
            InitializeComponent();
            this.ApplicationInit = AppInit;
        }

        private void FrmConfig_LocalDB_Load(object sender, EventArgs e)
        {
            this.components = new System.ComponentModel.Container();
            this.DoubleBuffered = true;

            this.Left = (System.Windows.Forms.Screen.PrimaryScreen.WorkingArea.Width / 2) - (this.Width / 2);
            this.Top = Convert.ToInt32(System.Windows.Forms.Screen.PrimaryScreen.WorkingArea.Height * 0.10);

            lblWinAuthText.Left = lblSQLUser.Left; lblWinAuthText.Top = lblSQLUser.Top;

            OriginalWindowsAuthValue = Properties.Settings.Default.LocalDB_WindowsAuth;
            CurrentWindowsAuthValue = OriginalWindowsAuthValue;

            OriginalSQLUserValue = Functions.DecryptedSQLUser; // Properties.Settings.Default.SQLUser;
            OriginalSQLPassValue = Functions.DecryptedSQLPassword; // Properties.Settings.Default.SQLPass;
            CurrentSQLUserValue = OriginalSQLUserValue;
            CurrentSQLPassValue = OriginalSQLPassValue;

            if (CurrentWindowsAuthValue)
            { SQLWindowsAuthentication.Checked = true; SQLCredentials_CheckedChanged(null, null); } 
            else 
            { SQLCredentials.Checked = true; }

            //this.ClientSize = new System.Drawing.Size(414, 147);
            //this.Size = this.ClientSize;

            this.ApplyLanguage();            
            this.BringToFront();
        }

        private SqlConnection getConnection()
        {
            if (CurrentWindowsAuthValue)
            {
                return Functions.getAlternateTrustedConnection();
            }
            else
            {
                return Functions.getAlternateConnection(CurrentSQLUserValue, CurrentSQLPassValue);
            }
        }

        private Boolean ValidateSettings()
        {
            //Boolean CorrectSettings = true;

            //if (ApplicationInit)
            //{
            //    if (Action == "Close") 
            //    { return CorrectSettings; }
            //    else if (Action != String.Empty)
            //    {
                    return CheckDatabaseAccess();
            //    }                
                
            //    if (!CheckDatabaseAccess())
            //    {
            //        this.Close();
            //        CorrectSettings = false;
            //    }
            //    else
            //    {
            //        Action = "Close"; this.Close(); return CorrectSettings;
            //    }
            //}
            //else
            //{
            //    CheckDatabaseAccess();
            //}

            //return CorrectSettings;
        }

        private void TriggerDatabaseCheck()
        {
            Timer myCheckDBTimer = ((Timer)this.components.Components.OfType<IComponent>().Where(ti => ti.GetType().FullName == "System.Windows.Forms.Timer")
            .ToList().Where(ti => ((Timer)ti).Tag != null).ToList().Find(ti => ((Timer)ti).Tag.ToString() == "myCheckDBTimer"));

            if (myCheckDBTimer == null)
            {
                myCheckDBTimer = new Timer();
                myCheckDBTimer.Tag = "myCheckDBTimer";
                myCheckDBTimer.Interval = 1500;
                this.components.Add(myCheckDBTimer);
                myCheckDBTimer.Tick += myCheckDBTimer_Tick;
                myCheckDBTimer.Enabled = true;
            }
            else
            {
                myCheckDBTimer.Enabled = false;
                myCheckDBTimer.Interval = 1500;
                myCheckDBTimer.Enabled = true;
            }
        }

        void myCheckDBTimer_Tick(object sender, EventArgs e)
        {
            Timer myCheckDBTimer = ((Timer)sender);

            myCheckDBTimer.Enabled = false;

            CheckDatabaseAccess();

            Functions.setTimeOut(2000, delegate(Object myTimer, System.Timers.ElapsedEventArgs ev)
            {
                try { 
                this.BeginInvoke((Action)(() =>
                {
                    try
                    {
                        if (!myCheckDBTimer.Enabled)
                        {
                            this.lblDatabaseAccess.Text = LanguageResources.GetText("FrmConfig_LocalDB.lblDatabaseAccess");
                            this.components.Remove(myCheckDBTimer); myCheckDBTimer.Dispose(); 
                        }
                    }
                    catch (Exception) { }
                }));
                } catch (Exception Any) { Console.WriteLine(Any.Message); }
                ((System.Timers.Timer)myTimer).Dispose();
            });            
        }

        private Boolean CheckDatabaseAccess()
        {
            lblDatabaseAccess.Text = LanguageResources.GetText("FrmConfig.Connecting");

            Boolean BD = Functions.CheckDBConnection(getConnection());

            if (BD) 
            { lblDatabaseAccess.Text = LanguageResources.GetText("FrmConfig.DBAccessGranted"); }
            else
            { lblDatabaseAccess.Text = LanguageResources.GetText("FrmConfig.DBAccessError"); }

            return BD;
        }             

        private void FrmConfig_LocalDB_Activated(object sender, EventArgs e)
        {
            if (OnPurposeActivation) 
            { 
                OnPurposeActivation = false; return;
            }            
            ValidateSettings();
        }

        private void FrmConfig_LocalDB_FormClosing(object sender, FormClosingEventArgs e)
        {
            if (FrmConfig.ApplicationInit && PreviousForm == null && NextForm == null)
            {
                IgnoreDefaultActivation();
                MessageBox.Show(LanguageResources.GetText("FrmConfig.FirstUseConfigurationWarning"));
                e.Cancel = true;
            }
            else
            {
                if (!ValidateSettings())
                {
                    e.Cancel = true;

                    if (ConfigInitCheck)
                    { ConfigInitCheck = false; }
                    else
                    {
                        IgnoreDefaultActivation();
                        MessageBox.Show(LanguageResources.GetText("FrmConfig.MsgDatabaseConnectionNotWorking"));
                    }
                }
                else
                {
                    Boolean DBAccessSettingsChangeWarning = false;

                    if (SQLWindowsAuthentication.Checked)
                    {
                        DBAccessSettingsChangeWarning = (OriginalWindowsAuthValue != SQLWindowsAuthentication.Checked);
                    }
                    else if (SQLCredentials.Checked)
                    {
                        DBAccessSettingsChangeWarning = (OriginalWindowsAuthValue != SQLWindowsAuthentication.Checked);

                        if (!DBAccessSettingsChangeWarning)
                        {
                            DBAccessSettingsChangeWarning = !SQLUser.Text.isUndefined();
                        }
                    }

                    if (DBAccessSettingsChangeWarning)
                    {
                        FormInput Input = new FormInput();

                        Input.Text = "Stellar ADS360";
                        Input.ActionTrue = LanguageResources.GetText("FrmConfig.ConfirmDBSettingsChange"); Input.ButtonTrue.Text = Input.ActionTrue;
                        Input.ActionFalse = LanguageResources.GetText("FrmConfig.DiscardDBSettingsChange"); Input.ButtonFalse.Text = Input.ActionFalse;
                        Input.Message.Text = LanguageResources.GetText("FrmConfig.DBAccessSettingsChangeWarning");
                        //+ Functions.NewLine() + Functions.NewLine() +
                        //LanguageResources.GetText("ActivationWindowChoiceMessageLine2");
                        Input.Message.ReadOnly = true;
                        IgnoreDefaultActivation();
                        Input.ShowDialog();

                        DBAccessSettingsChangeWarning = (Input.Action.Equals(Input.ActionTrue));
                    }

                    if (DBAccessSettingsChangeWarning && SQLWindowsAuthentication.Checked)
                    {
                        Properties.Settings.Default.LocalDB_WindowsAuth = true;
                        Properties.Settings.Default.Save();

                        Functions.ConnectionData = Functions.getConnectionData();
                    }
                    else if (DBAccessSettingsChangeWarning && SQLCredentials.Checked)
                    {
                        Properties.Settings.Default.LocalDB_WindowsAuth = false;

                        Properties.Settings.Default.LocalDB_SQLUser = DataEncryption.EncryptText(this.SQLUser.Text, Functions.EncryptionKey); // Crypt this.SQLUser.Text;
                        Properties.Settings.Default.LocalDB_SQLPass = DataEncryption.EncryptText(this.SQLPassword.Text, Functions.EncryptionKey); // Crypt this.SQLPassword.Text;
                        Properties.Settings.Default.Save();

                        Functions.DecryptedSQLUser = CurrentSQLUserValue;
                        Functions.DecryptedSQLPassword = CurrentSQLPassValue;
                        Functions.ConnectionData = Functions.getConnectionData();
                    }
                    // else Discard Changes.
                    else
                    {
                        // Other Connection Settings unaffected.
                    }
                    // Close:
                    this.Visible = false;
                    if (NextForm != null) NextForm.ShowDialog();
                    else if (PreviousForm != null) PreviousForm.ShowDialog();
                }
            }
        }
      
        private void ApplyLanguage()
        {
            this.Text = LanguageResources.GetText("FrmConfig_LocalDB.Title");

            this.lblDatabaseAccess.Text = LanguageResources.GetText("FrmConfig_LocalDB.lblDatabaseAccess");

            this.SQLWindowsAuthentication.Text = LanguageResources.GetText("FrmConfig.SQLWindowsAuthentication");
            this.SQLCredentials.Text = LanguageResources.GetText("FrmConfig.SQLCredentials");

            this.lblSQLUser.Text = LanguageResources.GetText("FrmConfig.lblSQLUser");
            this.lblSQLPassword.Text = LanguageResources.GetText("FrmConfig.lblSQLPassword");

            this.lblWinAuthText.Text = LanguageResources.GetText("FrmConfig.lblWinAuthText");

            this.Back.Text = LanguageResources.GetText("FrmConfig.GoBack");
            this.Next.Text = LanguageResources.GetText("FrmConfig.GoNext");
        }      

        private void DatabaseAccessSettingsChanged()
        {
            if (SQLWindowsAuthentication.Checked)
            {
                lblSQLUser.Visible = false;
                lblSQLPassword.Visible = false;
                SQLUser.Visible = false;
                SQLPassword.Visible = false;

                lblWinAuthText.Visible = true;

                CurrentWindowsAuthValue = true;
            } else if (SQLCredentials.Checked)
            {
                lblSQLUser.Visible = true;
                lblSQLPassword.Visible = true;
                SQLUser.Visible = true;
                SQLPassword.Visible = true;

                lblWinAuthText.Visible = false;

                CurrentWindowsAuthValue = false;
                if (!this.SQLUser.Text.isUndefined()) { 
                    this.CurrentSQLUserValue = this.SQLUser.Text;
                    this.CurrentSQLPassValue = this.SQLPassword.Text;        
                }
            }

            TriggerDatabaseCheck(); // CheckDatabaseAccess();

        }

        private void SQLWindowsAuthentication_CheckedChanged(object sender, EventArgs e)
        {
            DatabaseAccessSettingsChanged();
        }

        private void SQLCredentials_CheckedChanged(object sender, EventArgs e)
        {
            DatabaseAccessSettingsChanged();
        }

        private void SQLUser_TextChanged(object sender, EventArgs e)
        {
            DatabaseAccessSettingsChanged();
        }

        private void SQLPassword_TextChanged(object sender, EventArgs e)
        {
            DatabaseAccessSettingsChanged();
        }

        private void Next_Click(object sender, EventArgs e)
        {
            NextForm = new FrmConfig_RemoteDB(FrmConfig.ApplicationInit); this.Close();
        }

        private void Back_Click(object sender, EventArgs e)
        {
            PreviousForm = new FrmConfig_Language(FrmConfig.ApplicationInit); this.Close();
        }
    }
}
