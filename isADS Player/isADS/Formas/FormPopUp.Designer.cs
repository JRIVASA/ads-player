﻿namespace isADS
{
    partial class FormPopUp
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.Message = new System.Windows.Forms.Button();
            this.ImgIcon = new System.Windows.Forms.Button();
            this.QuitFocusControl = new System.Windows.Forms.PictureBox();
            ((System.ComponentModel.ISupportInitialize)(this.QuitFocusControl)).BeginInit();
            this.SuspendLayout();
            // 
            // Message
            // 
            this.Message.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(178)))));
            this.Message.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.Message.Dock = System.Windows.Forms.DockStyle.Right;
            this.Message.FlatAppearance.BorderColor = System.Drawing.Color.PaleGreen;
            this.Message.FlatAppearance.BorderSize = 0;
            this.Message.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.Message.Font = new System.Drawing.Font("Verdana", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Message.ForeColor = System.Drawing.Color.Black;
            this.Message.Location = new System.Drawing.Point(100, 0);
            this.Message.Margin = new System.Windows.Forms.Padding(0);
            this.Message.Name = "Message";
            this.Message.Size = new System.Drawing.Size(250, 90);
            this.Message.TabIndex = 0;
            this.Message.TabStop = false;
            this.Message.Text = "An Update is Available!";
            this.Message.UseVisualStyleBackColor = false;
            this.Message.Click += new System.EventHandler(this.Message_Click);
            // 
            // ImgIcon
            // 
            this.ImgIcon.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(178)))));
            this.ImgIcon.BackgroundImage = global::isADS.Properties.Resources.Info8;
            this.ImgIcon.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.ImgIcon.Dock = System.Windows.Forms.DockStyle.Left;
            this.ImgIcon.FlatAppearance.BorderSize = 0;
            this.ImgIcon.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.ImgIcon.Font = new System.Drawing.Font("Verdana", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ImgIcon.ForeColor = System.Drawing.Color.Black;
            this.ImgIcon.Location = new System.Drawing.Point(0, 0);
            this.ImgIcon.Margin = new System.Windows.Forms.Padding(0);
            this.ImgIcon.Name = "ImgIcon";
            this.ImgIcon.Size = new System.Drawing.Size(100, 90);
            this.ImgIcon.TabIndex = 1;
            this.ImgIcon.TabStop = false;
            this.ImgIcon.UseVisualStyleBackColor = false;
            this.ImgIcon.Click += new System.EventHandler(this.ImgIcon_Click);
            // 
            // QuitFocusControl
            // 
            this.QuitFocusControl.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(178)))));
            this.QuitFocusControl.Location = new System.Drawing.Point(104, 28);
            this.QuitFocusControl.Name = "QuitFocusControl";
            this.QuitFocusControl.Size = new System.Drawing.Size(1, 1);
            this.QuitFocusControl.TabIndex = 2;
            this.QuitFocusControl.TabStop = false;
            // 
            // FormPopUp
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.PaleGreen;
            this.ClientSize = new System.Drawing.Size(350, 90);
            this.Controls.Add(this.QuitFocusControl);
            this.Controls.Add(this.ImgIcon);
            this.Controls.Add(this.Message);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.MaximumSize = this.MinimumSize;
            this.MinimumSize = this.ClientSize;
            this.Name = "FormPopUp";
            this.Opacity = 0.75D;
            this.ShowIcon = false;
            this.ShowInTaskbar = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.Manual;
            this.Text = "FormPopUp";
            this.Activated += new System.EventHandler(this.FormPopUp_Activated);
            this.Deactivate += new System.EventHandler(this.FormPopUp_Deactivate);
            this.Load += new System.EventHandler(this.FormPopUp_Load);
            this.Resize += new System.EventHandler(this.FormPopUp_Resize);
            ((System.ComponentModel.ISupportInitialize)(this.QuitFocusControl)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        public System.Windows.Forms.Button Message;
        public System.Windows.Forms.Button ImgIcon;
        public System.Windows.Forms.PictureBox QuitFocusControl;
    }
}