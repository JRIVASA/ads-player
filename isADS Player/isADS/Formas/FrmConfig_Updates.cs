﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Data.SqlClient;
using isADS.Clases;

namespace isADS
{
    public partial class FrmConfig_Updates : Form
    {
        private Boolean ApplicationInit = false;
        private Boolean OnPurposeActivation = false;        

        private Boolean IgnoreDefaultActivation()
        {
            OnPurposeActivation = true; return OnPurposeActivation;
        }

        public FrmConfig_Updates()
        {
            InitializeComponent();
        }

        public FrmConfig_Updates(Boolean AppInit)
        {
            InitializeComponent();
            this.ApplicationInit = AppInit;
        }

        //private Form PreviousForm = null;
        private Form NextForm = null;

        private void FrmConfig_Updates_Load(object sender, EventArgs e)
        {
            this.components = new System.ComponentModel.Container();
            this.DoubleBuffered = true;

            this.Left = (System.Windows.Forms.Screen.PrimaryScreen.WorkingArea.Width / 2) - (this.Width / 2);
            this.Top = Convert.ToInt32(System.Windows.Forms.Screen.PrimaryScreen.WorkingArea.Height * 0.10);

            IgnoreDefaultActivation();
            NumberOfCampaigns.Value = (Decimal)Properties.Settings.Default.SoftwareUpdate_CampaignsInterval;
            IgnoreDefaultActivation();
            NumberOfCampaigns.Maximum = 250;
            IgnoreDefaultActivation();
            NumberOfCampaigns.Minimum = 1;
            IgnoreDefaultActivation();
            CheckForUpdates.Checked = Properties.Settings.Default.SoftwareUpdate_CheckForUpdates;
            
            OnPurposeActivation = false;           

            this.ApplyLanguage();            
            this.BringToFront();
        }

        private void ApplyLanguage()
        {

            this.Text = LanguageResources.GetText("FrmConfig.Title");
            
            this.lblUpdate.Text = LanguageResources.GetText("FrmConfig_Updates.lblUpdate");

            this.CheckForUpdates.Text = LanguageResources.GetText("FrmConfig_Updates.CheckForUpdates");

            this.lblNumberOfCampaigns.Text = LanguageResources.GetText("FrmConfig_Updates.lblNumberOfCampaigns");

            this.CloseBtn.Text = LanguageResources.GetText("FrmConfig.Finish");

        }

        private void FrmConfig_Updates_FormClosing(object sender, FormClosingEventArgs e)
        {
            if (FrmConfig.ApplicationInit && NextForm == null)
            {
                //IgnoreDefaultActivation();
                //MessageBox.Show(LanguageResources.GetText("FrmConfig.FirstUseConfigurationWarning"));
                //e.Cancel = true;
            }
            else
            {
                this.Visible = false; if (NextForm != null) NextForm.ShowDialog();
            }            
        }

        private void CloseBtn_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void CheckForUpdates_CheckedChanged(object sender, EventArgs e)
        {
            if (!OnPurposeActivation)
            {
                Properties.Settings.Default.SoftwareUpdate_CheckForUpdates = CheckForUpdates.Checked;
                Properties.Settings.Default.Save();
            }
        }

        private void NumberOfCampaigns_ValueChanged(object sender, EventArgs e)
        {
            if (!OnPurposeActivation)
            {
                Properties.Settings.Default.SoftwareUpdate_CampaignsInterval = (Int32)NumberOfCampaigns.Value;
                Properties.Settings.Default.Save();
            }
        }

    }
}
