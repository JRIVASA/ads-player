﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Data.SqlClient;
using isADS.Clases;

namespace isADS
{
    public partial class FrmConfig_SyncInterval : Form
    {
        private Boolean ApplicationInit = false;
        private Boolean OnPurposeActivation = false;      

        private Boolean IgnoreDefaultActivation()
        {
            OnPurposeActivation = true; return OnPurposeActivation;
        }

        public FrmConfig_SyncInterval()
        {
            InitializeComponent();
        }

        public FrmConfig_SyncInterval(Boolean AppInit)
        {
            InitializeComponent();
            this.ApplicationInit = AppInit;
        }

        //private Form PreviousForm = null;
        private Form NextForm = null;

        private void FrmConfig_SyncInterval_Load(object sender, EventArgs e)
        {
            this.components = new System.ComponentModel.Container();
            this.DoubleBuffered = true;

            this.Left = (System.Windows.Forms.Screen.PrimaryScreen.WorkingArea.Width / 2) - (this.Width / 2);
            this.Top = Convert.ToInt32(System.Windows.Forms.Screen.PrimaryScreen.WorkingArea.Height * 0.10);

            if (this.components == null)
            {
                this.components = new System.ComponentModel.Container();
            }

            try
            {
                TimeSpan TimeComponents = TimeSpan.Parse(Properties.Settings.Default.UpdateIntervalTimeSpan);
                Hours.Value = TimeComponents.Hours;
                Minutes.Value = TimeComponents.Minutes;
                Seconds.Value = TimeComponents.Seconds;
            } catch (Exception Any) { Console.WriteLine(Any.Message); }

            this.ApplyLanguage();            
            this.BringToFront();
        }

        private void ApplyLanguage()
        {
            this.Text = LanguageResources.GetText("FrmConfig.Title");

            this.lblVolume.Text = LanguageResources.GetText("FrmConfig.lblSyncInterval");

            this.CloseBtn.Text = LanguageResources.GetText("FrmConfig.Finish");
        }

        private void FrmConfig_SyncInterval_FormClosing(object sender, FormClosingEventArgs e)
        {
            if (FrmConfig.ApplicationInit && NextForm == null)
            {
                //IgnoreDefaultActivation();
                //MessageBox.Show(LanguageResources.GetText("FrmConfig.FirstUseConfigurationWarning"));
                //e.Cancel = true;
            }
            else
            {
                this.Visible = false; if (NextForm != null) NextForm.ShowDialog();
            }            
        }

        private void CloseBtn_Click(object sender, EventArgs e)
        {
            this.Close();
        }
        
        private void ChangeInterval()
        {
            try
            {                
                
                TimeSpan IntervalTimeSpan = new TimeSpan(Convert.ToInt32(Hours.Value), Convert.ToInt32(Minutes.Value), Convert.ToInt32(Seconds.Value));

                String Interval = IntervalTimeSpan.ToString(@"hh\:mm\:ss");

                Properties.Settings.Default.UpdateIntervalTimeSpan = Interval;
                Properties.Settings.Default.Save();

            } catch (Exception Any) { Console.WriteLine(Any.Message); }
        }

        private void Hours_ValueChanged(object sender, EventArgs e)
        {
            ChangeInterval();
        }

        private void Minutes_ValueChanged(object sender, EventArgs e)
        {
            ChangeInterval();
        }

        private void Seconds_ValueChanged(object sender, EventArgs e)
        {
            ChangeInterval();
        }                       

    }
}
