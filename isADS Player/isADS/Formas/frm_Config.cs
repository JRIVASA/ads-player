﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using isADS.Clases;

namespace isADS
{
    public partial class frm_Config : Form
    {
        public frm_Config()
        {
            InitializeComponent();
        }

        private void dirButton_Click(object sender, EventArgs e)
        {
            dirBrowser.ShowDialog();
            dirTextBox.Text = dirBrowser.SelectedPath;
        }

        private void frm_Config_Load(object sender, EventArgs e)
        {
            dirTextBox.Text = Properties.Settings.Default.CampaignFilesDir;
            validarTemplate();
        }

        private void saveButton_Click(object sender, EventArgs e)
        {
            //Properties.Settings.Default.CampaignFilesDir = dirBrowser.SelectedPath.ToString();
            //Properties.Settings.Default.template = selectedButton();
            Properties.Settings.Default.Save();            
            Functions.QuickMsgBox("Configuración guardada exitosamente.");
            //this.Close();
        }

        private void exitButton_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private string selectedButton()
        {
            var checkedButton = groupBox2.Controls.OfType<RadioButton>().FirstOrDefault(r => r.Checked);
            return checkedButton.Text;
        }

        private void validarTemplate()
        {
            string template = "1"; //string template = Properties.Settings.Default.template;
            if (template == "1")
            {
                radioButton1.Checked = true;
                return;
            }
            else if (template == "2")
            {
                radioButton2.Checked = true;
                return;
            }
            else if (template == "3")
            {
                radioButton3.Checked = true;
                return;
            }
            else if (template == "4")
            {
                radioButton4.Checked = true;
                return;
            }
            else if (template == "5")
            {
                radioButton5.Checked = true;
                return;
            }
            else if (template == "6")
            {
                radioButton6.Checked = true;
                return;
            }
            else if (template == "7")
            {
                radioButton7.Checked = true;
                return;
            }
            else if (template == "8")
            {
                radioButton8.Checked = true;
                return;
            }
        }
    }
}
