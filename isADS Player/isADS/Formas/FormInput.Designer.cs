﻿namespace isADS
{
    partial class FormInput
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.ButtonTrue = new System.Windows.Forms.Button();
            this.ButtonFalse = new System.Windows.Forms.Button();
            this.Message = new System.Windows.Forms.TextBox();
            this.SuspendLayout();
            // 
            // ButtonTrue
            // 
            this.ButtonTrue.Location = new System.Drawing.Point(72, 105);
            this.ButtonTrue.Name = "ButtonTrue";
            this.ButtonTrue.Size = new System.Drawing.Size(122, 38);
            this.ButtonTrue.TabIndex = 0;
            this.ButtonTrue.Text = "True";
            this.ButtonTrue.UseVisualStyleBackColor = true;
            this.ButtonTrue.Click += new System.EventHandler(this.ButtonTrue_Click);
            // 
            // ButtonFalse
            // 
            this.ButtonFalse.Location = new System.Drawing.Point(215, 105);
            this.ButtonFalse.Name = "ButtonFalse";
            this.ButtonFalse.Size = new System.Drawing.Size(122, 38);
            this.ButtonFalse.TabIndex = 1;
            this.ButtonFalse.Text = "False";
            this.ButtonFalse.UseVisualStyleBackColor = true;
            this.ButtonFalse.Click += new System.EventHandler(this.ButtonFalse_Click);
            // 
            // Message
            // 
            this.Message.Font = new System.Drawing.Font("Verdana", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Message.Location = new System.Drawing.Point(29, 21);
            this.Message.Multiline = true;
            this.Message.Name = "Message";
            this.Message.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.Message.Size = new System.Drawing.Size(349, 66);
            this.Message.TabIndex = 3;
            this.Message.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // FormInput
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(411, 155);
            this.ControlBox = false;
            this.Controls.Add(this.Message);
            this.Controls.Add(this.ButtonFalse);
            this.Controls.Add(this.ButtonTrue);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "FormInput";
            this.ShowInTaskbar = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Activated += new System.EventHandler(this.FormInput_Activated);
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.FormInput_FormClosing);
            this.Load += new System.EventHandler(this.FormInput_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        public System.Windows.Forms.Button ButtonTrue;
        public System.Windows.Forms.Button ButtonFalse;
        public System.Windows.Forms.TextBox Message;
    }
}