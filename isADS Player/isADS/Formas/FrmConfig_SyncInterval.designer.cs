﻿namespace isADS
{
    partial class FrmConfig_SyncInterval
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.LangPanel = new System.Windows.Forms.Panel();
            this.lblSeconds = new System.Windows.Forms.Label();
            this.lblMinutes = new System.Windows.Forms.Label();
            this.lblHours = new System.Windows.Forms.Label();
            this.Seconds = new System.Windows.Forms.NumericUpDown();
            this.Minutes = new System.Windows.Forms.NumericUpDown();
            this.Hours = new System.Windows.Forms.NumericUpDown();
            this.CloseBtn = new System.Windows.Forms.Button();
            this.lblVolume = new System.Windows.Forms.Label();
            this.LangPanel.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.Seconds)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Minutes)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Hours)).BeginInit();
            this.SuspendLayout();
            // 
            // LangPanel
            // 
            this.LangPanel.Controls.Add(this.lblSeconds);
            this.LangPanel.Controls.Add(this.lblMinutes);
            this.LangPanel.Controls.Add(this.lblHours);
            this.LangPanel.Controls.Add(this.Seconds);
            this.LangPanel.Controls.Add(this.Minutes);
            this.LangPanel.Controls.Add(this.Hours);
            this.LangPanel.Controls.Add(this.CloseBtn);
            this.LangPanel.Controls.Add(this.lblVolume);
            this.LangPanel.Dock = System.Windows.Forms.DockStyle.Top;
            this.LangPanel.Location = new System.Drawing.Point(0, 0);
            this.LangPanel.Name = "LangPanel";
            this.LangPanel.Size = new System.Drawing.Size(457, 103);
            this.LangPanel.TabIndex = 0;
            // 
            // lblSeconds
            // 
            this.lblSeconds.AutoSize = true;
            this.lblSeconds.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblSeconds.Location = new System.Drawing.Point(275, 13);
            this.lblSeconds.Name = "lblSeconds";
            this.lblSeconds.Size = new System.Drawing.Size(24, 16);
            this.lblSeconds.TabIndex = 7;
            this.lblSeconds.Text = "SS";
            // 
            // lblMinutes
            // 
            this.lblMinutes.AutoSize = true;
            this.lblMinutes.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblMinutes.Location = new System.Drawing.Point(207, 13);
            this.lblMinutes.Name = "lblMinutes";
            this.lblMinutes.Size = new System.Drawing.Size(30, 16);
            this.lblMinutes.TabIndex = 6;
            this.lblMinutes.Text = "MM";
            // 
            // lblHours
            // 
            this.lblHours.AutoSize = true;
            this.lblHours.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblHours.Location = new System.Drawing.Point(144, 13);
            this.lblHours.Name = "lblHours";
            this.lblHours.Size = new System.Drawing.Size(26, 16);
            this.lblHours.TabIndex = 5;
            this.lblHours.Text = "HH";
            // 
            // Seconds
            // 
            this.Seconds.Location = new System.Drawing.Point(265, 35);
            this.Seconds.Maximum = new decimal(new int[] {
            59,
            0,
            0,
            0});
            this.Seconds.Name = "Seconds";
            this.Seconds.Size = new System.Drawing.Size(45, 20);
            this.Seconds.TabIndex = 4;
            this.Seconds.ValueChanged += new System.EventHandler(this.Seconds_ValueChanged);
            // 
            // Minutes
            // 
            this.Minutes.Location = new System.Drawing.Point(200, 35);
            this.Minutes.Maximum = new decimal(new int[] {
            59,
            0,
            0,
            0});
            this.Minutes.Name = "Minutes";
            this.Minutes.Size = new System.Drawing.Size(45, 20);
            this.Minutes.TabIndex = 3;
            this.Minutes.ValueChanged += new System.EventHandler(this.Minutes_ValueChanged);
            // 
            // Hours
            // 
            this.Hours.Location = new System.Drawing.Point(135, 35);
            this.Hours.Maximum = new decimal(new int[] {
            23,
            0,
            0,
            0});
            this.Hours.Name = "Hours";
            this.Hours.Size = new System.Drawing.Size(45, 20);
            this.Hours.TabIndex = 2;
            this.Hours.ValueChanged += new System.EventHandler(this.Hours_ValueChanged);
            // 
            // CloseBtn
            // 
            this.CloseBtn.Font = new System.Drawing.Font("Verdana", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.CloseBtn.Location = new System.Drawing.Point(347, 33);
            this.CloseBtn.Name = "CloseBtn";
            this.CloseBtn.Size = new System.Drawing.Size(98, 22);
            this.CloseBtn.TabIndex = 1;
            this.CloseBtn.Tag = "";
            this.CloseBtn.Text = "Close";
            this.CloseBtn.UseVisualStyleBackColor = true;
            this.CloseBtn.Click += new System.EventHandler(this.CloseBtn_Click);
            // 
            // lblVolume
            // 
            this.lblVolume.AutoSize = true;
            this.lblVolume.Font = new System.Drawing.Font("Verdana", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblVolume.Location = new System.Drawing.Point(13, 36);
            this.lblVolume.Name = "lblVolume";
            this.lblVolume.Size = new System.Drawing.Size(103, 16);
            this.lblVolume.TabIndex = 1;
            this.lblVolume.Text = "Sync Interval:";
            // 
            // FrmConfig_SyncInterval
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(457, 102);
            this.Controls.Add(this.LangPanel);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "FrmConfig_SyncInterval";
            this.ShowInTaskbar = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.Manual;
            this.Text = "FrmConfig";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.FrmConfig_SyncInterval_FormClosing);
            this.Load += new System.EventHandler(this.FrmConfig_SyncInterval_Load);
            this.LangPanel.ResumeLayout(false);
            this.LangPanel.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.Seconds)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Minutes)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Hours)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Panel LangPanel;
        private System.Windows.Forms.Label lblVolume;
        private System.Windows.Forms.Button CloseBtn;
        private System.Windows.Forms.NumericUpDown Minutes;
        private System.Windows.Forms.NumericUpDown Hours;
        private System.Windows.Forms.NumericUpDown Seconds;
        private System.Windows.Forms.Label lblHours;
        private System.Windows.Forms.Label lblSeconds;
        private System.Windows.Forms.Label lblMinutes;
    }
}