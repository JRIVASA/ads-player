﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Data.SqlClient;
using isADS.Clases;

namespace isADS
{
    public partial class FrmConfig_RemoteDB : Form
    {

        private Boolean ApplicationInit = false;
        private Boolean ConfigInitCheck = true;
        String Action = String.Empty;
        private Boolean OriginalCustomServerValue;
        private Boolean OriginalWindowsAuthValue;
        private String OriginalServerInstanceValue = String.Empty;
        private String OriginalDBNameValue = String.Empty;
        private String OriginalSQLUserValue = String.Empty;
        private String OriginalSQLPassValue = String.Empty;
        private Boolean OriginalLocalSyncValue;
        private String OriginalLocalRepositoryValue = String.Empty;
        private Boolean CurrentCustomServerValue;
        private Boolean CurrentWindowsAuthValue;
        private String CurrentServerInstanceValue;
        private String CurrentDBNameValue;
        private String CurrentSQLUserValue;
        private String CurrentSQLPassValue;
        private Boolean CurrentLocalSyncValue;
        private String CurrentLocalRepositoryValue = String.Empty;
        private Boolean OnPurposeActivation = false;
        private int CheckInterval;

        private Color Checking = Color.FromArgb(250, 247, 133);
        private Color Alive = Color.PaleGreen;
        private Color Wrong = Color.FromArgb(230, 0, 0);
        private Color Inactive = Color.FromKnownColor(System.Drawing.KnownColor.Control);

        private Form PreviousForm = null;
        private Form NextForm = null;

        private Boolean IgnoreDefaultActivation()
        {
            OnPurposeActivation = true; return OnPurposeActivation;
        }

        public FrmConfig_RemoteDB()
        {
            InitializeComponent();
        }

        public FrmConfig_RemoteDB(Boolean AppInit)
        {
            InitializeComponent();
            this.ApplicationInit = AppInit;
        }

        private void FrmConfig_RemoteDB_Load(object sender, EventArgs e)
        {
            this.components = new System.ComponentModel.Container();
            this.DoubleBuffered = true;

            this.Left = (System.Windows.Forms.Screen.PrimaryScreen.WorkingArea.Width / 2) - (this.Width / 2);
            this.Top = Convert.ToInt32(System.Windows.Forms.Screen.PrimaryScreen.WorkingArea.Height * 0.10);
            this.LocalFilesSource.ReadOnly = true;

            lblWinAuthText.Left = lblSQLUser.Left; lblWinAuthText.Top = lblSQLUser.Top;

            OriginalCustomServerValue = Properties.Settings.Default.RemoteDB_CustomServer;
            CurrentCustomServerValue = OriginalCustomServerValue;

            OriginalWindowsAuthValue = Properties.Settings.Default.RemoteDB_WindowsAuth;
            CurrentWindowsAuthValue = OriginalWindowsAuthValue;

            OriginalServerInstanceValue = DataEncryption.DecryptText(Properties.Settings.Default.RemoteDB_SQLServerName, Functions.EncryptionKey);
            OriginalDBNameValue = DataEncryption.DecryptText(Properties.Settings.Default.RemoteDB_SQLDBName, Functions.EncryptionKey);
            OriginalSQLUserValue = DataEncryption.DecryptText(Properties.Settings.Default.RemoteDB_SQLUser, Functions.EncryptionKey);
            OriginalSQLPassValue = DataEncryption.DecryptText(Properties.Settings.Default.RemoteDB_SQLPass, Functions.EncryptionKey);

            CurrentServerInstanceValue = OriginalServerInstanceValue;
            CurrentDBNameValue = OriginalDBNameValue;
            CurrentSQLUserValue = OriginalSQLUserValue;
            CurrentSQLPassValue = OriginalSQLPassValue;

            OriginalLocalSyncValue = Properties.Settings.Default.LocalFilesSync;
            OriginalLocalRepositoryValue = Properties.Settings.Default.LocalFilesSyncDir;
            CurrentLocalSyncValue = OriginalLocalSyncValue;
            CurrentLocalRepositoryValue = OriginalLocalRepositoryValue;
            LocalFilesSource.Text = CurrentLocalRepositoryValue;

            CheckInterval = 1000;

            //this.ClientSize = new System.Drawing.Size(414, 147);
            //this.Size = this.ClientSize;

            //this.Tag = 1; // Using Tag as a FirstActivation Boolean.

            this.ApplyLanguage();            
            this.BringToFront();
        }

        private SqlConnection getConnection()
        {
            if (lblCustomServer.BackColor != Inactive)
            {                  
                if (CurrentWindowsAuthValue)
                {
                    return Functions.getAlternateTrustedConnection(
                        (CurrentServerInstanceValue.isUndefined() ? "undefined": CurrentServerInstanceValue), CurrentDBNameValue);
                }
                else
                {
                    return Functions.getAlternateConnection(CurrentServerInstanceValue, CurrentDBNameValue, CurrentSQLUserValue, CurrentSQLPassValue);
                }
            }
            else
            {
                return Functions.getAlternateConnection(
                    "A2NWPLSK14SQL-v05.shr.prod.iad2.secureserver.net", // Instancia //(Properties.Settings.Default.RemoteDB_Beta ? "isADSBeta.db.8609058.hostedresource.com" : "isADS.db.8609058.hostedresource.com"),
                    (Properties.Settings.Default.RemoteDB_Beta ? "isADSBeta" : "isADS"), // Base de Datos
                    (Properties.Settings.Default.RemoteDB_Beta ? "isADS" : "isADS"), // User Login
                    (Properties.Settings.Default.RemoteDB_Beta ? "W!s3Ads30" : "W!s3Ads30") // Pwd
                );                    
            }
        }

        private SqlConnection getConnection(int ConnectionTimeout = -1)
        {
            SqlConnection TimedConnection;

            if (lblCustomServer.BackColor != Inactive)
            {
                if (CurrentWindowsAuthValue)
                {
                    TimedConnection = Functions.getAlternateTrustedConnection(
                        (CurrentServerInstanceValue.isUndefined() ? "undefined" : CurrentServerInstanceValue), CurrentDBNameValue, ConnectionTimeout);
                }
                else
                {
                    TimedConnection = Functions.getAlternateConnection(CurrentServerInstanceValue, CurrentDBNameValue, CurrentSQLUserValue, CurrentSQLPassValue, ConnectionTimeout);
                }
            }
            else
            {
                TimedConnection = Functions.getAlternateConnection(
                    "A2NWPLSK14SQL-v05.shr.prod.iad2.secureserver.net", // Instancia //(Properties.Settings.Default.RemoteDB_Beta ? "isADSBeta.db.8609058.hostedresource.com" : "isADS.db.8609058.hostedresource.com"),
                    (Properties.Settings.Default.RemoteDB_Beta ? "isADSBeta" : "isADS"), // Base de Datos
                    (Properties.Settings.Default.RemoteDB_Beta ? "isADS" : "isADS"), // User Login
                    (Properties.Settings.Default.RemoteDB_Beta ? "W!s3Ads30" : "W!s3Ads30"), // Pwd
                    ConnectionTimeout // Connection Timeout.
                );
            }

            return TimedConnection;

        }

        private Boolean ValidateSettings()
        {
            //Boolean CorrectSettings = true;

            //if (ApplicationInit)
            //{
            //    if (Action == "Close") 
            //    { return CorrectSettings; }
            //    else if (Action != String.Empty)
            //    {
            //        return CheckDatabaseAccess();
            //    }                
                
            //    if (!CheckDatabaseAccess())
            //    {
            //        this.Close();
            //        CorrectSettings = false;
            //    }
            //    else
            //    {
            //        Action = "Close"; this.Close(); return CorrectSettings;
            //    }
            //}
            //else
            //{
                if (ConfigInitCheck)
                {
                    if (CurrentCustomServerValue)
                    {
                        this.lblCustomServer.BackColor = Checking;
                        
                        if (CurrentWindowsAuthValue)
                        { SQLWindowsAuthentication.Checked = true; SQLCredentials_CheckedChanged(null, null); }
                        else
                        { SQLCredentials.Checked = true; }

                    }
                    else
                    {
                        this.lblOnlineConfig.BackColor = Checking;
                        lblOnlineConfig_Click(null, null);
                    }

                    CheckInterval = 2500;

                    ConfigInitCheck = false;
                    return ConfigInitCheck;
                }
                else
                {
                    return true;
                }
            //}

            //return CorrectSettings;
        }

        private void TriggerDatabaseCheck()
        {
            Timer myCheckDBTimer = ((Timer)this.components.Components.OfType<IComponent>().Where(ti => ti.GetType().FullName == "System.Windows.Forms.Timer")
            .ToList().Where(ti => ((Timer)ti).Tag != null).ToList().Find(ti => ((Timer)ti).Tag.ToString() == "myCheckDBTimer"));

            if (myCheckDBTimer == null)
            {
                myCheckDBTimer = new Timer();
                myCheckDBTimer.Tag = "myCheckDBTimer";
                myCheckDBTimer.Interval = CheckInterval;
                this.components.Add(myCheckDBTimer);
                myCheckDBTimer.Tick += myCheckDBTimer_Tick;
                myCheckDBTimer.Enabled = true;
            }
            else
            {
                myCheckDBTimer.Enabled = false;
                myCheckDBTimer.Interval = CheckInterval;
                myCheckDBTimer.Enabled = true;
            }
        }

        void myCheckDBTimer_Tick(object sender, EventArgs e)
        {
            Timer myCheckDBTimer = ((Timer)sender);

            myCheckDBTimer.Enabled = false;

            /* Synchronous Continuation
             * 
             * CheckDatabaseAccess();
             * 
            Functions.setTimeOut(2000, delegate(Object myTimer, System.Timers.ElapsedEventArgs ev)
            {
                this.BeginInvoke((Action)(() =>
                {
                    try
                    {
                        if (!myCheckDBTimer.Enabled)
                        {
                            this.lblDatabaseAccess.Text = LanguageResources.GetText("FrmConfig_RemoteDB.lblDatabaseAccess");
                            this.components.Remove(myCheckDBTimer); myCheckDBTimer.Dispose(); 
                        }
                    }
                    catch (Exception) { }
                }));
                ((System.Timers.Timer)myTimer).Dispose();
            });  
             * 
             */

            // Now Testing for Async...

            Action Continuation = ((Action)(() => {

                Functions.setTimeOut(2000, delegate(Object myTimer, System.Timers.ElapsedEventArgs ev)
                {
                    try
                    {
                        this.BeginInvoke((Action)(() =>
                        {
                            try
                            {
                                if (!myCheckDBTimer.Enabled)
                                {
                                    this.lblDatabaseAccess.Text = LanguageResources.GetText("FrmConfig_RemoteDB.lblDatabaseAccess");
                                    this.components.Remove(myCheckDBTimer); myCheckDBTimer.Dispose();
                                }
                            }
                            catch (Exception) { }
                        }));
                    } catch (Exception Any) { Console.WriteLine(Any.Message); }
                    ((System.Timers.Timer)myTimer).Dispose();
                });              
                        
            }));

            CheckDatabaseAccess(Continuation);

        }

        private Boolean CheckDatabaseAccess()
        {            
            lblDatabaseAccess.Text = LanguageResources.GetText("FrmConfig.Connecting");
            this.Refresh();

            Application.DoEvents();

            Boolean BD = Functions.CheckDBConnection(getConnection(6));

            if (BD)
            { lblDatabaseAccess.Text = LanguageResources.GetText("FrmConfig.DBAccessGranted"); }
            else
            { lblDatabaseAccess.Text = LanguageResources.GetText("FrmConfig.DBAccessError"); }

            if (CurrentCustomServerValue)
            { this.lblCustomServer.BackColor = (BD ? Alive : Wrong); this.lblOnlineConfig.BackColor = Inactive; }
            else
            { this.lblOnlineConfig.BackColor = (BD ? Alive : Wrong); this.lblCustomServer.BackColor = Inactive; }    
    
            return true;

        }

        private Boolean CheckDatabaseAccess(Action Continuation)
        {
            lblDatabaseAccess.Text = LanguageResources.GetText("FrmConfig.Connecting");
            this.Refresh();

            Application.DoEvents();

            SqlConnection OtherConn = getConnection(6);
            Task test = OtherConn.OpenAsync();

            test.ContinueWith((((Action<Task>)((myTask) =>
            {
               
                Boolean BD = OtherConn.State == ConnectionState.Open;

                this.BeginInvoke((Action)(() =>
                {

                    if (BD)
                    { lblDatabaseAccess.Text = LanguageResources.GetText("FrmConfig.DBAccessGranted"); }
                    else
                    { lblDatabaseAccess.Text = LanguageResources.GetText("FrmConfig.DBAccessError"); }

                    if (CurrentCustomServerValue)
                    { this.lblCustomServer.BackColor = (BD ? Alive : Wrong); this.lblOnlineConfig.BackColor = Inactive; }
                    else
                    { this.lblOnlineConfig.BackColor = (BD ? Alive : Wrong); this.lblCustomServer.BackColor = Inactive; }

                }));

                Continuation.Invoke();

                Console.WriteLine(myTask.Status + " - State: " + OtherConn.State.ToString());
                if (BD) OtherConn.Close();

            }))));

            return true;
        }

        private void FrmConfig_RemoteDB_Activated(object sender, EventArgs e)
        {
            if (OnPurposeActivation) 
            { 
                OnPurposeActivation = false; return;
            }

            ValidateSettings();
        }

        private void FrmConfig_RemoteDB_FormClosing(object sender, FormClosingEventArgs e)
        {
            if (FrmConfig.ApplicationInit && PreviousForm == null && NextForm == null)
            {
                IgnoreDefaultActivation();
                MessageBox.Show(LanguageResources.GetText("FrmConfig.FirstUseConfigurationWarning"));
                e.Cancel = true;
            }
            else
            {
                //if (!ValidateSettings())
                //{
                //    e.Cancel = true;

                //    if (ConfigInitCheck) { ConfigInitCheck = false; }

                //    IgnoreDefaultActivation();
                //    MessageBox.Show(LanguageResources.GetText("FrmConfig.MsgDatabaseConnectionNotWorking"));
                //}
                //else
                //{
                    Boolean DBAccessSettingsChangeWarning = false;

                    if (OriginalCustomServerValue != CurrentCustomServerValue)
                    { DBAccessSettingsChangeWarning = true; }
                    else
                    {
                        if (CurrentCustomServerValue)
                        {
                            if (SQLWindowsAuthentication.Checked)
                            {
                                DBAccessSettingsChangeWarning = (OriginalWindowsAuthValue != SQLWindowsAuthentication.Checked);
                            }
                            else if (SQLCredentials.Checked)
                            {
                                DBAccessSettingsChangeWarning = (OriginalWindowsAuthValue != SQLWindowsAuthentication.Checked);

                                if (!DBAccessSettingsChangeWarning)
                                {
                                    DBAccessSettingsChangeWarning = !SQLUser.Text.isUndefined();
                                }
                            }
                        }
                        else
                        {
                            DBAccessSettingsChangeWarning = false;
                        }
                    }

                    if (DBAccessSettingsChangeWarning)
                    {
                        FormInput Input = new FormInput();

                        Input.Text = "Stellar ADS360";
                        Input.ActionTrue = LanguageResources.GetText("FrmConfig.ConfirmDBSettingsChange"); Input.ButtonTrue.Text = Input.ActionTrue;
                        Input.ActionFalse = LanguageResources.GetText("FrmConfig.DiscardDBSettingsChange"); Input.ButtonFalse.Text = Input.ActionFalse;
                        Input.Message.Text = LanguageResources.GetText("FrmConfig.DBAccessSettingsChangeWarning");
                        //+ Functions.NewLine() + Functions.NewLine() +
                        //LanguageResources.GetText("ActivationWindowChoiceMessageLine2");
                        Input.Message.ReadOnly = true;
                        IgnoreDefaultActivation();
                        Input.ShowDialog();

                        DBAccessSettingsChangeWarning = (Input.Action.Equals(Input.ActionTrue));
                    }

                    if (DBAccessSettingsChangeWarning && !CurrentCustomServerValue)
                    {
                        Properties.Settings.Default.RemoteDB_CustomServer = false;
                        Properties.Settings.Default.Save();
                    }
                    else if (DBAccessSettingsChangeWarning && SQLWindowsAuthentication.Checked)
                    {
                        Properties.Settings.Default.RemoteDB_CustomServer = true;
                        Properties.Settings.Default.RemoteDB_WindowsAuth = true;

                        Properties.Settings.Default.RemoteDB_SQLServerName = DataEncryption.EncryptText(this.ServerInstance.Text, Functions.EncryptionKey);
                        Properties.Settings.Default.RemoteDB_SQLDBName = DataEncryption.EncryptText(this.DBName.Text, Functions.EncryptionKey);

                        Properties.Settings.Default.Save();
                    }
                    else if (DBAccessSettingsChangeWarning && SQLCredentials.Checked)
                    {
                        Properties.Settings.Default.RemoteDB_CustomServer = true;
                        Properties.Settings.Default.RemoteDB_WindowsAuth = false;

                        Properties.Settings.Default.RemoteDB_SQLServerName = DataEncryption.EncryptText(this.ServerInstance.Text, Functions.EncryptionKey);
                        Properties.Settings.Default.RemoteDB_SQLDBName = DataEncryption.EncryptText(this.DBName.Text, Functions.EncryptionKey);
                        Properties.Settings.Default.RemoteDB_SQLUser = DataEncryption.EncryptText(this.SQLUser.Text, Functions.EncryptionKey);
                        Properties.Settings.Default.RemoteDB_SQLPass = DataEncryption.EncryptText(this.SQLPassword.Text, Functions.EncryptionKey); 
                        
                        Properties.Settings.Default.Save();
                    }
                    // else Discard Changes.
                    else
                    {
                        // Other Connection Settings unaffected.
                    }

                    Boolean FilesSyncSettingsChanged = false;

                    if (OriginalLocalSyncValue != CurrentLocalSyncValue)
                    { FilesSyncSettingsChanged = true; }
                    else
                    {
                        if (LocalFilesSync.Checked)
                        {
                            FilesSyncSettingsChanged = (OriginalLocalRepositoryValue != CurrentLocalRepositoryValue);
                        }
                        else if (OnlineFilesSync.Checked)
                        {
                            FilesSyncSettingsChanged = false;
                        }
                    }

                    if (FilesSyncSettingsChanged)
                    {
                        FormInput Input = new FormInput();

                        Input.Text = "Stellar ADS360";
                        Input.ActionTrue =  LanguageResources.GetText("FrmConfig.ConfirmDBSettingsChange"); Input.ButtonTrue.Text = Input.ActionTrue;
                        Input.ActionFalse = LanguageResources.GetText("FrmConfig.DiscardDBSettingsChange"); Input.ButtonFalse.Text = Input.ActionFalse;
                        Input.Message.Text = LanguageResources.GetText("FrmConfig.DiscardFilesSyncSettingsChangeWarning"); // LanguageResources.GetText("FrmConfig.DBAccessSettingsChangeWarning");
                        //+ Functions.NewLine() + Functions.NewLine() +
                        //LanguageResources.GetText("ActivationWindowChoiceMessageLine2");
                        Input.Message.ReadOnly = true;
                        IgnoreDefaultActivation();
                        Input.ShowDialog();

                        FilesSyncSettingsChanged = (Input.Action.Equals(Input.ActionTrue));
                    }

                    if (FilesSyncSettingsChanged && LocalFilesSync.Checked)
                    {
                        Properties.Settings.Default.LocalFilesSync = true;
                        Properties.Settings.Default.LocalFilesSyncDir = LocalFilesSource.Text;
                        Properties.Settings.Default.Save();
                    }
                    else if (FilesSyncSettingsChanged && OnlineFilesSync.Checked)
                    {
                        Properties.Settings.Default.LocalFilesSync = false;
                        Properties.Settings.Default.Save();
                    }
                    else
                    {
                        // Other Files Synchonization Settings unaffected.
                    }

                    this.Visible = false;
                    if (NextForm != null) NextForm.ShowDialog();
                    else if (PreviousForm != null) PreviousForm.ShowDialog();
                //}
            }
        }
      
        private void ApplyLanguage()
        {
            this.Text = LanguageResources.GetText("FrmConfig_RemoteDB.Title");

            this.lblOnlineConfig.Text = LanguageResources.GetText("FrmConfig_RemoteDB.lblOnlineConfig");
            this.lblCustomServer.Text = LanguageResources.GetText("FrmConfig_RemoteDB.lblCustomServer");

            this.lblDatabaseAccess.Text = LanguageResources.GetText("FrmConfig_RemoteDB.lblDatabaseAccess");

            this.SQLWindowsAuthentication.Text = LanguageResources.GetText("FrmConfig.SQLWindowsAuthentication");
            this.SQLCredentials.Text = LanguageResources.GetText("FrmConfig.SQLCredentials");

            this.lblServerInstance.Text = LanguageResources.GetText("FrmConfig_RemoteDB.lblServerInstance");
            this.lblDBName.Text = LanguageResources.GetText("FrmConfig_RemoteDB.lblDBName");
            this.lblSQLUser.Text = LanguageResources.GetText("FrmConfig.lblSQLUser");
            this.lblSQLPassword.Text = LanguageResources.GetText("FrmConfig.lblSQLPassword");

            this.lblWinAuthText.Text = LanguageResources.GetText("FrmConfig.lblWinAuthText");

            this.FilesSyncPanel.Text = LanguageResources.GetText("FrmConfig.FilesSyncPanel");
            this.LocalFilesSync.Text = LanguageResources.GetText("FrmConfig.LocalFilesSync");
            this.OnlineFilesSync.Text = LanguageResources.GetText("FrmConfig.OnlineFilesSync");
            this.lblLocalFilesSource.Text = LanguageResources.GetText("FrmConfig.lblLocalFilesSource");

            this.Back.Text = LanguageResources.GetText("FrmConfig.GoBack");
            this.Next.Text = LanguageResources.GetText("FrmConfig.GoNext");
        }      

        private void DatabaseAccessSettingsChanged()
        {
            if (CurrentCustomServerValue)
            { 
                if (SQLWindowsAuthentication.Checked)
                {
                    lblServerInstance.Visible = true;
                    lblDBName.Visible = true;
                    lblSQLUser.Visible = false;
                    lblSQLPassword.Visible = false;
                    ServerInstance.Visible = true;
                    DBName.Visible = true;
                    SQLUser.Visible = false;
                    SQLPassword.Visible = false;

                    lblWinAuthText.Visible = false;

                    CurrentWindowsAuthValue = true;

                    this.CurrentServerInstanceValue = this.ServerInstance.Text;
                    this.CurrentDBNameValue = this.DBName.Text;

                } else if (SQLCredentials.Checked)
                {
                    lblServerInstance.Visible = true;
                    lblDBName.Visible = true;
                    lblSQLUser.Visible = true;
                    lblSQLPassword.Visible = true;
                    ServerInstance.Visible = true;
                    DBName.Visible = true;
                    SQLUser.Visible = true;
                    SQLPassword.Visible = true;

                    lblWinAuthText.Visible = false;

                    CurrentWindowsAuthValue = false;

                    if (!(this.SQLUser.Text.isUndefined() && this.ServerInstance.Text.isUndefined() && this.DBName.Text.isUndefined())) 
                    {
                        this.CurrentServerInstanceValue = this.ServerInstance.Text;
                        this.CurrentDBNameValue = this.DBName.Text;
                        this.CurrentSQLUserValue = this.SQLUser.Text;
                        this.CurrentSQLPassValue = this.SQLPassword.Text;
                    }
                }

                CurrentCustomServerValue = true;
            }
            else
            {
                CurrentCustomServerValue = false;
                lblWinAuthText.Visible = false;
                // Online Connection
            }

            TriggerDatabaseCheck(); // CheckDatabaseAccess();

        }

        private void SQLWindowsAuthentication_CheckedChanged(object sender, EventArgs e)
        {
            DatabaseAccessSettingsChanged();
        }

        private void SQLCredentials_CheckedChanged(object sender, EventArgs e)
        {
            DatabaseAccessSettingsChanged();
        }

        private void SQLUser_TextChanged(object sender, EventArgs e)
        {
            DatabaseAccessSettingsChanged();
        }

        private void SQLPassword_TextChanged(object sender, EventArgs e)
        {
            DatabaseAccessSettingsChanged();
        }

        private void Next_Click(object sender, EventArgs e)
        {
            NextForm = new FrmConfig_OrganizationDetails(FrmConfig.ApplicationInit); this.Close();
        }

        private void Back_Click(object sender, EventArgs e)
        {
            PreviousForm = new FrmConfig_LocalDB(FrmConfig.ApplicationInit); this.Close();
        }

        private void ServerInstance_TextChanged(object sender, EventArgs e)
        {
            DatabaseAccessSettingsChanged();
        }

        private void DBName_TextChanged(object sender, EventArgs e)
        {
            DatabaseAccessSettingsChanged();
        }

        private void ToggleCustomConfig(Boolean CustomAccess)
        {
            if (CustomAccess) 
            { 
                SQLWindowsAuthentication.Enabled = true;
                SQLCredentials.Enabled = true;
                ServerInstance.Enabled = true;
                DBName.Enabled = true;
                SQLUser.Enabled = true;
                SQLPassword.Enabled = true;

                lblOnlineConfig.BackColor = Inactive;
                lblCustomServer.BackColor = Checking;

                LocalFilesSync.Checked = true;
                OnlineFilesSync.Enabled = false;
            } 
            else
            {
                SQLWindowsAuthentication.Enabled = false;
                SQLCredentials.Enabled = false;
                ServerInstance.Enabled = false;
                DBName.Enabled = false;
                SQLUser.Enabled = false;
                SQLPassword.Enabled = false;

                lblOnlineConfig.BackColor = Checking;
                lblCustomServer.BackColor = Inactive;

                if (OriginalLocalSyncValue) LocalFilesSync.Checked = true; else OnlineFilesSync.Checked = true;
                OnlineFilesSync.Enabled = true;
            }

            CurrentCustomServerValue = CustomAccess;

            DatabaseAccessSettingsChanged();
        }

        private void lblOnlineConfig_Click(object sender, EventArgs e)
        {
            ToggleCustomConfig(false);
        }

        private void lblCustomServer_Click(object sender, EventArgs e)
        {
            ToggleCustomConfig(true);
        }

        private void ShowDir_Click(object sender, EventArgs e)
        {
            Functions.QuickMsgBox(LocalFilesSource.Text);
        }

        [STAThread]
        private void ChangeDir_Click(object sender, EventArgs e)
        {
            System.Threading.Thread ChangeDirThread = new System.Threading.Thread(
                () => {

                    //ChangeDir.Enabled = false;

                    OpenFileDialog LocalFilesSourceSelector = new OpenFileDialog();
                    LocalFilesSourceSelector.Title = LanguageResources.GetText("FrmConfig.LocalFilesSourceSelector.Title");
                    LocalFilesSourceSelector.FileName = LanguageResources.GetText("FrmConfig.LocalFilesSourceSelector.HowTo");                  
                    LocalFilesSourceSelector.CheckPathExists = true;
                    LocalFilesSourceSelector.ShowReadOnly = false;
                    LocalFilesSourceSelector.ReadOnlyChecked = true;
                    LocalFilesSourceSelector.CheckFileExists = false;
                    LocalFilesSourceSelector.ValidateNames = false;
                    DialogResult Choice = System.Windows.Forms.DialogResult.None;

                    //System.Threading.ApartmentState Current = System.Threading.Thread.CurrentThread.ApartmentState;
                    try
                    {
                        //System.Threading.Thread.CurrentThread.ApartmentState = System.Threading.ApartmentState.STA;
                        if (System.IO.Directory.Exists(LocalFilesSource.Text))
                        { LocalFilesSourceSelector.InitialDirectory = LocalFilesSource.Text; }
                        Choice = LocalFilesSourceSelector.ShowDialog();
                    }
                    catch (Exception) { 
                        // Console.WriteLine(System.Threading.Thread.CurrentThread.ApartmentState); 
                    }
                    //finally { System.Threading.Thread.CurrentThread.ApartmentState = Current; }
                    
                    String ResultDir;

                    String LocalizedUndefinedValue = LanguageResources.GetText("undefined");

                    if (Choice != System.Windows.Forms.DialogResult.OK)
                    { ResultDir = LocalizedUndefinedValue; }
                    else
                    { 
                        int Position = LocalFilesSourceSelector.FileName.LastIndexOf("\\");

                        if (Position == -1) ResultDir = LocalizedUndefinedValue;
                        else
                        {
                            ResultDir = LocalFilesSourceSelector.FileName.Substring(0, Position + 1);
                        }
                    }

                    if (ResultDir != LocalFilesSource.Text)
                    {
                        this.BeginInvoke((Action)(() =>
                        {
                            try
                            {
                                LocalFilesSource.Text = ResultDir; 
                            }
                            catch (Exception) { }
                        }));                        
                    }

                    //ChangeDir.Enabled = true;

                });

            ChangeDirThread.TrySetApartmentState(System.Threading.ApartmentState.STA);
            ChangeDirThread.Start();

            //FolderBrowserDialog LocalFilesSourceSelector = new FolderBrowserDialog();
            //LocalFilesSourceSelector.SelectedPath = LocalFilesSource.Text;
            //LocalFilesSourceSelector.ShowNewFolderButton = true;
            ////System.Threading.ApartmentState Current = System.Threading.Thread.CurrentThread.ApartmentState;
            //try {                
            ////System.Threading.Thread.CurrentThread.ApartmentState = System.Threading.ApartmentState.STA;
            //if (System.IO.Directory.Exists(LocalFilesSourceSelector.SelectedPath)) 
            //{ LocalFilesSourceSelector.RootFolder = System.Environment.SpecialFolder.MyComputer; }
            //LocalFilesSourceSelector.ShowDialog();
            //}
            //catch (Exception) { Console.WriteLine(System.Threading.Thread.CurrentThread.ApartmentState); }
            ////finally { System.Threading.Thread.CurrentThread.ApartmentState = Current; }
            //if (LocalFilesSourceSelector.SelectedPath != LocalFilesSource.Text)
            //{ LocalFilesSource.Text = LocalFilesSourceSelector.SelectedPath; }
        }

        private void LocalFilesSyncSettingsChanged(){
            if (LocalFilesSync.Checked)
            {
                lblLocalFilesSource.Visible = true;
                LocalFilesSource.Visible = true;
                ShowDir.Visible = true;
                ChangeDir.Visible = true;

                CurrentLocalSyncValue = true;
                CurrentLocalRepositoryValue = LocalFilesSource.Text;
            }
            else if (OnlineFilesSync.Checked)
            {
                lblLocalFilesSource.Visible = false;
                LocalFilesSource.Visible = false;
                ShowDir.Visible = false;
                ChangeDir.Visible = false;

                CurrentLocalSyncValue = false;
            }
        }

        private void LocalFilesSync_CheckedChanged(object sender, EventArgs e)
        {
            LocalFilesSyncSettingsChanged();
        }

        private void OnlineFilesSync_CheckedChanged(object sender, EventArgs e)
        {
            LocalFilesSyncSettingsChanged();
        }

        private void LocalFilesSource_TextChanged(object sender, EventArgs e)
        {
            LocalFilesSyncSettingsChanged();
        }
    }
}
