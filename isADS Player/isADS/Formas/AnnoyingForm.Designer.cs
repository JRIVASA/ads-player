﻿namespace isADS
{
    partial class AnnoyingForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.Message = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // Message
            // 
            this.Message.BackColor = System.Drawing.Color.PaleGreen;
            this.Message.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.Message.Dock = System.Windows.Forms.DockStyle.Fill;
            this.Message.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.Message.Font = new System.Drawing.Font("Verdana", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Message.ForeColor = System.Drawing.Color.Black;
            this.Message.Location = new System.Drawing.Point(0, 0);
            this.Message.Name = "Message";
            this.Message.Size = new System.Drawing.Size(860, 181);
            this.Message.TabIndex = 0;
            this.Message.Text = "This Product is in DEMO mode. To remove limitations and these messages please ins" +
    "ert activation key.";
            this.Message.UseVisualStyleBackColor = false;
            this.Message.Click += new System.EventHandler(this.button1_Click);
            // 
            // AnnoyingForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(860, 181);
            this.MinimumSize = new System.Drawing.Size(System.Windows.Forms.Screen.PrimaryScreen.WorkingArea.Width, this.Height);
            this.MaximumSize = this.MinimumSize;
            this.Controls.Add(this.Message);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Name = "AnnoyingForm";
            this.Opacity = 0.8D;
            this.ShowIcon = false;
            this.ShowInTaskbar = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "AnnoyingForm";
            this.Activated += new System.EventHandler(this.AnnoyingForm_Activated);
            this.Deactivate += new System.EventHandler(this.AnnoyingForm_Deactivate);
            this.Load += new System.EventHandler(this.AnnoyingForm_Load);
            this.Resize += new System.EventHandler(this.AnnoyingForm_Resize);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Button Message;
    }
}