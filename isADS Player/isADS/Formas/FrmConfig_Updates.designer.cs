﻿namespace isADS
{
    partial class FrmConfig_Updates
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.LangPanel = new System.Windows.Forms.Panel();
            this.lblNumberOfCampaigns = new System.Windows.Forms.Label();
            this.NumberOfCampaigns = new System.Windows.Forms.NumericUpDown();
            this.CheckForUpdates = new System.Windows.Forms.CheckBox();
            this.CloseBtn = new System.Windows.Forms.Button();
            this.lblUpdate = new System.Windows.Forms.Label();
            this.LangPanel.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.NumberOfCampaigns)).BeginInit();
            this.SuspendLayout();
            // 
            // LangPanel
            // 
            this.LangPanel.Controls.Add(this.lblNumberOfCampaigns);
            this.LangPanel.Controls.Add(this.NumberOfCampaigns);
            this.LangPanel.Controls.Add(this.CheckForUpdates);
            this.LangPanel.Controls.Add(this.CloseBtn);
            this.LangPanel.Controls.Add(this.lblUpdate);
            this.LangPanel.Dock = System.Windows.Forms.DockStyle.Top;
            this.LangPanel.Location = new System.Drawing.Point(0, 0);
            this.LangPanel.Name = "LangPanel";
            this.LangPanel.Size = new System.Drawing.Size(457, 103);
            this.LangPanel.TabIndex = 0;
            // 
            // lblNumberOfCampaigns
            // 
            this.lblNumberOfCampaigns.AutoSize = true;
            this.lblNumberOfCampaigns.Font = new System.Drawing.Font("Verdana", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblNumberOfCampaigns.Location = new System.Drawing.Point(13, 59);
            this.lblNumberOfCampaigns.Name = "lblNumberOfCampaigns";
            this.lblNumberOfCampaigns.Size = new System.Drawing.Size(123, 16);
            this.lblNumberOfCampaigns.TabIndex = 4;
            this.lblNumberOfCampaigns.Text = "N° of Campaigns:";
            // 
            // NumberOfCampaigns
            // 
            this.NumberOfCampaigns.Location = new System.Drawing.Point(142, 59);
            this.NumberOfCampaigns.Name = "NumberOfCampaigns";
            this.NumberOfCampaigns.Size = new System.Drawing.Size(51, 20);
            this.NumberOfCampaigns.TabIndex = 3;
            this.NumberOfCampaigns.ValueChanged += new System.EventHandler(this.NumberOfCampaigns_ValueChanged);
            // 
            // CheckForUpdates
            // 
            this.CheckForUpdates.AutoSize = true;
            this.CheckForUpdates.Font = new System.Drawing.Font("Verdana", 9.75F);
            this.CheckForUpdates.Location = new System.Drawing.Point(97, 20);
            this.CheckForUpdates.Name = "CheckForUpdates";
            this.CheckForUpdates.Size = new System.Drawing.Size(149, 20);
            this.CheckForUpdates.TabIndex = 2;
            this.CheckForUpdates.Text = "Check for Updates";
            this.CheckForUpdates.UseVisualStyleBackColor = true;
            this.CheckForUpdates.CheckedChanged += new System.EventHandler(this.CheckForUpdates_CheckedChanged);
            // 
            // CloseBtn
            // 
            this.CloseBtn.Font = new System.Drawing.Font("Verdana", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.CloseBtn.Location = new System.Drawing.Point(347, 18);
            this.CloseBtn.Name = "CloseBtn";
            this.CloseBtn.Size = new System.Drawing.Size(98, 22);
            this.CloseBtn.TabIndex = 1;
            this.CloseBtn.Tag = "";
            this.CloseBtn.Text = "Close";
            this.CloseBtn.UseVisualStyleBackColor = true;
            this.CloseBtn.Click += new System.EventHandler(this.CloseBtn_Click);
            // 
            // lblUpdate
            // 
            this.lblUpdate.AutoSize = true;
            this.lblUpdate.Font = new System.Drawing.Font("Verdana", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblUpdate.Location = new System.Drawing.Point(13, 21);
            this.lblUpdate.Name = "lblUpdate";
            this.lblUpdate.Size = new System.Drawing.Size(61, 16);
            this.lblUpdate.TabIndex = 1;
            this.lblUpdate.Text = "Update:";
            // 
            // FrmConfig_Updates
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(457, 102);
            this.Controls.Add(this.LangPanel);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "FrmConfig_Updates";
            this.ShowInTaskbar = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.Manual;
            this.Text = "FrmConfig";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.FrmConfig_Updates_FormClosing);
            this.Load += new System.EventHandler(this.FrmConfig_Updates_Load);
            this.LangPanel.ResumeLayout(false);
            this.LangPanel.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.NumberOfCampaigns)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Panel LangPanel;
        private System.Windows.Forms.Label lblUpdate;
        private System.Windows.Forms.Button CloseBtn;
        private System.Windows.Forms.CheckBox CheckForUpdates;
        private System.Windows.Forms.Label lblNumberOfCampaigns;
        private System.Windows.Forms.NumericUpDown NumberOfCampaigns;
    }
}