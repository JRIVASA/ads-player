﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using isADS.Clases;
using Microsoft.Win32;

namespace isADS
{
    public partial class AnnoyingForm : Form    
    {

        Boolean Block = false;
        Boolean OnPurposeActivation = false;

        private Boolean IgnoreDefaultActivation()
        {
            OnPurposeActivation = true; return OnPurposeActivation;
        }

        public AnnoyingForm()
        {
            InitializeComponent();
        }

        protected override CreateParams CreateParams
        {
            get
            {
                CreateParams cp = null;
                cp = base.CreateParams;
                cp.ExStyle |= 0x80;  // Turn on WS_EX_TOOLWINDOW
                return cp;
            }
        }

        private void AnnoyingForm_Load(object sender, EventArgs e)
        {

            //this.TopMost = true;

            //Console.WriteLine( System.Threading.Thread.CurrentThread.CurrentUICulture.EnglishName);
            //Console.WriteLine(System.Threading.Thread.CurrentThread.CurrentCulture.EnglishName);

            //this.MaximumSize = this.Size;
            //this.MinimumSize = this.Size;

            this.DoubleBuffered = true;

            Microsoft.Win32.SystemEvents.DisplaySettingsChanging += SystemEvents_DisplaySettingsChanging;
            Microsoft.Win32.SystemEvents.DisplaySettingsChanged += SystemEvents_DisplaySettingsChanged;

            this.Width = Screen.PrimaryScreen.WorkingArea.Width;

            this.Height = Convert.ToInt32((double)Screen.PrimaryScreen.WorkingArea.Height * (double)(0.3));  // 200;
            this.Top = (Screen.PrimaryScreen.WorkingArea.Height / 2) - (this.Height / 2);
            this.Left = this.Width;
            this.Visible = true;

            float ScalingSize = (float) Math.Round(((((double)Screen.PrimaryScreen.WorkingArea.Width * (double)(0.64)) + ((double)Screen.PrimaryScreen.WorkingArea.Height * (double)(0.36))) / (double)53.90), 0);

            this.Message.Text = LanguageResources.GetText("AnnoyingFormText");
            this.Message.Font = new Font(this.Message.Font.FontFamily,
            ScalingSize          
            , FontStyle.Bold);

            this.BringToFront();
                     
        }

        void SystemEvents_DisplaySettingsChanged(object sender, EventArgs e)
        {
            
        }

        void SystemEvents_DisplaySettingsChanging(object sender, EventArgs e)
        {
            Microsoft.Win32.SystemEvents.DisplaySettingsChanging -= SystemEvents_DisplaySettingsChanging;
            Microsoft.Win32.SystemEvents.DisplaySettingsChanged -= SystemEvents_DisplaySettingsChanged;

            try
            {
                Block = true;

                if (Application.OpenForms["FrmConfig_ScreenSelect"] != null) { Application.OpenForms["FrmConfig_ScreenSelect"].Dispose(); }
                if (Application.OpenForms["FrmConfig_LocationSelect"] != null) { Application.OpenForms["FrmConfig_LocationSelect"].Dispose(); }
                if (Application.OpenForms["FormInput"] != null) { Application.OpenForms["FormInput"].Dispose(); }

                this.Dispose();
            }
            catch (Exception) { }
        }

        private void StartMoving()
        {
            while (this.Left >= (this.Width * -1))            
            {
                Application.DoEvents();
                if (Block) { return; }
                this.Left = this.Left - 1;
                //System.Threading.Thread.Sleep(new System.TimeSpan(10000000));
                System.Threading.Thread.Sleep(1);
                this.Refresh();                
            }
            this.Close();
        }

        private void AnnoyingForm_Activated(object sender, EventArgs e)
        {
            StartMoving();
        }

        private void button1_Click(object sender, EventArgs e)
        {

            if (
                (this.Left <= (this.Width * -1) + (this.Width * 0.4)) 
                ||
                (this.Left >= (this.Width * 0.6))
               ) { 
                    return; 
                 }

            if (Block) {
                return;
            }

            Boolean Did = false;
            do
            {
                FormInput Input = new FormInput();

                Input.Text = "Stellar ADS360";
                Input.ActionTrue = LanguageResources.GetText("ActivationWindowChooseDEMO"); Input.ButtonTrue.Text = Input.ActionTrue;
                Input.ActionFalse = LanguageResources.GetText("ActivationWindowChooseActivate"); Input.ButtonFalse.Text = Input.ActionFalse;
                Input.Message.Text = LanguageResources.GetText("ActivationWindowChoiceMessageLine1") + 
                    Functions.NewLine() + Functions.NewLine() +
                    LanguageResources.GetText("ActivationWindowChoiceMessageLine2");
                Input.Message.ReadOnly = true;

                Block = true;

                IgnoreDefaultActivation();
                Input.ShowDialog(this);
                
                if (Input.Action.Equals(Input.ActionTrue))
                {
                    // Continue DEMO
                    break;
                }
                else if (Input.Action.Equals(Input.ActionFalse))
                {
                    // OLD ActivationUI
                    //FormActivation Activate = new FormActivation();                    
                    //Activate.Text = "Stellar isADS";
                    //Activate.InputKey.Text = "";
                    //Activate.ShowDialog();

                    //if (Activate.InputKey.Text == "")
                    //{
                    //    Activate.Close();
                    //    Input.Close();
                    //    break;
                    //}

                    //Variables.DEMO = !(new Functions().Activate(Activate.InputKey.Text)); // Activate.InputKey.Text.Length <= 10;

                    //Activate.Close();

                    IgnoreDefaultActivation();
                    FrmConfig_LocationSelect Tmp = new FrmConfig_LocationSelect();
                    Tmp.ShowDialog();
                    Tmp.Dispose();

                    Variables.DEMO = !(new Functions().isActivated());

                    if (!Variables.DEMO) { 
                        Variables.myProgramDataFolder = Functions.getFilesFolder();
                        try
                        {
                            String FilesFolder = Variables.myProgramDataFolder;
                            if (!(System.IO.Directory.Exists(FilesFolder)))
                                System.IO.Directory.CreateDirectory(FilesFolder);
                        }
                        catch (Exception Any)
                        {
                            Console.WriteLine(Any.Message);
                        }
                        Functions.Sync(); 
                    }
                       
                }
                else if (Input.Action.Equals(Input.ActionCancel))
                {
                    // Continue DEMO
                    break;
                }

                Input.Close();

                Did = true;

            } while (!Did);

            Block = !Variables.DEMO;

            if (Block) { this.Close(); } else { StartMoving(); }

        }

        private void AnnoyingForm_Deactivate(object sender, EventArgs e)
        {  
        //   if (((Form)sender).CanFocus) { ((Form)sender).BringToFront(); }
        }

        private void AnnoyingForm_Resize(object sender, EventArgs e)
        {
            // Console.WriteLine("X");
            
            this.Size = this.MinimumSize;
            this.WindowState = FormWindowState.Normal;
        }

    }    

}
