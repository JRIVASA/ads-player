﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Data.SqlClient;
using isADS.Clases;

namespace isADS
{
    public partial class FrmConfig_ScreenSelect : Form
    {

        //public LanguageResourcesMT LanguageResources = new LanguageResourcesMT();

        private Boolean ApplicationInit = false;
        private String OriginalSQLUserValue = String.Empty;
        private String OriginalSQLPassValue = String.Empty;
        private Boolean OnPurposeActivation = false;

        private Form PreviousForm = null;
        private Form NextForm = null;

        Dictionary<String, MediaPlayer> MediaPlayers = null;

        private Boolean IgnoreDefaultActivation()
        {
            OnPurposeActivation = true; return OnPurposeActivation;
        }

        public FrmConfig_ScreenSelect()
        {
            InitializeComponent();
        }

        public FrmConfig_ScreenSelect(Boolean AppInit)
        {
            InitializeComponent();
            this.ApplicationInit = AppInit;
        }

        private void FrmConfig_ScreenSelect_Load(object sender, EventArgs e)
        {
            System.Threading.Thread.Sleep(1000);

            this.components = new System.ComponentModel.Container();
            this.DoubleBuffered = true;

            this.Left = (System.Windows.Forms.Screen.PrimaryScreen.WorkingArea.Width / 2) - (this.Width / 2);
            this.Top = Convert.ToInt32(System.Windows.Forms.Screen.PrimaryScreen.WorkingArea.Height * 0.10);

            this.Text = LanguageResources.GetText("FrmConfig.Title");

            this.ScreenIDsComboBox.Tag = this.ScreenIDsComboBox.Width; // InitialWidth
            this.ScreenIDsTextBox.Tag = this.ScreenIDsTextBox.Width; // InitialWidth

            //this.Next.Visible = false;

            this.ApplyLanguage();            
            this.BringToFront();
        }

        private Boolean ValidateSettings()
        {
            Boolean CorrectSettings = true;

            if (FrmConfig.ApplicationInit)
            {
                CheckConfig(); // return CheckConfig();               
            }
            else
            {
                CheckConfig();
            }

            return CorrectSettings;
        }

        private Boolean CheckConfig()
        {
            MediaPlayers = MediaPlayer.getAvailableMediaPlayers(Variables.SelectedOrganizationID, Variables.SelectedLocation.ID, Variables.TmpConnection);

            if (MediaPlayers != null)
            {
                // Show Selection.

                ScreenIDsTextBox.Visible = false;
                ScreenIDsComboBox.Left = ScreenIDsTextBox.Left;

                ScreenIDsComboBox.Width = Convert.ToInt32(this.ScreenIDsComboBox.Tag) +
                    Convert.ToInt32(this.ScreenIDsTextBox.Tag);

                ScreenIDsComboBox.Items.Clear();
                MediaPlayers.ToList<KeyValuePair<String, MediaPlayer>>().ForEach((TmpMediaPlayer) => ScreenIDsComboBox.Items.Add(TmpMediaPlayer.Key));

                if (MediaPlayers.Count >= 1) { ScreenIDsComboBox.SelectedItem = ScreenIDsComboBox.Items[0]; }

                return true;

            }
            else
            {
                // No DB data. Show Input.

                //ScreenIDsComboBox.Visible = false;
                //ScreenIDsTextBox.Width = Convert.ToInt32(this.ScreenIDsComboBox.Tag) +
                //    Convert.ToInt32(this.ScreenIDsTextBox.Tag);

                ScreenIDsTextBox.Visible = false;
                ScreenIDsComboBox.Left = ScreenIDsTextBox.Left;

                ScreenIDsComboBox.Width = Convert.ToInt32(this.ScreenIDsComboBox.Tag) +
                    Convert.ToInt32(this.ScreenIDsTextBox.Tag);

                IgnoreDefaultActivation();                
                //Functions.QuickMsgBox(LanguageResources.GetText("FrmConfig.ScreenSelect_ErrorMessage"));
                ShowMessage1(LanguageResources.GetText("FrmConfig.ScreenSelect_ErrorMessage"));
                this.Close();

                return false;

            }         
            
        }

        private void FrmConfig_ScreenSelect_Activated(object sender, EventArgs e)
        {
            if (OnPurposeActivation) 
            { 
                OnPurposeActivation = false; return;
            }            
            ValidateSettings();
        }

        private void FrmConfig_ScreenSelect_FormClosing(object sender, FormClosingEventArgs e)
        {
            //if (FrmConfig.ApplicationInit && PreviousForm == null && NextForm == null)
            //{
            //    IgnoreDefaultActivation();

            //    MessageBox.Show(LanguageResources.GetText("FrmConfig.FirstUseConfigurationWarning"));
            //    e.Cancel = true;
            //}
            //else
            //{
                //if (!ValidateSettings())
                //{
                //    e.Cancel = true;

                //    IgnoreDefaultActivation();
                //    MessageBox.Show(LanguageResources.GetText("FrmConfig.MsgUndefinedScreenID"));
                //}
                //else
                //{
                    this.Visible = false;
                    if (NextForm != null) NextForm.ShowDialog();
                    else if (PreviousForm != null) PreviousForm.ShowDialog();
                //}
            //}
        }

        private void ApplyLanguage()
        {
            this.Text = LanguageResources.GetText("FrmConfig_ScreenSelect.Title");

            this.lblScreenID.Text = LanguageResources.GetText("FrmConfig_ScreenSelect.lblScreenID");

            this.ScreenDataPanel.Text = LanguageResources.GetText("FrmConfig_ScreenSelect.ScreenDataPanel");

            this.lblScreenDescription.Text = LanguageResources.GetText("FrmConfig_ScreenSelect.lblScreenDescription");
            this.lblScreenBrand.Text = LanguageResources.GetText("FrmConfig_ScreenSelect.lblScreenBrand");
            this.lblScreenModel.Text = LanguageResources.GetText("FrmConfig_ScreenSelect.lblScreenModel");
            this.lblScreenUbication.Text = LanguageResources.GetText("FrmConfig_ScreenSelect.lblScreenUbication");

            this.Back.Text = LanguageResources.GetText("FrmConfig.GoBack");
            this.Next.Text = LanguageResources.GetText("FrmConfig.Finish");
        }

        private void Back_Click(object sender, EventArgs e)
        {
            PreviousForm = new FrmConfig_LocationSelect(FrmConfig.ApplicationInit); this.Close();
        }

        private void Next_Click(object sender, EventArgs e)
        {
            try
            {
                Variables.SelectedMediaPlayer = MediaPlayers[ScreenIDsComboBox.SelectedItem.ToString()];
            }
            catch (Exception) { }

            IgnoreDefaultActivation();
            //if (Functions.ActivateMediaPlayer()) { MessageBox.Show(LanguageResources.GetText("ActivationProcessSuccess")); }
            //else { MessageBox.Show(LanguageResources.GetText("ActivationProcessFailed")); }
            if (Functions.ActivateMediaPlayer()) { ShowMessage2(LanguageResources.GetText("ActivationProcessSuccess")); }
            else { ShowMessage2(LanguageResources.GetText("ActivationProcessFailed")); }

            this.Close();
        }

        private void ShowMessage1(String MessageText)
        {
            try
            {
            
                FormMessage Msg = new FormMessage();

                Msg.StartPosition = FormStartPosition.CenterParent;

                Msg.Text = "Stellar ADS360";
                Msg.Action.Text = LanguageResources.GetText("FrmConfig.Continue");
                Msg.Message.Text = MessageText;
                Msg.Message.Font = new Font("Verdana", 8F);

                Msg.Message.TextAlign = HorizontalAlignment.Left;
                Msg.Message.ScrollBars = ScrollBars.None;
                Msg.AutoScrollToEnd = false;
                Msg.Message.ReadOnly = true;

                Msg.ShowDialog(this);

                Msg.Close();

            } catch (Exception Any) { Console.WriteLine(Any.Message); }
        }

        private void ShowMessage2(String MessageText)
        {
            try
            {

                FormMessage Msg = new FormMessage();

                Msg.StartPosition = FormStartPosition.CenterParent;

                Msg.Text = "Stellar ADS360";
                Msg.Action.Text = LanguageResources.GetText("FrmConfig.Continue");
                Msg.Message.Text = Functions.NewLine() + MessageText;

                Msg.Message.TextAlign = HorizontalAlignment.Center;
                Msg.Message.ScrollBars = ScrollBars.None;
                Msg.AutoScrollToEnd = false;
                Msg.Message.ReadOnly = true;

                Msg.ShowDialog(this);

                Msg.Close();

            } catch (Exception Any) { Console.WriteLine(Any.Message); }
        }

        private void ChangeScreen(String ID)
        {
            try
            {
                ScreenDescription.Text = MediaPlayers[ID].Description;
                ScreenBrand.Text = MediaPlayers[ID].Brand;
                ScreenModel.Text = MediaPlayers[ID].Model;
                ScreenUbicationLine1.Text = (MediaPlayers[ID].Ubication.Length > 30 ? MediaPlayers[ID].Ubication.Substring(0, 30) : MediaPlayers[ID].Ubication);
                ScreenUbicationLine2.Text = (MediaPlayers[ID].Ubication.Length > 30 ? MediaPlayers[ID].Ubication.Substring(40) : String.Empty);                
            } catch { }
        }

        private void ScreenIDsComboBox_SelectedIndexChanged(object sender, EventArgs e)
        {
            ChangeScreen(ScreenIDsComboBox.SelectedItem.ToString());
        }       
        
    }
}
