﻿using MyConfig = isADS.Properties.Settings;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Management;
using System.Security.Cryptography;
using System.Security;
using System.Collections;
using System.Reflection;
using System.Diagnostics;

namespace isADS.Clases
{
    class Functions
    {

        public static String ConnectionData = getConnectionData();

        public static String DecryptedSQLUser; // = DataEncryption.DecryptText(MyConfig.Default.SQLUser, EncryptionKey); // MyConfig.Default.SQLUser;
        public static String DecryptedSQLPassword; // = DataEncryption.DecryptText(MyConfig.Default.SQLPass, EncryptionKey); // MyConfig.Default.SQLPass;    

        public static String EncryptionKey;

        public static String getConnectionData()
        {
            if (DecryptedSQLUser.isUndefined()) DecryptedSQLUser = DataEncryption.DecryptText(MyConfig.Default.LocalDB_SQLUser, EncryptionKey); // MyConfig.Default.SQLUser;
            if (DecryptedSQLPassword.isUndefined()) DecryptedSQLPassword = DataEncryption.DecryptText(MyConfig.Default.LocalDB_SQLPass, EncryptionKey); // MyConfig.Default.SQLPass;    

            return MyConfig.Default.LocalDB_WindowsAuth ?
            "Server=" + MyConfig.Default.LocalDB_SQLServerName +
            ";Database=" + MyConfig.Default.LocalDB_SQLDBName +
            ";Trusted_Connection=True;"
            :
            "Server=" + MyConfig.Default.LocalDB_SQLServerName +
            ";Database=" + MyConfig.Default.LocalDB_SQLDBName +
            ";User Id=" + DecryptedSQLUser +
            ";Password=" + DecryptedSQLPassword + ";";
        }

        public SqlConnection DBConnection = new SqlConnection(ConnectionData);

        public static String getAlternateTrustedConnectionString()
        {
            return
            "Server=" + MyConfig.Default.LocalDB_SQLServerName +
            ";Database=" + MyConfig.Default.LocalDB_SQLDBName +
            ";Trusted_Connection=True;"
            ;
        }

        public static SqlConnection getAlternateTrustedConnection(int ConnectionTimeout = -1)
        {
            return new SqlConnection(
            getAlternateTrustedConnectionString()
            + ((ConnectionTimeout != -1) ? "Connection Timeout = " + ConnectionTimeout + ";" : "")
            );
        }

        public static String getAlternateTrustedConnectionString(String ServerInstance, String DBName)
        {
            return
            "Server=" + ServerInstance +
            ";Database=" + DBName +
            ";Trusted_Connection=True;"
            ;
        }

        public static SqlConnection getAlternateTrustedConnection(String ServerInstance, String DBName, int ConnectionTimeout = -1)
        {
            return new SqlConnection(
            getAlternateTrustedConnectionString(ServerInstance, DBName)
            + ((ConnectionTimeout != -1) ? "Connection Timeout = " + ConnectionTimeout + ";" : "")          
            );
        }

        public static String getAlternateConnectionString(String UserID, String Password)
        {
            return
            "Server=" + MyConfig.Default.LocalDB_SQLServerName +
            ";Database=" + MyConfig.Default.LocalDB_SQLDBName +
            ";User Id=" + UserID +
            ";Password=" + Password + ";"
            ;
        }

        public static SqlConnection getAlternateConnection(String UserID, String Password, int ConnectionTimeout = -1)
        {
            return new SqlConnection(
            getAlternateConnectionString(UserID, Password)
            + ((ConnectionTimeout != -1) ? "Connection Timeout = " + ConnectionTimeout + ";" : "")
            );
        }

        public static String getAlternateConnectionString(String ServerInstance, String DBName, String UserID, String Password, int ConnectionTimeout = -1)
        {
            return
            "Server=" + ServerInstance +
            ";Database=" + DBName +
            ";User Id=" + UserID +
            ";Password=" + Password + ";"
            ;
        }

        public static SqlConnection getAlternateConnection(String ServerInstance, String DBName, String UserID, String Password, int ConnectionTimeout = -1)
        {
            return new SqlConnection(
            getAlternateConnectionString(ServerInstance, DBName, UserID, Password)
            + ((ConnectionTimeout != -1) ? "Connection Timeout = " + ConnectionTimeout + ";" : "" )
            );
        }

        public Functions()
        {
            
        }

        static Functions()
        {
            EncryptionKey = "isADS-SQL-Credentials-MinimumSecurity. 25374438746824544333443433443433414514554435231234543434";
        }

        public Functions OpenConnection()
        {
            this.DBConnection.Open();
            return this;
        }

        public static Boolean CheckDBConnection(SqlConnection pDBConnection, int CommandTimeout = -1)
        {
            Boolean Returns = false;

            SqlCommand Command = null;
            SqlDataReader Reader = null;

            try
            {
                Command = new SqlCommand("SELECT GETDATE() AS ServerTime", pDBConnection.OpenGet());

                if (CommandTimeout != -1) { Command.CommandTimeout = CommandTimeout; }

                Reader = Command.ExecuteReader();

                if (Reader.HasRows)
                {
                    Reader.Read();

                    Returns = true; // Convert.ToDateTime(reader["ServerTime"]);
                }

                return Returns;
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
                return false;
            }
            finally
            {
                if ((Reader != null ? !Reader.IsClosed : false)) { Reader.Close(); Reader.Dispose(); }
                Command.Dispose();      
                pDBConnection.Close();
            }
        }

        public static Boolean CheckForUpdates(SqlConnection pDBConnection, int CommandTimeout = -1)
        {

            Boolean UpdateAvailable = false;

            SqlCommand Command = null;
            SqlDataReader Reader = null;

            try
            {
                Command = new SqlCommand("SELECT * FROM SYSTEM_INFORMATION", pDBConnection.OpenGet());

                if (CommandTimeout != -1) { Command.CommandTimeout = CommandTimeout; }

                Reader = Command.ExecuteReader();

                if (Reader.HasRows)
                {
                    Reader.Read();

                    DateTime PublicVersionDate = Convert.ToDateTime(Reader["Windows_Player_PublicVersionDate"]);
                    DateTime CurrentVersionDate = Convert.ToDateTime(Reader["CurrentVersionDate"]);
                    String PublicVersion = Convert.ToString(Reader["Windows_Player_PublicVersion"]).Replace(" ", "").Replace("-", "").Replace("_", "");
                    String CurrentVersion = Convert.ToString(Reader["CurrentVersion"]).Replace(" ", "").Replace("-", "").Replace("_", "");

                    // The following comparisons can guarantee good results for most Common version string types, for example:
                    // Given Left String as Public Version and Right String as Current Version, these will be true:
                    // Just Numbers: 1.1 > 1.0, 1.1 > 1.0.9, 4 > 3, 1.5 < 1.5.1, 3.5.0.1 > 3.5.0.0
                    // Simple Beta / Alpha Support: 1.2 > 1.2 Beta, 1.4 Beta > 1.3, 0.5 Alpha 2 > 0.5 Alpha 1, 3.6 Beta < 4
                    // Both Alpha and Beta: 2.0 Beta > 0.9 Alpha
                    // All of the Above and Other Non-Alpha/Beta Qualifiers: 
                    // 1.1 Rev A < 1.1 Rev B, 1.0, 1.2 Stable/RTM/Final > 1.2 Beta

                    // Known issues: Things such as these will be wrong, either it would be bad practice or they will need a different workaround string.
                    // Basically, any non significant added strings such as
                    // 2.0.0 > 2.0, 2.1 < 2.1 RTM
                    // Or some valid ones that will need to be avoided:
                    // 3.0 < 3.0 Beta 2. In this scenario, the New Public versión would either need to be higher or with any other different string qualifier:
                    // 3.0 Stable/Final/RTM/Plus/+ > 3.0 Beta 1, OR 3.1 > 3.0 Beta 2 and just "3.0" is never released.
                    // 3.0 Final < 3.0 Rev 1. In this scenario, the New Public versión would either need to be higher or as:
                    // 3.0 Rev Final > 3.0 Rev 2, 3.2 > 3.0 Rev C.

                    // This is because in the end it's just a simple String, alphabetical sort order comparison, so it's better to keep things simple
                    // and avoid trying to support everything just to have some really complicated version scheme for the application...

                    // If both versions are equal, such as "1.0" and "1.0", then it will check if by any reason the public version date is higher.

                    var Result = 0;

                    if ((CurrentVersion + PublicVersion).Contains("Beta".ToUpper()) && (CurrentVersion + PublicVersion).Contains("Alpha".ToUpper()))
                    {
                        if (CurrentVersion.Contains("Alpha".ToUpper()) && PublicVersion.Contains("Beta".ToUpper())
                            ||
                            CurrentVersion.Contains("Beta".ToUpper()) && PublicVersion.Contains("Alpha".ToUpper()))
                        {
                            Result = PublicVersion.CompareTo(CurrentVersion);

                            if (Result == -1)
                                UpdateAvailable = false;
                            else if (Result == 0)
                                UpdateAvailable = (PublicVersionDate > CurrentVersionDate);
                            else if (Result == 1)
                                UpdateAvailable = true;
                        }
                    }
                    else if ((CurrentVersion + PublicVersion).Contains("Beta".ToUpper()))
                    {
                        if (CurrentVersion.Contains("Beta".ToUpper()) && PublicVersion.Contains("Beta".ToUpper()))
                        {
                            Result = PublicVersion.CompareTo(CurrentVersion);

                            if (Result == -1)
                                UpdateAvailable = false;
                            else if (Result == 0)
                                UpdateAvailable = (PublicVersionDate > CurrentVersionDate);
                            else if (Result == 1)
                                UpdateAvailable = true;
                        }

                        if (CurrentVersion.Contains("Beta".ToUpper()) && !PublicVersion.Contains("Beta".ToUpper()))
                        {
                            Result = PublicVersion.CompareTo(CurrentVersion.Replace("Beta".ToUpper(), ""));
                            UpdateAvailable = (Result == 0 || Result == 1);
                        }

                        if (!CurrentVersion.Contains("Beta".ToUpper()) && PublicVersion.Contains("Beta".ToUpper()))
                        {
                            Result = PublicVersion.Replace("Beta".ToUpper(), "").CompareTo(CurrentVersion);
                            UpdateAvailable = (Result == 1);
                        }
                    }
                    else if ((CurrentVersion + PublicVersion).Contains("Alpha".ToUpper()))
                    {
                        if (CurrentVersion.Contains("Alpha".ToUpper()) && PublicVersion.Contains("Alpha".ToUpper()))
                        {
                            Result = PublicVersion.CompareTo(CurrentVersion);

                            if (Result == -1)
                                UpdateAvailable = false;
                            else if (Result == 0)
                                UpdateAvailable = (PublicVersionDate > CurrentVersionDate);
                            else if (Result == 1)
                                UpdateAvailable = true;
                        }

                        if (CurrentVersion.Contains("Alpha".ToUpper()) && !PublicVersion.Contains("Alpha".ToUpper()))
                        {
                            Result = PublicVersion.CompareTo(CurrentVersion.Replace("Alpha".ToUpper(), ""));
                            UpdateAvailable = (Result == 0 || Result == 1);
                        }

                        if (!CurrentVersion.Contains("Alpha".ToUpper()) && PublicVersion.Contains("Alpha".ToUpper()))
                        {
                            Result = PublicVersion.Replace("Alpha".ToUpper(), "").CompareTo(CurrentVersion);
                            UpdateAvailable = (Result == 1);
                        }
                    }
                    else
                    {
                        Result = PublicVersion.CompareTo(CurrentVersion);

                        if (Result == -1)
                            UpdateAvailable = false;
                        else if (Result == 0)
                            UpdateAvailable = (PublicVersionDate > CurrentVersionDate);
                        else if (Result == 1)
                            UpdateAvailable = true;
                    }
                }

                return UpdateAvailable;
            }
            catch (Exception Any)
            {
                Console.WriteLine(Any.Message);
                return UpdateAvailable;
            }
            finally
            {
                if ((Reader != null ? !Reader.IsClosed : false)) { Reader.Close(); Reader.Dispose(); }
                Command.Dispose();                
                pDBConnection.Close();
            }
        }

        public DateTime getServerDatetime()
        {
            DateTime Returns = DateTime.Now;
                                  
            SqlCommand Command = null;
            SqlDataReader Reader = null;

            try
            {
                Command = new SqlCommand("SELECT GETDATE() AS ServerTime", DBConnection.OpenGet());

                Reader = Command.ExecuteReader();

                if (Reader.HasRows)
                {
                    Reader.Read();

                    Returns = Convert.ToDateTime(Reader["ServerTime"]);
                }
                                
                return Returns;
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
                return Returns;
            }
            finally
            {
                if ((Reader != null ? !Reader.IsClosed : false)) { Reader.Close(); Reader.Dispose(); }
                Command.Dispose();
                DBConnection.Close();                
            }
            
        }

        public String getNextCampaign(String pCurrentCampaign)
        {
            DateTime myCurrentDate = getServerDatetime();

            string currentDayField = string.Empty;

            //System.Globalization.CultureInfo.CurrentCulture = new System.Globalization.CultureInfo("es-VE").Calendar.GetDayOfWeek;

            //System.Globalization.DateTimeFormatInfo.CurrentInfo.FirstDayOfWeek = DayOfWeek.Monday;

            System.Globalization.DateTimeFormatInfo Info = new System.Globalization.DateTimeFormatInfo();
            Info.FirstDayOfWeek = DayOfWeek.Monday;

            switch (Info.Calendar.GetDayOfWeek(myCurrentDate)) //System.Globalization.DateTimeFormatInfo.CurrentInfo.Calendar.GetDayOfWeek(myCurrentDate))
            {
                case DayOfWeek.Monday:
                    {
                        currentDayField = "OnMonday"; break;
                    }
                case DayOfWeek.Tuesday:
                    {
                        currentDayField = "OnTuesday"; break;
                    }
                case DayOfWeek.Wednesday:
                    {
                        currentDayField = "OnWednesday"; break;
                    }
                case DayOfWeek.Thursday:
                    {
                        currentDayField = "OnThursday"; break;
                    }
                case DayOfWeek.Friday:
                    {
                        currentDayField = "OnFriday"; break;
                    }
                case DayOfWeek.Saturday:
                    {
                        currentDayField = "OnSaturday"; break;
                    }
                case DayOfWeek.Sunday:
                    {
                        currentDayField = "OnSunday"; break;
                    }
            }

            String returns = "0";

            SqlCommand Command = null;
            SqlDataReader Reader = null;

            try
            {
                
                Command = getParameterizedCommand(                                                       
                "SELECT TOP (1) Campaign.ID" +
                "\n" + "FROM CAMPAIGN Campaign INNER JOIN CAMPAIGNxMEDIAPLAYER Schedule" +
                "\n" + "ON Campaign.ID = Schedule.CAMPAIGN_ID" +
                "\n" + "WHERE GETDATE() BETWEEN InitialDate AND EndDate" +
                "\n" + "AND CAST(Campaign.ID AS INT) > @CurrentCampaign" +
                "\n" + "AND Schedule." + currentDayField + " = @CurrentDayField" +
                "\n" + "AND Schedule.MEDIAPLAYER_ID = @MediaPlayerID" +
                "\n" + "AND Campaign.Status = @Status" +
                "\n" + "ORDER BY Campaign.ID ASC, InitialDate ASC", DBConnection.OpenGet(), new SqlParameter[]{
                new SqlParameter() { ParameterName = "@CurrentCampaign", Value = Convert.ToInt32(pCurrentCampaign) },
                new SqlParameter() { ParameterName = "@CurrentDayField", Value = 1 },
                new SqlParameter() { ParameterName = "@MediaPlayerID", Value = MyConfig.Default.MediaPlayerID },
                new SqlParameter() { ParameterName = "@Status", Value = "Active" },
                });

                // Console.WriteLine(command.CommandText);                

                Reader = Command.ExecuteReader();

                if (Reader.HasRows)
                {
                    Reader.Read();

                    returns = Reader["ID"].ToString();                   
                }
                else
                {
                    return pCurrentCampaign;
                }
                
                return returns;
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
                return pCurrentCampaign;
            }
            finally
            {
                if ((Reader != null ? !Reader.IsClosed : false)) { Reader.Close(); Reader.Dispose(); }
                Command.Dispose();
                DBConnection.Dispose();
            }
        }

        public String getCampaignTemplate(String caId)
        {
            string returns = "0";
            try
            {
                SqlCommand command = getParameterizedCommand("SELECT TEMPLATES_ID FROM CAMPAIGN WHERE ID = @CampaignID",
                DBConnection.OpenGet(), new SqlParameter[] { 
                new SqlParameter() { ParameterName = "@CampaignID", Value = caId}
                });

                SqlDataReader reader = command.ExecuteReader();

                if (reader.HasRows)
                {
                    while (reader.Read())
                    {
                        returns = reader["TEMPLATES_ID"].ToString();
                    }
                    reader.NextResult();
                }
                else
                {
                    return "0";
                }

                reader.Close();
                return returns;
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
                return "0";
            }
            finally
            {
                DBConnection.Close();
            }
        }

        public String getCampaignTemplate(String caId, out int TemplateAdCount, out int BorderWidth, out System.Drawing.Color BorderColor)
        {
            string returns = "0"; TemplateAdCount = 0; BorderWidth = 0; BorderColor = System.Drawing.Color.Black;

            SqlCommand Command = null;
            SqlDataReader Reader = null;

            try
            {
                Command = getParameterizedCommand(
                "SELECT CAMPAIGN.TEMPLATES_ID, TEMPLATES.DisplayNumber, CAMPAIGN.BorderWidth, CAMPAIGN.BorderColor" +
                "\n" + "FROM CAMPAIGN INNER JOIN TEMPLATES" +
                "\n" + "ON CAMPAIGN.TEMPLATES_ID = TEMPLATES.ID" +
                "\n" + "WHERE CAMPAIGN.ID = @CampaignID",
                DBConnection.OpenGet(), new SqlParameter[] { 
                new SqlParameter() { ParameterName = "@CampaignID", Value = caId}
                });

                Reader = Command.ExecuteReader();

                if (Reader.HasRows)
                {
                    while (Reader.Read())
                    {
                        returns = Reader["TEMPLATES_ID"].ToString();
                        TemplateAdCount = Convert.ToInt32(Reader["DisplayNumber"]);
                        BorderWidth = Convert.ToInt32(Reader["BorderWidth"]);
                        BorderColor = getColorOrDefault(Reader["BorderColor"].ToString(), System.Drawing.Color.Black);
                    }
                    Reader.NextResult();
                }
                else
                {                    
                    return "0";
                }
                
                return returns;
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);               
                return "0";
            }
            finally
            {
                if ((Reader != null ? !Reader.IsClosed : false)) { Reader.Close(); Reader.Dispose(); }
                Command.Dispose();
                DBConnection.Dispose();
            }
        }

        public String nextSecuenceAd(String pCampaignId, int pSecuence, ref int pCurrentOrder)
        {
            String returns = "0";

            SqlCommand Command = null;
            SqlDataReader Reader = null;

            try
            {
                Command = getParameterizedCommand(
                "SELECT TOP (1) ADS_ID, SecuenceOrder FROM CAMPAIGNxADS" +
                "\n" + "WHERE Location = @Location" +
                "\n" + "AND CAMPAIGN_ID = @CampaignID" +
                "\n" + "AND SecuenceOrder > @CurrentOrder" + 
                "\n" + "ORDER BY SecuenceOrder ASC", DBConnection.OpenGet(), new SqlParameter[] {
                    new SqlParameter () { ParameterName = "@Location", Value = pSecuence },
                    new SqlParameter () { ParameterName = "@CurrentOrder", Value = pCurrentOrder },
                    new SqlParameter () { ParameterName = "@CampaignID", Value = pCampaignId },
                });

                Reader = Command.ExecuteReader();

                if (Reader.HasRows)
                {
                    Reader.Read();

                    returns = Reader["ADS_ID"].ToString();
                    pCurrentOrder = Convert.ToInt32(Reader["SecuenceOrder"].ToString());
                }
                else
                {
                    pCurrentOrder++;
                }
                
                return returns;
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
                return "0";
            }
            finally
            {
                if ((Reader != null ? !Reader.IsClosed : false)) { Reader.Close(); Reader.Dispose(); }
                Command.Dispose();
                DBConnection.Dispose();
            }
        }

        public int getCampaignLifeTime(String pCampaignId)
        {
            int returns = 1;

            SqlCommand Command = null;
            SqlDataReader Reader = null;

            try
            {

                Command = getParameterizedCommand(
                    "SELECT isNULL(MAX(SecuenceFullTime), 5) + ((50/100) * isNULL(MAX(NumSecuences),1)) AS CampaignLifeTime FROM (" +
                    "\n" + "SELECT CAMPAIGN_ID, Location, SUM(DisplayTime) AS SecuenceFullTime, MAX(SecuenceOrder) AS NumSecuences" +
                    "\n" + "FROM CAMPAIGNxADS" +
                    "\n" + "WHERE CAMPAIGN_ID = @CampaignID" +
                    "\n" + "GROUP BY CAMPAIGN_ID, Location" +
                    "\n" + ") TB", DBConnection.OpenGet(), new SqlParameter [] {
                    new SqlParameter () { ParameterName = "@CampaignID", Value = pCampaignId }
                    });

                Reader = Command.ExecuteReader();

                //if (reader.HasRows)
                //{
                Reader.Read();
                returns = Convert.ToInt32(Reader["CampaignLifeTime"].ToString());
                //}
                //else
                //{
                    //returns = 1;
                //}
                
                return returns;
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
                return returns;
            }
            finally
            {
                if ((Reader != null ? !Reader.IsClosed : false)) { Reader.Close(); Reader.Dispose(); }
                Command.Dispose();
                DBConnection.Dispose();
            }
        }

        public Boolean isActivated()
        {
            Boolean Returns = false;

            try
            {
                SqlCommand command = getParameterizedCommand(
                "SELECT Identifier, License, ActivationKey" +
                "\n" + "FROM MEDIAPLAYER WHERE ID = @MediaPlayerID", DBConnection.OpenGet(), new SqlParameter[] {
                    new SqlParameter () { ParameterName = "@MediaPlayerID", Value = MyConfig.Default.MediaPlayerID }
                });

                SqlDataReader reader = command.ExecuteReader();

                if (reader.HasRows)
                {
                    reader.Read();

                    //if (Variables.DebugMode) System.Windows.Forms.MessageBox.Show("I: " + Convert.ToString(reader["Identifier"]));
                    //if (Variables.DebugMode) System.Windows.Forms.MessageBox.Show("S: " + Variables.ProductSerial);
                    //if (Variables.DebugMode) System.Windows.Forms.MessageBox.Show("A: " + Convert.ToString(reader["ActivationKey"]));
                    //if (Variables.DebugMode) System.Windows.Forms.MessageBox.Show("C: " + Convert.ToString(Variables.ProductCode));

                    Returns = new BIGWISE.ProductStatus.Register().isActivated(
                        Convert.ToString(reader["Identifier"]),
                        Variables.ProductSerial, // Convert.ToString(reader["License"]),
                        Convert.ToString(reader["ActivationKey"]),
                        Variables.ProductCode 
                        );
                }
                else
                {
                    Returns = false;
                }

                reader.Close();
                return Returns;
            }
            catch (Exception Any)
            {
                Program.Logger.EscribirLog(Any, "Failed Check.");
                return false;
            }
            finally
            {
                DBConnection.Close();
            }
        }

        public Boolean Activate(String ActivationKey)
        {
            Boolean Returns = false;

            try
            {
                SqlCommand command = getParameterizedCommand(
                "UPDATE MEDIAPLAYER SET ActivationKey = @ActivationKey" +
                "\n" + "WHERE ID = @MediaPlayerID", DBConnection.OpenGet(), new SqlParameter[] {
                    new SqlParameter () { ParameterName = "@MediaPlayerID", Value = MyConfig.Default.MediaPlayerID },
                    new SqlParameter () { ParameterName = "@ActivationKey", Value = ActivationKey }
                });

                command.ExecuteNonQuery();

                Returns = isActivated();
              
                return Returns;
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
                return false;
            }
            finally
            {
                DBConnection.Close();
            }
        }    

        public static Boolean ActivateMediaPlayer()
        {

            // Variables.ProductSerial = GetSerial(); // Set at Init.

            String ActivationKey = String.Empty;                           

            SqlConnection DBConnection = null;
            SqlTransaction LocalTransaction = null;
            SqlTransaction TmpTransaction = null;
            SqlCommand Command = null;

            try
            {
                /*
                System.Windows.Forms.MessageBox.Show(
                    Variables.SelectedMediaPlayer.Identifier + Functions.NewLine() +
                    Variables.ProductSerial
                );
                */

                ActivationKey = new BIGWISE.ProductStatus.Register().GetActivationKey(
                    Variables.SelectedMediaPlayer.Identifier, Variables.ProductSerial, Variables.ProductCode
                );

                /*
                System.Windows.Forms.MessageBox.Show(
                    Variables.SelectedMediaPlayer.Identifier + Functions.NewLine() +
                    Variables.ProductSerial + Functions.NewLine() +
                    ActivationKey
                );
                */

                DBConnection = new SqlConnection(ConnectionData).OpenGet();
                LocalTransaction = DBConnection.BeginTransaction(System.Data.IsolationLevel.ReadUncommitted);
                TmpTransaction = Variables.TmpConnection.BeginTransaction(System.Data.IsolationLevel.ReadUncommitted);

                Command = getParameterizedCommand("SELECT isNULL(Count(MEDIAPLAYER.License), 0) AS MediaPlayerExists FROM MEDIAPLAYER" +
                    Functions.NewLine() + "WHERE MEDIAPLAYER.Status <> @Status AND MEDIAPLAYER.License = @License" +
                    Functions.NewLine() + "GROUP BY MEDIAPLAYER.License",
                    Variables.TmpConnection, TmpTransaction, new SqlParameter[]{
                        new SqlParameter(){ ParameterName = "@License", Value = Variables.ProductSerial },
                        new SqlParameter(){ ParameterName = "@Status",  Value = "Inactive" }
                    });

                Boolean LicenseDuplication = Convert.ToBoolean(Command.ExecuteScalar());

                /*
                System.Windows.Forms.MessageBox.Show(
                    LicenseDuplication.ToString() + " - " + Convert.ToInt32(Command.ExecuteScalar())
                );
                */

                if (LicenseDuplication) throw new Exception("License Serial already applied. Attempt to duplicate license has been denied.");

                Command = getParameterizedCommand(
                "TRUNCATE TABLE MEDIAPLAYER;" + 
                "\nINSERT INTO MEDIAPLAYER" +
                "\n(ID, ORGANIZATION_ID, LOCATION_ID, Description, Serial, MACAddress, Brand, Model, Identifier, License, ActivationKey, CreationDate, UpdateDate, Status, Ubication)" +
                "\nVALUES" +
                "\n(@ID, @OrgID, @LocationID, @Desc, @Serial, @MAC, @Brand, @Model, @Identifier, @License, @Key, GetDate(), GetDate(), @Status, @Ubication)",
                DBConnection, LocalTransaction, new SqlParameter[]{
                new SqlParameter(){ ParameterName = "@ID", Value = Variables.SelectedMediaPlayer.ID },
                new SqlParameter(){ ParameterName = "@OrgID", Value = Variables.SelectedOrganizationID },
                new SqlParameter(){ ParameterName = "@LocationID", Value = Variables.SelectedLocation.ID },
                new SqlParameter(){ ParameterName = "@Desc", Value = Variables.SelectedMediaPlayer.Description },
                new SqlParameter(){ ParameterName = "@Serial", Value = String.Empty }, // get Computer Serial?
                new SqlParameter(){ ParameterName = "@MAC", Value = String.Empty }, // get Computer MAC Address?
                new SqlParameter(){ ParameterName = "@Brand", Value = Variables.SelectedMediaPlayer.Brand },
                new SqlParameter(){ ParameterName = "@Model", Value = Variables.SelectedMediaPlayer.Model },
                new SqlParameter(){ ParameterName = "@Identifier", Value = Variables.SelectedMediaPlayer.Identifier },
                new SqlParameter(){ ParameterName = "@License", Value = Variables.ProductSerial },
                new SqlParameter(){ ParameterName = "@Key", Value = ActivationKey },
                //new SqlParameter(){ ParameterName = "@CDate", Value = "GetDate()" },
                //new SqlParameter(){ ParameterName = "@UDate", Value = "GetDate()" },
                new SqlParameter(){ ParameterName = "@Status", Value = "Active" },
                new SqlParameter(){ ParameterName = "@Ubication", Value = Variables.SelectedMediaPlayer.Ubication }
                });

                Command.ExecuteNonQuery();

                Command = getParameterizedCommand(
                "TRUNCATE TABLE LOCATION;" + 
                "INSERT INTO LOCATION" +
                "\n(ID, ORGANIZATION_ID, Name, Address, City, State, Country, Status)" +
                "\nVALUES" +
                "\n(@ID, @OrgID, @Name, @Address, @City, @State, @Country, @Status)",
                DBConnection, LocalTransaction, new SqlParameter[]{
                new SqlParameter(){ ParameterName = "@ID", Value = Variables.SelectedLocation.ID },
                new SqlParameter(){ ParameterName = "@OrgID", Value = Variables.SelectedOrganizationID },               
                new SqlParameter(){ ParameterName = "@Name", Value = Variables.SelectedLocation.Name },
                new SqlParameter(){ ParameterName = "@Address", Value = Variables.SelectedLocation.Address },
                new SqlParameter(){ ParameterName = "@City", Value = Variables.SelectedLocation.City },
                new SqlParameter(){ ParameterName = "@State", Value = Variables.SelectedLocation.State },
                new SqlParameter(){ ParameterName = "@Country", Value = Variables.SelectedLocation.Country},
                new SqlParameter(){ ParameterName = "@Status", Value = "Active" }                
                });

                Command.ExecuteNonQuery();

                Command = getParameterizedCommand(
                "UPDATE MEDIAPLAYER SET" +
                "\nSerial = @Serial," +
                "\nMACAddress = @MAC," +
                "\nLicense = @License," +
                "\nActivationKey = @Key," +
                "\nUpdateDate = GetDate()," +
                "\nStatus = @Status" +
                "\nWHERE ID = @ID", Variables.TmpConnection, TmpTransaction, new SqlParameter[]{
                new SqlParameter(){ ParameterName = "@ID", Value = Variables.SelectedMediaPlayer.ID },
                new SqlParameter(){ ParameterName = "@Serial", Value = Variables.Seed }, //Save our GUID.
                new SqlParameter(){ ParameterName = "@MAC", Value = String.Empty },
                new SqlParameter(){ ParameterName = "@Key", Value = ActivationKey },
                new SqlParameter(){ ParameterName = "@License", Value = Variables.ProductSerial },
                new SqlParameter(){ ParameterName = "@Status", Value = "Active" }
                });

                Command.ExecuteNonQuery();

                Command = getParameterizedCommand("UPDATE [ADS] SET ContentSerial = '', FileSerial = '', SizeInBytes = 0", DBConnection, LocalTransaction, null);
                Command.ExecuteNonQuery();

                LocalTransaction.Commit();
                TmpTransaction.Commit();

                //try
                //{
                    MyConfig.Default.MediaPlayerID = Variables.SelectedMediaPlayer.ID;
                    MyConfig.Default.Save();

                    Variables.SelectedMediaPlayer = null;
                    Variables.SelectedLocation = null;                       
                //} catch (Exception) { }                 

                return true;
            }
            catch (Exception ex)
            {   
                LocalTransaction.Rollback();
                TmpTransaction.Rollback();

                Console.WriteLine(ex.Message);
                
                /*
                System.Windows.Forms.MessageBox.Show(
                    ex.Message + Functions.NewLine(2) + Command.CommandText
                );
                */

                return false;
            }
            finally
            {
                DBConnection.Close();
                Variables.TmpConnection.Close();
            }

        }

        public static void Sync()
        {
            String CurrentAppPath = System.Reflection.Assembly.GetExecutingAssembly().Location;
            String CurrentAppDir = System.IO.Path.GetDirectoryName(CurrentAppPath);

            if (System.IO.Directory.Exists(CurrentAppDir + "\\Sync"))
            {
                RunProgram(CurrentAppDir + "\\Sync\\ADS360_Sync.exe");
            }
            else
            {
                RunProgram(CurrentAppDir + "\\Sync.exe");
            }
        }

        public int nextAd_DisplayTime(String pId, int pSecuence, int pCurrentOrder, String pCampaignId)
        {
            int returns = 1;

            try
            {
                SqlCommand command = getParameterizedCommand(
                "SELECT TOP (1) DisplayTime FROM CAMPAIGNxADS" +
                "\n" + "WHERE Location = @Secuence" +
                "\n" + "AND SecuenceOrder = @CurrentOrder" +
                "\n" + "AND CAMPAIGN_ID = @CampaignID" +
                "\n" + "AND ADS_ID = @ID", DBConnection.OpenGet(), new SqlParameter[] {
                    new SqlParameter () { ParameterName = "@Secuence", Value = pSecuence },
                    new SqlParameter () { ParameterName = "@CurrentOrder", Value = pCurrentOrder },
                    new SqlParameter () { ParameterName = "@CampaignID", Value = pCampaignId },
                    new SqlParameter () { ParameterName = "@ID", Value = pId }
                });

                SqlDataReader reader = command.ExecuteReader();

                if (reader.HasRows)
                {
                    reader.Read();
                    returns = Convert.ToInt32(reader["DisplayTime"].ToString());
                }
                else
                {
                    returns = 1;
                    pCurrentOrder++;
                }

                reader.Close();
                return returns;
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
                return 1;
            }
            finally
            {
                DBConnection.Close();
            }
        }

        public int nextAd_InteractionTime(String pId, int pSecuence, int pCurrentOrder, String pCampaignId)
        {
            int returns = 1;

            try
            {
                SqlCommand command = getParameterizedCommand(
                "SELECT TOP (1) InteractionTime FROM CAMPAIGNxADS" +
                "\n" + "WHERE Location = @Secuence" +
                "\n" + "AND SecuenceOrder = @CurrentOrder" +
                "\n" + "AND CAMPAIGN_ID = @CampaignID" +
                "\n" + "AND ADS_ID = @ID", DBConnection.OpenGet(), new SqlParameter[] {
                    new SqlParameter () { ParameterName = "@Secuence", Value = pSecuence },
                    new SqlParameter () { ParameterName = "@CurrentOrder", Value = pCurrentOrder },
                    new SqlParameter () { ParameterName = "@CampaignID", Value = pCampaignId },
                    new SqlParameter () { ParameterName = "@ID", Value = pId }
                });

                SqlDataReader reader = command.ExecuteReader();

                if (reader.HasRows)
                {
                    reader.Read();
                    returns = Convert.ToInt32(reader["InteractionTime"].ToString());
                }
                else
                {
                    returns = 0;
                    pCurrentOrder++;
                }

                reader.Close();
                return returns;
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
                return 0;
            }
            finally
            {
                DBConnection.Close();
            }
        }

        public int getAdCount(int pTemplateID)
        {
            int returns = 0;
            try
            {
                SqlCommand command = getParameterizedCommand("SELECT DisplayNumber FROM TEMPLATES WHERE ID = @ID", 
                DBConnection.OpenGet(), new SqlParameter[] {
                    new SqlParameter () { ParameterName = "@ID", Value = pTemplateID.ToString() }
                });

                SqlDataReader reader = command.ExecuteReader();

                if (reader.HasRows)
                {
                    reader.Read();
                    returns = int.Parse(reader["DisplayNumber"].ToString());                     
                }
                else
                {
                    returns = 0;
                }

                reader.Close();
                return returns;
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
                return 0;
            }
            finally
            {
                DBConnection.Close();                
            }
        }

        public Boolean FrmConfig_GetScreens(FrmConfig_ScreenSelect myFrmConfig) {

            Boolean Returns = false;

            try
            {
                SqlCommand command = new SqlCommand(
                "SELECT DISTINCT ID, Description " +
                "\n" + "FROM MEDIAPLAYER ", DBConnection.OpenGet());

                SqlDataReader reader = command.ExecuteReader();

                if (reader.HasRows)
                {
                    myFrmConfig.ScreenIDsComboBox.Items.Clear();

                    while(reader.Read()){
                        myFrmConfig.ScreenIDsComboBox.Items.Add(reader["ID"]);                     
                    }

                    Returns = true;
                }
                else
                {
                    Returns = false;
                }

                reader.Close();
                return Returns;
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
                return false;
            }
            finally
            {
                DBConnection.Close();
            }

        }

        public Boolean FrmConfig_GetScreens(FrmConfig myFrmConfig, SqlConnection pDBConnection, out String ErrorDescription)
        {
            ErrorDescription = String.Empty;
            Boolean Returns = false;

            try
            {
                SqlCommand command = new SqlCommand(
                "SELECT DISTINCT ID, Description " +
                "\n" + "FROM MEDIAPLAYER ", pDBConnection.OpenGet());

                SqlDataReader reader = command.ExecuteReader();

                if (reader.HasRows)
                {
                    myFrmConfig.ScreenIDsComboBox.Items.Clear();

                    while (reader.Read())
                    {
                        myFrmConfig.ScreenIDsComboBox.Items.Add(reader["ID"]);
                    }

                    Returns = true;
                }
                else
                {
                    Returns = false;
                }

                reader.Close();
                return Returns;
            }
            catch (Exception ex)
            {
                ErrorDescription = ex.Message;
                return false;
            }
            finally
            {
                pDBConnection.Close();
            }

        }

        public static String getOrganizationID(SqlConnection DBConnection, String OrganizationLicense, Boolean CloseConnection = false)
        {
            String returns = "undefined";

            if (OrganizationLicense.isUndefined()) return returns;

            try
            {
                SqlCommand command = getParameterizedCommand(
                "SELECT ID FROM ORGANIZATION WHERE SetupKey = @OrganizationLicense", 
                DBConnection.OpenGet(), new SqlParameter[] {
                    new SqlParameter () { ParameterName = "@OrganizationLicense", Value = OrganizationLicense },
                });

                SqlDataReader reader = command.ExecuteReader();

                if (reader.HasRows)
                {
                    reader.Read();

                    returns = Convert.ToString(reader["ID"]);
                }

                reader.Close();
                return returns;
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
                return returns;
            }
            finally
            {
                if (CloseConnection) DBConnection.Close();
            }
        }

        private static string CPU_ID()
        {
            //Uses first CPU identifier available in order of preference
            //Don't get all identifiers, as very time consuming
            string retVal = identifier("Win32_Processor", "UniqueId");
            if (retVal == "") //If no UniqueID, use ProcessorID
            {
                retVal = identifier("Win32_Processor", "ProcessorId");
                if (retVal == "") //If no ProcessorId, use Name
                {
                    retVal = identifier("Win32_Processor", "Name");
                    if (retVal == "") //If no Name, use Manufacturer
                    {
                        retVal = identifier("Win32_Processor", "Manufacturer");
                    }
                    //Add clock speed for extra security
                    retVal += identifier("Win32_Processor", "MaxClockSpeed");
                }
            }
            return retVal;
        }

            private static string identifier(string wmiClass, string wmiProperty)
            {
                string result = "";

                System.Management.ManagementClass mc = new System.Management.ManagementClass(wmiClass);
                System.Management.ManagementObjectCollection moc = mc.GetInstances();

                foreach (System.Management.ManagementObject mo in moc)
                {
                    //Only get the first one
                    if (result == "")
                    {
                        try
                        {
                            result = mo[wmiProperty].ToString();
                            break;
                        }
                        catch
                        {
                        }
                    }
                }

                return result;
            }

            public static string identifier(string wmiClass, string wmiProperty, string wmiMustBeTrue)
            {
                string result = "";

                System.Management.ManagementClass mc = new System.Management.ManagementClass(wmiClass);
                System.Management.ManagementObjectCollection moc = mc.GetInstances();

                foreach (System.Management.ManagementObject mo in moc)
                {
                    if (mo[wmiMustBeTrue].ToString() == "True")
                    {
                        //Only get the first one
                        if (result == "")
                        {
                            try
                            {
                                result = mo[wmiProperty].ToString();
                                break;
                            }
                            catch
                            {
                            }
                        }
                    }
                }

                return result;
            }

            private static string BIOS_ID()
            {
                return identifier("Win32_BIOS", "Manufacturer")
                + identifier("Win32_BIOS", "SMBIOSBIOSVersion")
                + identifier("Win32_BIOS", "IdentificationCode")
                + identifier("Win32_BIOS", "SerialNumber")
                + identifier("Win32_BIOS", "ReleaseDate")
                + identifier("Win32_BIOS", "Version");
            }

            public static string MAC_ADDRESS()
            {
                return identifier("Win32_NetworkAdapterConfiguration", "MACAddress", "IPEnabled");
            }

            private static string GetHexString(byte[] bt)
            {
                string s = string.Empty;
                for (int i = 0; i < bt.Length; i++)
                {
                    byte b = bt[i];
                    int n, n1, n2;
                    n = (int)b;
                    n1 = n & 15;
                    n2 = (n >> 4) & 15;
                    if (n2 > 9)
                        s += ((char)(n2 - 10 + (int)'A')).ToString();
                    else
                        s += n2.ToString();
                    if (n1 > 9)
                        s += ((char)(n1 - 10 + (int)'A')).ToString();
                    else
                        s += n1.ToString();
                    if ((i + 1) != bt.Length && (i + 1) % 2 == 0) s += "-";
                }
                return s;
            }

            private static string GetHash(String s)
            {
                try
                {
                    MD5 sec = new MD5CryptoServiceProvider();
                    ASCIIEncoding enc = new ASCIIEncoding();
                    byte[] bt = enc.GetBytes(s);
                    return GetHexString(sec.ComputeHash(bt));
                }
                catch (Exception)
                {
                    return String.Empty;            
                }
                
            }

            public static String GetSerial()
            {
                //return GetHash("[" + "CPU-ID->" + CPU_ID() + "|" + "BIOS-ID->" + BIOS_ID() + "]"); //High risk of duplication.
                //return GetHash("[" + "CPU-ID->" + CPU_ID() + "|" + "BIOS-ID->" + BIOS_ID() + "|" + "MAC-ADDRESS->" + Functions.MAC_ADDRESS() + "]"); //Non-Reliable.
                //if (Variables.DebugMode) System.Windows.Forms.MessageBox.Show("[" + "CPU-ID->" + CPU_ID() + "|" + "BIOS-ID->" + BIOS_ID() + "|" + "SOFT-ADDRESS->" + Variables.Seed + "]");
                return GetHash("[" + "CPU-ID->" + CPU_ID() + "|" + "BIOS-ID->" + BIOS_ID() + "|" + "SOFT-ADDRESS->" + Variables.Seed + "]");
            }

            /// <summary>
            /// <para> </para>
            /// This function should work like a quick Javascript setTimeout(CallBack, Time); 
            /// <para> </para>
            /// Executes the method provided by the parameter WhatToDo. Not sure how much this could do
            /// <para></para>
            /// but at least it is good for executing quick voids ().
            /// <para> </para>
            /// </summary>
            /// <param name="WhatToDo">The quick action you want to do. You can use a syntax like this:
            /// <para> </para>
            /// delegate(Object WhateverNameYouChooseForaSystem.Timers.Timer, System.Timers.ElapsedEventArgs anyName) {
            /// <para> </para>
            ///  Your code;
            /// <para> </para>
            ///  ((System.Timers.Timer)WhateverNameYouChooseForaSystem.Timers.Timer).Dispose(); // Don't Forget to dispose the timer to avoid wasting memory...            
            /// <para> </para>
            /// }
            /// <para> </para>
            /// Or... just provide a named method. Like: NamedMethod (Being defined somewhere as above). Example:
            /// <para> </para>
            /// public void NamedMethod(Object myTimer, System.Timers.ElapsedEventArgs Event)
            /// <para></para>
            /// {
            /// <para></para>
            /// ...
            /// <para></para>
            /// }
            /// </param>  
            /// <param name="Time">Time in milliseconds to do the action.</param>            
            public static void setTimeOut(long Time, Action<Object, System.Timers.ElapsedEventArgs> WhatToDo)
            {
                System.Timers.Timer TimeOut = new System.Timers.Timer(Time);
                TimeOut.AutoReset = false;
                TimeOut.Elapsed += new System.Timers.ElapsedEventHandler(WhatToDo);
                TimeOut.Enabled = true;
            }

            //public static string ByteToHex(byte[] bin)
            //{
            //    string Result = "";

            //    for (int i = 0; i < bin.Length; i++)
            //    {
            //        Result += bin[i].ToString("X"); // Without any 0 Padding.
            //    }
            //    return (Result);
            //}

            public static string ByteToHex2(byte[] bin)
            {
                string Result = "";

                for (int i = 0; i < bin.Length; i++)
                {
                    Result += bin[i].ToString("X2"); // Ex: byte [1] -> "01" instead of "1"
                }
                return (Result);
            }

            public static byte[] Hex2ToByte(String Text)
            {
                if (Text == null)
                    return null;

                //if (Text.Length % 2 == 1)
                //    Text = '0' + Text; // Up to you whether to pad the first or last byte

                byte[] bin = new byte[Text.Length / 2];

                for (int i = 0; i < bin.Length; i++)
                    bin[i] = Convert.ToByte(Text.Substring(i * 2, 2), 16);

                return bin;
            }

            public static void SimpleClickEffect(System.Windows.Forms.Form Activator)
            {
                try
                {
                    Variables.ClickEffects.ShowLocusEffect(Activator, 
                    new System.Drawing.Point(System.Windows.Forms.Control.MousePosition.X, System.Windows.Forms.Control.MousePosition.Y),
                    "DefaultBeacon");
                }
                catch (Exception)
                {

                }   
            }

            public static void LoadEffects()
            {
                try
                {
                    Variables.ClickEffects = new BigMansStuff.LocusEffects.LocusEffectsProvider();
                    Variables.ClickEffects.FramesPerSecond = 60;
                    Variables.ClickEffects.Initialize();

                    System.Drawing.Color AnimationColor = System.Drawing.Color.White;

                    (Variables.ClickEffects.GetLocusEffects()["DefaultBeacon"] as BigMansStuff.LocusEffects.BeaconLocusEffect).AnimationStartColor = AnimationColor;
                    (Variables.ClickEffects.GetLocusEffects()["DefaultBeacon"] as BigMansStuff.LocusEffects.BeaconLocusEffect).AnimationEndColor = AnimationColor; // Color.FromArgb(255, 220, 220, 0); // Color.Blue;
                    (Variables.ClickEffects.GetLocusEffects()["DefaultBeacon"] as BigMansStuff.LocusEffects.BeaconLocusEffect).AnimationOuterColor = AnimationColor;
                    (Variables.ClickEffects.GetLocusEffects()["DefaultBeacon"] as BigMansStuff.LocusEffects.BeaconLocusEffect).OuterRingWidth = 0;
                    (Variables.ClickEffects.GetLocusEffects()["DefaultBeacon"] as BigMansStuff.LocusEffects.BeaconLocusEffect).InitialSize = new System.Drawing.Size(130, 130);
                    (Variables.ClickEffects.GetLocusEffects()["DefaultBeacon"] as BigMansStuff.LocusEffects.BeaconLocusEffect).RingWidth = 5;
                    (Variables.ClickEffects.GetLocusEffects()["DefaultBeacon"] as BigMansStuff.LocusEffects.BeaconLocusEffect).AnimationTime = 300;
                }
                catch (Exception)
                {

                }                
            }

            public static void LoadSettings()
            {
                //if (MyConfig.Default.UpgradeRequired)
                //{
                //    MyConfig.Default.Upgrade();
                //    MyConfig.Default.UpgradeRequired = false;
                //    MyConfig.Default.Save();
                //}

                // Load All Settings using the LoadValue function which chains a SettingsProperty with
                // a Section and a Key Name in the final .ini style File. 
                try
                {
                    if (!(System.IO.Directory.Exists(QuickSettings.FolderPath))) 
                        System.IO.Directory.CreateDirectory(QuickSettings.FolderPath);
                }
                catch (Exception e)
                {
                    Console.WriteLine(e.Message);                   
                }

                if (MyConfig.Default.PropertyValues.Count <= 0)
                {
                    foreach (System.Configuration.SettingsProperty Property in MyConfig.Default.Properties.OfType<System.Configuration.SettingsProperty>())
                    {
                        System.Configuration.SettingsPropertyValue Value = new System.Configuration.SettingsPropertyValue(Property);
                        MyConfig.Default.PropertyValues.Add(Value);
                    }
                }

                MyConfig.Default.Properties["Language"].LoadValue("General", "Language");
                MyConfig.Default.Properties["MediaPlayerID"].LoadValue("General", "MediaPlayerID");
                MyConfig.Default.Properties["ApplicationVolume"].LoadValue("General", "Volume");

                MyConfig.Default.Properties["SyncToServer"].LoadValue("Sync", "SyncToServer");
                MyConfig.Default.Properties["UpdateIntervalTimeSpan"].LoadValue("Sync", "UpdateIntervalTimeSpan");

                MyConfig.Default.Properties["SoftwareUpdate_CheckForUpdates"].LoadValue("SoftwareUpdate", "CheckForUpdates");
                MyConfig.Default.Properties["SoftwareUpdate_CampaignsInterval"].LoadValue("SoftwareUpdate", "CampaignsInterval");

                MyConfig.Default.Properties["EnableDefaultCampaign"].LoadValue("General", "EnableDefaultCampaign");
                MyConfig.Default.Properties["DefaultCampaignID"].LoadValue("General", "DefaultCampaignID");
                MyConfig.Default.Properties["ResetTimeSpan"].LoadValue("General", "ResetTimeSpan");          

                MyConfig.Default.Properties["Organization_ID"].LoadValue("Organization", "LicenseKey"); // ID
                // MyConfig.Default.Properties["Organization_SetupKey"].LoadValue("Organization", "SetupKey");

                MyConfig.Default.Properties["CampaignFilesDir"].LoadValue("General", "CampaignFilesDir");
                MyConfig.Default.Properties["ProgramDataDir"].LoadValue("General", "ProgramDataDir");

                MyConfig.Default.Properties["LocalDB_WindowsAuth"].LoadValue("LocalDB", "WindowsAuth");
                MyConfig.Default.Properties["LocalDB_SQLServerName"].LoadValue("LocalDB", "SQLServerName");
                MyConfig.Default.Properties["LocalDB_SQLDBName"].LoadValue("LocalDB", "SQLDBName");
                MyConfig.Default.Properties["LocalDB_SQLUser"].LoadValue("LocalDB", "SQLUser");
                MyConfig.Default.Properties["LocalDB_SQLPass"].LoadValue("LocalDB", "SQLPass");

                MyConfig.Default.Properties["RemoteDB_CustomServer"].LoadValue("RemoteDB", "CustomServer");
                MyConfig.Default.Properties["RemoteDB_Beta"].LoadValue("RemoteDB", "Beta");
                MyConfig.Default.Properties["RemoteDB_WindowsAuth"].LoadValue("RemoteDB", "WindowsAuth");
                MyConfig.Default.Properties["RemoteDB_SQLServerName"].LoadValue("RemoteDB", "SQLServerName");
                MyConfig.Default.Properties["RemoteDB_SQLDBName"].LoadValue("RemoteDB", "SQLDBName");
                MyConfig.Default.Properties["RemoteDB_SQLUser"].LoadValue("RemoteDB", "SQLUser");
                MyConfig.Default.Properties["RemoteDB_SQLPass"].LoadValue("RemoteDB", "SQLPass");

                MyConfig.Default.Properties["LocalFilesSync"].LoadValue("FilesSynchronization", "LocalFilesSource");
                MyConfig.Default.Properties["LocalFilesSyncDir"].LoadValue("FilesSynchronization", "LocalFilesDir");

                MyConfig.Default.Properties["LastCampaignDisplayed"].LoadValue("LoggedValues_ReadOnly", "LastCampaignDisplayed");
                MyConfig.Default.Properties["TotalCampaignsFinished"].LoadValue("LoggedValues_ReadOnly", "TotalCampaignsFinished");
                MyConfig.Default.Properties["TotalCampaingsAndSessionsDisplayed"].LoadValue("LoggedValues_ReadOnly", "TotalCampaingsAndSessionsDisplayed");

                // Loading values mark properties as "Changed", but we are essentially initializing them so we 
                // use this function to Remove "Changed" status for all settings.

                MyConfig.Default.Clean();

                // From now on, we access settings using the regular strongly-typed settings class which we define
                // using the Properties.Settings UI. Note that you should define all properties which won't
                // be ReadOnly as UserScoped. If you use some ApplicationScoped Settings, just can just use them
                // but you can't write any values to them, you would get a Runtime Exception by using LoadValue or a
                // Compiler Error if you intend to assign other values explicitly using the strongly-typed members.

                // So here is as we use them:

                // MyConfig.Default.Language = "en";

                // And here we will save all settings which were Changed.

                // MyConfig.Default.Save();

                ConnectionData = getConnectionData();

            }

            public static void LocalizeApplication()
            {
                try
                {
                    if (Variables.DefaultOSCultureName.isUndefined()) Variables.DefaultOSCultureName = System.Threading.Thread.CurrentThread.CurrentCulture.Name;

                    //System.Globalization.CultureInfo.DefaultThreadCurrentCulture = System.Threading.Thread.CurrentThread.CurrentCulture;
                    //// In the above line we set NULL = DefaultOSCulture. According to tests DefaultOSUICulture might not be equal to DefaultOSCulture
                    //// Though DefaultOSCulture is the one with the right Location based on Culture. DefaultOSUICulture might refer just to the generic language default
                    //// Development Case: Location - Venezuela. DefaultOSCulture = es-VE, DefaultOSUICulture = es-ES.
                    //System.Globalization.CultureInfo.DefaultThreadCurrentUICulture = System.Globalization.CultureInfo.DefaultThreadCurrentCulture;
                    //// So... set DefaultOSUICulture = DefaultOSCulture.

                    try
                    {
                        if ((!MyConfig.Default.Language.Equals("Default", StringComparison.OrdinalIgnoreCase)) && (!MyConfig.Default.Language.isUndefined()))
                        {
                            System.Globalization.CultureInfo.DefaultThreadCurrentCulture = new System.Globalization.CultureInfo(MyConfig.Default.Language);
                            System.Globalization.CultureInfo.DefaultThreadCurrentUICulture =
                            System.Globalization.CultureInfo.DefaultThreadCurrentCulture;
                            goto SelectedLanguage;
                        }
                        else
                        {
                            goto DefaultLanguage;                           
                        }

                    }
                    catch (Exception)
                    {
                        goto DefaultLanguage;
                    }

                    DefaultLanguage:
                    { 
                        //Same as the first statements... Just to get rid of the Initial Cultures which might be read-Only.
                        System.Globalization.CultureInfo.DefaultThreadCurrentCulture = new System.Globalization.CultureInfo(Variables.DefaultOSCultureName);
                        System.Globalization.CultureInfo.DefaultThreadCurrentUICulture =
                        System.Globalization.CultureInfo.DefaultThreadCurrentCulture;             
                    }

                    SelectedLanguage:
                    { 
                        System.Threading.Thread.CurrentThread.CurrentCulture = System.Globalization.CultureInfo.DefaultThreadCurrentCulture;
                        System.Threading.Thread.CurrentThread.CurrentUICulture = System.Globalization.CultureInfo.DefaultThreadCurrentUICulture;
                    }

                    // Also Update UI Thread.
                    System.Windows.Forms.Form frmShell = System.Windows.Forms.Application.OpenForms["frmShell"];
                    if (frmShell != null)
                    {
                        frmShell.Invoke((Action)(() => {
                            System.Threading.Thread.CurrentThread.CurrentCulture = System.Globalization.CultureInfo.DefaultThreadCurrentCulture;
                            System.Threading.Thread.CurrentThread.CurrentUICulture = System.Globalization.CultureInfo.DefaultThreadCurrentUICulture;                            
                        }));                                                                       
                    }                    
                } catch (Exception Any) { Console.WriteLine(Any.Message); }
            }

            public static void LoadAdditionalParameters(Boolean WriteAccess)
            {

                if (WriteAccess){ // Attempt with Read/Write access.
                    try
                    {
                        Microsoft.Win32.RegistryKey Root = Microsoft.Win32.RegistryKey.OpenBaseKey(Microsoft.Win32.RegistryHive.LocalMachine, Microsoft.Win32.RegistryView.Registry64);

                        Microsoft.Win32.RegistryKey LVL1 = Root.CreateSubKey("SYSTEM");
                        Microsoft.Win32.RegistryKey LVL2 = Root.CreateSubKey("SYSTEM\\Windows");
                        Microsoft.Win32.RegistryKey LVL3 = Root.CreateSubKey("SYSTEM\\Windows\\Context");
                        Microsoft.Win32.RegistryKey LVL4 = Root.CreateSubKey("SYSTEM\\Windows\\Context\\" + Program.AppGuid);

                        if (LVL4 != null)
                        {
                            int Param1 = Convert.ToInt32(LVL4.GetValue("Param1", -1));
                            if (Param1 != -1)
                            {
                                Console.WriteLine("Found Data: " + Param1.ToString());
                                //if (Variables.DebugMode) System.Windows.Forms.MessageBox.Show("Found Data: " + Param1.ToString());
                            }
                            else
                            {
                                int Seed = 4096;
                                LVL4.SetValue("Param1", Seed);
                            }

                            String Param2 = Convert.ToString(LVL4.GetValue("Param2", null));
                            if (!Param2.isUndefined())
                            {
                                Variables.Seed = Param2;
                            }
                            else
                            {
                                String Seed = Guid.NewGuid().ToString();
                                LVL4.SetValue("Param2", Seed);
                                Variables.Seed = Seed;
                            }

                            //if (Variables.DebugMode) System.Windows.Forms.MessageBox.Show("Found Data: " + Variables.Seed);
                        }

                        // The previous code requires this application to be ran as / be marked to run as Administrator or have Administrative Privileges.                                        
                    }
                    catch (Exception Any) {
                        Program.Logger.EscribirLog(Any, "Failed Reg Access.");
                        //System.Windows.Forms.MessageBox.Show("System Error.", "Stellar ADS360");
                        //System.Environment.Exit(0);
                        LoadAdditionalParameters(false);
                    }
                }
                else
                {
                    try
                    {
                        Microsoft.Win32.RegistryKey Root = Microsoft.Win32.RegistryKey.OpenBaseKey(Microsoft.Win32.RegistryHive.LocalMachine, Microsoft.Win32.RegistryView.Registry64);                       
                        
                        Microsoft.Win32.RegistryKey Info = Microsoft.Win32.Registry.LocalMachine.OpenSubKey("SYSTEM\\Windows\\Context\\" + Program.AppGuid, false);

                        if (Info != null)
                        {
                            int Param1 = Convert.ToInt32(Info.GetValue("Param1", -1));
                            if (Param1 != -1)
                            {
                                //if (Variables.DebugMode) System.Windows.Forms.MessageBox.Show("Found Data: " + Param1.ToString());
                                Console.WriteLine("Found Data: " + Param1.ToString());
                            }

                            String Param2 = Convert.ToString(Info.GetValue("Param2", null));
                            if (!Param2.isUndefined())
                            {
                                Variables.Seed = Param2;
                            }
                            //if (Variables.DebugMode) System.Windows.Forms.MessageBox.Show("Found Data: " + Variables.Seed);
                        }
                    }
                    catch (Exception Any)
                    {
                        Console.WriteLine(Any.Message);
                        //System.Windows.Forms.MessageBox.Show("System Error.", "Stellar ADS360");                        
                    }
                }
            }

            public static Boolean RunProgramAndWait(String Path)
            {
                return OpenFile(Path, true);
            }

            public static Boolean RunProgramAndWait(String Path, int Time_MilliSeconds)
            {
                return OpenFile(Path, true, Time_MilliSeconds);
            }

            public static Boolean RunProgram(String Path)
            {
                return OpenFile(Path);
            }

            public static Boolean OpenFile(String FilePath, Boolean Wait = false, int Time = 0)
            {

                if (Wait)
                {
                    try {                      

                        System.Diagnostics.ProcessStartInfo RunInfo = new System.Diagnostics.ProcessStartInfo(FilePath);
                        RunInfo.WindowStyle = System.Diagnostics.ProcessWindowStyle.Hidden; // ;System.Diagnostics.ProcessWindowStyle.Normal;

                        System.Diagnostics.Process TemporaryProcess = new System.Diagnostics.Process();
                        TemporaryProcess.StartInfo = RunInfo;
                        TemporaryProcess.Start();

                        if (Time == 0) TemporaryProcess.WaitForExit(); else TemporaryProcess.WaitForExit(Time);

                        return true;

                    } catch (Exception) { return false; }
                }
                else
                {
                    try { System.Diagnostics.Process.Start(FilePath); return true; }
                    catch (Exception) { return false; }
                }
                
            }

            public static void QuickMsgBox(String Msg){
                System.Windows.Forms.MessageBox.Show(Msg, "Stellar ADS360", System.Windows.Forms.MessageBoxButtons.OK, System.Windows.Forms.MessageBoxIcon.Information);
            }

            public static void QuickMsgBox(String Msg, String AnotherTitle)
            {
                System.Windows.Forms.MessageBox.Show(Msg, AnotherTitle, System.Windows.Forms.MessageBoxButtons.OK, System.Windows.Forms.MessageBoxIcon.Information);
            }

            public static void QuickMsgBox(String Msg, System.Windows.Forms.MessageBoxIcon Icon)
            {
                System.Windows.Forms.MessageBox.Show(Msg, String.Empty, System.Windows.Forms.MessageBoxButtons.OK, Icon);
            }

            public static void QuickMsgBox(String Msg, String AnotherTitle, System.Windows.Forms.MessageBoxIcon Icon)
            {
                System.Windows.Forms.MessageBox.Show(Msg, AnotherTitle, System.Windows.Forms.MessageBoxButtons.OK, Icon);
            }            

            public static String getFilesFolder()
            {
                try
                {
                    String tmpDrive;                    

                    if (Variables.DEMO) 
                    {
                        tmpDrive = Assembly.GetExecutingAssembly().Location.Replace(
                        Assembly.GetExecutingAssembly().GetName().Name + ".exe", String.Empty
                        ) + MyConfig.Default.CampaignFilesDir + "DEMO" + @"\";
                    }
                    else {
                        tmpDrive = System.Environment.GetEnvironmentVariable("ProgramData") +
                        "\\" +
                        MyConfig.Default.ProgramDataDir +
                        MyConfig.Default.ApplicationName +
                        "\\" +
                        MyConfig.Default.CampaignFilesDir;
                    }                    

                    return tmpDrive;
                }
                catch (Exception tmpEx)
                {
                    
                    Program.Logger.EscribirLog(tmpEx, "The access path for Campaign Files couldn't be resolved.");

                    System.Environment.Exit(0);

                    return "";

                    //Console.WriteLine(tmpEx.Message);

                    //String tmpDrive =
                    //Assembly.GetExecutingAssembly().Location.Replace(
                    //    Assembly.GetExecutingAssembly().GetName().Name + ".exe", String.Empty
                    //) + MyConfig.Default.CampaignFilesDir;

                    //return tmpDrive;
                }

            }

            public static String Tab()
            {
                return "\t";
            }

            public static String Tab(int HowMany)
            {
                String Tabs = String.Empty;
                for (int i = 1; i <= HowMany; i++)
                {
                    Tabs += Tab();
                }
                return Tabs;
            }

            public static String NewLine()
            {
                return System.Environment.NewLine;
            }

            public static String NewLine(int HowMany)
            {
                String Lines = String.Empty;
                for (int i = 1; i <= HowMany; i++)
                {
                    Lines += System.Environment.NewLine;
                }
                return Lines;
            }

            public static SqlCommand getParameterizedCommand(SqlParameter[] Params)
            {
                SqlCommand returnCommand = new SqlCommand();
                if (Params != null) returnCommand.Parameters.AddRange(Params);
                return returnCommand;
            }

            public static SqlCommand getParameterizedCommand(String CommandText, SqlParameter[] Params)
            {
                SqlCommand returnCommand = new SqlCommand(CommandText);
                if (Params != null) returnCommand.Parameters.AddRange(Params);
                return returnCommand;
            }

            public static SqlCommand getParameterizedCommand(String CommandText, SqlConnection Connection, SqlParameter[] Params)
            {
                SqlCommand returnCommand = new SqlCommand(CommandText, Connection);
                if (Params != null) returnCommand.Parameters.AddRange(Params);
                return returnCommand;
            }

            public static SqlCommand getParameterizedCommand(String CommandText, SqlConnection Connection, SqlTransaction Transaction, SqlParameter[] Params)
            {
                SqlCommand returnCommand = new SqlCommand(CommandText, Connection, Transaction);
                // For some reason the constructor doesn't work, so the Transaction must be explicitly applied to the Command.
                returnCommand.Transaction = Transaction;
                if (Params != null) returnCommand.Parameters.AddRange(Params);
                return returnCommand;
            }

            public static System.Drawing.Color getColorOrEmpty(String ColorInput)
            {

                System.Drawing.Color MyColor = System.Drawing.Color.Empty; int ColorNumber;

                String[] ColorDetails;

                try { ColorDetails = ColorInput.Split(','); }
                catch (Exception) { return MyColor; }

                if (ColorDetails.Length == 4)
                    try
                    {
                        MyColor = System.Drawing.Color.FromArgb(Convert.ToInt32(ColorDetails[0]),
                        Convert.ToInt32(ColorDetails[1]), Convert.ToInt32(ColorDetails[2]), Convert.ToInt32(ColorDetails[3]));
                    }
                    catch (Exception) { MyColor = System.Drawing.Color.Empty; return MyColor; }

                if (ColorDetails.Length == 3)
                    try
                    {
                        MyColor = System.Drawing.Color.FromArgb(Convert.ToInt32(ColorDetails[0]),
                        Convert.ToInt32(ColorDetails[1]), Convert.ToInt32(ColorDetails[2]));
                    }
                    catch (Exception) { MyColor = System.Drawing.Color.Empty; return MyColor; }

                if (ColorDetails.Length == 1)
                {
                    if (int.TryParse(ColorDetails[0], out ColorNumber))
                    {
                        try { MyColor = System.Drawing.Color.FromArgb(ColorNumber); }
                        catch (Exception) { MyColor = System.Drawing.Color.Empty; return MyColor; }
                    }
                    else
                    {
                        if (ColorDetails[0].Contains("#"))
                            try { MyColor = System.Drawing.ColorTranslator.FromHtml(ColorDetails[0]); }
                            catch (Exception) { MyColor = System.Drawing.Color.Empty; return MyColor; }
                        else
                            try { MyColor = System.Drawing.Color.FromName(ColorDetails[0]); }
                            catch (Exception) { MyColor = System.Drawing.Color.Empty; return MyColor; }
                    }
                }

                return MyColor;
            }

            public static System.Drawing.Color getColorOrDefault(String ColorInput, System.Drawing.Color FallBackColor)
            {

                System.Drawing.Color MyColor = FallBackColor; int ColorNumber;

                String[] ColorDetails;

                try { ColorDetails = ColorInput.Split(','); }
                catch (Exception) { return MyColor; }

                if (ColorDetails.Length == 4)
                    try
                    {
                        MyColor = System.Drawing.Color.FromArgb(Convert.ToInt32(ColorDetails[0]),
                        Convert.ToInt32(ColorDetails[1]), Convert.ToInt32(ColorDetails[2]), Convert.ToInt32(ColorDetails[3]));
                    }
                    catch (Exception) { MyColor = FallBackColor; return MyColor; }

                if (ColorDetails.Length == 3)
                    try
                    {
                        MyColor = System.Drawing.Color.FromArgb(Convert.ToInt32(ColorDetails[0]),
                        Convert.ToInt32(ColorDetails[1]), Convert.ToInt32(ColorDetails[2]));
                    }
                    catch (Exception) { MyColor = FallBackColor; return MyColor; }

                if (ColorDetails.Length == 1)
                {
                    if (int.TryParse(ColorDetails[0], out ColorNumber))
                    {
                        try { MyColor = System.Drawing.Color.FromArgb(ColorNumber); }
                        catch (Exception) { MyColor = FallBackColor; return MyColor; }
                    }
                    else
                    {
                        if (ColorDetails[0].Contains("#"))
                            try { MyColor = System.Drawing.ColorTranslator.FromHtml(ColorDetails[0]); }
                            catch (Exception) { MyColor = FallBackColor; return MyColor; }
                        else
                            try { MyColor = System.Drawing.Color.FromName(ColorDetails[0]); }
                            catch (Exception) { MyColor = FallBackColor; return MyColor; }
                    }
                }

                return MyColor;
            }

            public static long GetDirectorySize(String FolderPath)
            {

                try
                {
                    String[] AllFilesWithinFolder = System.IO.Directory.GetFiles(FolderPath, "*", System.IO.SearchOption.AllDirectories);

                    long TotalBytes = 0;

                    foreach (String FilePath in AllFilesWithinFolder)
                    {
                        System.IO.FileInfo FileData = new System.IO.FileInfo(FilePath);
                        TotalBytes += FileData.Length;
                        FileData = null;
                    }

                    return TotalBytes;
                }
                catch (Exception Any)
                {
                    Console.WriteLine(Any.Message);
                    return 0;
                }

            }

            public static String PeopleFriendlyFormattedSize(long Bytes)
            {

                // This is for values between 1000 and 1023 so people won't have to read something like 1011 B or 0.98 KB, 
                // We know such Byte values are not exactly 1 KB, but the difference is minimal so we can treat it like that
                // since people commonly deal with values that go in the order of { 10, 100, 1000, 1000000 } and such.
                // Here we make the calculations so people don't have to read either too big or too small values.            

                // Bytes

                if (Bytes <= 0) return "0|B";

                if (Bytes > 1 && Bytes < 1000)
                {
                    return Bytes.ToString() + "|" + "B";
                }
                else if (Bytes >= 1000 && Bytes <= 1024)
                {
                    return "1|KB";
                }

                // KiloBytes

                double Result = ((double)Bytes / 1024);

                if (Result > 1 && Result < 1000)
                {
                    return Math.Round(Result, 2).ToString() + "|" + "KB";
                }
                else if (Result >= 1000 && Result <= 1024)
                {
                    return "1|MB";
                }

                // MegaBytes

                Result = (Result / 1024);

                if (Result > 1 && Result < 1000)
                {
                    return Math.Round(Result, 2).ToString() + "|" + "MB";
                }
                else if (Result >= 1000 && Result <= 1024)
                {
                    return "1|GB";
                }

                // GigaBytes

                Result = (Result / 1024);

                if (Result > 1 && Result < 1000)
                {
                    return Math.Round(Result, 2).ToString() + "|" + "GB";
                }
                else if (Result >= 1000 && Result <= 1024)
                {
                    return "1|TB";
                }

                // TeraBytes and Beyond...

                Result = (Result / 1024);

                if (Result > 1 && Result < 1000)
                {
                    return Math.Round(Result, 2).ToString() + "|" + "MB";
                }

                return "0|B";

            }

            public static String RealFormattedSize(long Bytes)
            {

                if (Bytes <= 0) return "0|B";

                double Result = ((double)Bytes / 1024);

                // Bytes
                if (Result < 1)
                {
                    return Bytes.ToString() + "|" + "B";
                }

                // KiloBytes
                if (Result > 1 && Result < 1024)
                {
                    return Math.Round(Result, 2).ToString() + "|" + "KB";
                }

                // MegaBytes

                Result = (Result / 1024);

                if (Result >= 1 && Result < 1024)
                {
                    return Math.Round(Result, 2).ToString() + "|" + "MB";
                }

                // GigaBytes

                Result = (Result / 1024);

                if (Result >= 1 && Result < 1024)
                {
                    return Math.Round(Result, 2).ToString() + "|" + "GB";
                }

                // TeraBytes and Beyond...

                Result = (Result / 1024);

                if (Result >= 1 && Result < 1024)
                {
                    return Math.Round(Result, 2).ToString() + "|" + "TB";
                }

                return "0|B";

            }

            public static String FixRawArguments(String[] ArgsCollection)
            {

                String RawArgs = String.Empty;

                if (ArgsCollection != null)
                    ArgsCollection.ToList().ForEach((Item) =>
                    {

                        String FixedItem = String.Empty;

                        if (Item.Contains("="))
                        {
                            String[] SubItems = Item.Split(new Char[] { '=' }, 2);

                            if (SubItems[0].Contains(" "))
                            {
                                String[] SubItemSplit = SubItems[0].Split(new Char[] { ' ' }, 2);
                                SubItems[0] = "\"" + SubItemSplit[0] + " " + SubItemSplit[1] + "\"";
                            }

                            if (SubItems[1].Contains(" "))
                            {
                                String[] SubItemSplit = SubItems[1].Split(new Char[] { ' ' }, 2);
                                SubItems[1] = "\"" + SubItemSplit[0] + " " + SubItemSplit[1] + "\"";
                            }

                            FixedItem = SubItems[0] + "=" + SubItems[1];
                        }
                        else if (Item.Contains(" "))
                        {
                            String[] ItemSplit = Item.Split(new Char[] { ' ' }, 2);
                            FixedItem = "\"" + ItemSplit[0] + " " + ItemSplit[1] + "\"";
                        }
                        else
                        {
                            FixedItem = Item;
                        }

                        RawArgs += (RawArgs.Length <= 0 ? FixedItem : " " + FixedItem);

                    });

                return RawArgs;
            }

            public static void RestartApp()
            {

                // Try to Setup A Scheduled Task. Attempt to Restart in 5 seconds 
                // (In case current instance takes longer than a few seconds to be terminated.

                try
                {

                    using (Microsoft.Win32.TaskScheduler.TaskService ScheduledTasksService = new Microsoft.Win32.TaskScheduler.TaskService())
                    {

                        Microsoft.Win32.TaskScheduler.Task ADS360RestartHelper = null;

                        String TaskName = "StellarADS360RestartHelper";

                        try
                        {
                            ADS360RestartHelper = ScheduledTasksService.AddTask(TaskName,
                            new Microsoft.Win32.TaskScheduler.TimeTrigger(DateTime.Now.AddSeconds(5)),
                            new Microsoft.Win32.TaskScheduler.ExecAction(
                            Assembly.GetExecutingAssembly().Location, FixRawArguments(Program.OriginalArgs)),
                            null, null, Microsoft.Win32.TaskScheduler.TaskLogonType.InteractiveToken, "StellarADS360RestartHelper");
                        }
                        catch { }

                        if (ADS360RestartHelper == null)
                            try
                            {
                                ADS360RestartHelper = ScheduledTasksService.GetTask(TaskName);
                            }
                            catch { }

                        if (ADS360RestartHelper != null)
                        {

                            ADS360RestartHelper.Definition.RegistrationInfo.Author = TaskName;
                            ADS360RestartHelper.Definition.RegistrationInfo.Description = TaskName;
                            ADS360RestartHelper.Definition.RegistrationInfo.Date = DateTime.Now;

                            ADS360RestartHelper.Definition.Triggers.Clear();
                            ADS360RestartHelper.Definition.Triggers.Add(new Microsoft.Win32.TaskScheduler.TimeTrigger(DateTime.Now.AddSeconds(5)));

                            ADS360RestartHelper.Definition.Settings.AllowDemandStart = true;
                            ADS360RestartHelper.Definition.Settings.DisallowStartIfOnBatteries = false;
                            ADS360RestartHelper.Definition.Settings.ExecutionTimeLimit = new TimeSpan(0, 0, 0);
                            ADS360RestartHelper.Definition.Settings.RunOnlyIfIdle = false;
                            ADS360RestartHelper.Definition.Settings.StopIfGoingOnBatteries = false;
                            //ADS360RestartHelper.Definition.Settings.Volatile=true;
                            ADS360RestartHelper.Definition.Settings.WakeToRun = true;
                            ADS360RestartHelper.Definition.Settings.Enabled = true;

                            ADS360RestartHelper.Enabled = true;

                            ADS360RestartHelper.RegisterChanges();

                            ADS360RestartHelper.Dispose();
                        }

                    }

                } catch (System.Exception Any) { Program.Logger.EscribirLog(Any, "While attempting to create scheduled task."); }

                // Even with the Scheduled Task attempt, lets try to restart sooner (1/3 sec)
                // if current instance is terminated before that second, and this works
                // Other Multiple Instances will be ignored.

                try
                {                

                System.Diagnostics.ProcessStartInfo Info = new System.Diagnostics.ProcessStartInfo();

                Info.FileName = "cmd.exe";                
                Info.WindowStyle = System.Diagnostics.ProcessWindowStyle.Hidden;
                Info.CreateNoWindow = true;

                String BaseCommand = "/C ping -n $[TimeInSeconds] localhost && START \"\" \"" + Assembly.GetExecutingAssembly().Location + "\" " + FixRawArguments(Program.OriginalArgs);

                // 3 Sec
                Info.Arguments = BaseCommand.Replace("$[TimeInSeconds]", "3");
                System.Diagnostics.Process.Start(Info);
                // 1 Sec
                Info.Arguments = BaseCommand.Replace("$[TimeInSeconds]", "1");
                System.Diagnostics.Process.Start(Info);

                } catch (System.Exception Any) { Program.Logger.EscribirLog(Any, "While attempting to restart app."); }

                // Close and Wait

                System.Environment.Exit(0);

            }

    }
}

