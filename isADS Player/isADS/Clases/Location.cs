﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace isADS.Clases
{
    public class Location
    {
        public String ID { get; set; }
        public String Name { get; set; }
        public String Address { get; set; }
        public String City { get; set; }
        public String State { get; set; }
        public String Country { get; set; }        

        public Location(String ID, String Name, String Address, String City, String State, String Country)
        {
            this.ID = ID;
            this.Name = Name;
            this.Address = Address;
            this.City = City;
            this.State = State;
            this.Country = Country;
        }

        public Location()
        {
        }

        public Location(String ID)
        {
            this.ID = ID;
            // Search for other Fields.
        }

        public static Dictionary<String, Location> getAvailableLocations(String OrganizationID, SqlConnection pDBConnection, Boolean CloseConnection = true)
        {
            Dictionary<String, Location> Locations = null;

            try
            {

                SqlCommand command = Functions.getParameterizedCommand("SELECT * FROM LOCATION " +
                "\nWHERE Status = 'Active'" +
                "\nAND ORGANIZATION_ID = @OrganizationID" +
                "\nORDER BY ID ASC", pDBConnection, new SqlParameter[]{
                new SqlParameter(){ ParameterName = "@OrganizationID", Value = OrganizationID}
                });

                if (pDBConnection.State == System.Data.ConnectionState.Closed) pDBConnection.Open();

                SqlDataReader reader = command.ExecuteReader();

                if (reader.HasRows)
                {
                    Locations = new Dictionary<String, Location>();

                    while (reader.Read())
                    {
                        Locations.Add(reader["ID"].ToString(), new Location(
                            reader["ID"].ToString(),
                            reader["Name"].ToString(),
                            reader["Address"].ToString(),
                            reader["City"].ToString(),
                            reader["State"].ToString(),
                            reader["Country"].ToString()

                            ));
                    }
                }

                reader.Close();                

                return Locations;

            }
            catch (Exception ex)
            {
                if (CloseConnection) pDBConnection.Close();
                Console.WriteLine(ex.Message);
                return Locations;
            }
        }    
    
    }
        
}
