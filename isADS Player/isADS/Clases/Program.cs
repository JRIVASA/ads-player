﻿using isADS.Clases;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Threading;
using System.Windows.Forms;

namespace isADS
{
    static class Program
    {
        public static System.Collections.ArrayList Args;
        public static String[] OriginalArgs = null;
        public static String AppGuid = "409cb2ac-6f64-4137-885c-e054a3ab5fbe";
        public static FSLogger Logger = new FSLogger(System.Environment.CurrentDirectory + "\\Log.txt");        
        public static DateTime StartupTime = DateTime.Now;
        public static Boolean RestartRequired = false;
        private static Mutex MultiInstanceCheck = null;

        //private void VerificarRegistro()
        //{
            //Functions Func = new Functions();
        
            //DateTime returns = DateTime.Now;

            //try
            //{
            //    SqlCommand command = new SqlCommand("SELECT Description as DeviceName() AS ServerTime", DBConnection);
            //    DBConnection.Open();

            //    SqlDataReader reader = command.ExecuteReader();

            //    if (reader.HasRows)
            //    {
            //        reader.Read();

            //        returns = Convert.ToDateTime(reader["ServerTime"]);
            //    }

            //    reader.Close();
            //    return returns;
            //}
            //catch (Exception ex)
            //{
            //    Console.WriteLine(ex.Message);
            //    return returns;
            //}
            //finally
            //{
            //    DBConnection.Close();
            //}

        //}
        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        [STAThread]
        static void Main(String[] Args)
        {

            using (System.Diagnostics.Process MyProc = System.Diagnostics.Process.GetCurrentProcess())
            {
                System.IO.FileInfo ProgramFileInfo = new System.IO.FileInfo(MyProc.MainModule.FileName);
                Variables.RunningProcessBaseDir = ProgramFileInfo.Directory.FullName;
                Variables.RunningProcessFileName = ProgramFileInfo.Name;
                Variables.RunningProcessFilePath = ProgramFileInfo.FullName;
                Variables.RunningProcessFileExtension = ProgramFileInfo.Extension;                
            }

            Variables.RunningProcessDrive = System.IO.Path.GetPathRoot(Variables.RunningProcessBaseDir);

            System.IO.FileInfo DebugFile = new System.IO.FileInfo(Variables.RunningProcessBaseDir + "\\DebugMode.Active");
            Variables.DebugMode = DebugFile.Exists;

            System.IO.FileInfo IniFile = new System.IO.FileInfo(Variables.RunningProcessBaseDir + "\\UseIniSettings.Config");
            QuickSettings.DocType = (IniFile.Exists ? QuickSettings.Mode.Ini : QuickSettings.Mode.Xml);
            
            QuickSettings.Configure();

            if (QuickSettings.DocType == QuickSettings.Mode.Xml)
            {
                if (System.IO.File.Exists(QuickSettings.FilePath) )
                {
                    try
                    {
                        if (!System.IO.File.Exists(QuickSettings.FolderPath + "\\" + "Stellar.Config.XML_BAK"))
                        {
                            System.Xml.XmlDocument TmpDoc = new System.Xml.XmlDocument();
                            TmpDoc.Load(QuickSettings.FilePath);

                            if (TmpDoc.HasChildNodes)
                            {

                                String OldFilePath = QuickSettings.FilePath;

                                // Convert to Ini (Safe Option)
                                System.IO.File.Copy(OldFilePath, QuickSettings.FolderPath + "\\" + "Stellar.Config.XML_BAK");
                                QuickSettings.DocType = QuickSettings.Mode.Ini;
                                QuickSettings.Configure();

                                // Write Variables

                                String IniFileContent = String.Empty;

                                foreach (System.Xml.XmlNode TmpSection in TmpDoc.SelectNodes("/Stellar/" + QuickSettings.ApplicationName + "/*[@Type='Section']"))
                                {
                                    IniFileContent += (!IniFileContent.Equals(String.Empty) ? System.Environment.NewLine : String.Empty) +
                                    "[" + TmpSection.Name + "]";
                                    foreach (System.Xml.XmlNode TmpValue in TmpSection)
                                    {
                                        IniFileContent += System.Environment.NewLine + TmpValue.Name + "=" + TmpValue.InnerText;
                                    }
                                }

                                using (System.IO.StreamWriter TmpW = System.IO.File.CreateText(QuickSettings.FilePath))
                                {
                                    TmpW.Write(IniFileContent);
                                }

                                // Change Config Source

                                using (System.IO.StreamWriter TmpW = System.IO.File.CreateText(Variables.RunningProcessBaseDir + "\\UseIniSettings.Config"))
                                {
                                    TmpW.Write(String.Empty);
                                }

                                IniFile = new System.IO.FileInfo(Variables.RunningProcessBaseDir + "\\UseIniSettings.Config");
                                QuickSettings.DocType = (IniFile.Exists ? QuickSettings.Mode.Ini : QuickSettings.Mode.Xml);
                                QuickSettings.Configure();

                                if (IniFile.Exists && System.IO.File.Exists(OldFilePath))
                                {
                                    //String CurrentAppPath = System.Reflection.Assembly.GetExecutingAssembly().Location;
                                    //String CurrentAppDir = System.IO.Path.GetDirectoryName(CurrentAppPath);
                                    String CurrentAppDir = Variables.RunningProcessBaseDir;

                                    if (System.IO.Directory.Exists(CurrentAppDir + "\\Sync"))
                                    {
                                        if (!System.IO.File.Exists(CurrentAppDir + "\\Sync\\UseIniSettings.Config"))
                                        {
                                            using (System.IO.StreamWriter TmpW = System.IO.File.CreateText(CurrentAppDir + "\\Sync\\UseIniSettings.Config"))
                                            {
                                                TmpW.Write(String.Empty);
                                            }
                                        }
                                    }

                                    System.IO.File.Delete(OldFilePath);

                                    System.Diagnostics.Process.Start("CMD.exe", "/C TASKKILL /F /IM ADS360_SYNC.exe");
                                    // Y Por si acaso.
                                    System.Diagnostics.Process.Start(Variables.RunningProcessDrive + "Windows\\System32\\cmd.exe", 
                                    "/C TASKKILL /F /IM ADS360_SYNC.exe");

                                    //MessageBox.Show("CHECK");

                                }

                            }

                        }

                    }
                    catch (Exception ConvertEx)
                    {
                        Console.WriteLine(ConvertEx);
                    }

                }

            }

            //String ContentSerial = new BIGWISE.ProductStatus.Register().GetActivationKey("Ardilla.mp4", "1d1a0U0x1a180x15E", Variables.ProductCode);
            //Boolean Active = new BIGWISE.ProductStatus.Register().isActivated("Ardilla.mp4", "1d1a0U0x1a180x15E", ContentSerial, Variables.ProductCode);

            Program.OriginalArgs = Args;
            Program.Args = new System.Collections.ArrayList();            

            // Load Args

            if ((Args == null ? false : (Args.Length > 0))) Array.ForEach<String>(Args, (Item) => { Program.Args.Add(Item); });            
            
            // Find Args:
            // Console.WriteLine(Program.Args.Contains("/ArgItem1")); // Call From CMD: Start "" ...Program.exe /ArgItem1 /ArgItem2.
            // The "/" before the argument is not needed but it helps to separate parameters in a clean, fast way. 
            // It can be replaced by any other sign or none, but the Contains Expression must exactly equal what's been submitted in the CMD.    

            try
            {

                Boolean SingleInstance;
                MultiInstanceCheck = new Mutex(true, "Global\\" + AppGuid, out SingleInstance);
                {
                    if (!SingleInstance)
                    {
                        Console.WriteLine("Instance already running.");
                        Thread.Sleep(200);
                        return;
                    }
                }

                GC.KeepAlive(MultiInstanceCheck);

            }

            catch (Exception Any) { Console.WriteLine(Any.Message); }

            //if (Variables.DebugMode) Array.ForEach<String>(Args, (Item) => { MessageBox.Show("Argumento: " + Item); });

            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);

            Functions.LoadEffects();
            Functions.LoadSettings();
            Functions.LocalizeApplication();
            Functions.LoadAdditionalParameters(true);

            Application.Run(new frm_Main());

            //Application.Run(new frm_test());
            //Application.Run(new frm_Config());
        }
    }
}
