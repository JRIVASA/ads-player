﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace isADS.Clases
{
    public class NavigationParams
    {
        public String Link { get; set; }
        public int DisplayTime { get; set; }

        public int InteractionTime { get; set; }

        public NextCampaignToShow NextCampaign { get; set; }

        public String CurrentCampaignID { get; set; }

        public String CallerAdID { get; set; }

        public enum NextCampaignToShow
        {           
            // Back,
            Current,
            Next
        }

        public Boolean EnableContextMenu { get; set; }

        public static NavigationParams Copy(NavigationParams AnotherObject)
        {
            NavigationParams CopyParams = new NavigationParams()
            {
                Link = AnotherObject.Link,
                DisplayTime = AnotherObject.DisplayTime,
                InteractionTime = AnotherObject.InteractionTime,
                NextCampaign = AnotherObject.NextCampaign,
                CurrentCampaignID = AnotherObject.CurrentCampaignID,
                CallerAdID = AnotherObject.CallerAdID,
                EnableContextMenu = AnotherObject.EnableContextMenu
            };
            return CopyParams;
        }

        public NavigationParams()
        {

        }

    }
}
