﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Reflection;
using System.Globalization;
using System.Windows.Forms;

namespace isADS
{
    public static class LanguageResources
    {
        public static String GetText(String Key)
        {
            TextResourceHandler Handler = new TextResourceHandler
                                            ("isADS.Language.Language",
                                            Assembly.GetExecutingAssembly());

            String Data = Handler.GetString(Key);
            return Data;
        }

        public static Object GetImage(String Key)
        {
            TextResourceHandler Handler;

            Handler = new TextResourceHandler("isADS.Language.Language",
                                            Assembly.GetExecutingAssembly());

            Object ImageStream = Handler.GetImage(Key);
            return ImageStream;
        }

        public static void ApplyLanguage(this Form Target)
        {
            TextResourceHandler Handler;

            Handler = new TextResourceHandler("isADS.Language.Language",
                                                        Assembly.GetExecutingAssembly());

            Handler.PopulateFormControls(Target);
        }

    }
}