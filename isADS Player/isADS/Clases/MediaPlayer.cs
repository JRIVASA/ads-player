﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace isADS.Clases
{
    public class MediaPlayer
    {
        public String ID { get; set; }
        public String Organization_ID { get; set; }
        public String Location_ID { get; set; }
        public String Description { get; set; }
        public String Serial { get; set; }
        public String MACAddress { get; set; }
        public String Brand { get; set; }
        public String Model { get; set; }
        public String Identifier { get; set; }
        public String License { get; set; }
        public String ActivationKey { get; set; }
        public String Status { get; set; }
        public String Ubication { get; set; }

        public MediaPlayer(String ID, String Organization_ID, String Location_ID, String Description, String Serial, String MACAddress,
        String Brand, String Model, String Identifier, String License, String ActivationKey, String Status, String Ubication)
        {
            this.ID = ID;
            this.Organization_ID = Organization_ID;
            this.Location_ID = Location_ID;
            this.Description = Description;
            this.Serial = Serial;
            this.MACAddress = MACAddress;
            this.Brand = Brand;
            this.Model = Model;
            this.Identifier = Identifier;
            this.License = License;
            this.ActivationKey = ActivationKey;
            this.Status = Status;
            this.Ubication = Ubication;
        }

        public MediaPlayer()
        {
        }

        public MediaPlayer(String ID)
        {
            this.ID = ID;
            // Search for other Fields.
        }

        public static Dictionary <String, MediaPlayer> getAvailableMediaPlayers(String OrganizationID, String LocationID,
        SqlConnection pDBConnection, Boolean CloseConnection = false)
        {
            Dictionary <String, MediaPlayer> MediaPlayers = null;

            try
            {
                SqlCommand command = Functions.getParameterizedCommand("SELECT * FROM MEDIAPLAYER" +
                "\nWHERE Status = 'Inactive'" +
                "\nAND ORGANIZATION_ID = @OrgID" + 
                "\nAND LOCATION_ID = @LocationID" +
                "\nORDER BY ID ASC", pDBConnection, new SqlParameter[]{
                new SqlParameter(){ ParameterName = "OrgID", Value = OrganizationID },
                new SqlParameter(){ ParameterName = "LocationID", Value = LocationID }
                });
                
                if (pDBConnection.State == System.Data.ConnectionState.Closed) pDBConnection.Open();

                SqlDataReader reader = command.ExecuteReader();

                if (reader.HasRows)
                {
                    MediaPlayers = new Dictionary <String, MediaPlayer> ();

                    while (reader.Read())
                    {

                        MediaPlayers.Add(reader["ID"].ToString(), new MediaPlayer(
                            reader["ID"].ToString(),
                            reader["Organization_ID"].ToString(),
                            reader["Location_ID"].ToString(),
                            reader["Description"].ToString(),
                            reader["Serial"].ToString(),
                            reader["MACAddress"].ToString(),
                            reader["Brand"].ToString(),
                            reader["Model"].ToString(),
                            reader["Identifier"].ToString(),
                            reader["License"].ToString(),
                            reader["ActivationKey"].ToString(),
                            reader["Status"].ToString(),
                            reader["Ubication"].ToString()
                            ));
                                                
                    }
                }

                reader.Close();                

                return MediaPlayers;

            }
            catch (Exception ex)
            {
                if (CloseConnection) pDBConnection.Close();
                Console.WriteLine(ex.Message);
                return MediaPlayers;
            }
        }    
    
    }
        
}
