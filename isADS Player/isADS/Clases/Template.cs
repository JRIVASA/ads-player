﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace isADS
{
    class Template
    {
        public TableLayoutPanel layout { get; set; }
        public int rows { get; set; }
        public int cols { get; set; }
        public int style { get; set; }

        public Template(TableLayoutPanel Layout, int Rows, int Cols, int Style)
        {
            this.layout = Layout;
            this.rows = Rows;
            this.cols = Cols;
            this.style = Style;
        }

        public void setLayout(TableLayoutPanel layout)
        {
            layout.ColumnCount = cols;
            layout.RowCount = rows;
        }
    }
}
