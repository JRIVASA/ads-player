﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.SqlClient;

namespace isADS.Clases
{
    public class QuickDatabaseAccess
    {

        public String ConnectionString = "Server=" + Properties.Settings.Default.LocalDB_SQLServerName +
        ";Database=" + Properties.Settings.Default.LocalDB_SQLDBName +
        ";User Id=" + Properties.Settings.Default.LocalDB_SQLUser +
        ";Password=" + Properties.Settings.Default.LocalDB_SQLPass + ";";

        public SqlConnection Connection = new SqlConnection();
        public SqlCommand Command = new SqlCommand();
        public SqlDataReader Reader;
        public String thisQuery = "";

        public void Query(String QuerySQL)
        {
            thisQuery = QuerySQL;

            if (this.Connection.State == System.Data.ConnectionState.Closed)
            {
                this.Connection.ConnectionString = this.ConnectionString;
                this.Connection.Open();
            }

            Command.CommandText = thisQuery;

            Reader = Command.ExecuteReader();

            if (QueryData()) { QueryAdvance(); }
        }

        public void Query(String QuerySQL, Object Parameters) //, QueryParams)
        {
            thisQuery = QuerySQL;

            if (this.Connection.State == System.Data.ConnectionState.Closed)
            {
                this.Connection.ConnectionString = this.ConnectionString;
                this.Connection.Open();
            }

            Command.CommandText = thisQuery;

            Reader = Command.ExecuteReader();

            if (QueryData()) { QueryAdvance(); }
        }

        public Object QueryObject(String Param)
        {
            return Reader[Param];
        }

        public Boolean QueryAdvance()
        {
            return Reader.Read();
        }

        public Boolean QueryData(){
            return Reader.HasRows;
        }

        public void QueryClose()
        {
            Reader.Close();
            Connection.Close();
        }

    }
}
