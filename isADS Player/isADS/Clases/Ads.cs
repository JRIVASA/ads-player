﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace isADS.Clases
{
    class Ads
    {
        public String ID { get; set; }   
        public String AdvertiserID { get; set; }
        public String Description { get; set; }
        public int Type { get; set; }
        public String FileName { get; set; }
        public String Extension { get; set; }
        public String MIMEType { get; set; }
        public String ContentSerial { get; set; }
        public String FileSerial { get; set; }

        public enum ContentType : int
        {
            Image = 0,
            Video = 1,
            WebApp = 2,
            WebLink = 3
        }

        public Ads(String Description, int Type, String FileName, String Extension, String MIMEType, String AdvertiserID, String Link, String ContentSerial, String FileSerial)
        {
            this.Description = Description;
            this.Type = Type;
            this.FileName = FileName;
            this.Extension = Extension;
            this.MIMEType = MIMEType;
            this.AdvertiserID = AdvertiserID;
            this.ContentSerial = ContentSerial;
            this.FileSerial = FileSerial;
        }

        public Ads()
        {
        }

        public Ads(String ID)
        {
            this.ID = ID;
            getAdById(ID);
        }

        public void getAdById(String ID)
        {
            Functions f = new Functions();

            SqlCommand Command = null;
            SqlDataReader Reader = null;

            try
            {

                Command = Functions.getParameterizedCommand("SELECT * FROM ADS " +
                "\nWHERE ID = @ID", f.DBConnection.OpenGet(), new SqlParameter[]{
                new SqlParameter(){ ParameterName = "@ID", Value = ID }
                });

                Reader = Command.ExecuteReader();

                if (Reader.HasRows)
                {
                    Reader.Read();

                    this.Description = Reader["Description"].ToString();
                    this.Type = Convert.ToInt32(Reader["Type"]);
                    this.FileName = Reader["FileName"].ToString();
                    this.Extension = Reader["Extension"].ToString();
                    this.MIMEType = Reader["MIMEType"].ToString();
                    this.AdvertiserID = Reader["Advertiser_ID"].ToString();
                    this.ContentSerial = Reader["ContentSerial"].ToString();
                    this.FileSerial = Reader["FileSerial"].ToString();
                }                                   
                else
                {
                    this.ID = "undefined";
                }
                
            }
            catch (Exception ex)
            {                
                this.ID = "undefined";

                Console.WriteLine(ex.Message);
            }
            finally
            {
                if ((Reader != null ? !Reader.IsClosed : false)) { Reader.Close(); Reader.Dispose(); }                               
                Command.Dispose();
                f.DBConnection.Dispose();
                f = null;
            }
        }

        public void saveAdClick(String caId, String adId, String adType, int click, int hit)
        {
            if (adId.ToUpper(System.Globalization.CultureInfo.InvariantCulture).Contains("DEMO"))
                return; 

            Functions f = new Functions();

            String Query = "INSERT INTO ADSxINTERACTIONS (CAMPAIGN_ID, ADS_ID, MEDIAPLAYER_ID, Type, Hit, Click) VALUES (@caId, @adId, @mpId, @type, @hit, @click)";
            SqlCommand Command = new SqlCommand(Query, f.DBConnection.OpenGet());

            try
            {                
                Command.Parameters.AddWithValue("@caId", caId);
                Command.Parameters.AddWithValue("@adId", adId);
                Command.Parameters.AddWithValue("@mpId", Properties.Settings.Default.MediaPlayerID);
                Command.Parameters.AddWithValue("@type", adType);
                Command.Parameters.AddWithValue("@hit", hit);
                Command.Parameters.AddWithValue("@click", click);

                Command.ExecuteNonQuery();                
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }
            finally
            {
                Command.Parameters.Clear();                
                Command.Dispose();
                f.DBConnection.Dispose();
                f = null;
            }
        }

        public void saveAdClick(String caId, String adId, String adType, int click, int hit, string comment)
        {
            if (adId.ToUpper(System.Globalization.CultureInfo.InvariantCulture).Contains("DEMO")) 
                return; 

            Functions f = new Functions();
            string query = "INSERT INTO ADSxINTERACTIONS (CAMPAIGN_ID, ADS_ID, MEDIAPLAYER_ID, Type, Hit, Click, Comment) VALUES (@caId, @adId, @mpId, @type, @hit, @click, @comment)";
            SqlCommand command = new SqlCommand(query, f.DBConnection);
            try
            {
                f.DBConnection.Open();
                command.Parameters.AddWithValue("@caId", caId);
                command.Parameters.AddWithValue("@adId", adId);
                command.Parameters.AddWithValue("@mpId", Properties.Settings.Default.MediaPlayerID);
                command.Parameters.AddWithValue("@type", adType);
                command.Parameters.AddWithValue("@hit", hit);
                command.Parameters.AddWithValue("@click", click);
                command.Parameters.AddWithValue("@comment", comment);
                command.ExecuteNonQuery();

                command.Parameters.Clear();
                command.Dispose();
                command = null;
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }
            finally
            {
                f.DBConnection.Close();
            }
        }
    }
}
