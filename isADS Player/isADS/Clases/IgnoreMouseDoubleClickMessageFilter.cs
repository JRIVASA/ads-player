﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace isADS.Clases
{
    public class IgnoreMouseDoubleClickMessageFilter : IMessageFilter
    {
        private Control _Parent { get; set; }
        private Control _Target { get; set; }

        public IgnoreMouseDoubleClickMessageFilter(Control parent, Control target)
        {
            _Parent = parent;
            _Target = target;
        }

        public bool PreFilterMessage(ref Message m)
        {
            if (!(m.Msg == 0x203)) { return false; }

            if (_Parent == null)
                return false;

            // Get the Control beneath the current X, Y coordinates
            // of the cursor
            Control child = null;
            try
            {
                child = _Parent.GetChildAtPoint(Cursor.Position);
            }
            catch (Exception) { }
            
            if (child == null)
                return false;

            // Determine if the Control over which the cursor is currently
            // hovering if the target control for which we want to ignore any
            // mouse click activity
            if (child != _Target) { return false; }

            ((frm_Main)_Parent.Parent).frm_Main_Click(null, null);

            return true;

        }
    }
}
