﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace isADS.Clases
{
    class CurrentAdSettings
    {

        public int DisplayTime { get; set; }
        public int InteractionTime { get; set; }
        public String ContentLink { get; set; }

        public CurrentAdSettings(int DisplayTime, int InteractionTime, String ContentLink)
        {
            this.DisplayTime = DisplayTime;
            this.InteractionTime = InteractionTime;
            this.ContentLink = ContentLink;
        }

        public CurrentAdSettings()
        {
        }

        public static CurrentAdSettings FromParams(String pId, int pSecuence, int pCurrentOrder, String pCampaignId)
        {
            Functions f = new Functions();

            CurrentAdSettings returns = new CurrentAdSettings(1, 0, String.Empty);
            
            SqlCommand Command = null;
            SqlDataReader Reader = null;

            try
            {
                Command = Functions.getParameterizedCommand(
                "SELECT TOP (1) DisplayTime, InteractionTime, ContentLink FROM CAMPAIGNxADS" +
                "\n" + "WHERE Location = @Secuence" +
                "\n" + "AND SecuenceOrder = @CurrentOrder" +
                "\n" + "AND CAMPAIGN_ID = @CampaignID" +
                "\n" + "AND ADS_ID = @ID", f.DBConnection.OpenGet(), new SqlParameter[] {
                    new SqlParameter () { ParameterName = "@ID", Value = pId },
                    new SqlParameter () { ParameterName = "@Secuence", Value = pSecuence },
                    new SqlParameter () { ParameterName = "@CurrentOrder", Value = pCurrentOrder },
                    new SqlParameter () { ParameterName = "@CampaignID", Value = pCampaignId }                    
                });

                Reader = Command.ExecuteReader();

                if (Reader.HasRows)
                {
                    Reader.Read();

                    returns = new CurrentAdSettings(Convert.ToInt32(Reader["DisplayTime"]),
                    Convert.ToInt32(Reader["InteractionTime"]), Reader["ContentLink"].ToString());
                }
                else
                {
                    returns = null;
                }
                
                return returns;
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
                return null;
            }
            finally
            {
                if ((Reader != null ? !Reader.IsClosed : false)) { Reader.Close(); Reader.Dispose(); }
                Command.Dispose();
                f.DBConnection.Dispose();
                f = null;
            }       
        }
        
    }
}
