﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Drawing;
using System.Runtime.InteropServices;

    public sealed class TaskBar
    {
        private const String ClassName = "Shell_TrayWnd";

        public Rectangle Bounds
        {
            get;
            private set;
        }

        public enum TaskBarPosition
        {
            Unknown = -1,
            Left,
            Top,
            Right,
            Bottom,
        }

        public TaskBarPosition Position
        {
            get;
            private set;
        }

        public Point Location
        {
            get
            {
                return this.Bounds.Location;
            }
        }

        public Size Size
        {
            get
            {
                return this.Bounds.Size;
            }
        }

        //Always returns false under Windows 7

        public bool AlwaysOnTop
        {
            get;
            private set;
        }

        public bool AutoHide
        {
            get;
            private set;
        }

        public TaskBar()
        {
            IntPtr TaskBarHandle = User32.FindWindow(TaskBar.ClassName, null);

            APPBARDATA Data = new APPBARDATA();
            Data.cbSize = (uint)Marshal.SizeOf(typeof(APPBARDATA));
            Data.hWnd = TaskBarHandle;
            IntPtr Result = Shell32.SHAppBarMessage(ABM.GetTaskbarPos, ref Data);
            if (Result == IntPtr.Zero)
                throw new InvalidOperationException();

            this.Position = (TaskBarPosition)Data.uEdge;
            this.Bounds = Rectangle.FromLTRB(Data.Rec.Left, Data.Rec.Top, Data.Rec.Right, Data.Rec.Bottom);

            Data.cbSize = (uint)Marshal.SizeOf(typeof(APPBARDATA));
            Result = Shell32.SHAppBarMessage(ABM.GetState, ref Data);
            int State = Result.ToInt32();
            this.AlwaysOnTop = (State & ABS.AlwaysOnTop) == ABS.AlwaysOnTop;
            this.AutoHide = (State & ABS.Autohide) == ABS.Autohide;
        }
    }

    public enum ABM : uint
    {
        New = 0x00000000,
        Remove = 0x00000001,
        QueryPos = 0x00000002,
        SetPos = 0x00000003,
        GetState = 0x00000004,
        GetTaskbarPos = 0x00000005,
        Activate = 0x00000006,
        GetAutoHideBar = 0x00000007,
        SetAutoHideBar = 0x00000008,
        WindowPosChanged = 0x00000009,
        SetState = 0x0000000A,
    }

    public enum ABE : uint
    {
        Left = 0,
        Top = 1,
        Right = 2,
        Bottom = 3
    }

    public static class ABS
    {
        public const int Autohide = 0x0000001;
        public const int AlwaysOnTop = 0x0000002;
    }

    public static class Shell32
    {
        [DllImport("shell32.dll", SetLastError = true)]
        public static extern IntPtr SHAppBarMessage(ABM dwMessage, [In] ref APPBARDATA pData);
    }

    public static class User32
    {
        [DllImport("user32.dll", SetLastError = true)]
        public static extern IntPtr FindWindow(string lpClassName, string lpWindowName);
    }

    [StructLayout(LayoutKind.Sequential)]
    public struct APPBARDATA
    {
        public uint cbSize;
        public IntPtr hWnd;
        public uint uCallbackMessage;
        public ABE uEdge;
        public RECT Rec;
        public int lParam;
    }

    [StructLayout(LayoutKind.Sequential)]
    public struct RECT
    {
        public int Left;
        public int Top;
        public int Right;
        public int Bottom;
    }


