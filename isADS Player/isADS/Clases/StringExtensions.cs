﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using isADS.Clases;
using System.Configuration;
using System.Data.SqlClient;

namespace isADS
{
    static class StringExtensions
    {
        /// <summary>
        /// Returns the last few characters of the string with a length
        /// specified by the given parameter. If the string's length is less than the 
        /// given length the complete string is returned. If length is zero or 
        /// less an empty string is returned
        /// </summary>
        /// <param name="s">the string to process</param>
        /// <param name="length">Number of characters to return</param>
        /// <returns></returns>
        public static string Right(this string s, int length)
        {
            length = Math.Max(length, 0);

            if (s.Length > length)
            {
                return s.Substring(s.Length - length, length);
            }
            else 
            {                
                return s;
            }
        }

        /// <summary>
        /// Returns the first few characters of the string with a length
        /// specified by the given parameter. If the string's length is less than the 
        /// given length the complete string is returned. If length is zero or 
        /// less an empty string is returned
        /// </summary>
        /// <param name="s">the string to process</param>
        /// <param name="length">Number of characters to return</param>
        /// <returns></returns>
        public static string Left(this string s, int length)
        {
            length = Math.Max(length, 0);

            if (s.Length > length)
            {
                return s.Substring(0, length);
            }
            else
            {
                return s;
            }
        }

        public static Boolean isUndefined(this String Text)
        {
            if (Text != null)
            {
                return ((Text.Equals(string.Empty)) || (Text.Equals("undefined", StringComparison.OrdinalIgnoreCase)));
            }
            else { return true; }
        }

        public static Boolean isUndefined_Localizable(this String Text, Boolean AllowDefaultUndefinedString = true)
        {
            if (Text != null)
            {
                if (AllowDefaultUndefinedString) {
                    return ((Text.Equals(string.Empty)) 
                        || (Text.Equals("undefined", StringComparison.OrdinalIgnoreCase))
                        || (Text.Equals(LanguageResources.GetText("undefined"), StringComparison.OrdinalIgnoreCase)));
                }
                else
                {
                    return ((Text.Equals(string.Empty))                                        
                    || (Text.Equals(LanguageResources.GetText("undefined"), StringComparison.OrdinalIgnoreCase)));
                }                
            }
            else { return true; }
        }

        public static void LoadValue(this SettingsProperty Property, String Section, String Key)
        {
            try
            {
                Properties.Settings.Default.PropertyValues[Property.Name].PropertyValue =
                    Convert.ChangeType(QuickSettings.Get(Section, Key, Property), Property.PropertyType);
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
            }
                //QuickSettings.Get(Section, Key, Property);
        }

        public static void LoadValue(this SettingsProperty Property)
        {
            try
            {
                Properties.Settings.Default.PropertyValues[Property.Name].PropertyValue =
                    Convert.ChangeType(QuickSettings.Get(Property), Property.PropertyType);
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
            }
            //QuickSettings.Get(Section, Key, Property);
        }

        public static void KeepValue(this SettingsPropertyValue Item)
        {
            //QuickSettings.Set(
            //    Item.Property.Attributes["Section"].ToString(),
            //    Item.Property.Attributes["Key"].ToString(),
            //    Item.PropertyValue.ToString()
            //    );

            QuickSettings.Set(Item);

        }

        public static void Clean(this Properties.Settings Settings)
        {
            foreach (SettingsPropertyValue Item in Settings.PropertyValues)
            {
                Item.IsDirty = false;
            } 
        }

        public static Exception PutData (this Exception Any, Object Key, Object Value)
        {
            Any.Data.Add(Key, Value);
            return Any;
        }

        public static String SecureHash(this String Input)
        {
            try
            {
                System.Text.Encoding CharSet = System.Text.Encoding.Unicode;

                byte[] bin = CharSet.GetBytes(Input);

                byte[] Key = CharSet.GetBytes("isADS-SQL-Credentials-JustToBeSure");
                
                System.Security.Cryptography.HMACSHA512 Algorithym = new System.Security.Cryptography.HMACSHA512(Key);

                byte[] Return = Algorithym.ComputeHash(bin);

                String Result = isADS.Clases.Functions.ByteToHex2(Return);

                return Result;
            }
            catch (Exception)
            {
                return "undefined";
            }
        }

        public static String SecureHash(this String Input, String CustomKey)
        {
            try
            {
                System.Text.Encoding CharSet = System.Text.Encoding.Unicode;

                byte[] bin = CharSet.GetBytes(Input);

                byte[] Key = CharSet.GetBytes(CustomKey);

                System.Security.Cryptography.HMACSHA512 Algorithym = new System.Security.Cryptography.HMACSHA512(Key);

                byte[] Return = Algorithym.ComputeHash(bin);

                String Result = isADS.Clases.Functions.ByteToHex2(Return);

                return Result;
            }
            catch (Exception)
            {
                return "undefined";
            }
        }

        public static void SimpleClickEffect(this System.Windows.Forms.Form Activator)
        {
            Functions.SimpleClickEffect(Activator);
        }

        public static SqlConnection OpenGet(this SqlConnection Connection)
        {
            try { if (Connection.State == System.Data.ConnectionState.Closed) Connection.Open(); }
            catch (Exception e) { Console.WriteLine(e.Message); }
            return Connection;
        }

        public static Boolean IsNullOrEmpty(this Array Elements)
        {           
            return ((Elements == null) ? (true) : (Elements.Length <= 0));
        }

        public static Boolean isValid(this Ads Ad)
        {

            // DateTime Ini = DateTime.Now;

            //Boolean Valid = true;

            //

            Boolean Valid = false; Boolean ValidContentSerial = false; Boolean ValidFileSerial = false;
            
            SqlConnection Conn = new SqlConnection(Functions.getConnectionData()).OpenGet();
            SqlCommand MediaPlayerQuery = Functions.getParameterizedCommand("SELECT TOP (1) ActivationKey FROM MEDIAPLAYER", Conn, null);

            try
            {                

                String Key = MediaPlayerQuery.ExecuteScalar().ToString();

                if (!Key.isUndefined())
                {                    
                    /* if (!(new BIGWISE.ProductStatus.Register().isActivated(Ad.FileName, Key, Ad.ContentSerial, Variables.ProductCode)))
                    {
                        Program.Logger.EscribirLog("The File [" + Ad.ID + "][" + Ad.FileName + "][" + Ad.Type + "] has an invalid Content Serial [" + Ad.ContentSerial + "]");
                        ValidContentSerial = false;
                    } else { ValidContentSerial = true; } */

                    // Lo anterior fue comentado hasta que se certifique el mecanismo de funcionamiento de LA DLL de Licencia del BUSINESS.
                    // y que la DLL no genere ninguna clave de activación inválida. 20170718.

                    ValidContentSerial = true;

                    Ads.ContentType ContentType = (Ads.ContentType) Ad.Type;

                    long Bytes = -1;

                    switch (ContentType)
                    {
                        case Ads.ContentType.Image:
                        case Ads.ContentType.Video:
                        {
                            String FilePath = Variables.myProgramDataFolder + (Ad.AdvertiserID.isUndefined() ? String.Empty : Ad.AdvertiserID + @"\") + (Ad.ID.isUndefined() ? String.Empty : Ad.ID + @"\") + Ad.FileName;
                            System.IO.FileInfo Info = null;

                            if (System.IO.File.Exists(FilePath))
                            {
                                Info = new System.IO.FileInfo(FilePath);
                                Bytes = Info.Length;
                                ValidFileSerial = (DataEncryption.EncryptText(Ad.FileName + "|" + Bytes.ToString(), Functions.EncryptionKey).Equals(Ad.FileSerial, StringComparison.Ordinal));
                                Info = null;
                            }

                            if (!ValidFileSerial) { Program.Logger.EscribirLog("The File [" + FilePath + "] has an invalid file serial."); }

                            break;
                        }
                        case Ads.ContentType.WebApp:
                        {
                            String WebAppFolderPath = Variables.myProgramDataFolder + (Ad.AdvertiserID.isUndefined() ? String.Empty : Ad.AdvertiserID + @"\") + (Ad.ID.isUndefined() ? String.Empty : Ad.ID + @"\") + Ad.FileName.Substring(0, Ad.FileName.Length - Ad.Extension.Length);

                            if (System.IO.Directory.Exists(WebAppFolderPath))
                            {
                                Bytes = Functions.GetDirectorySize(WebAppFolderPath);
                                ValidFileSerial = (DataEncryption.DecryptText(Ad.FileSerial, Functions.EncryptionKey).Equals(Ad.FileName + "|" + Bytes.ToString(), StringComparison.Ordinal));
                            }

                            if (!ValidFileSerial) { Program.Logger.EscribirLog("The Directory [" + WebAppFolderPath + "] has an invalid file serial."); }

                            break;
                        }
                        case Ads.ContentType.WebLink:
                        {
                            ValidFileSerial = true;
                            break;
                        }
                    }

                    Valid = (ValidContentSerial && ValidFileSerial);

                    if (!Valid)
                    {

                        MediaPlayerQuery.Dispose(); MediaPlayerQuery = null;

                        MediaPlayerQuery = Functions.getParameterizedCommand("UPDATE [ADS] SET ContentSerial = '', FileSerial = '', SizeInBytes = 0" +
                        Functions.NewLine() + "WHERE ID = @ADS_ID", Conn,
                        new SqlParameter[]{
                            new SqlParameter(){ ParameterName = "ADS_ID", Value = Ad.ID }
                        });

                        MediaPlayerQuery.ExecuteNonQuery();
                    }

                }
            }
            catch (Exception Any)
            {
                Console.WriteLine(Any.Message);
                Program.Logger.EscribirLog(Any, "While attempting to validate ADS Content for [" + Ad.ID + "][" + Ad.FileName + "][" + Ad.Type + "]");
            }
            finally
            {
                Conn.Dispose();
                MediaPlayerQuery.Dispose();
            }

            // DateTime End = DateTime.Now;
            // var Time = End.Subtract(Ini);
            // Console.WriteLine(Time);
            
            //

            return Valid;

        }

        public static Boolean In<T>(this T Item, params T[] Items)
        {
            if (Items == null)
                throw new ArgumentNullException("items");

            return Items.Contains(Item);
        }

    }
}
