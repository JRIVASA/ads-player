﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace isADS.Clases
{
    public static class Variables
    {

        //public static System.Threading.Thread SecondThread; 

        // Para variables globales.

        public static Boolean DEMO = true;

        public static String DefaultOSCultureName;

        public const Int32 ProductCode = 337;
        public static String ProductSerial = String.Empty;
        public static String Seed = String.Empty;

        public static String mySystemDrive;
        public static String myAppDrive;
        public static String myProgramDataFolder;

        public static System.Data.SqlClient.SqlConnection TmpConnection = null;

        public static String SelectedOrganizationID = String.Empty;
        public static Location SelectedLocation;
        public static MediaPlayer SelectedMediaPlayer;

        public static Int64 CurrentAppSession_CampaingsAndSessionsDisplayed = 0;
        public static Int64 CurrentAppSession_FinishedCampaigns = 0;

        public static String RunningProcessFileName = null;
        public static String RunningProcessFilePath = null;
        public static String RunningProcessBaseDir = null;
        public static String RunningProcessDrive = null;
        public static String RunningProcessFileExtension = null;

        public static BigMansStuff.LocusEffects.LocusEffectsProvider ClickEffects;

        public static Boolean DebugMode = false;

        public static QuickSettings.Mode AppSettingsMode;

    }
}
