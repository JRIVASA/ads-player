﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Resources;
using System.Reflection;

namespace isADS
{
    public class TextResourceHandler
    {
        private ResourceManager myResources;
        public TextResourceHandler(string Basename, Assembly pAssembly)
        {
            myResources = new ResourceManager(Basename, pAssembly);
        }
        public void PopulateFormControls(Control Ctl)
        {
            foreach (var Item in Ctl.Controls)
            {
                try
                {
                    string PropertyData = GetString(((Control)Item).Parent.Name + "." + ((Control)Item).Name);
                    if (PropertyData != null)
                        ((Control)Item).Text = PropertyData;
                }
                catch (Exception)
                {
                }
            }

        }
        public String GetString(String name)
        {
            return myResources.GetString(name);
        }

        public Object GetImage(String name)
        {
            return myResources.GetObject(name);
        }
    }
}