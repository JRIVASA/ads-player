﻿using System;
using System.ComponentModel;
using System.Data;
using System.Text;
using System.Runtime.InteropServices;

using isADS.Clases;

using System.Diagnostics;
using NAudio;
using NAudio.Mixer;
using NAudio.CoreAudioApi.Interfaces;
using NAudio.CoreAudioApi;

namespace System
{
    public partial class Audio
    {
        // [DllImport("winmm.dll")]
        // static extern int waveOutGetVolume(IntPtr hwo, out uint dwVolume);

        //[DllImport("winmm.dll")]
        // static extern int waveOutSetVolume(IntPtr hwo, uint dwVolume);

        public static int Volume
        {
            get
            {
                // Works but anyway will use NAudio as it works reliably for Get and Set
                //uint CurrVol = 0;
                //waveOutGetVolume(IntPtr.Zero, out CurrVol);
                //ushort CalcVol = (ushort)(CurrVol & 0x0000ffff);
                //return CalcVol / (ushort.MaxValue / 100);

                try
                {
                    MMDeviceEnumerator DeviceCollection = new MMDeviceEnumerator();
                    MMDevice CurrentAudioDevice = DeviceCollection.GetDefaultAudioEndpoint(DataFlow.Render, Role.Multimedia);
                    AudioSessionManager tmpAudioSessionManager = CurrentAudioDevice.AudioSessionManager;
                    SessionCollection AudioSessions = tmpAudioSessionManager.Sessions;                    
                    AudioSessionControl AudioStream = null;

                    String MyProcessName = Variables.RunningProcessFileName.Replace(Variables.RunningProcessFileExtension, String.Empty);

                    for (int i = 0; i < AudioSessions.Count; i++)
                    {
                        Process ProcessData = null;
                        try { ProcessData = Process.GetProcessById( (int) AudioSessions[i].GetProcessID ); }
                        catch (Exception) { }
                        if (ProcessData != null)
                        {       
                            AudioStream = AudioSessions[i];

                            if (ProcessData.ProcessName.Equals(MyProcessName, StringComparison.OrdinalIgnoreCase) ||
                                ProcessData.ProcessName.Equals(MyProcessName + ".vshost", StringComparison.OrdinalIgnoreCase))
                            { 
                                ProcessData.Dispose();
                                break; 
                            } 
                            else 
                            {
                                AudioStream.Dispose();
                                ProcessData.Dispose();
                            }
                        }
                    }

                    tmpAudioSessionManager.Dispose();

                    if (AudioStream != null)
                    {
                        SimpleAudioVolume tmpSimpleAudioVolume = AudioStream.SimpleAudioVolume;
                        int CurrentAudioVolume = Convert.ToInt32((tmpSimpleAudioVolume.Volume * 100));
                        tmpSimpleAudioVolume.Dispose();
                        AudioStream.Dispose();
                        return CurrentAudioVolume;
                    }
                }
                catch (Exception Any) { Console.WriteLine(Any.Message); }

                return isADS.Properties.Settings.Default.ApplicationVolume;
            }
            set
            {
                // Visual Cheat... doesn't work.
                //if (value < 0 || value > 100)
                //    throw new ArgumentOutOfRangeException();
                //int NewVolume = ((ushort.MaxValue / 100) * value);
                //uint NewVolumeAllChannels = (((uint)NewVolume & 0x0000ffff) | ((uint)NewVolume << 16));
                //Console.WriteLine(waveOutSetVolume(IntPtr.Zero, NewVolumeAllChannels));

                try
                {   
                    MMDeviceEnumerator DeviceCollection = new MMDeviceEnumerator();
                    MMDevice CurrentAudioDevice = DeviceCollection.GetDefaultAudioEndpoint(DataFlow.Render, Role.Multimedia);
                    AudioSessionManager tmpAudioSessionManager = CurrentAudioDevice.AudioSessionManager;
                    SessionCollection AudioSessions = tmpAudioSessionManager.Sessions;                    
                    AudioSessionControl AudioStream = null;

                    String MyProcessName = Variables.RunningProcessFileName.Replace(Variables.RunningProcessFileExtension, String.Empty);

                    for (int i = 0; i < AudioSessions.Count; i++)
                    {
                        Process ProcessData = null;
                        try { ProcessData = Process.GetProcessById( (int) AudioSessions[i].GetProcessID ); }
                        catch (Exception) { }
                        if (ProcessData != null)
                        {
                            
                            AudioStream = AudioSessions[i];

                            if (ProcessData.ProcessName.Equals(MyProcessName, StringComparison.OrdinalIgnoreCase) ||
                                ProcessData.ProcessName.Equals(MyProcessName + ".vshost", StringComparison.OrdinalIgnoreCase) ||
                                ProcessData.ProcessName.Equals("Teamviewer", StringComparison.OrdinalIgnoreCase))
                            {                                                               
                                SimpleAudioVolume tmpSimpleAudioVolume = AudioStream.SimpleAudioVolume;
                                tmpSimpleAudioVolume.Volume = (value / 100f);
                                tmpSimpleAudioVolume.Dispose();   
                            }

                            AudioStream.Dispose();
                            ProcessData.Dispose();
                        }
                    }

                    tmpAudioSessionManager.Dispose();                    

                } catch (Exception Any) { Console.WriteLine(Any.Message); }

            }
        }
    }
}
