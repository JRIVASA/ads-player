﻿using System;
using System.Runtime.InteropServices;
using System.Windows.Forms;

namespace isADS.Clases
{

    public static class AnimationAPI
    {
        /// <summary>
        /// Animates the window from left to right. This flag can be used with roll or slide animation.
        /// </summary>
        public const int AW_HOR_POSITIVE = 0X1;
        /// <summary>
        /// Animates the window from right to left. This flag can be used with roll or slide animation.
        /// </summary>
        public const int AW_HOR_NEGATIVE = 0X2;
        /// <summary>
        /// Animates the window from top to bottom. This flag can be used with roll or slide animation.
        /// </summary>
        public const int AW_VER_POSITIVE = 0X4;
        /// <summary>
        /// Animates the window from bottom to top. This flag can be used with roll or slide animation.
        /// </summary>
        public const int AW_VER_NEGATIVE = 0X8;
        /// <summary>
        /// Makes the window appear to collapse inward if AW_HIDE is used or expand outward if the AW_HIDE is not used.
        /// </summary>
        public const int AW_CENTER = 0X10;
        /// <summary>
        /// Hides the window. By default, the window is shown.
        /// </summary>
        public const int AW_HIDE = 0X10000;
        /// <summary>
        /// Activates the window.
        /// </summary>
        public const int AW_ACTIVATE = 0X20000;
        /// <summary>
        /// Uses slide animation. By default, roll animation is used.
        /// </summary>
        public const int AW_SLIDE = 0X40000;
        /// <summary>
        /// Uses a fade effect. This flag can be used only if hwnd is a top-level window.
        /// </summary>
        public const int AW_BLEND = 0X80000;

        /// <summary>
        /// Animates a window.
        /// </summary>
        [DllImport("user32.dll", CharSet = CharSet.Auto)]
        public static extern int AnimateWindow(IntPtr hwand, int dwTime, int dwFlags);

        // Show Animations.

        public static void AnimationDrawContentLeftToRight(this Form Target, int Time)
        {
           AnimateWindow(Target.Handle, Time, AW_ACTIVATE | AW_HOR_POSITIVE);
        }

        public static void AnimationSlideContentLeftToRight(this Form Target, int Time)
        {
            AnimateWindow(Target.Handle, Time, AW_ACTIVATE | AW_HOR_POSITIVE | AW_SLIDE);
        }

        public static void AnimationDrawContentRightToLeft(this Form Target, int Time)
        {
            AnimateWindow(Target.Handle, Time, AW_ACTIVATE | AW_HOR_NEGATIVE);
        }

        public static void AnimationSlideContentRightToLeft(this Form Target, int Time)
        {
            AnimateWindow(Target.Handle, Time, AW_ACTIVATE | AW_HOR_NEGATIVE | AW_SLIDE);
        }

        public static void AnimationDrawContentBottomToTop(this Form Target, int Time)
        {
            AnimateWindow(Target.Handle, Time, AW_ACTIVATE | AW_VER_NEGATIVE);
        }

        public static void AnimationSlideContentBottomToTop(this Form Target, int Time)
        {
            AnimateWindow(Target.Handle, Time, AW_ACTIVATE | AW_VER_NEGATIVE | AW_SLIDE);
        }

        public static void AnimationDrawContentTopToBottom(this Form Target, int Time)
        {
            AnimateWindow(Target.Handle, Time, AW_ACTIVATE | AW_VER_POSITIVE);
        }

        public static void AnimationSlideContentTopToBottom(this Form Target, int Time)
        {
            AnimateWindow(Target.Handle, Time, AW_ACTIVATE | AW_VER_POSITIVE | AW_SLIDE);
        }

        public static void AnimationExpandContent(this Form Target, int Time)
        {
            AnimateWindow(Target.Handle, Time, AW_ACTIVATE | AW_CENTER);
        }

        public static void AnimationFadeIn(this Form Target, int Time)
        {
            AnimateWindow(Target.Handle, Time, AW_ACTIVATE | AW_BLEND);
        }


        // Hide Animations.

        public static void AnimationHideContentLeftToRight(this Form Target, int Time)
        {
           AnimateWindow(Target.Handle, Time, AW_HIDE | AW_HOR_POSITIVE);
        }

        public static void AnimationHideAndSlideContentLeftToRight(this Form Target, int Time)
        {
            AnimateWindow(Target.Handle, Time, AW_HIDE | AW_HOR_POSITIVE | AW_SLIDE);
        }

        public static void AnimationHideContentRightToLeft(this Form Target, int Time)
        {
            AnimateWindow(Target.Handle, Time, AW_HIDE | AW_HOR_NEGATIVE);
        }

        public static void AnimationHideAndSlideContentRightToLeft(this Form Target, int Time)
        {
            AnimateWindow(Target.Handle, Time, AW_HIDE | AW_HOR_NEGATIVE | AW_SLIDE);
        }

        public static void AnimationHideContentBottomToTop(this Form Target, int Time)
        {
            AnimateWindow(Target.Handle, Time, AW_HIDE | AW_VER_NEGATIVE);
        }

        public static void AnimationHideAndSlideContentBottomToTop(this Form Target, int Time)
        {
            AnimateWindow(Target.Handle, Time, AW_HIDE | AW_VER_NEGATIVE | AW_SLIDE);
        }

        public static void AnimationHideContentTopToBottom(this Form Target, int Time)
        {
            AnimateWindow(Target.Handle, Time, AW_HIDE | AW_VER_POSITIVE);
        }

        public static void AnimationHideAndSlideContentTopToBottom(this Form Target, int Time)
        {
            AnimateWindow(Target.Handle, Time, AW_HIDE | AW_VER_POSITIVE | AW_SLIDE);
        }

        public static void AnimationHideAndCollapseContent(this Form Target, int Time)
        {
            AnimateWindow(Target.Handle, Time, AW_HIDE | AW_CENTER);
        }

        public static void AnimationFadeOut(this Form Target, int Time)
        {
            AnimateWindow(Target.Handle, Time, AW_HIDE | AW_BLEND);
        }        

    }

}
