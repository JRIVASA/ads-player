Smart

Ofrece una cobertura básica a las necesidades de organizaciones autónomas que no requieran de mucho apoyo.


One

Solución que se ajusta a diferentes escenarios dentro del normal desempeño tanto de organizaciones con recorrido, como de aquellas que se inician en el mundo del retail.


Expert

Pensado para organizaciones que manejen grandes cantidades de datos que requieran un mayor control y mantenimiento.


Unlimited

Pensada para aquellas organizaciones carentes de un departamento de IT. Diseñada para brindar apoyo a la plataforma Stellar.


Unlimited+

Pensada para aquellas organizaciones carentes de un departamento de IT. Diseñada para brindar apoyo tanto de la plataforma Stellar como de su infraestructura tecnológica.