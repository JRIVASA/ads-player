﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace isADS_Seed
{

    static class Extensions
    {

        public static Boolean isUndefined(this String Text)
        {
            if (Text != null)
            {
                return ((Text.Equals(string.Empty)) || (Text.Equals("undefined", StringComparison.OrdinalIgnoreCase)));
            }
            else { return true; }
        }

    }

}
