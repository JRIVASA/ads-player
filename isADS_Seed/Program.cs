﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace isADS_Seed
{

    static class Program
    {

        public static String AppGuid = "409cb2ac-6f64-4137-885c-e054a3ab5fbe";

        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        [STAThread]
        static void Main()
        {
            LoadAdditionalParameters();
            return;
        }

        public static void LoadAdditionalParameters()
        {

            try
            {
                Microsoft.Win32.RegistryKey Root = Microsoft.Win32.RegistryKey.OpenBaseKey(Microsoft.Win32.RegistryHive.LocalMachine, Microsoft.Win32.RegistryView.Registry64);

                Microsoft.Win32.RegistryKey LVL1 = Root.CreateSubKey("SYSTEM");
                Microsoft.Win32.RegistryKey LVL2 = Root.CreateSubKey("SYSTEM\\Windows");
                Microsoft.Win32.RegistryKey LVL3 = Root.CreateSubKey("SYSTEM\\Windows\\Context");
                Microsoft.Win32.RegistryKey LVL4 = Root.CreateSubKey("SYSTEM\\Windows\\Context\\" + Program.AppGuid);

                if (LVL4 != null)
                {
                    int Param1 = Convert.ToInt32(LVL4.GetValue("Param1", -1));                                       
                    if (Param1 == -1)
                    {
                        int Seed = 4096;
                        LVL4.SetValue("Param1", Seed);
                    }

                    String Param2 = Convert.ToString(LVL4.GetValue("Param2", null));
                    if (Param2.isUndefined())                    
                    {
                        String Seed = Guid.NewGuid().ToString();
                        LVL4.SetValue("Param2", Seed);                        
                    }
                }

            }
            catch (Exception Any)
            {
                Console.WriteLine(Any.Message);
                //System.Windows.Forms.MessageBox.Show("Access Error.");
                System.Environment.Exit(0);
            }

        }

    }

}
