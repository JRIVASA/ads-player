﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Forms;
using isADS_SYNC.Clases;
using System.Threading;
using System.Reflection;
using System.Runtime.InteropServices;
using System.Data.SqlClient;

namespace isADS_SYNC
{
    static class Program
    {

        public enum ContentType : int { Image = 0, Video = 1, WebApp = 2, WebLink = 3 }

        private static Thread BackGroundProcess;
        //private static Thread CleaningProcess;

        private static Mutex MultiInstanceCheck = null;

        private static String AppGuid = "5240f8d3-4d29-47ef-89fe-f8151253b314";
        private static String AssemblyGuid = (Assembly.GetExecutingAssembly().GetCustomAttributes(typeof(GuidAttribute), true)[0] as GuidAttribute).Value;

        public const int ProductCode = 337;
        private static String OrganizationID = "undefined";
        private static String MediaPlayerOrgID = "undefined";
        private static String ServerConnectionString = "undefined";

        private static DBSync.HashAlgorythims_Strength Algorythim = DBSync.HashAlgorythims_Strength.HIGH_SHA1;

        public static FSLogger Logger = new FSLogger(System.Environment.CurrentDirectory + "\\Log.txt");

        public static Boolean DebugMode = false;

        public static String RunningProcessFileName = null;
        public static String RunningProcessFilePath = null;
        public static String RunningProcessBaseDir = null;
        public static String RunningProcessFileExtension = null;

        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        [STAThread]
        static void Main() // String[] Args
        {
            //if (Args.Length > 0) Args.ToList().ForEach((Item) => MessageBox.Show(Item.ToString()));   

            //Console.WriteLine(Functions.getColorOrEmpty("#52f4efdssdsdsd").ToString());

            try { 

                Boolean SingleInstance;
                MultiInstanceCheck = new Mutex(true, "Global\\" + AppGuid, out SingleInstance);
                {
                    if (!SingleInstance)
                    {
                        Console.WriteLine("Instance already running.");
                        return;
                    }
                }

                GC.KeepAlive(MultiInstanceCheck);

            }

            catch (Exception Any) { Console.WriteLine(Any.Message); }

            try
            {

                using (System.Diagnostics.Process MyProc = System.Diagnostics.Process.GetCurrentProcess())
                {
                    System.IO.FileInfo ProgramFileInfo = new System.IO.FileInfo(MyProc.MainModule.FileName);
                    Program.RunningProcessBaseDir = ProgramFileInfo.Directory.FullName;
                    Program.RunningProcessFileName = ProgramFileInfo.Name;
                    Program.RunningProcessFilePath = ProgramFileInfo.FullName;
                    Program.RunningProcessFileExtension = ProgramFileInfo.Extension;
                }

                System.IO.FileInfo DebugFile = new System.IO.FileInfo(Program.RunningProcessBaseDir + "\\SyncDebugMode.Active");
                Program.DebugMode = DebugFile.Exists;

                System.IO.FileInfo IniFile = new System.IO.FileInfo(Program.RunningProcessBaseDir + "\\UseIniSettings.Config");
                QuickSettings.DocType = (IniFile.Exists ? QuickSettings.Mode.Ini : QuickSettings.Mode.Xml);

                QuickSettings.Configure();

            }
            catch { }

            //DateTime LastCleanup = DateTime.FromBinary(0);

            while (true)
            {

                // Class instances

                //Functions f = new Functions();
                //FileSync FileSync = new FileSync();
                //ProvisionServer Server = new ProvisionServer();
                //ProvisionClient Client = new ProvisionClient();
                //DBSync DB = new DBSync();

                //Load setup Settings
                Functions.LoadSettings();

                //Files Synchronization Process

                ServerConnectionString = (
                Properties.Settings.Default.RemoteDB_CustomServer ?
                (Properties.Settings.Default.RemoteDB_WindowsAuth ?
                Functions.getAlternateTrustedConnectionString(
                    DataEncryption.DecryptText(Properties.Settings.Default.RemoteDB_SQLServerName, Functions.EncryptionKey),
                    DataEncryption.DecryptText(Properties.Settings.Default.RemoteDB_SQLDBName, Functions.EncryptionKey))
                    :
                Functions.getAlternateConnectionString(
                    DataEncryption.DecryptText(Properties.Settings.Default.RemoteDB_SQLServerName, Functions.EncryptionKey),
                    DataEncryption.DecryptText(Properties.Settings.Default.RemoteDB_SQLDBName, Functions.EncryptionKey),
                    DataEncryption.DecryptText(Properties.Settings.Default.RemoteDB_SQLUser, Functions.EncryptionKey),
                    DataEncryption.DecryptText(Properties.Settings.Default.RemoteDB_SQLPass, Functions.EncryptionKey))
                ) : (
                Functions.getAlternateConnectionString(
                "A2NWPLSK14SQL-v05.shr.prod.iad2.secureserver.net", // Instancia //(Properties.Settings.Default.RemoteDB_Beta ? "isADSBeta.db.8609058.hostedresource.com" : "isADS.db.8609058.hostedresource.com"),
                (Properties.Settings.Default.RemoteDB_Beta ? "isADSBeta" : "isADS"), // Base de Datos
                (Properties.Settings.Default.RemoteDB_Beta ? "isADS" : "isADS"), // User Login
                (Properties.Settings.Default.RemoteDB_Beta ? "W!s3Ads30" : "W!s3Ads30") // Pwd
                )));

                //MessageBox.Show("ENTRANDO A SINCRONIZAR ARCHIVOS " + Properties.Settings.Default.LocalFilesSync.ToString()); // Debug

                SyncFiles();

                //MessageBox.Show("SINCRONIZAR BASE DE DATOS " + Properties.Settings.Default.RemoteDB_CustomServer.ToString()); // Debug

                if (Properties.Settings.Default.RemoteDB_CustomServer) 
                    SyncDBCustomServer();
                else
                {
                    SyncDBOnline();
                    CleanUnusedLocalContent();
                }

                // Pause for a while.

                OrganizationID = String.Empty;
                MediaPlayerOrgID = String.Empty;

                Console.WriteLine(Functions.NewLine() + "Taking a break...");

                // The following block of code was disabled because it was requested that the unused file
                // clearing process was also done in the SyncFiles() method as regular sync process,
                // And instead of checking old files existence to remove them, remove only the files
                // that are inactive in the database, and also remove those records. Changes were done
                // as well in the SyncDBOnline() method to avoid synchronization of inactive records.

                /*
                
                // Delete Unused Files by querying the Server for expired campaigns.
                // Check Once each Day.

                RestartCleanupThread:

                if (LastCleanup != DateTime.Today)
                {
                    LastCleanup = DateTime.Today;

                    if (CleaningProcess == null)
                    {
                
                        CleaningProcess = new Thread(() => {

                            Console.WriteLine(Functions.NewLine() + "Checking for unused files..." + Functions.NewLine());

                            CleanUnusedFiles();

                        });

                        CleaningProcess.Start();
                    }
                    else
                    {
                        if (!CleaningProcess.IsAlive) { CleaningProcess = null; goto RestartCleanupThread; }
                    }
                    
                }

                */
                 
                try
                {
                    System.Threading.Thread.Sleep(TimeSpan.Parse(Properties.Settings.Default.UpdateIntervalTimeSpan));
                }
                catch (Exception) { System.Threading.Thread.Sleep(TimeSpan.FromMinutes(3)); }

            }

        }

        private static void SyncFiles()
        {

            if (Properties.Settings.Default.LocalFilesSync)
            {
                
                /*
                * The downside with this method is that it does a FULL synchronization of all files 
                * Thus, the screens should have better than average Hard Disk space.
                * I'm aware this might be changed in the future to some sort of LAN FTP Server to do
                * synchronization the same way it's done against StellarADS360.com
                * Anyway, this business model has not been requested yet and that's why this is just
                * a quick / partial solution.
                */

                try { 

                    String FullCampaignFilesDir = System.Environment.GetEnvironmentVariable("ProgramData") + @"\" + Properties.Settings.Default.ProgramDataDir + Properties.Settings.Default.ApplicationName + @"\" + Properties.Settings.Default.CampaignFilesDir;

                    String SyncError = "undefined";

                    if (!System.IO.Directory.Exists(FullCampaignFilesDir))
                    {
                        SyncError = "The Local Campaign Files Dir does not exist or is invalid.";
                    }
                    else if (!System.IO.Directory.Exists(Properties.Settings.Default.LocalFilesSyncDir))
                    {
                        SyncError = "The Source Dir for Files does not exist or is invalid.";
                    }

                    try { System.IO.File.Delete(FullCampaignFilesDir + "filesync.metadata"); } catch (Exception Any) { Console.WriteLine(Any.Message); }

                    if (!SyncError.isUndefined())
                    {
                        // Log(SyncError);
                    }
                    else
                    {
                        //Files
                        new FileSync().ExecuteSync(Properties.Settings.Default.LocalFilesSyncDir, FullCampaignFilesDir);
                    }

                } catch (Exception Any) { Console.WriteLine(Any.Message); }
            }
            else
            {

                int MaxTries = 2;

                int CurrentTry = 1;

                System.Data.SqlClient.SqlConnection ServerConnection = null;

                RetryRemote:

                try
                {
                    if (CurrentTry <= MaxTries)
                    {
                        ServerConnection = new System.Data.SqlClient.SqlConnection(ServerConnectionString).OpenGet();
                    }
                }
                catch (Exception TransExc) { Console.WriteLine(TransExc.Message); CurrentTry++; goto RetryRemote; }                

                OrganizationID = Functions.getOrganizationID(ServerConnection,
                    DataEncryption.DecryptText(Properties.Settings.Default.OrganizationLicense, Functions.EncryptionKey));

                if (OrganizationID.isUndefined())
                {
                    Program.Logger.EscribirLog("There was a problem retrieving the current Organization Data." + Functions.NewLine(2) +
                    "Please check both connection settings and the applied Organization License is correct." + Functions.NewLine());
                    return;
                }

                //MessageBox.Show(OrganizationID); // Debug

                if (Properties.Settings.Default.SyncToServer)
                {
                    if (ServerConnection.State == System.Data.ConnectionState.Open) ServerConnection.Close();
                    try
                    {
                        //Files
                        new FileSync().DownloadFromFTP(OrganizationID);                        
                    }
                    catch (Exception Any) { Console.WriteLine(Any.Message); }
                }
                else 
                {

                    try {

                        MediaPlayerOrgID = Functions.getParameterizedCommand(
                            "SELECT ORGANIZATION_ID FROM MEDIAPLAYER WHERE ID = @MediaPlayerID"
                            , ServerConnection, new SqlParameter[]{
                            new SqlParameter(){ ParameterName = "@MediaPlayerID", Value = Properties.Settings.Default.MediaPlayerID }
                            }).ExecuteScalar().ToString();

                        //MessageBox.Show(MediaPlayerOrgID + " - " + Properties.Settings.Default.MediaPlayerID); // Debug

                        // SecurityCheck
                        if (OrganizationID != MediaPlayerOrgID) throw new Exception("Access not Allowed against Private Data.");

                        //Properties.Settings.Default.MediaPlayerID = "000000078";

                        String TmpSQL = 
                        "SELECT REPLACE((" +
                        "\n\tSELECT ID AS 'data()' FROM(" +
                        "\n\t\tSELECT * FROM ADS WHERE ID IN(" +
                        "\n\t\t\tSELECT ADS_ID FROM CAMPAIGNxADS WHERE CAMPAIGN_ID IN(" +
                        "\n\t\t\t\tSELECT CAMPAIGN_ID FROM CAMPAIGNxMEDIAPLAYER WHERE MEDIAPLAYER_ID = @MediaPlayerID" +
                        "\n\t\t\t\tAND CAMPAIGN_ID IN (" +
                        "\n\t\t\t\t\tSELECT ID FROM CAMPAIGN WHERE Status = 'Active' AND @FechaBD <= CAST(EndDate AS DATE)" +
                        "\n\t\t\t\t)" +
                        "\n\t\t\t)" +
                        "\n\t\t)" +
                        "\n\t) TB1 FOR XML PATH(''))" +
                        "\n, ' ', '|') AS ADS";

                        String RawADS = Functions.getParameterizedCommand(TmpSQL,
                            ServerConnection, new SqlParameter[]{
                            new SqlParameter(){ ParameterName = "@MediaPlayerID", Value = Properties.Settings.Default.MediaPlayerID },
                            new SqlParameter(){ ParameterName = "@FechaBD", Value = Functions.FechaBD(DateTime.Now) }
                            }).ExecuteScalar().ToString();

                        //MessageBox.Show(RawADS); // Debug

                        String[] OnlyNeededADS = ((RawADS == null) ? null : RawADS.Split('|')); 

                        if (OnlyNeededADS != null)
                        {
                            //Files
                            new FileSync().DownloadFromFTP(OrganizationID, OnlyNeededADS);
                        }

                    }
                    catch (Exception Any) { 
                        Console.WriteLine(Any.Message);
                        Program.Logger.EscribirLog(Any);
                    }
                    finally { if (ServerConnection.State == System.Data.ConnectionState.Open) ServerConnection.Close(); }

                }
            }
        }

        private static void CleanUnusedLocalContent()
        {

            String LocalConnectionString = Properties.Settings.Default.LocalDB_WindowsAuth ?
                Functions.getAlternateTrustedConnectionString()
                    :
                Functions.getAlternateConnectionString(
                    DataEncryption.DecryptText(Properties.Settings.Default.LocalDB_SQLUser, Functions.EncryptionKey),
                    DataEncryption.DecryptText(Properties.Settings.Default.LocalDB_SQLPass, Functions.EncryptionKey));

            // LocalConnectionString = Functions.getAlternateConnectionString("BIG-P-02", "isADS_Client", "SA", "");  // For Testing...            

            int MaxTries = 3;

            int CurrentTry = 1;

            System.Data.SqlClient.SqlTransaction ApplyACIDLocal = null;
            System.Data.SqlClient.SqlConnection LocalConnection = null;

            RetryLocal:

            try
            {
                if (CurrentTry <= MaxTries)
                {
                    LocalConnection = new System.Data.SqlClient.SqlConnection(LocalConnectionString).OpenGet();
                }
            }
            catch (Exception TransExc) { Console.WriteLine(TransExc.Message); CurrentTry++; goto RetryLocal; }

            if (LocalConnection == null) return;
            if (LocalConnection.State != System.Data.ConnectionState.Open) return;

            try
            {

                MediaPlayerOrgID = Functions.getParameterizedCommand(
                    "SELECT ORGANIZATION_ID FROM MEDIAPLAYER WHERE ID = @MediaPlayerID"
                    , LocalConnection, new SqlParameter[]{
                            new SqlParameter(){ ParameterName = "@MediaPlayerID", Value = Properties.Settings.Default.MediaPlayerID }
                }).ExecuteScalar().ToString();

                //MessageBox.Show(MediaPlayerOrgID + " - " + Properties.Settings.Default.MediaPlayerID); // Debug

                // SecurityCheck
                if (OrganizationID != MediaPlayerOrgID) throw new Exception("Access not Allowed against Private Data.");

                //Properties.Settings.Default.MediaPlayerID = "000000078";

                String TmpSQL =
                "SELECT REPLACE((" +
                "\n\tSELECT ADS AS 'data()' FROM (" +
                "\n\t\tSELECT ADVERTISER_ID + ':|:' + ID + ':|:' + FileName + '[|]' AS ADS FROM ADS " +
                "\n\t\tWHERE ID NOT IN (SELECT ADS_ID FROM CAMPAIGNxADS)" +
                "\n\t\tAND ORGANIZATION_ID = @OrganizationID" +
                "\n\t\tUNION" +
                "\n\t\tSELECT ADVERTISER_ID + ':|:' + ID + ':|:' + FileName  + '[|]' AS ADS FROM ADS WHERE ID NOT IN (" +
                "\n\t\t\tSELECT ADS_ID FROM CAMPAIGNxADS WHERE CAMPAIGN_ID IN (" +
                "\n\t\t\t\tSELECT CAMPAIGN_ID FROM CAMPAIGNxMEDIAPLAYER WHERE 1 = 1" +
                "\n\t\t\t\tAND CAMPAIGN_ID IN (" +
                "\n\t\t\t\t\tSELECT ID FROM CAMPAIGN WHERE @CurrentDate <= CAST(EndDate AS DATE)" +
                "\n\t\t\t\t)" +
                "\n\t\t\t\tAND MEDIAPLAYER_ID = @MediaPlayerID" +
                "\n\t\t\t)" +
                "\n\t\t)" +
                "\n\t\tAND ORGANIZATION_ID = @OrganizationID" +
                "\n\t) TB1 FOR XML PATH(''))" +
                "\n, '[|] ', '[|]') AS ADS"
                ;

                String ADSInfo = Functions.getParameterizedCommand(TmpSQL,
                    LocalConnection, new SqlParameter[]{
                            new SqlParameter(){ ParameterName = "@OrganizationID", Value = OrganizationID },
                            new SqlParameter(){ ParameterName = "@MediaPlayerID", Value = Properties.Settings.Default.MediaPlayerID },
                            new SqlParameter(){ ParameterName = "@CurrentDate", Value = Functions.FechaBD(DateTime.Now) }
                }).ExecuteScalar().ToString();

                if (Program.DebugMode) System.Windows.Forms.MessageBox.Show("ADSInfo:" + "\n\n" + ADSInfo);

                //MessageBox.Show(RawADS); // Debug

                String[] OnlyNeededADS = ((ADSInfo == null) ? null : ADSInfo.Split(new String[] { "[|]" }, StringSplitOptions.RemoveEmptyEntries));

                Boolean ContentClear = false;

                if (OnlyNeededADS != null)
                {
                    //Files
                    ContentClear = new FileSync().DeleteUnusedFiles(OrganizationID, OnlyNeededADS);
                }

                // TO DO....

                // CLEAR CAMPAIGN, CAMPAIGNxADS, CAMPAIGNxMEDIAPLAYER, ADS -> WHERE CurrentDate > EndDate

                if (ContentClear)
                {

                    String CurrentDate = Functions.FechaBD(DateTime.Now);

                    ApplyACIDLocal = LocalConnection.BeginTransaction
                    (System.Data.IsolationLevel.ReadUncommitted, "LocalACID");

                    Int32 CleaningRowCount = 0;

                    if (Program.DebugMode) System.Windows.Forms.MessageBox.Show("Eliminando registros inactivos de BD");

                    // ----------------------------------------------------------------------------

                    // Update: The CAMPAIGN table records cannot be deleted because they would be synchronized again and they are needed
                    // to cause this Cleaning event, so they always need to be updated because otherwise the user would not be able to disable a campaign.

                    /*
                    TmpSQL = "DELETE FROM CAMPAIGN WHERE ID NOT IN(" +
                    "\n\tSELECT CAMPAIGN_ID FROM CAMPAIGNxMEDIAPLAYER WHERE 1 = 1" +
                    "\n\tAND CAMPAIGN_ID IN(" +
                    "\n\t\tSELECT ID FROM CAMPAIGN WHERE @CurrentDate <= CAST(EndDate AS DATE)" + 
                    "\n\t) AND MEDIAPLAYER_ID = @MediaPlayerID" +
                    "\n)";

                    CleaningRowCount = Functions.getParameterizedCommand(TmpSQL, LocalConnection, ApplyACIDLocal, new SqlParameter[]{
                        new SqlParameter(){ ParameterName = "@MediaPlayerID", Value = Properties.Settings.Default.MediaPlayerID },
                        new SqlParameter(){ ParameterName = "@CurrentDate", Value = CurrentDate }
                    }).ExecuteNonQuery();
                    
                    if (Program.DebugMode) System.Windows.Forms.MessageBox.Show("Campañas removidas: " + CleaningRowCount);
                    
                    */

                    // ----------------------------------------------------------------------------

                    TmpSQL = "DELETE FROM CAMPAIGNxMEDIAPLAYER WHERE CAMPAIGN_ID NOT IN(" +
                    "\n\tSELECT CAMPAIGN_ID FROM CAMPAIGNxMEDIAPLAYER WHERE 1 = 1" +
                    "\n\tAND CAMPAIGN_ID IN(" +
                    "\n\t\tSELECT ID FROM CAMPAIGN WHERE @CurrentDate <= CAST(EndDate AS DATE)" +
                    "\n\t) AND MEDIAPLAYER_ID = @MediaPlayerID" +
                    "\n)";

                    CleaningRowCount = Functions.getParameterizedCommand(TmpSQL, LocalConnection, ApplyACIDLocal, new SqlParameter[]{
                        new SqlParameter(){ ParameterName = "@MediaPlayerID", Value = Properties.Settings.Default.MediaPlayerID },
                        new SqlParameter(){ ParameterName = "@CurrentDate", Value = CurrentDate }
                    }).ExecuteNonQuery();

                    if (Program.DebugMode) System.Windows.Forms.MessageBox.Show("CxM: " + CleaningRowCount);

                    // ----------------------------------------------------------------------------

                    TmpSQL = "DELETE FROM CAMPAIGNxADS WHERE CAMPAIGN_ID NOT IN(" +
                    "\n\tSELECT CAMPAIGN_ID FROM CAMPAIGNxMEDIAPLAYER WHERE 1 = 1" +
                    "\n\tAND CAMPAIGN_ID IN(" +
                    "\n\t\tSELECT ID FROM CAMPAIGN WHERE @CurrentDate <= CAST(EndDate AS DATE)" +
                    "\n\t) AND MEDIAPLAYER_ID = @MediaPlayerID" +
                    "\n)";

                    CleaningRowCount = Functions.getParameterizedCommand(TmpSQL, LocalConnection, ApplyACIDLocal, new SqlParameter[]{
                        new SqlParameter(){ ParameterName = "@MediaPlayerID", Value = Properties.Settings.Default.MediaPlayerID },
                        new SqlParameter(){ ParameterName = "@CurrentDate", Value = CurrentDate }
                    }).ExecuteNonQuery();

                    if (Program.DebugMode) System.Windows.Forms.MessageBox.Show("CxA: " + CleaningRowCount);

                    // ----------------------------------------------------------------------------

                    String AdvID = null; String ContentID = null; String[] ArrInfo = null; String FileName = null;

                    foreach (String RawInfo in OnlyNeededADS)
                    {

                        ArrInfo = RawInfo.Split(new String[] { ":|:" }, StringSplitOptions.RemoveEmptyEntries);

                        AdvID = ArrInfo[0];
                        ContentID = ArrInfo[1];
                        FileName = ArrInfo[2];

                        TmpSQL = "DELETE FROM ADS WHERE ID = @ContentID AND ADVERTISER_ID = @AdvertiserID";

                        CleaningRowCount = Functions.getParameterizedCommand(TmpSQL, LocalConnection, ApplyACIDLocal, new SqlParameter[]{
                            new SqlParameter(){ ParameterName = "@AdvertiserID", Value = AdvID },
                            new SqlParameter(){ ParameterName = "@ContentID", Value = ContentID }
                        }).ExecuteNonQuery();

                        if (Program.DebugMode) System.Windows.Forms.MessageBox.Show("Contenido eliminado:\n\n[" + ContentID + "]\n\n" + FileName);

                    }

                    ApplyACIDLocal.Commit();

                    // ----------------------------------------------------------------------------

                }

            }
            catch (Exception Any)
            {
                if (ApplyACIDLocal != null) ApplyACIDLocal.Rollback();
                //Console.WriteLine(Any.Message);
                if (Program.DebugMode) System.Windows.Forms.MessageBox.Show("Error al eliminar registros inactivos en la base de datos.");
                Program.Logger.EscribirLog(Any, "Error while clearing unused database content.");
            }
            finally { if (LocalConnection.State == System.Data.ConnectionState.Open) LocalConnection.Close(); }

        }

        private static void CleanUnusedFiles()
        {

            int MaxTries = 2;

            int CurrentTry = 1;

            System.Data.SqlClient.SqlConnection ServerConnection = null;

        RetryRemote:

            try
            {
                if (CurrentTry <= MaxTries)
                {
                    ServerConnection = new System.Data.SqlClient.SqlConnection(ServerConnectionString).OpenGet();
                }
            }
            catch (Exception TransExc) { Console.WriteLine(TransExc.Message); CurrentTry++; goto RetryRemote; }

            OrganizationID = Functions.getOrganizationID(ServerConnection,
                DataEncryption.DecryptText(Properties.Settings.Default.OrganizationLicense, Functions.EncryptionKey));

            if (OrganizationID.isUndefined())
            {
                Program.Logger.EscribirLog("There was a problem retrieving the current Organization Data." + Functions.NewLine(2) +
                "Please check both connection settings and the applied Organization License is correct." + Functions.NewLine());
                return;
            }

            //MessageBox.Show(OrganizationID); // Debug

            if (!Properties.Settings.Default.SyncToServer)
            {
                try
                {

                    MediaPlayerOrgID = Functions.getParameterizedCommand(
                        "SELECT ORGANIZATION_ID FROM MEDIAPLAYER WHERE ID = @MediaPlayerID"
                        , ServerConnection, new SqlParameter[]{
                        new SqlParameter(){ ParameterName = "@MediaPlayerID", Value = Properties.Settings.Default.MediaPlayerID }
                        }).ExecuteScalar().ToString();

                    //MessageBox.Show(MediaPlayerOrgID + " - " + Properties.Settings.Default.MediaPlayerID); // Debug

                    // SecurityCheck
                    if (OrganizationID != MediaPlayerOrgID) throw new Exception("Access not Allowed against Private Data.");

                    //Properties.Settings.Default.MediaPlayerID = "000000078";

                    String TmpSQL =
                    "SELECT REPLACE((" +
                    "\n\tSELECT ADS AS 'data()' FROM (" +
                    "\n\t\tSELECT ADVERTISER_ID + ':|:' + ID + ':|:' + FileName + '[|]' AS ADS FROM ADS " +
                    "\n\t\tWHERE ID NOT IN (SELECT ADS_ID FROM CAMPAIGNxADS)" +
                    "\n\t\tAND ORGANIZATION_ID = @OrganizationID" +
                    "\n\t\tUNION" +
                    "\n\t\tSELECT ADVERTISER_ID + ':|:' + ID + ':|:' + FileName  + '[|]' AS ADS FROM ADS WHERE ID NOT IN (" +
                    "\n\t\t\tSELECT ADS_ID FROM CAMPAIGNxADS WHERE CAMPAIGN_ID IN (" +
                    "\n\t\t\t\tSELECT CAMPAIGN_ID FROM CAMPAIGNxMEDIAPLAYER WHERE 1 = 1" +
                    "\n\t\t\t\tAND CAMPAIGN_ID IN (" +
                    "\n\t\t\t\t\tSELECT ID FROM CAMPAIGN WHERE @CurrentDate <= CAST(EndDate AS DATE)" +
                    "\n\t\t\t\t)" +
                    "\n\t\t\t\tAND MEDIAPLAYER_ID = @MediaPlayerID" +
                    "\n\t\t\t)" +
                    "\n\t\t)" +
                    "\n\t\tAND ORGANIZATION_ID = @OrganizationID" +
                    "\n\t) TB1 FOR XML PATH(''))" +
                    "\n, '[|] ', '[|]') AS ADS"
                    ;

                    String ADSInfo = Functions.getParameterizedCommand(TmpSQL,
                        ServerConnection, new SqlParameter[]{
                            new SqlParameter(){ ParameterName = "@OrganizationID", Value = OrganizationID },
                            new SqlParameter(){ ParameterName = "@MediaPlayerID", Value = Properties.Settings.Default.MediaPlayerID },
                            new SqlParameter(){ ParameterName = "@CurrentDate", Value = Functions.FechaBD(DateTime.Now) }
                    }).ExecuteScalar().ToString();

                    //MessageBox.Show(RawADS); // Debug

                    String[] OnlyNeededADS = ((ADSInfo == null) ? null : ADSInfo.Split(new String[] { "[|]" }, StringSplitOptions.RemoveEmptyEntries));

                    Boolean ContentClear = false;

                    if (OnlyNeededADS != null)
                    {
                        //Files
                        ContentClear = new FileSync().DeleteUnusedFiles(OrganizationID, OnlyNeededADS);
                    }

                }
                catch (Exception Any)
                {
                    //Console.WriteLine(Any.Message);
                    Program.Logger.EscribirLog(Any, "Error while clearing unused database content from Server.");
                }
                finally { if (ServerConnection.State == System.Data.ConnectionState.Open) ServerConnection.Close(); }
            }
            
        }

        private static void SyncDBOnline()
        {

            // SyncDBOnline() connects to StellarADS360.com and synchronizes data for the current organization ID.

            String LocalConnectionString = Properties.Settings.Default.LocalDB_WindowsAuth ?
                Functions.getAlternateTrustedConnectionString()
                    :
                Functions.getAlternateConnectionString(
                    DataEncryption.DecryptText(Properties.Settings.Default.LocalDB_SQLUser, Functions.EncryptionKey),
                    DataEncryption.DecryptText(Properties.Settings.Default.LocalDB_SQLPass, Functions.EncryptionKey));

            // LocalConnectionString = Functions.getAlternateConnectionString("BIG-P-02", "isADS_Client", "SA", "");  // For Testing...            

            int MaxTries = 3;

            int CurrentTry = 1;

            System.Data.SqlClient.SqlTransaction ApplyACIDLocal = null;
            System.Data.SqlClient.SqlConnection LocalConnection = null;

        RetryLocal:

            try
            {
                if (CurrentTry <= MaxTries)
                {
                    LocalConnection = new System.Data.SqlClient.SqlConnection(LocalConnectionString).OpenGet();
                    ApplyACIDLocal = LocalConnection.BeginTransaction
                    (System.Data.IsolationLevel.ReadUncommitted, "LocalACID");
                }
            }
            catch (Exception TransExc) { Console.WriteLine(TransExc.Message); CurrentTry++; goto RetryLocal; }

            CurrentTry = 1;
            //System.Data.SqlClient.SqlTransaction ApplyACIDRemote = null;
            System.Data.SqlClient.SqlConnection ServerConnection = null;

        RetryRemote:

            try
            {
                if (CurrentTry <= MaxTries)
                {
                    ServerConnection = new System.Data.SqlClient.SqlConnection(ServerConnectionString).OpenGet();                    
                    //ApplyACIDRemote = ServerConnection.BeginTransaction
                    //    (System.Data.IsolationLevel.ReadUncommitted, "RemoteACID");
                }
            }
            catch (Exception TransExc) { Console.WriteLine(TransExc.Message); CurrentTry++; goto RetryRemote; }

            if (LocalConnection == null || ApplyACIDLocal == null || ServerConnection == null) return;
            if ((LocalConnection.State != System.Data.ConnectionState.Open) || (ServerConnection.State != System.Data.ConnectionState.Open)) return;
            if (ApplyACIDLocal.Connection == null) return; 

            if (OrganizationID.isUndefined())
            {
                Program.Logger.EscribirLog("There was a problem retrieving the current Organization Data." + Functions.NewLine(2) +
                "Please check both connection settings and the applied Organization License is correct." + Functions.NewLine());
                return;
            }

            // SecurityCheck
            if (!Properties.Settings.Default.SyncToServer)
                if (OrganizationID != MediaPlayerOrgID) return; // "Access not Allowed against Private Data."

            try
            {

                // Check if Organization is Demo and Expiry Date

                Boolean Demo = Convert.ToBoolean(Functions.getParameterizedCommand(
                               "SELECT Demo FROM ORGANIZATION WHERE ID = @OrganizationID", ServerConnection, new SqlParameter[]{
                            new SqlParameter(){ ParameterName = "@OrganizationID", Value = OrganizationID }
                            }).ExecuteScalar());

                if (Demo) // Demo Status
                {

                    // Select the difference between actual date with the min date value from an organization.
                    // We consider an organization Creation Date AS the Oldest User Creation Date.

                    Int32 DemoDays = Convert.ToInt32(Functions.getParameterizedCommand(
                            "SELECT DATEDIFF(day,(SELECT CAST(MIN(u.CreationDate) AS Date) AS Fecha  FROM ORGANIZATIONxUSERS u  WHERE u.ORGANIZATION_ID = @OrganizationID ), GetDate()) AS DiffDate", ServerConnection, new SqlParameter[]{
                            new SqlParameter(){ ParameterName = "@OrganizationID", Value = OrganizationID }
                    }).ExecuteScalar());

                    if (DemoDays >= 30) // Reset Mediaplayer License
                    {

                        Int32 UpdateMediaPlayer = 0;

                        UpdateMediaPlayer = Functions.getParameterizedCommand(
                            "UPDATE MEDIAPLAYER SET ActivationKey = '', License = '', Status = 'Inactive', UpdateDate = GetDate() WHERE ID = @MediaPlayerID ", LocalConnection, ApplyACIDLocal, new SqlParameter[]{
                            new SqlParameter(){ ParameterName = "@MediaPlayerID", Value = Properties.Settings.Default.MediaPlayerID }
                            }).ExecuteNonQuery();

                        UpdateMediaPlayer = Functions.getParameterizedCommand(
                            "UPDATE MEDIAPLAYER SET Status = 'Request', UpdateDate = GetDate() WHERE Status <> 'Request' AND ID = @MediaPlayerID AND ORGANIZATION_ID = @OrgID ", ServerConnection, new SqlParameter[]{
                            new SqlParameter(){ ParameterName = "@MediaPlayerID", Value = Properties.Settings.Default.MediaPlayerID },
                            new SqlParameter(){ ParameterName = "@OrgID", Value = OrganizationID }
                        }).ExecuteNonQuery();

                        //Console.WriteLine(updateMediaPlayer);
                    }

                }

            }
            catch (Exception Any) { Program.Logger.EscribirLog(Any, "An error has occurred while attempting to Check for Demo & ExpiryDate. More info below:"); }

            Boolean SyncSuccess = true;

            //MessageBox.Show("EMPEZANDO A SINCRONIZAR TABLAS"); // Debug

            Algorythim = DBSync.HashAlgorythims_Strength.HIGH_SHA256;

            try
            {

                String SQLServerVersion = Functions.getParameterizedCommand("SELECT SERVERPROPERTY('ProductVersion') AS Version",
                LocalConnection, ApplyACIDLocal, null).ExecuteScalar().ToString();

                if (SQLServerVersion.StartsWith("10", StringComparison.OrdinalIgnoreCase))
                    Algorythim = DBSync.HashAlgorythims_Strength.HIGH_SHA1; // SQL Server 2008 doesn't support more t                
                else
                    Algorythim = DBSync.HashAlgorythims_Strength.HIGH_SHA256;
                    // 2012 And Up.

            } catch { }

            // Regarding SyncToServer... This is a business model / case we haven't figured out entirely yet. Basically
            // It implies Web Management (StellarADS360.COM), but Content And Database only synchronizes to 
            // a Master Server belonging to the customer, to reduce bandwidth and stopping every single screen from
            // doing the same. Every screen now would synchronize against the Central Server by LAN Networking.
            // This is not totally defined yet but remains as a future possibility. 
            // SyncToServer would be true for the Central Server, it syncs all the data from the OrganizationID, 
            // but it would be false for regular screens, they don't sync everything but a part of the organization data.

            String SyncDate = Functions.FechaBD(DateTime.Now);

            if (Properties.Settings.Default.SyncToServer)  
            {

                for (CurrentTry = 1; CurrentTry <= MaxTries; CurrentTry++)
                {
                    SyncSuccess = DBSync.SyncTable(LocalConnection, ServerConnection, "ADS", "ADS", ApplyACIDLocal,
                        false, null, Functions.getParameterizedCommand("WHERE ORGANIZATION_ID = @ID", new SqlParameter[]{
                    new SqlParameter() { ParameterName = "@ID", Value = OrganizationID }
                }), false, true, new String[] { "ContentSerial", "FileSerial", "SizeInBytes" }, Algorythim);
                    if (!SyncSuccess) { if (CurrentTry == MaxTries) goto Transaction; } else { break; }
                }

                // Don't fill ADS Serials here, this is to Sync the Main distribution server, 
                // so the fields should remain blank until Synchronizatio to screens occur.

                for (CurrentTry = 1; CurrentTry <= MaxTries; CurrentTry++)
                {
                    SyncSuccess = DBSync.SyncTable(LocalConnection, ServerConnection, "CAMPAIGN", "CAMPAIGN", ApplyACIDLocal,
                        false, null, Functions.getParameterizedCommand("WHERE ORGANIZATION_ID = @ID", new SqlParameter[]{
                    new SqlParameter() { ParameterName = "@ID", Value = OrganizationID }
                }), false, true, null, Algorythim);
                    if (!SyncSuccess) { if (CurrentTry == MaxTries) goto Transaction; } else { break; }
                }

                for (CurrentTry = 1; CurrentTry <= MaxTries; CurrentTry++)
                {
                    SyncSuccess = DBSync.SyncTable(LocalConnection, ServerConnection, "CAMPAIGNxADS", "CAMPAIGNxADS", ApplyACIDLocal,
                        false, null, Functions.getParameterizedCommand("WHERE ORGANIZATION_ID = @ID", new SqlParameter[]{
                    new SqlParameter() { ParameterName = "@ID", Value = OrganizationID }
                }), false, true, null, Algorythim);
                    if (!SyncSuccess) { if (CurrentTry == MaxTries) goto Transaction; } else { break; }
                }

                for (CurrentTry = 1; CurrentTry <= MaxTries; CurrentTry++)
                {
                    SyncSuccess = DBSync.SyncTable(LocalConnection, ServerConnection, "CAMPAIGNxMEDIAPLAYER", "CAMPAIGNxMEDIAPLAYER", ApplyACIDLocal,
                        false, null, Functions.getParameterizedCommand("WHERE ORGANIZATION_ID = @ID", new SqlParameter[]{
                    new SqlParameter() { ParameterName = "@ID", Value = OrganizationID }
                }), false, true, null, Algorythim);
                    if (!SyncSuccess) { if (CurrentTry == MaxTries) goto Transaction; } else { break; }
                }

            }
            else // This is the code for Screens. Synchronize partial data from the organization.
            {

                // Starting from now, the Synchronization here occurs for each Table, but considering the following:
                // 1) Sync only the content which applies to the current screen ID.
                // 2) Sync only the content which has an expiration date higher than current O.S date,
                // to avoid synchronizing inactive content. This is because starting from now
                // the inactive content will be deleted and should not be downloaded again unless
                // the customer reactivated the campaign by changing the campaign EndDate.

                // Update: The Table CAMPAIGN cannot be filtered by Date, because otherwise it wouldn't be possible to modify the CAMPAIGN.
                // And those record changes are needed to cause the content cleaning event. 2017/07/18

                //MessageBox.Show("ADS"); // Debug

                for (CurrentTry = 1; CurrentTry <= MaxTries; CurrentTry++)
                {                    
                    SyncSuccess = DBSync.SyncTable(LocalConnection, ServerConnection, "ADS", "ADS", ApplyACIDLocal,
                        false, null, Functions.getParameterizedCommand(
                        "WHERE ID IN(" +
                        "\n\tSELECT ADS_ID FROM CAMPAIGNxADS WHERE CAMPAIGN_ID IN(" +
                        "\n\t\tSELECT CAMPAIGN_ID FROM CAMPAIGNxMEDIAPLAYER WHERE MEDIAPLAYER_ID = @MediaPlayerID AND CAMPAIGN_ID IN(" +
                        "\n\t\t\tSELECT ID FROM CAMPAIGN WHERE @CurrentDate <= CAST(EndDate AS DATE)" +
                        "\n\t\t)" +
                        "\n\t)" + 
                        "\n)", new SqlParameter[]{
                    new SqlParameter() { ParameterName = "@MediaPlayerID", Value = Properties.Settings.Default.MediaPlayerID },
                    new SqlParameter() { ParameterName = "@CurrentDate", Value = SyncDate }
                }), false, false, new String[] { "ContentSerial", "FileSerial", "SizeInBytes" }, Algorythim);
                    if (!SyncSuccess) { if (CurrentTry == MaxTries) goto Transaction; } else { break; }
                }

                //MessageBox.Show("SERIALS"); // Debug

                // Fill ADS Serials.

                for (CurrentTry = 1; CurrentTry <= MaxTries; CurrentTry++)
                {
                    SyncSuccess = DBSync.FillContentDistributionSerials(LocalConnection, ApplyACIDLocal);
                    if (!SyncSuccess) { if (CurrentTry == MaxTries) goto Transaction; } else { break; }
                }

                //

                //MessageBox.Show("CAMPAIGN"); // Debug

                // Forget about date filter on this one, because of the reasons above... 2017/07/18

                for (CurrentTry = 1; CurrentTry <= MaxTries; CurrentTry++)
                {
                    SyncSuccess = DBSync.SyncTable(LocalConnection, ServerConnection, "CAMPAIGN", "CAMPAIGN", ApplyACIDLocal,
                        false, null, Functions.getParameterizedCommand(
                        "WHERE ID IN(" +
                        "\n\tSELECT CAMPAIGN_ID FROM CAMPAIGNxMEDIAPLAYER WHERE MEDIAPLAYER_ID = @MediaPlayerID" +
                        "\n)" // + "--AND @CurrentDate <= CAST(EndDate AS DATE)"
                    , new SqlParameter[]{
                    new SqlParameter() { ParameterName = "@MediaPlayerID", Value = Properties.Settings.Default.MediaPlayerID }//,
                    //new SqlParameter() { ParameterName = "@CurrentDate", Value = SyncDate }
                }), false, false, null, Algorythim);
                    if (!SyncSuccess) { if (CurrentTry == MaxTries) goto Transaction; } else { break; }
                }

                //MessageBox.Show("CAMPAIGNxADS"); // Debug

                for (CurrentTry = 1; CurrentTry <= MaxTries; CurrentTry++)
                {
                    SyncSuccess = DBSync.SyncTable(LocalConnection, ServerConnection, "CAMPAIGNxADS", "CAMPAIGNxADS", ApplyACIDLocal,
                        false, null, Functions.getParameterizedCommand(
                        "WHERE CAMPAIGN_ID IN(" +
                        "\n\tSELECT CAMPAIGN_ID FROM CAMPAIGNxMEDIAPLAYER WHERE MEDIAPLAYER_ID = @MediaPlayerID AND CAMPAIGN_ID IN(" +
                        "\n\t\tSELECT ID FROM CAMPAIGN WHERE @CurrentDate <= CAST(EndDate AS DATE)" +
                        "\n\t)" +
                        "\n)", new SqlParameter[]{
                    new SqlParameter() { ParameterName = "@MediaPlayerID", Value = Properties.Settings.Default.MediaPlayerID },
                    new SqlParameter() { ParameterName = "@CurrentDate", Value = SyncDate }
                }), false, false, null, Algorythim);
                    if (!SyncSuccess) { if (CurrentTry == MaxTries) goto Transaction; } else { break; }
                }

                //MessageBox.Show("CAMPAIGNxMEDIAPLAYER"); // Debug

                for (CurrentTry = 1; CurrentTry <= MaxTries; CurrentTry++)
                {
                    SyncSuccess = DBSync.SyncTable(LocalConnection, ServerConnection, "CAMPAIGNxMEDIAPLAYER", "CAMPAIGNxMEDIAPLAYER", ApplyACIDLocal,
                        false, null, Functions.getParameterizedCommand(
                        "WHERE MEDIAPLAYER_ID = @MediaPlayerID AND CAMPAIGN_ID IN(" +
                        "\n\tSELECT ID FROM CAMPAIGN WHERE @CurrentDate <= CAST(EndDate AS DATE)" +
                        "\n)", new SqlParameter[]{
                    new SqlParameter() { ParameterName = "@MediaPlayerID", Value = Properties.Settings.Default.MediaPlayerID },
                    new SqlParameter() { ParameterName = "@CurrentDate", Value = SyncDate }
                }), false, false, null, Algorythim);
                    if (!SyncSuccess) { if (CurrentTry == MaxTries) goto Transaction; } else { break; }
                }
            }
            
            // The following table won't be synchronized anymore, because of the licensing changes.

            //for (CurrentTry = 1; CurrentTry <= MaxTries; CurrentTry++)
            //{
            //    SyncSuccess = DBSync.SyncTable(LocalConnection, ServerConnection, "MEDIAPLAYER", "MEDIAPLAYER", ApplyACIDLocal,
            //        false, null, Functions.getParameterizedCommand("WHERE ORGANIZATION_ID = @ID", new SqlParameter[]{
            //        new SqlParameter() { ParameterName = "@ID", Value = OrganizationID }
            //    }));
            //    if (!SyncSuccess) { if (CurrentTry == MaxTries) goto Transaction; } else { break; }
            //}

            // The following one is going to start without filter, every organization will have the same templates for now.
            // This could change in case New Templates are built and sold separately.

            //for (CurrentTry = 1; CurrentTry <= MaxTries; CurrentTry++)
            //{
            //    SyncSuccess = DBSync.SyncTable(LocalConnection, ServerConnection, "TEMPLATES", "TEMPLATES", ApplyACIDLocal,
            //        false, null, Functions.getParameterizedCommand("WHERE ORGANIZATION_ID = @ID", new SqlParameter[]{
            //        new SqlParameter() { ParameterName = "@ID", Value = OrganizationID }
            //    }));
            //    if (!SyncSuccess) { if (CurrentTry == MaxTries) goto Transaction; } else { break; }
            //}

            //MessageBox.Show("TEMPLATES"); // Debug

            for (CurrentTry = 1; CurrentTry <= MaxTries; CurrentTry++)
            {
                SyncSuccess = DBSync.SyncTable(LocalConnection, ServerConnection, "TEMPLATES", "TEMPLATES", ApplyACIDLocal,
                false, null, null, false, true, null, Algorythim);
                if (!SyncSuccess) { if (CurrentTry == MaxTries) goto Transaction; } else { break; }
            }

            MaxTries = 2;

            for (CurrentTry = 1; CurrentTry <= MaxTries; CurrentTry++)
            {
                Boolean OptionalSyncSuccess = DBSync.SyncTable(LocalConnection, ServerConnection, "SYSTEM_INFORMATION", "SYSTEM_INFORMATION", ApplyACIDLocal,
                false, null, null, true, true, new String[] { "CurrentVersion", "CurrentVersionDate" }, Algorythim);
                if (OptionalSyncSuccess) break;
            }            

            Transaction:
            try {

                //MessageBox.Show(SyncSuccess.ToString()); // Debug

                if (SyncSuccess)
                {
                    ApplyACIDLocal.Commit();
                    //ApplyACIDRemote.Commit();
                    Console.WriteLine(Functions.NewLine() + "Transaction Success.");
                }
                else
                {
                    ApplyACIDLocal.Rollback();
                    //ApplyACIDRemote.Rollback();
                    // Maybe output some log...
                    Console.WriteLine(Functions.NewLine() + "Transaction Rollback.");
                }

                LocalConnection.Close();
                ServerConnection.Close();

            } catch (Exception Any) {                                
                Program.Logger.EscribirLog(Any, "Sync Transaction Error");
            }

            RestartThread:
            if (BackGroundProcess == null)
            {
                
                BackGroundProcess = new Thread(() => {

                Console.WriteLine(Functions.NewLine() + "Initializing Database Transfers..." + Functions.NewLine());

                RetryLocalTransfer:

                    try
                    {
                        if (CurrentTry <= MaxTries)
                        {
                            LocalConnection = new System.Data.SqlClient.SqlConnection(LocalConnectionString).OpenGet();
                            ApplyACIDLocal = LocalConnection.BeginTransaction
                            (System.Data.IsolationLevel.ReadUncommitted, "LocalACID");
                        }
                    }
                    catch (Exception TransExc) { Console.WriteLine(TransExc.Message); CurrentTry++; goto RetryLocalTransfer; }

                    CurrentTry = 1;
                    System.Data.SqlClient.SqlTransaction ApplyACIDRemote = null;
                    ServerConnection = null;

                RetryRemoteTransfer:

                    try
                    {
                        if (CurrentTry <= 3)
                        {
                            ServerConnection = new System.Data.SqlClient.SqlConnection(ServerConnectionString).OpenGet();
                            ApplyACIDRemote = ServerConnection.BeginTransaction
                            (System.Data.IsolationLevel.ReadUncommitted, "RemoteACID");
                        }
                    }
                    catch (Exception TransExc) { Console.WriteLine(TransExc.Message); CurrentTry++; goto RetryRemoteTransfer; }

                    if (LocalConnection == null || ApplyACIDLocal == null || ServerConnection == null || ApplyACIDRemote == null) return;
                    if ((LocalConnection.State != System.Data.ConnectionState.Open) || (ServerConnection.State != System.Data.ConnectionState.Open)) return;
                    if (ApplyACIDLocal.Connection == null || ApplyACIDRemote.Connection == null) return;

                    Boolean TransferSuccess = true;
                    
                    /*
                    String SQLServerVersion = Functions.getParameterizedCommand("SELECT SERVERPROPERTY('ProductVersion') AS Version", LocalConnection, ApplyACIDLocal, null).ExecuteScalar().ToString();
                    
                    DBSync.HashAlgorythims_Strength Algorythim;
                    
                    if (SQLServerVersion.StartsWith("10.0", StringComparison.OrdinalIgnoreCase))
                        Algorythim = DBSync.HashAlgorythims_Strength.HIGH_SHA1; // SQL Server 2008 doesn't support more than SHA1.
                    else 
                        Algorythim = DBSync.HashAlgorythims_Strength.HIGH_SHA256; 
                        // 2012 And Up.                     
                    */ // Overkill stuff. Not Needed so far. Just testing...

                    for (CurrentTry = 1; CurrentTry <= MaxTries; CurrentTry++)
                    {
                        TransferSuccess = DBSync.TransferTable(ServerConnection, LocalConnection, "ADSxINTERACTIONS", "ADSxINTERACTIONS",
                        ApplyACIDRemote, ApplyACIDLocal, false, null, null, Algorythim);
                        if (!TransferSuccess) { if (CurrentTry == MaxTries) goto TransferTransaction; } else { break; }
                    }                        

                TransferTransaction:
                    try {
                        if (SyncSuccess)
                        {
                            // First let's make sure the inserts execute successfully in Server, then apply deletes in Local.

                            ApplyACIDRemote.Commit();
                            ApplyACIDLocal.Commit();
                            
                            Console.WriteLine(Functions.NewLine() + "Transfer Success.");
                        }
                        else
                        {
                            ApplyACIDRemote.Rollback();
                            ApplyACIDLocal.Rollback();
                            
                            Console.WriteLine(Functions.NewLine() + "Transfer Failed.");
                            // Maybe output some log...
                        }
                        LocalConnection.Close();
                        ServerConnection.Close();
                    } catch (Exception Any) { Console.WriteLine(Any.Message); }

                });

                BackGroundProcess.Start();
            }
            else
            {
                if (!BackGroundProcess.IsAlive) { BackGroundProcess = null; goto RestartThread; }
            }

            //DBSync.TransferTable(ServerConnectionString, LocalConnectionString, "ADSxINTERACTIONS", "ADSxINTERACTIONS");
        }

        private static void SyncDBCustomServer()
        {

            // IMPORTANT NOTES:

            // SyncDBCustomServer() code is not actively updated AS SyncDBOnline() because this business model
            // Has not been requested or in use yet. Basically, this means that it should do the same as SyncDBOnline()
            // but instead of Synchronizing against StellarADS360.com, it is for a private customer server 
            // which contains the StellarADS360.com application and functionality but has not any relationship
            // with the former website. It is for people / organizations who want to have the complete 
            // Stellar ADS 360 solution in their hands without subscription to the Software as a Service (Saas).

                //String ServerConnectionString = (
                //    Properties.Settings.Default.RemoteDB_CustomServer ?
                //    (Properties.Settings.Default.RemoteDB_WindowsAuth ?
                //    Functions.getAlternateTrustedConnectionString(
                //        DataEncryption.DecryptText(Properties.Settings.Default.RemoteDB_SQLServerName, Functions.EncryptionKey),
                //        DataEncryption.DecryptText(Properties.Settings.Default.RemoteDB_SQLDBName, Functions.EncryptionKey))
                //        :
                //    Functions.getAlternateConnectionString(
                //        DataEncryption.DecryptText(Properties.Settings.Default.RemoteDB_SQLServerName, Functions.EncryptionKey),
                //        DataEncryption.DecryptText(Properties.Settings.Default.RemoteDB_SQLDBName, Functions.EncryptionKey),
                //        DataEncryption.DecryptText(Properties.Settings.Default.RemoteDB_SQLUser, Functions.EncryptionKey),
                //        DataEncryption.DecryptText(Properties.Settings.Default.RemoteDB_SQLPass, Functions.EncryptionKey))
                //    ) : (
                //    Functions.getAlternateConnectionString(
                //    "isADSBeta.db.8609058.hostedresource.com",
                //    "isADSBeta",
                //    "isADSBeta",
                //    "W!s3Ads30"
                //    )
                //    ));

                String LocalConnectionString = Properties.Settings.Default.LocalDB_WindowsAuth ?
                    Functions.getAlternateTrustedConnectionString()
                        :
                    Functions.getAlternateConnectionString(
                        DataEncryption.DecryptText(Properties.Settings.Default.LocalDB_SQLUser, Functions.EncryptionKey),
                        DataEncryption.DecryptText(Properties.Settings.Default.LocalDB_SQLPass, Functions.EncryptionKey));

                Console.WriteLine(Functions.NewLine() + "Initializing Database Synchronization..." + Functions.NewLine());

                int MaxTries = 3;

                int CurrentTry = 1;

                System.Data.SqlClient.SqlTransaction ApplyACIDLocal = null;
                System.Data.SqlClient.SqlConnection LocalConnection = null;

            RetryLocal:

                try
                {
                    if (CurrentTry <= MaxTries)
                    {
                        LocalConnection = new System.Data.SqlClient.SqlConnection(LocalConnectionString).OpenGet();
                        ApplyACIDLocal = LocalConnection.BeginTransaction
                        (System.Data.IsolationLevel.ReadUncommitted, "LocalACID");
                    }
                }
                catch (Exception TransExc) { Console.WriteLine(TransExc.Message); CurrentTry++; goto RetryLocal; }

                CurrentTry = 1;
                //System.Data.SqlClient.SqlTransaction ApplyACIDRemote = null;
                System.Data.SqlClient.SqlConnection ServerConnection = null;

            RetryRemote:

                try
                {
                    if (CurrentTry <= 3)
                    {
                        ServerConnection = new System.Data.SqlClient.SqlConnection(ServerConnectionString);
                        ServerConnection.Open();
                        //ApplyACIDRemote = ServerConnection.BeginTransaction
                        //    (System.Data.IsolationLevel.ReadUncommitted, "RemoteACID");
                    }
                }
                catch (Exception TransExc) { Console.WriteLine(TransExc.Message); CurrentTry++; goto RetryRemote; }

                if (LocalConnection == null || ApplyACIDLocal == null || ServerConnection == null) return;
                if ((LocalConnection.State != System.Data.ConnectionState.Open) || (ServerConnection.State != System.Data.ConnectionState.Open)) return;
                if (ApplyACIDLocal.Connection == null) return;

                Boolean SyncSuccess = true;

                Algorythim = DBSync.HashAlgorythims_Strength.HIGH_SHA256;

                try
                {
                    String SQLServerVersion = Functions.getParameterizedCommand("SELECT SERVERPROPERTY('ProductVersion') AS Version", LocalConnection, ApplyACIDLocal, null).ExecuteScalar().ToString();

                    if (SQLServerVersion.StartsWith("10.0", StringComparison.OrdinalIgnoreCase))
                        Algorythim = DBSync.HashAlgorythims_Strength.HIGH_SHA1; // SQL Server 2008 doesn't support more than SHA1.
                    else
                        Algorythim = DBSync.HashAlgorythims_Strength.HIGH_SHA256;
                    // 2012 And Up.                     
                } catch { } 

                for (CurrentTry = 1; CurrentTry <= MaxTries; CurrentTry++)
                {
                    SyncSuccess = DBSync.SyncTable(LocalConnection, ServerConnection, "ADS", "ADS", ApplyACIDLocal,
                    false, null, null, false, true, null, Algorythim);
                    if (!SyncSuccess) { if (CurrentTry == MaxTries) goto Transaction; } else { break; }
                }

                for (CurrentTry = 1; CurrentTry <= MaxTries; CurrentTry++)
                {
                    SyncSuccess = DBSync.SyncTable(LocalConnection, ServerConnection, "CAMPAIGN", "CAMPAIGN", ApplyACIDLocal,
                    false, null, null, false, true, null, Algorythim);                    
                    if (!SyncSuccess) { if (CurrentTry == MaxTries) goto Transaction; } else { break; }                    
                }

                for (CurrentTry = 1; CurrentTry <= MaxTries; CurrentTry++)
                {
                    SyncSuccess = DBSync.SyncTable(LocalConnection, ServerConnection, "CAMPAIGNxADS", "CAMPAIGNxADS", ApplyACIDLocal,
                    false, null, null, false, true, null, Algorythim);
                    if (!SyncSuccess) { if (CurrentTry == MaxTries) goto Transaction; } else { break; }
                }

                for (CurrentTry = 1; CurrentTry <= MaxTries; CurrentTry++)
                {
                    SyncSuccess = DBSync.SyncTable(LocalConnection, ServerConnection, "CAMPAIGNxMEDIAPLAYER", "CAMPAIGNxMEDIAPLAYER", ApplyACIDLocal,
                    false, null, null, false, true, null, Algorythim);
                    if (!SyncSuccess) { if (CurrentTry == MaxTries) goto Transaction; } else { break; }
                }

                for (CurrentTry = 1; CurrentTry <= MaxTries; CurrentTry++)
                {
                    SyncSuccess = DBSync.SyncTable(LocalConnection, ServerConnection, "MEDIAPLAYER", "MEDIAPLAYER", ApplyACIDLocal,
                    false, null, null, false, true, null, Algorythim);
                    if (!SyncSuccess) { if (CurrentTry == MaxTries) goto Transaction; } else { break; }
                }

                for (CurrentTry = 1; CurrentTry <= MaxTries; CurrentTry++)
                {
                    SyncSuccess = DBSync.SyncTable(LocalConnection, ServerConnection, "TEMPLATES", "TEMPLATES", ApplyACIDLocal,
                    false, null, null, false, true, null, Algorythim);
                    if (!SyncSuccess) { if (CurrentTry == MaxTries) goto Transaction; } else { break; }
                }

                Transaction:
                try
                {
                    if (SyncSuccess)
                    {
                        ApplyACIDLocal.Commit();
                        //ApplyACIDRemote.Commit();
                    }
                    else
                    {
                        ApplyACIDLocal.Rollback();
                        //ApplyACIDRemote.Rollback();
                        // Maybe output some log...
                    }
                    LocalConnection.Close();
                    ServerConnection.Close();
                } catch (Exception Any) { Console.WriteLine(Any.Message); }

                RestartThread:

                if (BackGroundProcess == null)
                {
                
                    BackGroundProcess = new Thread(() => {

                    Console.WriteLine(Functions.NewLine() + "Initializing Database Transfers..." + Functions.NewLine());

                    RetryLocalTransfer:

                        try
                        {
                            if (CurrentTry <= MaxTries)
                            {
                                LocalConnection = new System.Data.SqlClient.SqlConnection(LocalConnectionString).OpenGet();
                                ApplyACIDLocal = LocalConnection.BeginTransaction
                                (System.Data.IsolationLevel.ReadUncommitted, "LocalACID");
                            }
                        }
                        catch (Exception TransExc) { Console.WriteLine(TransExc.Message); CurrentTry++; goto RetryLocalTransfer; }

                        CurrentTry = 1;
                        System.Data.SqlClient.SqlTransaction ApplyACIDRemote = null;
                        ServerConnection = null;

                    RetryRemoteTransfer:

                        try
                        {
                            if (CurrentTry <= 3)
                            {
                                ServerConnection = new System.Data.SqlClient.SqlConnection(ServerConnectionString).OpenGet();
                                ApplyACIDRemote = ServerConnection.BeginTransaction
                                (System.Data.IsolationLevel.ReadUncommitted, "RemoteACID");
                            }
                        }
                        catch (Exception TransExc) { Console.WriteLine(TransExc.Message); CurrentTry++; goto RetryRemoteTransfer; }

                        if (LocalConnection == null || ApplyACIDLocal == null || ServerConnection == null || ApplyACIDRemote == null) return;
                        if ((LocalConnection.State != System.Data.ConnectionState.Open) || (ServerConnection.State != System.Data.ConnectionState.Open)) return;
                        if (ApplyACIDLocal.Connection == null || ApplyACIDRemote.Connection == null) return;

                        Boolean TransferSuccess = true;

                        for (CurrentTry = 1; CurrentTry <= MaxTries; CurrentTry++)
                        {
                            TransferSuccess = DBSync.TransferTable(ServerConnection, LocalConnection, "ADSxINTERACTIONS", "ADSxINTERACTIONS",
                            ApplyACIDRemote, ApplyACIDLocal, false, null, null, Algorythim);
                            if (!TransferSuccess) { if (CurrentTry == MaxTries) goto TransferTransaction; } else { break; }
                        }                        

                    TransferTransaction:
                        try {
                            if (SyncSuccess)
                            {
                                // First let's make sure the inserts execute successfully in Server, then apply deletes in Local.

                                ApplyACIDRemote.Commit();
                                ApplyACIDLocal.Commit();
                            
                                Console.WriteLine(Functions.NewLine() + "Transfer Success.");
                            }
                            else
                            {
                                ApplyACIDRemote.Rollback();
                                ApplyACIDLocal.Rollback();
                            
                                Console.WriteLine(Functions.NewLine() + "Transfer Failed.");
                                // Maybe output some log...
                            }
                            LocalConnection.Close();
                            ServerConnection.Close();
                        } catch (Exception Any) { Console.WriteLine(Any.Message); }

                    });

                    BackGroundProcess.Start();
                }
                else
                {
                    if (!BackGroundProcess.IsAlive) { BackGroundProcess = null; goto RestartThread; }
                }

                //DBSync.TransferTable(ServerConnectionString, LocalConnectionString, "ADSxINTERACTIONS", "ADSxINTERACTIONS");
        }

    }
}
