﻿using isADS_SYNC.Clases;
using Microsoft.Synchronization;
using Microsoft.Synchronization.Data;
using Microsoft.Synchronization.Data.SqlServer;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Linq.Expressions;

namespace isADS_SYNC.Database_Sync_Classes
{
    class DBSyncBak
    {

        public static Boolean TransferTable(String LocalDBConnectionString, String ServerDBConnectionString, String LocalDBTableName, String ServerDBTableName, Boolean TruncateLocalFirst = false)
        {

            try
            {

                SqlConnection DefaultLocalConnection = new SqlConnection(LocalDBConnectionString).OpenGet();
                SqlConnection DefaultServerConnection = new SqlConnection(ServerDBConnectionString).OpenGet();

                DataTable Server = new DataTable("Server");

                Server.Columns.AddRange(
                    new DataColumn[]{
                    new DataColumn("PrimaryKey", typeof(Int64))
                }
                    );

                Server.PrimaryKey = new DataColumn[] { Server.Columns["PrimaryKey"] };

                // ________________ Get Sync Tables Columns ________________

                SqlDataReader RL;

                SqlCommand CL = new SqlCommand(
                    "SELECT * FROM SYS.all_columns WHERE" + Functions.NewLine() +
                    "is_identity = 0" + Functions.NewLine() +
                    "AND OBJECT_ID IN (" + Functions.NewLine() +
                    "SELECT OBJECT_ID FROM SYS.all_objects WHERE is_ms_shipped = 0 AND TYPE = 'U' AND NAME = '" + LocalDBTableName + "')",
                    DefaultLocalConnection);

                RL = CL.ExecuteReader();

                String LocalTableCols = String.Empty;

                while (RL.Read())
                {
                    LocalTableCols += ", " + RL["Name"].ToString();
                }

                RL.Close();

                LocalTableCols = LocalTableCols.Substring(2); // Ignore the first ", "

                // ________________ Get Sync Tables Primary Key Columns ________________

                String PrimaryKey = String.Empty;

                CL = new SqlCommand(
                    "SELECT *" + Functions.NewLine() +
                    "FROM INFORMATION_SCHEMA.KEY_COLUMN_USAGE" + Functions.NewLine() +
                    "WHERE OBJECTPROPERTY(OBJECT_ID(CONSTRAINT_NAME), 'IsPrimaryKey') = 1" + Functions.NewLine() +
                    "AND TABLE_NAME = '" + LocalDBTableName + "'",
                    DefaultLocalConnection);

                RL = CL.ExecuteReader();

                while (RL.Read())
                {
                    PrimaryKey += ", " + RL["Column_Name"].ToString();
                }

                RL.Close();

                if (PrimaryKey.isUndefined()) { return false; }

                PrimaryKey = PrimaryKey.Substring(2); // Ignore the first ", "

                // ________________ Get Server Table Data Summary ________________

                SqlCommand CS = new SqlCommand("SELECT BINARY_CHECKSUM(" + PrimaryKey + ") AS PrimaryKey FROM " + ServerDBTableName + "",
                DefaultServerConnection);

                SqlDataReader RS;

                RS = CS.ExecuteReader();

                while (RS.Read())
                {
                    Server.Rows.Add(new Object[] {
                    Convert.ToInt64(RS["PrimaryKey"])
                });
                }

                RS.Close();

                // ________________ Start Sync ________________

                DataSet LocalData = new DataSet("LocalData");

                SqlCommand LocalItemQuery = new SqlCommand("SELECT * FROM " + LocalDBTableName + " WHERE 1 = 2",
                DefaultLocalConnection);

                SqlDataAdapter LocalAdapter = new SqlDataAdapter();
                LocalAdapter.SelectCommand = LocalItemQuery;

                SqlCommandBuilder LocalDML = new SqlCommandBuilder(LocalAdapter);
                LocalAdapter.Fill(LocalData, LocalDBTableName);

                LocalAdapter.DeleteCommand = LocalDML.GetDeleteCommand(false);
                LocalAdapter.InsertCommand = LocalDML.GetInsertCommand(false);

                foreach (DataRow Item in Server.Rows)
                {

                    DataSet ServerData = new DataSet("ServerData");

                    SqlDataAdapter ServerAdapter = new SqlDataAdapter(new SqlCommand("SELECT * FROM " + ServerDBTableName + " WHERE BINARY_CHECKSUM(" + PrimaryKey + ") = " + Item["PrimaryKey"].ToString(),
                    DefaultServerConnection));

                    SqlCommandBuilder ServerDML = new SqlCommandBuilder(ServerAdapter);
                    ServerAdapter.Fill(ServerData, ServerDBTableName);

                    ServerAdapter.DeleteCommand = ServerDML.GetDeleteCommand(false);
                    //ServerAdapter.InsertCommand = ServerDML.GetInsertCommand(false);

                    LocalData.Tables[LocalDBTableName].Rows.Add(
                        ServerData.Tables[ServerDBTableName].Rows[0].ItemArray);

                    Console.WriteLine("DELETING Primary Key " + Item["PrimaryKey"].ToString());

                    ServerAdapter.DeleteCommand.CommandText = "DELETE FROM " + ServerDBTableName + " WHERE BINARY_CHECKSUM(" + PrimaryKey + ") = " + Item["PrimaryKey"].ToString();
                    ServerAdapter.DeleteCommand.Parameters.Clear();
                    ServerAdapter.DeleteCommand.ExecuteNonQuery();

                }

                if (TruncateLocalFirst)
                {
                    Console.WriteLine(Functions.NewLine() + "Truncating Table before bulk inserts.");

                    LocalAdapter.DeleteCommand.CommandText = "Truncate Table " + ServerDBTableName;
                    LocalAdapter.DeleteCommand.Parameters.Clear();
                    LocalAdapter.DeleteCommand.ExecuteNonQuery();
                }

                try
                {
                    // Console.WriteLine(LocalAdapter.UpdateBatchSize);
                    LocalAdapter.UpdateBatchSize = (Server.Rows.Count < 4000 ? 1000 : Convert.ToInt32(Server.Rows.Count * 0.25));
                }
                catch (Exception Any) { Console.WriteLine(Any.Message); }

                Console.WriteLine(Functions.NewLine() + "Submitting Bulk Inserts into Local...");
                // LocalData.AcceptChanges();
                LocalAdapter.Update(LocalData, LocalDBTableName);

                Console.WriteLine(Functions.NewLine() + "Transfer Complete.");

                Boolean Success = true;

                return Success;

            }
            catch (Exception e)
            {
                Boolean Fail = false;

                Console.WriteLine(Functions.NewLine() + "Transfer Failed:" + Functions.NewLine() + e.Message);
                return Fail;
            }

        }

        public static Boolean TransferTable(SqlConnection LocalDBConnection, SqlConnection ServerDBConnection,
        String LocalDBTableName, String ServerDBTableName,
        SqlTransaction LocalTransactionForInserts = null, SqlTransaction ServerTransactionForDeletes = null,
        Boolean TruncateLocalFirst = false)
        {

            try
            {

                DataTable Server = new DataTable("Server");

                Server.Columns.AddRange(
                    new DataColumn[]{
                    new DataColumn("PrimaryKey", typeof(Int64))
                }
                    );

                Server.PrimaryKey = new DataColumn[] { Server.Columns["PrimaryKey"] };

                // ________________ Get Sync Tables Columns ________________

                SqlDataReader RL;

                SqlCommand CL = new SqlCommand(
                    "SELECT * FROM SYS.all_columns WHERE" + Functions.NewLine() +
                    "is_identity = 0" + Functions.NewLine() +
                    "AND OBJECT_ID IN (" + Functions.NewLine() +
                    "SELECT OBJECT_ID FROM SYS.all_objects WHERE is_ms_shipped = 0 AND TYPE = 'U' AND NAME = '" + LocalDBTableName + "')",
                    LocalDBConnection);

                CL.Transaction = LocalTransactionForInserts;

                RL = CL.ExecuteReader();

                String LocalTableCols = String.Empty;

                while (RL.Read())
                {
                    LocalTableCols += ", " + RL["Name"].ToString();
                }

                RL.Close();

                LocalTableCols = LocalTableCols.Substring(2); // Ignore the first ", "

                // ________________ Get Sync Tables Primary Key Columns ________________

                String PrimaryKey = String.Empty;

                CL = new SqlCommand(
                    "SELECT *" + Functions.NewLine() +
                    "FROM INFORMATION_SCHEMA.KEY_COLUMN_USAGE" + Functions.NewLine() +
                    "WHERE OBJECTPROPERTY(OBJECT_ID(CONSTRAINT_NAME), 'IsPrimaryKey') = 1" + Functions.NewLine() +
                    "AND TABLE_NAME = '" + LocalDBTableName + "'",
                    LocalDBConnection);

                CL.Transaction = LocalTransactionForInserts;

                RL = CL.ExecuteReader();

                while (RL.Read())
                {
                    PrimaryKey += ", " + RL["Column_Name"].ToString();
                }

                RL.Close();

                if (PrimaryKey.isUndefined()) { return false; }

                PrimaryKey = PrimaryKey.Substring(2); // Ignore the first ", "

                // ________________ Get Server Table Data Summary ________________

                SqlCommand CS = new SqlCommand("SELECT BINARY_CHECKSUM(" + PrimaryKey + ") AS PrimaryKey FROM " + ServerDBTableName + "",
                ServerDBConnection);

                CS.Transaction = ServerTransactionForDeletes;

                SqlDataReader RS;

                RS = CS.ExecuteReader();

                while (RS.Read())
                {
                    Server.Rows.Add(new Object[] {
                    Convert.ToInt64(RS["PrimaryKey"])
                });
                }

                RS.Close();

                // ________________ Start Sync ________________

                DataSet LocalData = new DataSet("LocalData");

                SqlCommand LocalItemQuery = new SqlCommand("SELECT * FROM " + LocalDBTableName + " WHERE 1 = 2",
                LocalDBConnection);

                LocalItemQuery.Transaction = LocalTransactionForInserts;

                SqlDataAdapter LocalAdapter = new SqlDataAdapter();
                LocalAdapter.SelectCommand = LocalItemQuery;

                SqlCommandBuilder LocalDML = new SqlCommandBuilder(LocalAdapter);
                LocalAdapter.Fill(LocalData, LocalDBTableName);

                LocalAdapter.DeleteCommand = LocalDML.GetDeleteCommand(false);
                LocalAdapter.InsertCommand = LocalDML.GetInsertCommand(false);

                foreach (DataRow Item in Server.Rows)
                {

                    DataSet ServerData = new DataSet("ServerData");

                    SqlCommand ServerItemQuery = new SqlCommand("SELECT * FROM " + ServerDBTableName + " WHERE BINARY_CHECKSUM(" + PrimaryKey + ") = " + Item["PrimaryKey"].ToString(),
                    ServerDBConnection);

                    ServerItemQuery.Transaction = ServerTransactionForDeletes;

                    SqlDataAdapter ServerAdapter = new SqlDataAdapter(ServerItemQuery);
                    SqlCommandBuilder ServerDML = new SqlCommandBuilder(ServerAdapter);
                    ServerAdapter.Fill(ServerData, ServerDBTableName);

                    ServerAdapter.DeleteCommand = ServerDML.GetDeleteCommand(false);
                    //ServerAdapter.InsertCommand = ServerDML.GetInsertCommand(false);

                    LocalData.Tables[LocalDBTableName].Rows.Add(
                        ServerData.Tables[ServerDBTableName].Rows[0].ItemArray);

                    Console.WriteLine("DELETING Primary Key " + Item["PrimaryKey"].ToString());

                    ServerAdapter.DeleteCommand.CommandText = "DELETE FROM " + ServerDBTableName + " WHERE BINARY_CHECKSUM(" + PrimaryKey + ") = " + Item["PrimaryKey"].ToString();
                    ServerAdapter.DeleteCommand.Parameters.Clear();
                    ServerAdapter.DeleteCommand.ExecuteNonQuery();

                }

                if (TruncateLocalFirst)
                {
                    Console.WriteLine(Functions.NewLine() + "Truncating Table before bulk inserts.");

                    LocalAdapter.DeleteCommand.CommandText = "Truncate Table " + ServerDBTableName;
                    LocalAdapter.DeleteCommand.Parameters.Clear();
                    LocalAdapter.DeleteCommand.ExecuteNonQuery();
                }

                Console.WriteLine(Functions.NewLine() + "Submitting Bulk Inserts into Local...");
                // LocalData.AcceptChanges();

                try
                {
                    // Console.WriteLine(LocalAdapter.UpdateBatchSize);
                    LocalAdapter.UpdateBatchSize = (Server.Rows.Count < 4000 ? 1000 : Convert.ToInt32(Server.Rows.Count * 0.25));
                }
                catch (Exception Any) { Console.WriteLine(Any.Message); }

                LocalAdapter.Update(LocalData, LocalDBTableName);

                Console.WriteLine(Functions.NewLine() + "Transfer Complete.");

                Boolean Success = true;

                return Success;

            }
            catch (Exception e)
            {
                Boolean Fail = false;

                Console.WriteLine(Functions.NewLine() + "Transfer Failed:" + Functions.NewLine() + e.Message);
                return Fail;
            }

        }

        public static Boolean SyncTable(String LocalDBConnectionString, String ServerDBConnectionString,
        String LocalDBTableName, String ServerDBTableName, SqlTransaction LocalTransactionForChanges = null,
        Boolean TransferMode = false, Boolean OpenConnectionPerRow = false, SqlCommand ServerTableDataCriteria = null,
        String[] IgnoredFields = null)
        {

            SqlConnection DefaultLocalConnection = null;
            SqlConnection DefaultServerConnection = null;

            if (!OpenConnectionPerRow)
            {
                DefaultLocalConnection = new SqlConnection(LocalDBConnectionString).OpenGet();
                DefaultServerConnection = new SqlConnection(ServerDBConnectionString).OpenGet();
            }

            try
            {

                DataTable Local = new DataTable("Local");
                DataTable Server = new DataTable("Server");
                DataTable SyncTable = new DataTable("Sync");

                SyncTable.Columns.AddRange(
                                new DataColumn[]{
                    new DataColumn("PrimaryKey", typeof(Int64)),
                    new DataColumn("LocalData", typeof(Int64)),
                    new DataColumn("ServerData", typeof(Int64))
                }
                                );

                Local.Columns.AddRange(
                    new DataColumn[]{
                    new DataColumn("PrimaryKey", typeof(Int64)),
                    new DataColumn("LocalData", typeof(Int64)),
                    new DataColumn("ServerData", typeof(Int64))
                }
                    );

                Local.PrimaryKey = new DataColumn[] { Local.Columns["PrimaryKey"] };

                Server.Columns.AddRange(
                    new DataColumn[]{
                    new DataColumn("PrimaryKey", typeof(Int64)),
                    new DataColumn("LocalData", typeof(Int64)),
                    new DataColumn("ServerData", typeof(Int64))
                }
                    );

                Server.PrimaryKey = new DataColumn[] { Server.Columns["PrimaryKey"] };

                // ________________ Get Sync Tables Columns ________________

                String LocalTableIgnoredCols = String.Empty;
                String LocalTableIgnoredValues = String.Empty;

                if (!IgnoredFields.IsNullOrEmpty())
                {
                    Array.ForEach(IgnoredFields, (Item) => { LocalTableIgnoredCols += ", " + Item; LocalTableIgnoredValues += ", " + "'" + Item + "'"; });
                    LocalTableIgnoredCols = LocalTableIgnoredCols.Substring(2);
                    LocalTableIgnoredValues = LocalTableIgnoredValues.Substring(2);
                }

                SqlDataReader RL;

                SqlCommand CL = new SqlCommand(
                    "SELECT * FROM SYS.all_columns WHERE" + Functions.NewLine() +
                    "is_identity = 0" + Functions.NewLine() +
                    "AND OBJECT_ID IN (" + Functions.NewLine() +
                    "SELECT OBJECT_ID FROM SYS.all_objects WHERE is_ms_shipped = 0 AND TYPE = 'U' AND NAME = '" + LocalDBTableName + "')" +
                    (LocalTableIgnoredCols.isUndefined() ? String.Empty
                    : Functions.NewLine() + "AND Name NOT IN (" + LocalTableIgnoredValues + ")"),
                    (!OpenConnectionPerRow ? DefaultLocalConnection : new SqlConnection(LocalDBConnectionString).OpenGet()));

                CL.Transaction = LocalTransactionForChanges;

                RL = CL.ExecuteReader();

                String LocalTableCols = String.Empty;

                while (RL.Read())
                {
                    LocalTableCols += ", " + RL["Name"].ToString();
                }

                RL.Close();

                LocalTableCols = LocalTableCols.Substring(2); // Ignore the first ", "

                // ________________ Get Sync Tables Primary Key Columns ________________

                String PrimaryKey = String.Empty;

                CL = new SqlCommand(
                    "SELECT *" + Functions.NewLine() +
                    "FROM INFORMATION_SCHEMA.KEY_COLUMN_USAGE" + Functions.NewLine() +
                    "WHERE OBJECTPROPERTY(OBJECT_ID(CONSTRAINT_NAME), 'IsPrimaryKey') = 1" + Functions.NewLine() +
                    "AND TABLE_NAME = '" + LocalDBTableName + "'",
                    (!OpenConnectionPerRow ? DefaultLocalConnection : new SqlConnection(LocalDBConnectionString).OpenGet()));

                CL.Transaction = LocalTransactionForChanges;

                RL = CL.ExecuteReader();

                while (RL.Read())
                {
                    PrimaryKey += ", " + RL["Column_Name"].ToString();
                }

                RL.Close();

                if (PrimaryKey.isUndefined()) { return false; }

                PrimaryKey = PrimaryKey.Substring(2); // Ignore the first ", "

                // ________________ Get Server Table Data Summary ________________

                SqlDataReader RS;

                SqlCommand CS = null;

                Boolean CriteriaExists = (ServerTableDataCriteria != null);
                CriteriaExists = (CriteriaExists ? !ServerTableDataCriteria.CommandText.isUndefined() : false);

                if (CriteriaExists)
                {
                    CS = new SqlCommand("SELECT BINARY_CHECKSUM(" + PrimaryKey + ") AS PrimaryKey, " +
                    "0 AS LocalData, BINARY_CHECKSUM(" + LocalTableCols + ") AS ServerData FROM " + ServerDBTableName +
                    Functions.NewLine() + ServerTableDataCriteria.CommandText, // WHERE...
                    (!OpenConnectionPerRow ? DefaultServerConnection : new SqlConnection(ServerDBConnectionString).OpenGet()));

                    ServerTableDataCriteria.Parameters.OfType<SqlParameter>().ToList<SqlParameter>().ForEach((Param) => CS.Parameters.Add(new SqlParameter(Param.ParameterName, Param.Value)));

                    ServerTableDataCriteria.Dispose();
                }
                else
                {
                    CS = new SqlCommand("SELECT BINARY_CHECKSUM(" + PrimaryKey + ") AS PrimaryKey, 0 AS LocalData, BINARY_CHECKSUM(" + LocalTableCols + ") AS ServerData FROM " + ServerDBTableName + "",
                    (!OpenConnectionPerRow ? DefaultServerConnection : new SqlConnection(ServerDBConnectionString).OpenGet()));
                }

                RS = CS.ExecuteReader();

                while (RS.Read())
                {
                    Server.Rows.Add(new Object[] {
                    Convert.ToInt64(RS["PrimaryKey"]), Convert.ToInt64(RS["LocalData"]), Convert.ToInt64(RS["ServerData"])
                });
                }

                RS.Close();

                // ________________ Get Local Table Data Summary ________________

                if (!TransferMode)
                {

                    CL = new SqlCommand("SELECT BINARY_CHECKSUM(" + PrimaryKey + ") AS PrimaryKey, BINARY_CHECKSUM(" + LocalTableCols + ") AS LocalData, 0 AS ServerData FROM " + LocalDBTableName + "",
                    (!OpenConnectionPerRow ? DefaultLocalConnection : new SqlConnection(LocalDBConnectionString).OpenGet()));

                    CL.Transaction = LocalTransactionForChanges;

                    RL = CL.ExecuteReader();

                    while (RL.Read())
                    {
                        Local.Rows.Add(new Object[] {
                        Convert.ToInt64(RL["PrimaryKey"]), Convert.ToInt64(RL["LocalData"]), Convert.ToInt64(RL["ServerData"])
                    });
                    }

                    RL.Close();
                }

                // ________________ Data Summary Tables Union ________________

                SyncTable.Merge(Local, true, MissingSchemaAction.Ignore);
                SyncTable.Merge(Server, true, MissingSchemaAction.Ignore);

                // ________________ Group by Primary Key ________________ 

                var MixedData = from Rows in SyncTable.AsEnumerable()
                                group Rows by Rows.Field<Int64>("PrimaryKey") into GroupedData
                                select new
                                {
                                    PrimaryKey = GroupedData.Key,
                                    LocalData = GroupedData.Sum(Rows => Rows.Field<Int64>("LocalData")),
                                    ServerData = GroupedData.Sum(Rows => Rows.Field<Int64>("ServerData"))
                                };

                // ________________ Start Sync ________________

                foreach (var Item in MixedData)
                {
                    Console.WriteLine(
                        "PrimaryKey:" + Item.PrimaryKey + " - " +
                        "LocalData:" + Item.LocalData + " - " +
                        "ServerData:" + Item.ServerData + " - "
                        + (Item.ServerData == 0 ? "Action: Delete in Local" : (Item.LocalData == 0 ? "Action: Insert in Local" : (Item.ServerData != Item.LocalData ? "Action: Update in Local" : "Values are up to date")))
                        );

                    if (Item.LocalData == Item.ServerData) { Console.WriteLine("Values up to date! Ignored."); continue; }

                    DataSet LocalData = new DataSet("LocalData");

                    SqlCommand LocalItemQuery = new SqlCommand("SELECT " + (LocalTableIgnoredCols.isUndefined() ? "*" : LocalTableCols) +
                    Functions.NewLine() + "FROM " + LocalDBTableName + " WHERE BINARY_CHECKSUM(" + PrimaryKey + ") = " + Item.PrimaryKey.ToString(),
                    (!OpenConnectionPerRow ? DefaultLocalConnection : new SqlConnection(LocalDBConnectionString).OpenGet()));

                    LocalItemQuery.Transaction = LocalTransactionForChanges;

                    SqlDataAdapter LocalAdapter = new SqlDataAdapter();
                    LocalAdapter.SelectCommand = LocalItemQuery;

                    SqlCommandBuilder LocalDML = new SqlCommandBuilder(LocalAdapter);
                    LocalAdapter.Fill(LocalData, LocalDBTableName);

                    LocalAdapter.DeleteCommand = LocalDML.GetDeleteCommand(false);
                    LocalAdapter.DeleteCommand.Transaction = LocalTransactionForChanges;
                    LocalAdapter.InsertCommand = LocalDML.GetInsertCommand(false);
                    LocalAdapter.InsertCommand.Transaction = LocalTransactionForChanges;

                    DataSet ServerData = new DataSet("ServerData");
                    SqlDataAdapter ServerAdapter = new SqlDataAdapter(new SqlCommand("SELECT " + (LocalTableIgnoredCols.isUndefined() ? "*" : LocalTableCols) +
                    Functions.NewLine() + "FROM " + ServerDBTableName + " WHERE BINARY_CHECKSUM(" + PrimaryKey + ") = " + Item.PrimaryKey.ToString(),
                    (!OpenConnectionPerRow ? DefaultServerConnection : new SqlConnection(ServerDBConnectionString).OpenGet())));
                    SqlCommandBuilder ServerDML = new SqlCommandBuilder(ServerAdapter);
                    ServerAdapter.Fill(ServerData, ServerDBTableName);

                    ServerAdapter.DeleteCommand = ServerDML.GetDeleteCommand(false);
                    //ServerAdapter.InsertCommand = ServerDML.GetInsertCommand(false);

                    if (TransferMode)
                    {
                        LocalData.Tables[LocalDBTableName].Rows.Add(
                            ServerData.Tables[ServerDBTableName].Rows[0].ItemArray);

                        // LocalData.AcceptChanges();
                        LocalAdapter.Update(LocalData, LocalDBTableName);

                        ServerAdapter.DeleteCommand.CommandText = "DELETE FROM " + ServerDBTableName + " WHERE BINARY_CHECKSUM(" + PrimaryKey + ") = " + Item.PrimaryKey.ToString();
                        ServerAdapter.DeleteCommand.Parameters.Clear();
                        ServerAdapter.DeleteCommand.ExecuteNonQuery(); // ServerAdapter.Update(LocalData, LocalDBTableName);                    
                    }
                    else
                    {
                        if (Item.ServerData == 0)
                        {
                            //LocalData.Tables[" + LocalDBTableName + "].Rows.RemoveAt(0);

                            // LocalData.AcceptChanges();

                            LocalAdapter.DeleteCommand.CommandText = "DELETE FROM " + LocalDBTableName + " WHERE BINARY_CHECKSUM(" + PrimaryKey + ") = " + Item.PrimaryKey.ToString();
                            LocalAdapter.DeleteCommand.Parameters.Clear();
                            LocalAdapter.DeleteCommand.ExecuteNonQuery(); // LocalAdapter.Update(LocalData, LocalDBTableName);

                            // Local.Rows.Remove(Local.Rows.Find(new Object[] { Item.PrimaryKey }));
                        }
                        else if (Item.LocalData == 0)
                        {
                            LocalData.Tables[LocalDBTableName].Rows.Add(
                                ServerData.Tables[ServerDBTableName].Rows[0].ItemArray);

                            // LocalData.AcceptChanges();
                            LocalAdapter.Update(LocalData, LocalDBTableName);
                        }
                        else if (Item.ServerData != Item.LocalData)
                        {
                            //LocalData.Tables[LocalDBTableName].Rows.RemoveAt(0);
                            LocalAdapter.DeleteCommand.CommandText = "DELETE FROM " + LocalDBTableName + " WHERE BINARY_CHECKSUM(" + PrimaryKey + ") = " + Item.PrimaryKey.ToString();
                            LocalAdapter.DeleteCommand.Parameters.Clear();
                            LocalAdapter.DeleteCommand.ExecuteNonQuery(); // LocalAdapter.Update(LocalData, LocalDBTableName);

                            LocalData.Tables[LocalDBTableName].Rows.Add(
                                ServerData.Tables[LocalDBTableName].Rows[0].ItemArray);
                            LocalAdapter.Update(LocalData, LocalDBTableName);

                            // LocalData.AcceptChanges();
                            // LocalAdapter.Update(LocalData, LocalDBTableName);

                        }
                    }

                }

                Boolean Success = true;

                return Success;

            }
            catch (Exception e)
            {
                Boolean Fail = false;

                Console.WriteLine(Functions.NewLine() + e.Message);
                return Fail;
            }

        }

        public static Boolean SyncTable(SqlConnection LocalDBConnection, SqlConnection ServerDBConnection,
        String LocalDBTableName, String ServerDBTableName, SqlTransaction LocalTransactionForChanges = null,
        Boolean TransferMode = false, SqlTransaction RemoteTransactionForTransfer = null, SqlCommand ServerTableDataCriteria = null, String[] IgnoredFields = null)
        {

            try
            {

                DataTable Local = new DataTable("Local");
                DataTable Server = new DataTable("Server");
                DataTable SyncTable = new DataTable("Sync");

                SyncTable.Columns.AddRange(
                    new DataColumn[]{
                    new DataColumn("PrimaryKey", typeof(String)),
                    new DataColumn("LocalData", typeof(String)),
                    new DataColumn("ServerData", typeof(String))
                });

                Local.Columns.AddRange(
                    new DataColumn[]{
                    new DataColumn("PrimaryKey", typeof(String)),
                    new DataColumn("LocalData", typeof(String)),
                    new DataColumn("ServerData", typeof(String))
                });

                Local.PrimaryKey = new DataColumn[] { Local.Columns["PrimaryKey"] };

                Server.Columns.AddRange(
                    new DataColumn[]{
                    new DataColumn("PrimaryKey", typeof(String)),
                    new DataColumn("LocalData", typeof(String)),
                    new DataColumn("ServerData", typeof(String))
                });

                Server.PrimaryKey = new DataColumn[] { Server.Columns["PrimaryKey"] };

                // ________________ Get Sync Tables Columns ________________                

                String LocalTableIgnoredCols = String.Empty;
                String LocalTableIgnoredValues = String.Empty;

                if (!IgnoredFields.IsNullOrEmpty())
                {
                    Array.ForEach(IgnoredFields, (Item) => { LocalTableIgnoredCols += ", " + Item; LocalTableIgnoredValues += ", " + "'" + Item + "'"; });
                    LocalTableIgnoredCols = LocalTableIgnoredCols.Substring(2);
                    LocalTableIgnoredValues = LocalTableIgnoredValues.Substring(2);
                }

                SqlDataReader RL;

                SqlCommand CL = new SqlCommand(
                    "SELECT * FROM SYS.all_columns WHERE" + Functions.NewLine() +
                    "is_identity = 0" + Functions.NewLine() +
                    "AND OBJECT_ID IN (" + Functions.NewLine() +
                    "SELECT OBJECT_ID FROM SYS.all_objects WHERE is_ms_shipped = 0" + Functions.NewLine() +
                    "AND TYPE = 'U' AND NAME = '" + LocalDBTableName + "')" +
                    (LocalTableIgnoredCols.isUndefined() ? String.Empty
                    : Functions.NewLine() + "AND Name NOT IN (" + LocalTableIgnoredValues + ")"),
                    LocalDBConnection);

                CL.Transaction = LocalTransactionForChanges;

                RL = CL.ExecuteReader();

                String LocalTableCols = String.Empty;

                while (RL.Read())
                {
                    LocalTableCols += ", " + RL["Name"].ToString();
                }

                RL.Close();

                LocalTableCols = LocalTableCols.Substring(2); // Ignore the first ", "

                // ________________ Get Sync Tables Primary Key Columns ________________

                String PrimaryKey = String.Empty;

                CL = new SqlCommand(
                    "SELECT *" + Functions.NewLine() +
                    "FROM INFORMATION_SCHEMA.KEY_COLUMN_USAGE" + Functions.NewLine() +
                    "WHERE OBJECTPROPERTY(OBJECT_ID(CONSTRAINT_NAME), 'IsPrimaryKey') = 1" + Functions.NewLine() +
                    "AND TABLE_NAME = '" + LocalDBTableName + "'",
                    LocalDBConnection);

                CL.Transaction = LocalTransactionForChanges;

                RL = CL.ExecuteReader();

                while (RL.Read())
                {
                    PrimaryKey += ", " + RL["Column_Name"].ToString();
                }

                RL.Close();

                if (PrimaryKey.isUndefined()) { return false; }

                PrimaryKey = PrimaryKey.Substring(2); // Ignore the first ", "

                // ________________ Get Server Table Data Summary ________________

                SqlDataReader RS;

                SqlCommand CS = null;

                Boolean CriteriaExists = (ServerTableDataCriteria != null);
                CriteriaExists = (CriteriaExists ? !ServerTableDataCriteria.CommandText.isUndefined() : false);

                if (CriteriaExists)
                {
                    CS = new SqlCommand("SELECT BINARY_CHECKSUM(" + PrimaryKey + ") AS PrimaryKey, " +
                    "0 AS LocalData, BINARY_CHECKSUM(" + LocalTableCols + ") AS ServerData FROM " + ServerDBTableName +
                    Functions.NewLine() + ServerTableDataCriteria.CommandText, // WHERE...
                    ServerDBConnection);

                    ServerTableDataCriteria.Parameters.OfType<SqlParameter>().ToList<SqlParameter>().ForEach((Param) => CS.Parameters.Add(new SqlParameter(Param.ParameterName, Param.Value)));

                    ServerTableDataCriteria.Dispose();
                }
                else
                {
                    CS = new SqlCommand("SELECT BINARY_CHECKSUM(" + PrimaryKey + ") AS PrimaryKey, 0 AS LocalData, BINARY_CHECKSUM(" + LocalTableCols + ") AS ServerData FROM " + ServerDBTableName + "",
                    ServerDBConnection);
                }

                CS.Transaction = RemoteTransactionForTransfer;

                RS = CS.ExecuteReader();

                while (RS.Read())
                {
                    Server.Rows.Add(new Object[] {
                    Convert.ToInt64(RS["PrimaryKey"]), Convert.ToInt64(RS["LocalData"]), Convert.ToInt64(RS["ServerData"])
                });
                }

                RS.Close();

                // ________________ Get Local Table Data Summary ________________

                if (!TransferMode)
                {

                    CL = new SqlCommand("SELECT BINARY_CHECKSUM(" + PrimaryKey + ") AS PrimaryKey, BINARY_CHECKSUM(" + LocalTableCols + ") AS LocalData, 0 AS ServerData FROM " + LocalDBTableName + "",
                    LocalDBConnection);

                    CL.Transaction = LocalTransactionForChanges;

                    RL = CL.ExecuteReader();

                    while (RL.Read())
                    {
                        Local.Rows.Add(new Object[] {
                        Convert.ToInt64(RL["PrimaryKey"]), Convert.ToInt64(RL["LocalData"]), Convert.ToInt64(RL["ServerData"])
                    });
                    }

                    RL.Close();

                }

                // ________________ Data Summary Tables Union ________________

                SyncTable.Merge(Local, true, MissingSchemaAction.Ignore);
                SyncTable.Merge(Server, true, MissingSchemaAction.Ignore);

                // ________________ Group by Primary Key ________________ 

                var MixedData = from Rows in SyncTable.AsEnumerable()
                                group Rows by Rows.Field<Int64>("PrimaryKey") into GroupedData
                                select new
                                {
                                    PrimaryKey = GroupedData.Key,
                                    LocalData = GroupedData.Sum(Rows => Rows.Field<Int64>("LocalData")),
                                    ServerData = GroupedData.Sum(Rows => Rows.Field<Int64>("ServerData"))
                                };

                // ________________ Start Sync ________________

                foreach (var Item in MixedData)
                {
                    Console.WriteLine(
                        "PrimaryKey:" + Item.PrimaryKey + " - " +
                        "LocalData:" + Item.LocalData + " - " +
                        "ServerData:" + Item.ServerData + " - "
                        + (Item.ServerData == 0 ? "Action: Delete in Local" : (Item.LocalData == 0 ? "Action: Insert in Local" : (Item.ServerData != Item.LocalData ? "Action: Update in Local" : "Values are up to date")))
                        );

                    if (Item.LocalData == Item.ServerData) { Console.WriteLine("Values up to date! Ignored."); continue; }

                    DataSet LocalData = new DataSet("LocalData");

                    SqlCommand LocalItemQuery = new SqlCommand("SELECT " + (LocalTableIgnoredCols.isUndefined() ? "*" : LocalTableCols) +
                    Functions.NewLine() + "FROM " + LocalDBTableName + " WHERE BINARY_CHECKSUM(" + PrimaryKey + ") = " + Item.PrimaryKey.ToString(),
                    LocalDBConnection);

                    LocalItemQuery.Transaction = LocalTransactionForChanges;

                    SqlDataAdapter LocalAdapter = new SqlDataAdapter();
                    LocalAdapter.SelectCommand = LocalItemQuery;

                    SqlCommandBuilder LocalDML = new SqlCommandBuilder(LocalAdapter);
                    LocalAdapter.Fill(LocalData, LocalDBTableName);

                    LocalAdapter.DeleteCommand = LocalDML.GetDeleteCommand(false);
                    LocalAdapter.DeleteCommand.Transaction = LocalTransactionForChanges;
                    LocalAdapter.InsertCommand = LocalDML.GetInsertCommand(false);
                    LocalAdapter.InsertCommand.Transaction = LocalTransactionForChanges;

                    DataSet ServerData = new DataSet("ServerData");
                    SqlDataAdapter ServerAdapter = new SqlDataAdapter(new SqlCommand("SELECT " + (LocalTableIgnoredCols.isUndefined() ? "*" : LocalTableCols) +
                    Functions.NewLine() + "FROM " + ServerDBTableName + " WHERE BINARY_CHECKSUM(" + PrimaryKey + ") = " + Item.PrimaryKey.ToString(),
                    ServerDBConnection));
                    SqlCommandBuilder ServerDML = new SqlCommandBuilder(ServerAdapter);
                    ServerAdapter.Fill(ServerData, ServerDBTableName);

                    ServerAdapter.DeleteCommand = ServerDML.GetDeleteCommand(false);
                    ServerAdapter.DeleteCommand.Transaction = RemoteTransactionForTransfer;
                    //ServerAdapter.InsertCommand = ServerDML.GetInsertCommand(false);

                    if (TransferMode)
                    {
                        LocalData.Tables[LocalDBTableName].Rows.Add(
                            ServerData.Tables[ServerDBTableName].Rows[0].ItemArray);

                        // LocalData.AcceptChanges();
                        LocalAdapter.Update(LocalData, LocalDBTableName);

                        ServerAdapter.DeleteCommand.CommandText = "DELETE FROM " + ServerDBTableName + " WHERE BINARY_CHECKSUM(" + PrimaryKey + ") = " + Item.PrimaryKey.ToString();
                        ServerAdapter.DeleteCommand.Parameters.Clear();
                        ServerAdapter.DeleteCommand.ExecuteNonQuery(); // ServerAdapter.Update(LocalData, LocalDBTableName);                    
                    }
                    else
                    {
                        if (Item.ServerData == 0)
                        {
                            //LocalData.Tables[" + LocalDBTableName + "].Rows.RemoveAt(0);

                            // LocalData.AcceptChanges();

                            LocalAdapter.DeleteCommand.CommandText = "DELETE FROM " + LocalDBTableName + " WHERE BINARY_CHECKSUM(" + PrimaryKey + ") = " + Item.PrimaryKey.ToString();
                            LocalAdapter.DeleteCommand.Parameters.Clear();
                            LocalAdapter.DeleteCommand.ExecuteNonQuery(); // LocalAdapter.Update(LocalData, LocalDBTableName);

                            // Local.Rows.Remove(Local.Rows.Find(new Object[] { Item.PrimaryKey }));
                        }
                        else if (Item.LocalData == 0)
                        {
                            LocalData.Tables[LocalDBTableName].Rows.Add(
                                ServerData.Tables[ServerDBTableName].Rows[0].ItemArray);

                            // LocalData.AcceptChanges();
                            LocalAdapter.Update(LocalData, LocalDBTableName);
                        }
                        else if (Item.ServerData != Item.LocalData)
                        {
                            //LocalData.Tables[LocalDBTableName].Rows.RemoveAt(0);
                            LocalAdapter.DeleteCommand.CommandText = "DELETE FROM " + LocalDBTableName + " WHERE BINARY_CHECKSUM(" + PrimaryKey + ") = " + Item.PrimaryKey.ToString();
                            LocalAdapter.DeleteCommand.Parameters.Clear();
                            LocalAdapter.DeleteCommand.ExecuteNonQuery(); // LocalAdapter.Update(LocalData, LocalDBTableName);

                            LocalData.Tables[LocalDBTableName].Rows.Add(
                                ServerData.Tables[LocalDBTableName].Rows[0].ItemArray);
                            LocalAdapter.Update(LocalData, LocalDBTableName);

                            // LocalData.AcceptChanges();
                            // LocalAdapter.Update(LocalData, LocalDBTableName);

                        }
                    }

                }

                Boolean Success = true;

                return Success;

            }
            catch (Exception e)
            {
                Boolean Fail = false;

                Console.WriteLine(Functions.NewLine() + e.Message);
                return Fail;
            }

        }       

    }
}
