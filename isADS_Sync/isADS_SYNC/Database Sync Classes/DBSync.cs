﻿using isADS_SYNC.Clases;
using Microsoft.Synchronization;
using Microsoft.Synchronization.Data;
using Microsoft.Synchronization.Data.SqlServer;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Linq.Expressions;

namespace isADS_SYNC
{
    class DBSync
    {       

        public enum HashAlgorythims_Strength {
            LOW_MD2,
            LOW_MD4,
            MEDIUM_MD5,
            MEDIUM_SHA0,
            HIGH_SHA1,
            HIGH_SHA256,
            EXTREME_SHA512
        }

        /// <summary>
        /// Read the Summary from the SyncTable function to get a general overview of this function.
        /// <para> </para>
        /// The difference is, that this function doesn't exactly sync rows, it actually inserts the data into the 
        /// <para></para>
        /// local table, and then removes it from the server side table. There is another optional parameter for the 
        /// <para></para>
        /// server side transaction, as in this function, both tables data are affected.
        /// <para></para>
        /// Also, an optional parameter to Truncate the local table first, before the inserts, BUT,
        /// <para></para>
        /// be careful with this and avoid information loss...
        /// <para> </para>
        /// Basically, this function works as a Bulk Transfer.     
        /// </summary>
        public static Boolean TransferTable(SqlConnection LocalDBConnection, SqlConnection ServerDBConnection, 
        String LocalDBTableName, String ServerDBTableName, 
        SqlTransaction LocalTransactionForInserts = null, SqlTransaction ServerTransactionForDeletes = null,    
        Boolean TruncateLocalFirst = false, SqlCommand ServerTableDataCriteria = null, String[] IgnoredFields = null,
        HashAlgorythims_Strength RowIdentityCollisionError_PreventionLevel = HashAlgorythims_Strength.HIGH_SHA1)
        {
            
            String HashAlgorythim;

            switch (RowIdentityCollisionError_PreventionLevel)
            {
                case HashAlgorythims_Strength.LOW_MD2:
                    { HashAlgorythim = "MD2"; break; }
                case HashAlgorythims_Strength.LOW_MD4:
                    { HashAlgorythim = "MD4"; break; }
                case HashAlgorythims_Strength.MEDIUM_MD5:
                    { HashAlgorythim = "MD5"; break; }
                case HashAlgorythims_Strength.MEDIUM_SHA0:
                    { HashAlgorythim = "SHA"; break; }
                case HashAlgorythims_Strength.HIGH_SHA1:
                    { HashAlgorythim = "SHA1"; break; }
                case HashAlgorythims_Strength.HIGH_SHA256:
                    { HashAlgorythim = "SHA2_256"; break; }
                case HashAlgorythims_Strength.EXTREME_SHA512:
                    { HashAlgorythim = "SHA2_512"; break; }
                default:
                    { HashAlgorythim = "SHA1"; break; }
            }

            SqlDataReader RL = null, RS = null;

            try
            {

                DataTable Server = new DataTable("Server");

                Server.Columns.AddRange(
                    new DataColumn[]{
                    new DataColumn("PrimaryKey", typeof(String))
                });

                Server.PrimaryKey = new DataColumn[] { Server.Columns["PrimaryKey"] };

                // ________________ Get Sync Tables Columns And Primary Keys ________________

                String LocalTableIgnoredCols = String.Empty;
                String LocalTableIgnoredValues = String.Empty;
                String PrimaryKey = String.Empty;

                if (!IgnoredFields.IsNullOrEmpty())
                {
                    Array.ForEach(IgnoredFields, (Item) => { LocalTableIgnoredCols += ", " + Item; LocalTableIgnoredValues += ", " + "'" + Item + "'"; });
                    LocalTableIgnoredCols = LocalTableIgnoredCols.Substring(2);
                    LocalTableIgnoredValues = LocalTableIgnoredValues.Substring(2);
                }                
                
                 SqlCommand CL = new SqlCommand(
                "SELECT ColsInfo.*, ColsInfo.is_identity AS isIdentity," + Functions.NewLine() +
                "isPrimaryKey = CASE WHEN KeysInfo.COLUMN_NAME IS NULL THEN 0 ELSE 1 END," + Functions.NewLine() +
                "PrimaryKeyOrdinalPosition = KeysInfo.ORDINAL_POSITION" + Functions.NewLine() +
                "FROM SYS.all_columns ColsInfo LEFT JOIN INFORMATION_SCHEMA.KEY_COLUMN_USAGE KeysInfo" + Functions.NewLine() +
                "ON"  + Functions.NewLine() +
                "ColsInfo.Name = KeysInfo.COLUMN_NAME" + Functions.NewLine() +
                "AND OBJECTPROPERTY(OBJECT_ID(KeysInfo.CONSTRAINT_NAME), 'IsPrimaryKey') = 1" + Functions.NewLine() +
                "AND KeysInfo.TABLE_NAME = '" + LocalDBTableName + "'" + Functions.NewLine() +
                "WHERE" + Functions.NewLine() +
                "OBJECT_ID IN (" + Functions.NewLine() +
                "SELECT OBJECT_ID FROM SYS.all_objects WHERE is_ms_shipped = 0" + Functions.NewLine() + 
                "AND TYPE = 'U' AND NAME = '" + LocalDBTableName + "'"  + Functions.NewLine() + 
                ")" + 
                (LocalTableIgnoredCols.isUndefined() ? String.Empty
                : Functions.NewLine() + "AND Name NOT IN (" + LocalTableIgnoredValues + ")"),
                LocalDBConnection);

                CL.Transaction = LocalTransactionForInserts;

                RL = CL.ExecuteReader();

                String LocalTableCols = String.Empty;

                while (RL.Read())
                {
                    if (Convert.ToBoolean(RL["isPrimaryKey"]))                    
                        PrimaryKey += ", " + RL["Name"].ToString(); // Getting Primary Key Columns.
                    
                    if (Convert.ToBoolean(RL["isIdentity"]) && !Convert.ToBoolean(RL["isPrimaryKey"])) 
                        continue; // Avoiding Identity Columns but only THOSE that are not also a Primary Key.                

                    LocalTableCols += ", " + RL["Name"].ToString();
                }

                RL.Close();

                if (PrimaryKey.isUndefined()) { return false; }

                LocalTableCols = LocalTableCols.Substring(2); // Ignore the first ", "                                

                PrimaryKey = PrimaryKey.Substring(2); // Ignore the first ", "

                // ________________ Get Server Table Data Summary ________________

                SqlCommand CS = null;

                Boolean CriteriaExists = (ServerTableDataCriteria != null);
                CriteriaExists = (CriteriaExists ? !ServerTableDataCriteria.CommandText.isUndefined() : false);

                if (CriteriaExists)
                {
                    CS = new SqlCommand("SELECT CONVERT(VARCHAR(MAX), HASHBYTES('" + HashAlgorythim + "', Row.Primarykey), 1) AS PrimaryKey, " +
                    "'' AS LocalData, CONVERT(VARCHAR(MAX), HASHBYTES('" + HashAlgorythim + "', Row.Contents), 1) AS ServerData FROM " + ServerDBTableName + Functions.NewLine() +
                    "CROSS APPLY (" + Functions.NewLine() +
                    "SELECT PrimaryKey = CAST((SELECT " + PrimaryKey + " FROM (SELECT 1) AS Row (y) FOR XML AUTO, BINARY BASE64) AS VARCHAR(MAX))" + Functions.NewLine() +
                    ", Contents = CAST((SELECT " + LocalTableCols + " FROM (SELECT 1) AS Row (y) FOR XML AUTO, BINARY BASE64) AS VARCHAR(MAX))" + Functions.NewLine() +
                    ") AS Row"
                    + Functions.NewLine() + ServerTableDataCriteria.CommandText, // WHERE...
                    ServerDBConnection);

                    ServerTableDataCriteria.Parameters.OfType<SqlParameter>().ToList<SqlParameter>().ForEach((Param) => CS.Parameters.Add(new SqlParameter(Param.ParameterName, Param.Value)));

                    ServerTableDataCriteria.Dispose();
                }
                else
                {
                    CS = new SqlCommand("SELECT CONVERT(VARCHAR(MAX), HASHBYTES('" + HashAlgorythim + "', Row.Primarykey), 1) AS PrimaryKey, " +
                    "'' AS LocalData, CONVERT(VARCHAR(MAX), HASHBYTES('" + HashAlgorythim + "', Row.Contents), 1) AS ServerData FROM " + ServerDBTableName + Functions.NewLine() +
                    "CROSS APPLY (" + Functions.NewLine() +
                    "SELECT PrimaryKey = CAST((SELECT " + PrimaryKey + " FROM (SELECT 1) AS Row (y) FOR XML AUTO, BINARY BASE64) AS VARCHAR(MAX))," + Functions.NewLine() +
                    "Contents = CAST((SELECT " + LocalTableCols + " FROM (SELECT 1) AS Row (y) FOR XML AUTO, BINARY BASE64) AS VARCHAR(MAX))" + Functions.NewLine() +
                    ") AS Row",
                    ServerDBConnection);
                }          

                CS.Transaction = ServerTransactionForDeletes;                

                RS = CS.ExecuteReader();

                while (RS.Read())
                {
                    Server.Rows.Add(new Object[] {
                    Convert.ToString(RS["PrimaryKey"])
                    });
                }

                RS.Close();

                // ________________ Start Sync ________________

                DataSet LocalData = new DataSet("LocalData");

                SqlCommand LocalItemQuery = new SqlCommand("SELECT " + LocalTableCols + " FROM " + LocalDBTableName + " WHERE 1 = 2",
                LocalDBConnection);                

                LocalItemQuery.Transaction = LocalTransactionForInserts;
                
                SqlDataAdapter LocalAdapter = new SqlDataAdapter();
                LocalAdapter.SelectCommand = LocalItemQuery;

                SqlCommandBuilder LocalDML = new SqlCommandBuilder(LocalAdapter);
                LocalAdapter.Fill(LocalData, LocalDBTableName);

                LocalAdapter.DeleteCommand = LocalDML.GetDeleteCommand(false);
                LocalAdapter.InsertCommand = LocalDML.GetInsertCommand(false);

                foreach (DataRow Item in Server.Rows)
                {

                    DataSet ServerData = new DataSet("ServerData");

                    SqlCommand ServerItemQuery = new SqlCommand("SELECT " + LocalTableCols +
                    Functions.NewLine() + "FROM " + ServerDBTableName + Functions.NewLine() +
                    "CROSS APPLY (" + Functions.NewLine() +
                    "SELECT PrimaryKey = CAST((SELECT " + PrimaryKey + " FROM (SELECT 1) AS Row (y) FOR XML AUTO, BINARY BASE64) AS VARCHAR(MAX))" + Functions.NewLine() +
                    ") AS Row" + Functions.NewLine() +
                    "WHERE CONVERT(VARCHAR(MAX), HASHBYTES('" + HashAlgorythim + "', Row.PrimaryKey), 1) = '" + Item["PrimaryKey"].ToString() + "' COLLATE Latin1_General_CS_AS",
                    ServerDBConnection);

                    ServerItemQuery.Transaction = ServerTransactionForDeletes;

                    SqlDataAdapter ServerAdapter = new SqlDataAdapter(ServerItemQuery);
                    SqlCommandBuilder ServerDML = new SqlCommandBuilder(ServerAdapter);
                    ServerAdapter.Fill(ServerData, ServerDBTableName);

                    ServerAdapter.DeleteCommand = ServerDML.GetDeleteCommand(false);
                    //ServerAdapter.InsertCommand = ServerDML.GetInsertCommand(false);

                    LocalData.Tables[LocalDBTableName].Rows.Add(
                        ServerData.Tables[ServerDBTableName].Rows[0].ItemArray);

                    Console.WriteLine("DELETING Primary Key " + Item["PrimaryKey"].ToString());

                    ServerAdapter.DeleteCommand.CommandText = "DELETE " + ServerDBTableName + " FROM " + ServerDBTableName + Functions.NewLine() +
                    "CROSS APPLY (" + Functions.NewLine() +
                    "SELECT PrimaryKey = CAST((SELECT " + PrimaryKey + " FROM (SELECT 1) AS Row (y) FOR XML AUTO, BINARY BASE64) AS VARCHAR(MAX))" + Functions.NewLine() +
                    ") AS Row" + Functions.NewLine() +
                    "WHERE CONVERT(VARCHAR(MAX), HASHBYTES('" + HashAlgorythim + "', Row.PrimaryKey), 1) = '" + Item["PrimaryKey"].ToString() + "' COLLATE Latin1_General_CS_AS";
                                                                        
                    ServerAdapter.DeleteCommand.Parameters.Clear();
                    ServerAdapter.DeleteCommand.ExecuteNonQuery();

                }

                if (TruncateLocalFirst)
                {
                    Console.WriteLine(Functions.NewLine() + "Truncating Table before bulk inserts.");

                    LocalAdapter.DeleteCommand.CommandText = "Truncate Table " + LocalDBTableName;
                    LocalAdapter.DeleteCommand.Parameters.Clear();
                    LocalAdapter.DeleteCommand.ExecuteNonQuery();
                }

                Console.WriteLine(Functions.NewLine() + "Submitting Bulk Inserts into Local...");
                // LocalData.AcceptChanges();
                
                try { 
                    // Console.WriteLine(LocalAdapter.UpdateBatchSize);
                    LocalAdapter.UpdateBatchSize =  (Server.Rows.Count < 4000 ? 1000 : Convert.ToInt32(Server.Rows.Count * 0.25));
                    Console.WriteLine(Server.Rows.Count.ToString() + " - " + LocalAdapter.UpdateBatchSize.ToString());
                } catch (Exception Any) { Console.WriteLine(Any.Message); }

                LocalAdapter.Update(LocalData, LocalDBTableName);
                
                Console.WriteLine(Functions.NewLine() + "Transfer Complete.");

                Boolean Success = true;

                return Success;
                
            }
            catch (Exception e)
            {

                if ((RL != null ? !RL.IsClosed : false))
                    RL.Close();
                if ((RS != null ? !RS.IsClosed : false))
                    RS.Close();

                Boolean Fail = false;

                Console.WriteLine(Functions.NewLine() + "Transfer Failed:" + Functions.NewLine() + e.Message);
                return Fail;
            }

        }

        /// <summary>
        /// <para> </para>
        /// This function Synchronizes Data from SQL Server Tables with several options. The whole
        /// <para></para>
        /// sync mechanism is based in the Local Table Cols. Any Extra columns in the Server Side Table 
        /// <para></para>
        /// are ignored. 
        /// <para> </para>
        /// There is also a Parameter to define Local Table Ignored Cols: Columns which you would need to define 
        /// <para></para>
        /// to avoid syncing columns that only exist in the local table.
        /// <para> </para>
        /// The Sync occurs for each applicable row based only on the contents of the Non-Ignored Columns Data, which
        /// <para></para>
        /// contain ANY differences, because the comparison is made through Row Contents Hashes.
        /// <para> </para>
        /// As Required Params, provide Local and Server Connections and their Table Names, and as optionals, 
        /// <para></para>
        /// you can provide both SQLTransactions, a String Array for LocalIgnoredCols, a enum value Hashing Algorythim,
        /// <para></para>
        /// a Boolean to determine wether you persist the local Data (Sync updates non-ignored fields) or not 
        /// <para></para>
        /// (Local Rows get deleted and reinserted, but LocalIgnoredCols reset to their Default Value),
        /// <para></para>
        /// a Boolean to determine if Local Deletes are handled by the application or the server should clean those rows,
        /// <para></para>
        /// and a SQLCommand which contains in it's CommandText Property, a WHERE clause as part of the query 
        /// <para></para>
        /// in case there is a need to filter some data from the Table, and of course, this Command should already 
        /// <para></para>
        /// contain any required params in it's own SQLParameterCollection Property.         
        /// </summary>               

        public static Boolean SyncTable(SqlConnection LocalDBConnection, SqlConnection ServerDBConnection,
        String LocalDBTableName, String ServerDBTableName, SqlTransaction LocalTransactionForChanges = null,
        Boolean TransferMode = false, SqlTransaction RemoteTransactionForTransfer = null, 
        SqlCommand ServerTableDataCriteria = null, Boolean PersistLocalInfo = false, Boolean DeleteLocalRows = true, 
        String[] IgnoredFields = null, HashAlgorythims_Strength RowIdentityCollisionError_PreventionLevel = HashAlgorythims_Strength.HIGH_SHA1)
        {
            String HashAlgorythim;

            switch (RowIdentityCollisionError_PreventionLevel) {
                case HashAlgorythims_Strength.LOW_MD2:
                    { HashAlgorythim = "MD2"; break; }
                case HashAlgorythims_Strength.LOW_MD4:
                    { HashAlgorythim = "MD4"; break; }
                case HashAlgorythims_Strength.MEDIUM_MD5:
                    { HashAlgorythim = "MD5"; break; }
                case HashAlgorythims_Strength.MEDIUM_SHA0:
                    { HashAlgorythim = "SHA"; break; }
                case HashAlgorythims_Strength.HIGH_SHA1:
                    { HashAlgorythim = "SHA1"; break; }
                case HashAlgorythims_Strength.HIGH_SHA256:
                    { HashAlgorythim = "SHA2_256"; break; }
                case HashAlgorythims_Strength.EXTREME_SHA512:
                    { HashAlgorythim = "SHA2_512"; break; }
                default:
                    { HashAlgorythim = "SHA1"; break; }
            }            
            
            SqlDataReader RL = null, RS = null;

            try
            {

                DataTable Local = new DataTable("Local");
                DataTable Server = new DataTable("Server");
                DataTable SyncTable = new DataTable("Sync");                                

                SyncTable.Columns.AddRange(
                                new DataColumn[]{
                    new DataColumn("PrimaryKey", typeof(String)),
                    new DataColumn("LocalData", typeof(String)),
                    new DataColumn("ServerData", typeof(String))
                });

                Local.Columns.AddRange(
                    new DataColumn[]{
                    new DataColumn("PrimaryKey", typeof(String)),
                    new DataColumn("LocalData", typeof(String)),
                    new DataColumn("ServerData", typeof(String))
                });

                Local.PrimaryKey = new DataColumn[] { Local.Columns["PrimaryKey"] };

                Server.Columns.AddRange(
                    new DataColumn[]{
                    new DataColumn("PrimaryKey", typeof(String)),
                    new DataColumn("LocalData", typeof(String)),
                    new DataColumn("ServerData", typeof(String))
                });

                Server.PrimaryKey = new DataColumn[] { Server.Columns["PrimaryKey"] };

                // ________________ Get Sync Tables Columns ________________                

                String LocalTableIgnoredCols = String.Empty;
                String LocalTableIgnoredValues = String.Empty;
              
                if (!IgnoredFields.IsNullOrEmpty()){
                    Array.ForEach(IgnoredFields, (Item) => { LocalTableIgnoredCols += ", " + Item; LocalTableIgnoredValues += ", " + "'" + Item + "'"; });
                    LocalTableIgnoredCols = LocalTableIgnoredCols.Substring(2);
                    LocalTableIgnoredValues = LocalTableIgnoredValues.Substring(2);
                }                

                SqlCommand CL = new SqlCommand(
                    "SELECT * FROM SYS.all_columns WHERE" + Functions.NewLine() +
                    "is_identity = 0" + Functions.NewLine() +
                    "AND OBJECT_ID IN (" + Functions.NewLine() +
                    "SELECT OBJECT_ID FROM SYS.all_objects WHERE is_ms_shipped = 0" + Functions.NewLine() + 
                    "AND TYPE = 'U' AND NAME = '" + LocalDBTableName + "')" + 
                    (LocalTableIgnoredCols.isUndefined() ? String.Empty
                    : Functions.NewLine() + "AND Name NOT IN (" + LocalTableIgnoredValues + ")"),
                    LocalDBConnection);

                CL.Transaction = LocalTransactionForChanges;

                RL = CL.ExecuteReader();

                String LocalTableCols = String.Empty;

                while (RL.Read())
                {
                    LocalTableCols += ", " + RL["Name"].ToString();
                }

                RL.Close();

                LocalTableCols = LocalTableCols.Substring(2); // Ignore the first ", "

                // ________________ Get Sync Tables Primary Key Columns ________________

                String PrimaryKey = String.Empty;

                CL = new SqlCommand(
                    "SELECT *" + Functions.NewLine() +
                    "FROM INFORMATION_SCHEMA.KEY_COLUMN_USAGE" + Functions.NewLine() +
                    "WHERE OBJECTPROPERTY(OBJECT_ID(CONSTRAINT_NAME), 'IsPrimaryKey') = 1" + Functions.NewLine() +
                    "AND TABLE_NAME = '" + LocalDBTableName + "'",
                    LocalDBConnection);

                CL.Transaction = LocalTransactionForChanges;

                RL = CL.ExecuteReader();

                while (RL.Read())
                {
                    PrimaryKey += ", " + RL["Column_Name"].ToString();
                }

                RL.Close();

                if (PrimaryKey.isUndefined()) { return false; }

                PrimaryKey = PrimaryKey.Substring(2); // Ignore the first ", "

                // ________________ Get Server Table Data Summary ________________                

                SqlCommand CS = null;

                Boolean CriteriaExists = (ServerTableDataCriteria != null);
                CriteriaExists = (CriteriaExists ? !ServerTableDataCriteria.CommandText.isUndefined() : false);

                if (CriteriaExists)
                {
                    CS = new SqlCommand("SELECT CONVERT(VARCHAR(MAX), HASHBYTES('" + HashAlgorythim + "', Row.Primarykey), 1) AS PrimaryKey, " +
                    "'' AS LocalData, CONVERT(VARCHAR(MAX), HASHBYTES('" + HashAlgorythim + "', Row.Contents), 1) AS ServerData FROM " + ServerDBTableName + Functions.NewLine() + 
                    "CROSS APPLY (" + Functions.NewLine() + 
                    "SELECT PrimaryKey = CAST((SELECT " + PrimaryKey + " FROM (SELECT 1) AS Row (y) FOR XML AUTO, BINARY BASE64) AS VARCHAR(MAX))" + Functions.NewLine() +
                    ", Contents = CAST((SELECT " + LocalTableCols + " FROM (SELECT 1) AS Row (y) FOR XML AUTO, BINARY BASE64) AS VARCHAR(MAX))" + Functions.NewLine() +
                    ") AS Row" 
                    + Functions.NewLine() + ServerTableDataCriteria.CommandText, // WHERE...
                    ServerDBConnection);

                    ServerTableDataCriteria.Parameters.OfType<SqlParameter>().ToList<SqlParameter>().ForEach((Param) => CS.Parameters.Add(new SqlParameter(Param.ParameterName, Param.Value)));

                    ServerTableDataCriteria.Dispose();
                }
                else
                {
                    CS = new SqlCommand("SELECT CONVERT(VARCHAR(MAX), HASHBYTES('" + HashAlgorythim + "', Row.Primarykey), 1) AS PrimaryKey, " +
                    "'' AS LocalData, CONVERT(VARCHAR(MAX), HASHBYTES('" + HashAlgorythim + "', Row.Contents), 1) AS ServerData FROM " + ServerDBTableName + Functions.NewLine() +
                    "CROSS APPLY (" + Functions.NewLine() +
                    "SELECT PrimaryKey = CAST((SELECT " + PrimaryKey + " FROM (SELECT 1) AS Row (y) FOR XML AUTO, BINARY BASE64) AS VARCHAR(MAX))," + Functions.NewLine() +
                    "Contents = CAST((SELECT " + LocalTableCols + " FROM (SELECT 1) AS Row (y) FOR XML AUTO, BINARY BASE64) AS VARCHAR(MAX))" + Functions.NewLine() +
                    ") AS Row",
                    ServerDBConnection);
                }

                CS.Transaction = RemoteTransactionForTransfer;

                RS = CS.ExecuteReader();

                while (RS.Read())
                {
                    Server.Rows.Add(new Object[] {
                    Convert.ToString(RS["PrimaryKey"]), Convert.ToString(RS["LocalData"]), Convert.ToString(RS["ServerData"])
                });
                }               

                RS.Close();

                // ________________ Get Local Table Data Summary ________________

                if (!TransferMode)
                {

                    CL = new SqlCommand("SELECT CONVERT(VARCHAR(MAX), HASHBYTES('" + HashAlgorythim + "', Row.Primarykey), 1) AS PrimaryKey, " +
                    "CONVERT(VARCHAR(MAX), HASHBYTES('" + HashAlgorythim + "', Row.Contents), 1) AS LocalData, '' AS ServerData FROM " + LocalDBTableName + Functions.NewLine() +
                    "CROSS APPLY (" + Functions.NewLine() +
                    "SELECT PrimaryKey = CAST((SELECT " + PrimaryKey + " FROM (SELECT 1) AS Row (y) FOR XML AUTO, BINARY BASE64) AS VARCHAR(MAX))," + Functions.NewLine() +
                    "Contents = CAST((SELECT " + LocalTableCols + " FROM (SELECT 1) AS Row (y) FOR XML AUTO, BINARY BASE64) AS VARCHAR(MAX))" + Functions.NewLine() +
                    ") AS Row",
                    LocalDBConnection);

                    CL.Transaction = LocalTransactionForChanges;

                    RL = CL.ExecuteReader();

                    while (RL.Read())
                    {
                        Local.Rows.Add(new Object[] {
                        Convert.ToString(RL["PrimaryKey"]), Convert.ToString(RL["LocalData"]), Convert.ToString(RL["ServerData"])
                    });
                    }

                    RL.Close();       

                }

                // ________________ Data Summary Tables Union ________________

                SyncTable.Merge(Local, true, MissingSchemaAction.Ignore);
                SyncTable.Merge(Server, true, MissingSchemaAction.Ignore);

                // ________________ Group by Primary Key ________________ 

                var MixedData = from Rows in SyncTable.AsEnumerable()
                                group Rows by Rows.Field<String>("PrimaryKey") into GroupedData
                                select new
                                {
                                    PrimaryKey = GroupedData.Key,                                    
                                    LocalData = String.Join(String.Empty, GroupedData.Select(Rows => Rows.Field<String>("LocalData"))),
                                    ServerData = String.Join(String.Empty, GroupedData.Select(Rows => Rows.Field<String>("ServerData")))
                                };

                // ________________ Start Sync ________________

                foreach (var Item in MixedData)
                {
                    Console.WriteLine(
                        "PrimaryKey: " + '"' + Item.PrimaryKey + '"' + " - " +
                        "LocalData:" + '"' + Item.LocalData + '"' + " - " +
                        "ServerData:" + '"' + Item.ServerData + '"' + " - "
                        + (Item.ServerData == String.Empty ? "Action: Delete in Local" : (Item.LocalData == String.Empty ? "Action: Insert in Local" : (!(Item.ServerData == Item.LocalData) ? "Action: Update in Local" : "Values are up to date")))
                        );

                    if (Item.LocalData == Item.ServerData) { Console.WriteLine("Values up to date! Ignored."); continue; }

                    DataSet LocalData = new DataSet("LocalData");
                    
                    SqlCommand LocalItemQuery = new SqlCommand("SELECT " + LocalTableCols +
                    Functions.NewLine() + "FROM " + LocalDBTableName + Functions.NewLine() +
                    "CROSS APPLY (" + Functions.NewLine() +
                    "SELECT PrimaryKey = CAST((SELECT " + PrimaryKey + " FROM (SELECT 1) AS Row (y) FOR XML AUTO, BINARY BASE64) AS VARCHAR(MAX))" + Functions.NewLine() +
                    ") AS Row" + Functions.NewLine() +
                    "WHERE CONVERT(VARCHAR(MAX), HASHBYTES('" + HashAlgorythim + "', Row.PrimaryKey), 1) = '" + Item.PrimaryKey + "' COLLATE Latin1_General_CS_AS",
                    LocalDBConnection);

                    LocalItemQuery.Transaction = LocalTransactionForChanges;

                    SqlDataAdapter LocalAdapter = new SqlDataAdapter();
                    LocalAdapter.SelectCommand = LocalItemQuery;

                    SqlCommandBuilder LocalDML = new SqlCommandBuilder(LocalAdapter);
                    LocalAdapter.Fill(LocalData, LocalDBTableName);
                    
                    LocalAdapter.DeleteCommand = LocalDML.GetDeleteCommand(false);
                    LocalAdapter.DeleteCommand.Transaction = LocalTransactionForChanges;
                    LocalAdapter.InsertCommand = LocalDML.GetInsertCommand(false);
                    LocalAdapter.InsertCommand.Transaction = LocalTransactionForChanges;
                    LocalAdapter.UpdateCommand = LocalDML.GetUpdateCommand(false);
                    LocalAdapter.UpdateCommand.Transaction = LocalTransactionForChanges;

                    DataSet ServerData = new DataSet("ServerData");

                    SqlCommand ServerItemQuery = new SqlCommand("SELECT " + LocalTableCols + 
                    Functions.NewLine() + "FROM " + ServerDBTableName + Functions.NewLine() +
                    "CROSS APPLY (" + Functions.NewLine() +
                    "SELECT PrimaryKey = CAST((SELECT " + PrimaryKey + " FROM (SELECT 1) AS Row (y) FOR XML AUTO, BINARY BASE64) AS VARCHAR(MAX))" + Functions.NewLine() +
                    ") AS Row" + Functions.NewLine() +
                    "WHERE CONVERT(VARCHAR(MAX), HASHBYTES('" + HashAlgorythim + "', Row.PrimaryKey), 1) = '" + Item.PrimaryKey + "' COLLATE Latin1_General_CS_AS", ServerDBConnection);

                    SqlDataAdapter ServerAdapter = new SqlDataAdapter(ServerItemQuery);
                    SqlCommandBuilder ServerDML = new SqlCommandBuilder(ServerAdapter);
                    ServerAdapter.Fill(ServerData, ServerDBTableName);

                    ServerAdapter.DeleteCommand = ServerDML.GetDeleteCommand(false);
                    ServerAdapter.DeleteCommand.Transaction = RemoteTransactionForTransfer;
                    //ServerAdapter.InsertCommand = ServerDML.GetInsertCommand(false);

                    if (TransferMode) // Means you only want to send new data from a table to another. It's different than Synchronizing Data...
                    {
                        LocalData.Tables[LocalDBTableName].Rows.Add(
                            ServerData.Tables[ServerDBTableName].Rows[0].ItemArray);

                        // LocalData.AcceptChanges();
                        LocalAdapter.Update(LocalData, LocalDBTableName);

                        ServerAdapter.DeleteCommand.CommandText = "DELETE " + ServerDBTableName + " FROM " + ServerDBTableName + Functions.NewLine() +
                        "CROSS APPLY (" + Functions.NewLine() +
                        "SELECT PrimaryKey = CAST((SELECT " + PrimaryKey + " FROM (SELECT 1) AS Row (y) FOR XML AUTO, BINARY BASE64) AS VARCHAR(MAX))" + Functions.NewLine() +
                        ") AS Row" + Functions.NewLine() +
                        "WHERE CONVERT(VARCHAR(MAX), HASHBYTES('" + HashAlgorythim + "', Row.PrimaryKey), 1) = '" + Item.PrimaryKey + "' COLLATE Latin1_General_CS_AS";
                        
                        ServerAdapter.DeleteCommand.Parameters.Clear();
                        ServerAdapter.DeleteCommand.ExecuteNonQuery(); // ServerAdapter.Update(LocalData, LocalDBTableName);                    
                    }
                    else
                    {
                        // If the row no longer exists in the Source, it shall be deleted locally as well,
                        // unless DeleteLocalRows = false, in this case Row Deletions should be handled locally by some application
                        // and the Server Query is filtered by some condition.

                        if (Item.ServerData == String.Empty)
                        {

                            if (DeleteLocalRows)
                            { 
                                //LocalData.Tables[" + LocalDBTableName + "].Rows.RemoveAt(0);

                                // LocalData.AcceptChanges();

                                LocalAdapter.DeleteCommand.CommandText = "DELETE " + LocalDBTableName + " FROM " + LocalDBTableName + Functions.NewLine() +
                                "CROSS APPLY (" + Functions.NewLine() +
                                "SELECT PrimaryKey = CAST((SELECT " + PrimaryKey + " FROM (SELECT 1) AS Row (y) FOR XML AUTO, BINARY BASE64) AS VARCHAR(MAX))" + Functions.NewLine() +
                                ") AS Row" + Functions.NewLine() +
                                "WHERE CONVERT(VARCHAR(MAX), HASHBYTES('" + HashAlgorythim + "', Row.PrimaryKey), 1) = '" + Item.PrimaryKey + "' COLLATE Latin1_General_CS_AS";                                
                            
                                LocalAdapter.DeleteCommand.Parameters.Clear();
                                LocalAdapter.DeleteCommand.ExecuteNonQuery(); // LocalAdapter.Update(LocalData, LocalDBTableName);
                            }

                            // Local.Rows.Remove(Local.Rows.Find(new Object[] { Item.PrimaryKey }));

                        }
                        else if (Item.LocalData == String.Empty) // If the row doesn't exist in the Local table, it's new and shall be synchronized locally.
                        {
                            LocalData.Tables[LocalDBTableName].Rows.Add(
                                ServerData.Tables[ServerDBTableName].Rows[0].ItemArray);

                            // LocalData.AcceptChanges();
                            LocalAdapter.Update(LocalData, LocalDBTableName);
                        }
                        else if (!(Item.ServerData == Item.LocalData)) // If the row exists in both, but the content of their NON-PK fields is different, then it shall be synchronized locally.
                        {

                            if (PersistLocalInfo) // This means: Update the Local row and persist any data not included by LocalIgnoredCols in the Local table.
                            {   
                                
                                foreach (DataColumn Field in LocalData.Tables[LocalDBTableName].Columns)
                                {
                                    LocalData.Tables[LocalDBTableName].Rows[0][Field] = ServerData.Tables[ServerDBTableName].Rows[0][Field.ColumnName];
                                }

                                LocalAdapter.Update(LocalData, LocalDBTableName);

                            }
                            else // Don't care about LocalIgnoredCols and reinsert the row. LocalIgnoredCols will contain default values.
                            {

                                //LocalData.Tables[LocalDBTableName].Rows.RemoveAt(0);

                                LocalAdapter.DeleteCommand.CommandText = "DELETE " + LocalDBTableName + " FROM " + LocalDBTableName + Functions.NewLine() +
                                "CROSS APPLY (" + Functions.NewLine() +
                                "SELECT PrimaryKey = CAST((SELECT " + PrimaryKey + " FROM (SELECT 1) AS Row (y) FOR XML AUTO, BINARY BASE64) AS VARCHAR(MAX))" + Functions.NewLine() +
                                ") AS Row" + Functions.NewLine() +
                                "WHERE CONVERT(VARCHAR(MAX), HASHBYTES('" + HashAlgorythim + "', Row.PrimaryKey), 1) = '" + Item.PrimaryKey + "' COLLATE Latin1_General_CS_AS";

                                LocalAdapter.DeleteCommand.Parameters.Clear();
                                LocalAdapter.DeleteCommand.ExecuteNonQuery(); // LocalAdapter.Update(LocalData, LocalDBTableName);

                                LocalData.Tables[LocalDBTableName].Rows.Add(
                                    ServerData.Tables[ServerDBTableName].Rows[0].ItemArray
                                );

                                LocalAdapter.Update(LocalData, LocalDBTableName);

                                // LocalData.AcceptChanges();
                                // LocalAdapter.Update(LocalData, LocalDBTableName);

                            }

                        }

                        // else { } // Both rows are equal in their content so they are fine. Ignore them.

                    }

                }

                Boolean Success = true;

                return Success;

            }
            catch (Exception e)
            {

                if ((RL != null ? !RL.IsClosed : false))               
                    RL.Close();
                if ((RS != null ? !RS.IsClosed : false))
                    RS.Close();

                Boolean Fail = false;                

                Console.WriteLine(Functions.NewLine() + e.Message);
                return Fail;
            }

        }       

        public static Boolean FillContentDistributionSerials(SqlConnection Connection, SqlTransaction LocalTransactionForChanges = null)
        {

            try
            {

                DataSet Data = new DataSet("Data");

                SqlCommand MediaPlayerQuery = Functions.getParameterizedCommand("SELECT TOP (1) ActivationKey FROM MEDIAPLAYER", Connection, null);
                
                MediaPlayerQuery.Transaction = LocalTransactionForChanges;

                String Key = MediaPlayerQuery.ExecuteScalar().ToString();

                //System.Windows.Forms.MessageBox.Show(Key); // Debug

                SqlCommand ADSQuery = new SqlCommand("SELECT ID, ADVERTISER_ID, FileName, Extension, ContentSerial, FileSerial, Type, SizeInBytes" +
                Functions.NewLine() + "FROM ADS WHERE ((ContentSerial = '' OR ContentSerial IS NULL) OR (FileSerial = '' OR FileSerial IS NULL))",
                Connection);

                ADSQuery.Transaction = LocalTransactionForChanges;

                SqlDataAdapter Adapter = new SqlDataAdapter();

                Adapter.SelectCommand = ADSQuery;

                SqlCommandBuilder LocalDML = new SqlCommandBuilder(Adapter);
                
                Adapter.Fill(Data, "ADS");

                Adapter.UpdateCommand = LocalDML.GetUpdateCommand(false);
                Adapter.UpdateCommand.Transaction = LocalTransactionForChanges;

                Adapter.UpdateBatchSize = 5000;

                foreach (DataRow Item in Data.Tables["ADS"].Rows)
                {
                    try
                    {

                        String ContentSerial = String.Empty;
                        String FileSerial = String.Empty;
                        String FileName = Item["FileName"].ToString();
                        String AdvertiserID = Item["ADVERTISER_ID"].ToString();
                        String ContentID = Item["ID"].ToString();
                        String Extension = Item["Extension"].ToString();

                        //System.Windows.Forms.MessageBox.Show(FileName + " - " + AdvertiserID + " - " + ContentID + " - " + Extension); // Debug

                        Program.ContentType ContentType = (Program.ContentType) Convert.ToInt32(Item["Type"]);
                        
                        if (!Key.isUndefined())
                        {
                            ContentSerial = new BIGWISE.ProductStatus.Register().GetActivationKey(FileName, Key, Program.ProductCode);
                            Item["ContentSerial"] = ContentSerial;
                        }
                        
                        //System.Windows.Forms.MessageBox.Show(ContentSerial); // Debug

                        long Bytes = -1;

                        switch (ContentType)
                        {
                             
                            case Program.ContentType.Image: case Program.ContentType.Video:
                            {

                                String FilePath = System.Environment.GetEnvironmentVariable("ProgramData") + @"\" +
                                Properties.Settings.Default.ProgramDataDir + Properties.Settings.Default.ApplicationName + @"\" + Properties.Settings.Default.CampaignFilesDir +
                                (AdvertiserID.isUndefined() ? String.Empty : AdvertiserID + @"\") + (ContentID.isUndefined() ? String.Empty : ContentID + @"\") + FileName;

                                if (System.IO.File.Exists(FilePath))
                                {
                                    Bytes = new System.IO.FileInfo(FilePath).Length;
                                    FileSerial = DataEncryption.EncryptText(FileName + "|" + Bytes.ToString(), Functions.EncryptionKey);
                                    Item["FileSerial"] = FileSerial;
                                    Item["SizeInBytes"] = Bytes;
                                } else { Item["ContentSerial"] = String.Empty; }

                                break;

                            }       
                     
                            case Program.ContentType.WebApp:
                            {

                                /*
                                String WebAppFolderPath = System.Environment.GetEnvironmentVariable("ProgramData") + @"\" +
                                Properties.Settings.Default.ProgramDataDir + Properties.Settings.Default.ApplicationName + @"\" + Properties.Settings.Default.CampaignFilesDir +
                                (AdvertiserID.isUndefined() ? String.Empty : AdvertiserID + @"\") + (ContentID.isUndefined() ? String.Empty : ContentID + @"\") + FileName.Substring(0, FileName.Length - Extension.Length);
                                */

                                String WebAppFilePath = System.Environment.GetEnvironmentVariable("ProgramData") + @"\" +
                                Properties.Settings.Default.ProgramDataDir + Properties.Settings.Default.ApplicationName + @"\" + Properties.Settings.Default.CampaignFilesDir +
                                (AdvertiserID.isUndefined() ? String.Empty : AdvertiserID + @"\") + (ContentID.isUndefined() ? String.Empty : ContentID + @"\") + FileName;

                                if (System.IO.File.Exists(WebAppFilePath)) //if (System.IO.Directory.Exists(WebAppFolderPath))
                                {
                                    Bytes = Functions.GetZipExtractedSize(WebAppFilePath); //Functions.GetDirectorySize (WebAppFolderPath);
                                    FileSerial = DataEncryption.EncryptText(FileName + "|" + Bytes.ToString(), Functions.EncryptionKey);
                                    Item["FileSerial"] = FileSerial;
                                    Item["SizeInBytes"] = Bytes;
                                } else { Item["ContentSerial"] = String.Empty; }

                                break;

                            }

                            case Program.ContentType.WebLink:
                            {

                                Bytes = 0;
                                FileSerial = DataEncryption.EncryptText(FileName + "|" + Bytes.ToString(), Functions.EncryptionKey);
                                Item["FileSerial"] = FileSerial;
                                Item["SizeInBytes"] = Bytes;
                                break;
                            }

                        }
                                               
                    }
                    catch (Exception Any)
                    {
                        Console.WriteLine(Any.Message);
                    }
                }

                Adapter.Update(Data, "ADS");

                return true;

            }
            catch (Exception Any)
            {
                Console.WriteLine(Any.Message);

                return false;
            }   
        
        }

        public void executeSync()
        {
            syncScope("AdsScope");
            syncScope("CampaignScope");
            syncScope("CampaignxAdsScope");
            syncScope("CampaignxMediaplayerScope");
            syncScope("MediaplayerScope");
            syncScope("TemplatesScope");
            Console.WriteLine("Synchronization Process Completed.");
        }

        static void Program_ApplyChangeFailed(object sender, DbApplyChangeFailedEventArgs e)
        {
            Console.WriteLine(e.Conflict.Type);
            Console.WriteLine(e.Error);
        }

        public void syncScope(string scopeName)
        {
            Functions f = new Functions();

            try
            {
                //SqlConnection clientConn = new SqlConnection(@"Data Source=" + f.localServer + "; Initial Catalog=DSSRV; User Id=" + f.localDBUser + "; Password=" + f.localDBPw + "");
                //SqlConnection clientConn = Properties.Settings.Default.LocalDB_WindowsAuth ? Functions.getAlternateTrustedConnection() : Functions.getAlternateConnection(DataEncryption.DecryptText(Properties.Settings.Default.LocalDB_SQLUser, Properties.Settings.Default.EncryptionKey), DataEncryption.DecryptText(Properties.Settings.Default.LocalDB_SQLPass, Properties.Settings.Default.EncryptionKey));

                //SqlConnection serverConn = new SqlConnection("Data Source=198.71.226.6; Initial Catalog=DSSRV; User Id=aleacosta5; Password=R131100a");
                //SqlConnection serverConn = new SqlConnection(@"Data Source=" + f.remoteServer + "; Initial Catalog=DSSRV; User Id=" + f.remoteDBUser + "; Password=" + f.remoteDBPw + "");
                //SqlConnection serverConn = Properties.Settings.Default.RemoteDB_WindowsAuth ? Functions.getAlternateTrustedConnection() : Functions.getAlternateConnection(DataEncryption.DecryptText(Properties.Settings.Default.RemoteDB_SQLUser, Properties.Settings.Default.EncryptionKey), DataEncryption.DecryptText(Properties.Settings.Default.RemoteDB_SQLPass, Properties.Settings.Default.EncryptionKey));

                SqlConnection clientConn = Properties.Settings.Default.LocalDB_WindowsAuth ?
                    Functions.getAlternateTrustedConnection()
                        :
                    Functions.getAlternateConnection(
                        DataEncryption.DecryptText(Properties.Settings.Default.LocalDB_SQLUser, Functions.EncryptionKey),
                        DataEncryption.DecryptText(Properties.Settings.Default.LocalDB_SQLPass, Functions.EncryptionKey));
                SqlConnection serverConn = Properties.Settings.Default.RemoteDB_WindowsAuth ?
                    Functions.getAlternateTrustedConnection(
                        DataEncryption.DecryptText(Properties.Settings.Default.RemoteDB_SQLServerName, Functions.EncryptionKey),
                        DataEncryption.DecryptText(Properties.Settings.Default.RemoteDB_SQLDBName, Functions.EncryptionKey))
                        :
                    Functions.getAlternateConnection(
                        DataEncryption.DecryptText(Properties.Settings.Default.RemoteDB_SQLServerName, Functions.EncryptionKey),
                        DataEncryption.DecryptText(Properties.Settings.Default.RemoteDB_SQLDBName, Functions.EncryptionKey),
                        DataEncryption.DecryptText(Properties.Settings.Default.RemoteDB_SQLUser, Functions.EncryptionKey),
                        DataEncryption.DecryptText(Properties.Settings.Default.RemoteDB_SQLPass, Functions.EncryptionKey));

                // create the sync orhcestrator
                SyncOrchestrator syncOrchestrator = new SyncOrchestrator();

                // set local provider of orchestrator to a sync provider associated with the 
                // ProductsScope in the SyncExpressDB express client database
                syncOrchestrator.LocalProvider = new SqlSyncProvider(scopeName, clientConn);

                // set the remote provider of orchestrator to a server sync provider associated with
                // the ProductsScope in the SyncDB server database
                syncOrchestrator.RemoteProvider = new SqlSyncProvider(scopeName, serverConn);

                // set the direction of sync session to Upload and Download
                syncOrchestrator.Direction = SyncDirectionOrder.Download;

                // subscribe for errors that occur when applying changes to the client
                ((SqlSyncProvider)syncOrchestrator.LocalProvider).ApplyChangeFailed += new EventHandler<DbApplyChangeFailedEventArgs>(Program_ApplyChangeFailed);

                // execute the synchronization process
                SyncOperationStatistics syncStats = syncOrchestrator.Synchronize();

                // print statistics
                Console.WriteLine("Start Time: " + syncStats.SyncStartTime);
                Console.WriteLine("Total Changes Uploaded: " + syncStats.UploadChangesTotal);
                Console.WriteLine("Total Changes Downloaded: " + syncStats.DownloadChangesTotal);
                Console.WriteLine("Complete Time: " + syncStats.SyncEndTime);
                Console.WriteLine(String.Empty);
            }
            catch (SyncException se)
            {
                Console.WriteLine(se.Message);
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
            }
        }
    }
}
