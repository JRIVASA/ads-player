﻿using isADS_SYNC.Clases;
using Microsoft.Synchronization.Data;
using Microsoft.Synchronization.Data.SqlServer;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace isADS_SYNC
{
    class ProvisionClient
    {
        public void executeProvision()
        {
            createScope("AdsScope");
            createScope("CampaignScope");
            createScope("CampaignxAdsScope");
            createScope("CampaignxMediaplayerScope");
            createScope("MediaplayerScope");
            createScope("TemplatesScope");
            Console.WriteLine("Client provision already updated.");
        }

        public void createScope(string scopeName)
        {
            Functions f = new Functions();

            try
            {
                //SqlConnection clientConn = new SqlConnection(@"Data Source=" + f.localServer + "; Initial Catalog=DSSRV; User Id=" + f.localDBUser + "; Password=" + f.localDBPw + "");
                //SqlConnection serverConn = new SqlConnection(@"Data Source=" + f.remoteServer + "; Initial Catalog=DSSRV; User Id=" + f.remoteDBUser + "; Password=" + f.remoteDBPw + "");
                SqlConnection clientConn = Properties.Settings.Default.LocalDB_WindowsAuth ?
                    Functions.getAlternateTrustedConnection()
                        :
                    Functions.getAlternateConnection(
                        DataEncryption.DecryptText(Properties.Settings.Default.LocalDB_SQLUser, Functions.EncryptionKey),
                        DataEncryption.DecryptText(Properties.Settings.Default.LocalDB_SQLPass, Functions.EncryptionKey));
                SqlConnection serverConn = Properties.Settings.Default.RemoteDB_WindowsAuth ?
                    Functions.getAlternateTrustedConnection(
                        DataEncryption.DecryptText(Properties.Settings.Default.RemoteDB_SQLServerName, Functions.EncryptionKey),
                        DataEncryption.DecryptText(Properties.Settings.Default.RemoteDB_SQLDBName, Functions.EncryptionKey))
                        :
                    Functions.getAlternateConnection(
                        DataEncryption.DecryptText(Properties.Settings.Default.RemoteDB_SQLServerName, Functions.EncryptionKey),
                        DataEncryption.DecryptText(Properties.Settings.Default.RemoteDB_SQLDBName, Functions.EncryptionKey),
                        DataEncryption.DecryptText(Properties.Settings.Default.RemoteDB_SQLUser, Functions.EncryptionKey),
                        DataEncryption.DecryptText(Properties.Settings.Default.RemoteDB_SQLPass, Functions.EncryptionKey));


                // get the description of ProductsScope from the SyncDB server database
                DbSyncScopeDescription scopeDesc = SqlSyncDescriptionBuilder.GetDescriptionForScope(scopeName, serverConn);

                // create server provisioning object based on the ProductsScope
                SqlSyncScopeProvisioning clientProvision = new SqlSyncScopeProvisioning(clientConn, scopeDesc);

                if (!clientProvision.ScopeExists(scopeName))
                {
                    clientProvision.Apply();
                    Console.WriteLine("Client Scope created on table: " + scopeName);
                }
            }
            catch (DbNotProvisionedException dbnot)
            {
                Console.WriteLine(dbnot.Message + " Ejecutar primero el ProvisionServer");
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
            }
        }
    }
}
