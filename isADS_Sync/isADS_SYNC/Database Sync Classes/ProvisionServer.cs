﻿using isADS_SYNC.Clases;
using Microsoft.Synchronization.Data;
using Microsoft.Synchronization.Data.SqlServer;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace isADS_SYNC
{
    class ProvisionServer
    {
        public void executeProvision()
        {
            createScope("AdsScope", "ADS");
            createScope("CampaignScope", "CAMPAIGN");
            createScope("CampaignxAdsScope", "CAMPAIGNxADS");
            createScope("CampaignxMediaplayerScope", "CAMPAIGNxMEDIAPLAYER");
            createScope("MediaplayerScope", "MEDIAPLAYER");
            createScope("TemplatesScope", "TEMPLATES");
            Console.WriteLine("Server provision already updated.");
        }

        public void createScope(string scopeName, string tableName)
        {
            Functions f = new Functions();

            try
            {
                //SqlConnection serverConn = new SqlConnection(@"Data Source=" + f.remoteServer + "; Initial Catalog=DSSRV; User Id=" + f.remoteDBUser + "; Password=" + f.remoteDBPw + "");
                SqlConnection serverConn = Properties.Settings.Default.RemoteDB_WindowsAuth ?
                    Functions.getAlternateTrustedConnection(
                        DataEncryption.DecryptText(Properties.Settings.Default.RemoteDB_SQLServerName, Functions.EncryptionKey),
                        DataEncryption.DecryptText(Properties.Settings.Default.RemoteDB_SQLDBName, Functions.EncryptionKey))
                        :
                    Functions.getAlternateConnection(
                        DataEncryption.DecryptText(Properties.Settings.Default.RemoteDB_SQLServerName, Functions.EncryptionKey),
                        DataEncryption.DecryptText(Properties.Settings.Default.RemoteDB_SQLDBName, Functions.EncryptionKey),
                        DataEncryption.DecryptText(Properties.Settings.Default.RemoteDB_SQLUser, Functions.EncryptionKey),
                        DataEncryption.DecryptText(Properties.Settings.Default.RemoteDB_SQLPass, Functions.EncryptionKey));

                // define a new scope named ProductsScope
                DbSyncScopeDescription scopeDesc = new DbSyncScopeDescription(scopeName);

                // get the description of the Products table from SyncDB dtabase
                DbSyncTableDescription tableDesc = SqlSyncDescriptionBuilder.GetDescriptionForTable(tableName, serverConn);

                // add the table description to the sync scope definition
                scopeDesc.Tables.Add(tableDesc);

                // create a server scope provisioning object based on the ProductScope
                SqlSyncScopeProvisioning serverProvision = new SqlSyncScopeProvisioning(serverConn, scopeDesc);

                // skipping the creation of table since table already exists on server
                serverProvision.SetCreateTableDefault(DbSyncCreationOption.Skip);

                if (!serverProvision.ScopeExists(scopeName))
                {
                    serverProvision.Apply();
                    Console.WriteLine("Server Provision created on table: " + tableName);
                }
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
            }
        }

        private String getCurrentOrganization()
        {

            string License = Properties.Settings.Default.OrganizationLicense;

            SqlConnection serverConn = Properties.Settings.Default.RemoteDB_WindowsAuth ?
                Functions.getAlternateTrustedConnection(
                    DataEncryption.DecryptText(Properties.Settings.Default.RemoteDB_SQLServerName, Functions.EncryptionKey),
                    DataEncryption.DecryptText(Properties.Settings.Default.RemoteDB_SQLDBName, Functions.EncryptionKey))
                    :
                Functions.getAlternateConnection(
                    DataEncryption.DecryptText(Properties.Settings.Default.RemoteDB_SQLServerName, Functions.EncryptionKey),
                    DataEncryption.DecryptText(Properties.Settings.Default.RemoteDB_SQLDBName, Functions.EncryptionKey),
                    DataEncryption.DecryptText(Properties.Settings.Default.RemoteDB_SQLUser, Functions.EncryptionKey),
                    DataEncryption.DecryptText(Properties.Settings.Default.RemoteDB_SQLPass, Functions.EncryptionKey));
            
            string OrganizationID = Functions.getOrganizationID(serverConn, License);

            return OrganizationID;

        }

        public void executeFilteredProvision()
        {

            String OrganizationID = getCurrentOrganization(/* Get OrganizationID Based on Properties.Settings.Default.OrganizationLicense */);

            //createFilteredScope("AdsScope", "ADS", OrganizationID);
            //createFilteredScope("CampaignScope", "CAMPAIGN", OrganizationID);
            //createFilteredScope("CampaignxAdsScope", "CAMPAIGNxADS", OrganizationID);
            //createFilteredScope("CampaignxMediaplayerScope", "CAMPAIGNxMEDIAPLAYER", OrganizationID);
            //createFilteredScope("MediaplayerScope", "MEDIAPLAYER", OrganizationID);
            //createFilteredScope("TemplatesScope", "TEMPLATES", OrganizationID);
            Console.WriteLine("Server provision already updated.");
        }

        public void createFilteredScope(string scopeName, string tableName, string OrganizationID, string FilterQuery)
        {
            Functions f = new Functions();

            try
            {
                //SqlConnection serverConn = new SqlConnection(@"Data Source=" + f.remoteServer + "; Initial Catalog=DSSRV; User Id=" + f.remoteDBUser + "; Password=" + f.remoteDBPw + "");
                SqlConnection serverConn = Properties.Settings.Default.RemoteDB_WindowsAuth ?
                    Functions.getAlternateTrustedConnection(
                        DataEncryption.DecryptText(Properties.Settings.Default.RemoteDB_SQLServerName, Functions.EncryptionKey),
                        DataEncryption.DecryptText(Properties.Settings.Default.RemoteDB_SQLDBName, Functions.EncryptionKey))
                        :
                    Functions.getAlternateConnection(
                        DataEncryption.DecryptText(Properties.Settings.Default.RemoteDB_SQLServerName, Functions.EncryptionKey),
                        DataEncryption.DecryptText(Properties.Settings.Default.RemoteDB_SQLDBName, Functions.EncryptionKey),
                        DataEncryption.DecryptText(Properties.Settings.Default.RemoteDB_SQLUser, Functions.EncryptionKey),
                        DataEncryption.DecryptText(Properties.Settings.Default.RemoteDB_SQLPass, Functions.EncryptionKey));

                // define a new scope named ProductsScope
                DbSyncScopeDescription scopeDesc = new DbSyncScopeDescription(scopeName);

                // get the description of the Products table from SyncDB dtabase
                DbSyncTableDescription tableDesc = SqlSyncDescriptionBuilder.GetDescriptionForTable(tableName, serverConn);

                // add the table description to the sync scope definition
                scopeDesc.Tables.Add(tableDesc);

                // create a server scope provisioning object based on the ProductScope
                SqlSyncScopeProvisioning serverProvision = new SqlSyncScopeProvisioning(serverConn, scopeDesc);

                serverProvision.Tables["isADS." + tableName].AddFilterColumn("Organization_ID");
                serverProvision.Tables["isADS." + tableName].FilterClause = "[" + tableName + "].[Organization_ID] = '" + OrganizationID + "'";

                // skipping the creation of table since table already exists on server
                serverProvision.SetCreateTableDefault(DbSyncCreationOption.Skip);

                if (!serverProvision.ScopeExists(scopeName))
                {
                    serverProvision.Apply();
                    Console.WriteLine("Server Provision created on table: " + tableName);
                }
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
            }
        }


    }
}
