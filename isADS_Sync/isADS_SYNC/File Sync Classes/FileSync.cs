﻿using Microsoft.Synchronization;
using Microsoft.Synchronization.Files;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Net;
using isADS_SYNC.Clases;

namespace isADS_SYNC
{
    class FileSync
    {
        private enum ContentType
        {
            File,
            Folder
        }

        public void ExecuteSync(string sourceDir, string remoteDir)
        {
            FileSyncProvider origen = new FileSyncProvider(@sourceDir, null, FileSyncOptions.CompareFileStreams);
            FileSyncProvider destino = new FileSyncProvider(@remoteDir, null, FileSyncOptions.CompareFileStreams);

            try
            {
                origen.DetectChanges();
                //destino.DetectChanges();

                SyncOrchestrator so = new SyncOrchestrator();
                so.Direction = SyncDirectionOrder.Download;
                so.RemoteProvider = origen;
                so.LocalProvider = destino;

                SyncOperationStatistics syncStats = so.Synchronize();

                Console.WriteLine("Hora Inicio: " + syncStats.SyncStartTime);
                Console.WriteLine("Archivos Subidos: " + syncStats.UploadChangesTotal);
                Console.WriteLine("Archivos Descargados: " + syncStats.DownloadChangesTotal);
                Console.WriteLine("Hora Fin: " + syncStats.SyncEndTime);
            }
            catch (Exception e)
            {
                Console.WriteLine("Message:" + e.Message);
            }
            finally
            {
                origen.Dispose();
                destino.Dispose();
            }
        }

        private void Analyze(String Root, String Parent, String Content, ftp Ftp)
        {

            Root = Root.isUndefined() ? String.Empty : Root + "/";
            Parent = Parent.isUndefined() ? String.Empty : Parent + "\\"; 

            String LocalName = System.Environment.GetEnvironmentVariable("ProgramData") + @"\" + 
                Properties.Settings.Default.ProgramDataDir + Properties.Settings.Default.ApplicationName + @"\" + Properties.Settings.Default.CampaignFilesDir 
                + Parent + Content;

            ContentType ContentType;
            
            // Doing this because we will control files and won't allow dots for folders because it is problematic...
            // so far we didn't find a way with the current ftp facilities to determine if an archive was either
            // file or folder in the hosting repository, that's why we enforce this kind of validation.
            if (Content.Contains(".")) ContentType = ContentType.File; else ContentType = ContentType.Folder;

            switch (ContentType)
            {
                case ContentType.File:
                {
                    if (!File.Exists(LocalName))
                    {
                        String FilePath = Root.isUndefined() ? Content : Root + Content;
                            
                        try
                        {
                            Ftp.download(FilePath, LocalName);
                                
                            // Byte[] file;
                            // file = File.ReadAllBytes(LocalName);
                        }
                        catch (Exception e)
                        {
                            Console.WriteLine(e.Message);
                        }
                    }
                    else
                    {
                        String FilePath = Root.isUndefined() ? Content : Root + Content;

                        try
                        {

                            long RemoteFileSize = Ftp.getFileSizeinBytes(FilePath);

                            long CurrentFileSize = new FileInfo(LocalName).Length;

                            if (RemoteFileSize != CurrentFileSize)
                            {
                                // Most likely the Previous File Download process was cut by any reason or the file got updates.
                                File.Delete(LocalName);
                                Ftp.download(FilePath, LocalName);
                            }

                        }
                        catch (Exception Any) { Console.WriteLine(Any.Message); }

                    }

                    try
                    {
                        if (File.Exists(LocalName))
                        {
                            String Ext = new FileInfo(LocalName).Extension;
                            if (Ext.Equals(".zip", StringComparison.InvariantCultureIgnoreCase))
                            {                                                                       

                                //String ZipFileName = System.IO.Path.GetFileNameWithoutExtension(LocalName); 
                                // Replace Filename by Filename without extension would work but
                                // would be trouble if by any means it would match more than one
                                // Better be sure and just take off the extension part.

                                String NewDir = LocalName.Substring(0, LocalName.Length - Ext.Length);

                                if (!System.IO.Directory.Exists(NewDir)){
                                    System.IO.Directory.CreateDirectory(NewDir);
                                    System.IO.Compression.ZipFile.ExtractToDirectory(LocalName, NewDir);
                                }
                                else
                                {
                                    DirectoryInfo ExistingDirInfo = new DirectoryInfo(NewDir);
                                    if (ExistingDirInfo.GetDirectories().Length == 0 && ExistingDirInfo.GetFiles().Length == 0)
                                    {
                                        System.IO.Compression.ZipFile.ExtractToDirectory(LocalName, NewDir);
                                    }
                                }                                    
                            }
                        }
                    }
                    catch (Exception e)
                    {
                        try
                        {
                            String ZipFileName = System.IO.Path.GetFileNameWithoutExtension(LocalName);
                            String NewDir = LocalName.Replace(Content, ZipFileName);
                            System.IO.Directory.Delete(NewDir, true); 
                        }
                        catch (Exception) { }
                        Console.WriteLine(e.Message);
                    }

                    break;
                }

                case ContentType.Folder:
                {
                    if (!Content.StartsWith("Preview_", StringComparison.InvariantCultureIgnoreCase))
                    {

                        if (!System.IO.Directory.Exists(LocalName))
                        {
                            System.IO.Directory.CreateDirectory(LocalName);
                        }

                        Root = Root + Content;
                        Parent = Parent + Content;                    

                        string[] DirectoryContents = Ftp.directoryListSimple(Root);
                        Array.Sort(DirectoryContents);

                        foreach (string SubContent in DirectoryContents){
                            if (SubContent.Equals("")) continue;

                            Analyze(Root, Parent, SubContent, Ftp);
                        }

                    }
                    break;
                }
            }
        }

        private void Analyze(String Root, String Parent, String Content, ftp Ftp, String[] OnlyNeededADS)
        {

            Root = Root.isUndefined() ? String.Empty : Root + "/";
            Parent = Parent.isUndefined() ? String.Empty : Parent + "\\";

            String LocalName = System.Environment.GetEnvironmentVariable("ProgramData") + @"\" +
                Properties.Settings.Default.ProgramDataDir + Properties.Settings.Default.ApplicationName + @"\" + Properties.Settings.Default.CampaignFilesDir
                + Parent + Content;

            //System.Windows.Forms.MessageBox.Show(LocalName + " - " + Root + " - " + Content); // Debug

            ContentType ContentType;

            // Doing this because we will control files and won't allow dots for folders because it is problematic...
            // so far we didn't find a way with the current ftp facilities to determine if an archive was either
            // file or folder in the hosting repository, that's why we enforce this kind of validation.
            if (Content.Contains(".")) ContentType = ContentType.File; else ContentType = ContentType.Folder;

            switch (ContentType)
            {
                case ContentType.File:
                    {
                        if (!File.Exists(LocalName))
                        {
                            String FilePath = Root.isUndefined() ? Content : Root + Content;

                            try
                            {
                                Ftp.download(FilePath, LocalName);

                                // Byte[] file;
                                // file = File.ReadAllBytes(LocalName);
                            }
                            catch (Exception e)
                            {
                                Console.WriteLine(e.Message);
                            }
                        }
                        else
                        {
                            String FilePath = Root.isUndefined() ? Content : Root + Content;

                            try
                            {

                                long RemoteFileSize = Ftp.getFileSizeinBytes(FilePath);

                                long CurrentFileSize = new FileInfo(LocalName).Length;

                                if (RemoteFileSize != CurrentFileSize)
                                {
                                    // Most likely the Previous File Download process was cut by any reason or the file got updates.
                                    File.Delete(LocalName);
                                    Ftp.download(FilePath, LocalName);
                                }

                            } catch (Exception Any) { Console.WriteLine(Any.Message); }

                        }

                        try
                        {
                            if (File.Exists(LocalName))
                            {
                                String Ext = new FileInfo(LocalName).Extension;
                                if (Ext.Equals(".zip", StringComparison.InvariantCultureIgnoreCase))
                                {

                                    //String ZipFileName = System.IO.Path.GetFileNameWithoutExtension(LocalName); 
                                    // Replace Filename by Filename without extension would work but
                                    // would be trouble if by any means it would match more than one
                                    // Better be sure and just take off the extension part.

                                    String Dir = LocalName.Substring(0, LocalName.Length - Ext.Length);

                                    if (!System.IO.Directory.Exists(Dir))
                                    {
                                        System.IO.Directory.CreateDirectory(Dir);
                                        System.IO.Compression.ZipFile.ExtractToDirectory(LocalName, Dir);                                        
                                    }
                                    else
                                    {

                                        /*
                                        DirectoryInfo ExistingDirInfo = new DirectoryInfo(Dir);
                                        if (ExistingDirInfo.GetDirectories().Length == 0 && ExistingDirInfo.GetFiles().Length == 0)
                                        {
                                            System.IO.Compression.ZipFile.ExtractToDirectory(LocalName, Dir);
                                        }
                                        */

                                        long ExtractedBytes = Functions.GetZipExtractedSize(LocalName);
                                        long ExistingDirBytes = Functions.GetDirectorySize(Dir);

                                        if (ExistingDirBytes == 0) System.IO.Compression.ZipFile.ExtractToDirectory(LocalName, Dir);
                                        else if (ExistingDirBytes != ExtractedBytes)
                                        {
                                            System.IO.Directory.Delete(Dir, true);
                                            System.IO.Compression.ZipFile.ExtractToDirectory(LocalName, Dir);
                                        }
                                        //else The Application is unmodified, fully extracted and should be displaying correctly.

                                    }
                                }
                            }
                        }
                        catch (Exception e)
                        {
                            try
                            {
                                String ZipFileName = System.IO.Path.GetFileNameWithoutExtension(LocalName);
                                String Dir = LocalName.Replace(Content, ZipFileName);
                                System.IO.Directory.Delete(Dir, true);
                            }
                            catch (Exception) { }
                            Console.WriteLine(e.Message);
                        }

                        break;
                    }

                case ContentType.Folder:
                    {                       
                        if (!Content.StartsWith("Preview_", StringComparison.InvariantCultureIgnoreCase))
                        {

                            Root = Root + Content;
                            Parent = Parent + Content;

                            int ADSDirectoryLevel = 3;

                            Console.WriteLine(Root.Count(TmpChar => TmpChar.Equals('/')).ToString() + " - " + LocalName);

                            if (Root.Count(TmpChar => TmpChar.Equals('/')) == ADSDirectoryLevel)
                                if (!(OnlyNeededADS.Contains(Content, StringComparer.OrdinalIgnoreCase))) 
                                    break;

                            if (!System.IO.Directory.Exists(LocalName))
                            {
                                System.IO.Directory.CreateDirectory(LocalName);
                            }

                            string[] DirectoryContents = Ftp.directoryListSimple(Root);
                            Array.Sort(DirectoryContents);

                            foreach (string SubContent in DirectoryContents)
                            {
                                if (SubContent.Equals("")) continue;

                                Analyze(Root, Parent, SubContent, Ftp, OnlyNeededADS);
                            }

                        }
                        break;
                    }
            }
        }

        public void DownloadFromFTP(String OrganizationID)
        {
            // string downloadFile;
            string ContentName;
            // string localName;
            
            string[] registroFTP;

            ftp f = null;

            //f = (Properties.Settings.Default.RemoteDB_Beta ? new ftp("ftp://ftp.bigwise.com", "sgngebeta", "Bigwise123#") : new ftp("ftp://ftp.bigwise.com", "sgngemanager", "Bigwise123#")); // OLD

            f = (!Properties.Settings.Default.RemoteDB_Beta ? new ftp("ftp://107.180.104.255", "campaignfiles", "#qaA266t") : // Sitio Main (Prod)
            new ftp("ftp://107.180.104.255", "betasgngecf", "Me93um*9")); // Beta

            string folder = (Properties.Settings.Default.RemoteDB_Beta ? "/" + OrganizationID : "/" + OrganizationID);

            try
            {
                string[] ftpObject = f.directoryListSimple(folder);
                // string[] content;
                Array.Sort(ftpObject);
                // int index = ftpObject.Length - 2;

                foreach (string Object in ftpObject)
                {
                    if (Object.Equals("")) continue;

                    registroFTP = Object.Split('/');
                    ContentName = registroFTP[0];
                 

                    Analyze(folder, String.Empty, ContentName, f);
                }
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
            }
        }

        public void DownloadFromFTP(String OrganizationID, String[] OnlyNeededADS)
        {
            // string downloadFile;
            String ContentName;
            // string localName;

            String[] registroFTP;

            ftp f = null;

            //f = (Properties.Settings.Default.RemoteDB_Beta ? new ftp("ftp://ftp.bigwise.com", "sgngebeta", "Bigwise123#") : new ftp("ftp://ftp.bigwise.com", "sgngemanager", "Bigwise123#")); // OLD

            f = (!Properties.Settings.Default.RemoteDB_Beta ? new ftp("ftp://107.180.104.255", "campaignfiles", "#qaA266t") : // Sitio Main (Prod)
            new ftp("ftp://107.180.104.255", "betasgngecf", "Me93um*9")); // Beta

            String folder = (Properties.Settings.Default.RemoteDB_Beta ? "/" + OrganizationID : "/" + OrganizationID);

            try
            {
                String[] ftpObject = f.directoryListSimple(folder);

                //System.Windows.Forms.MessageBox.Show("ENTRO AL FTP:" + folder + "/" + ftpObject.Length.ToString() + " - " + ftpObject.ToString()); // Debug

                // string[] content;
                Array.Sort(ftpObject);
                // int index = ftpObject.Length - 2;

                foreach (String Object in ftpObject)
                {

                    //System.Windows.Forms.MessageBox.Show(Object); // Debug

                    if (Object.Equals("")) continue;

                    registroFTP = Object.Split('/');
                    ContentName = registroFTP[0];

                    Analyze(folder, String.Empty, ContentName, f, OnlyNeededADS);
                    
                }
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
            }
        }

        public Boolean DeleteUnusedFiles(String OrganizationID, String[] OnlyNeededADS)
        {

            String AdvID = null; String ContentID = null; String FileName = null; String[] ArrInfo = null; String LocalName = null;
            System.IO.FileInfo UnusedFile = null;

            foreach (String RawInfo in OnlyNeededADS)
            {

                ArrInfo = RawInfo.Split(new String[] { ":|:" }, StringSplitOptions.RemoveEmptyEntries);

                AdvID = ArrInfo[0];
                ContentID = ArrInfo[1];
                FileName = ArrInfo[2];

                LocalName = System.Environment.GetEnvironmentVariable("ProgramData") + @"\" +
                Properties.Settings.Default.ProgramDataDir + Properties.Settings.Default.ApplicationName + @"\" + Properties.Settings.Default.CampaignFilesDir
                + AdvID + @"\" + ContentID + @"\" + FileName;

                try
                {
                    
                    if (Program.DebugMode) System.Windows.Forms.MessageBox.Show("Verificando archivo inactivo:\n\n" + LocalName);
                    UnusedFile = new FileInfo(LocalName);

                    if (Program.DebugMode) System.Windows.Forms.MessageBox.Show(UnusedFile.Exists ? "Procediendo a eliminar...": "El archivo no existe o fue eliminado.");

                    if (UnusedFile.Exists)
                    {

                        UnusedFile.Directory.Delete(true);

                        if (Program.DebugMode) System.Windows.Forms.MessageBox.Show("Directorio Eliminado:\n\n" + UnusedFile.Directory.FullName);

                        UnusedFile = new FileInfo(LocalName);
                        if (UnusedFile.Exists)
                        {
                            if (Program.DebugMode) System.Windows.Forms.MessageBox.Show("No se pudo eliminar el directorio. Borrando archivo: \n\n" + LocalName);
                            UnusedFile.Delete();
                        }

                    }

                    if (Program.DebugMode) System.Windows.Forms.MessageBox.Show("Archivo eliminado.");

                }
                catch (Exception Any)
                {
                    if (Program.DebugMode) System.Windows.Forms.MessageBox.Show("Existe un Problema al eliminar el archivo:\n\n" + LocalName + "\n\nVerifique el Log de Error");
                    Program.Logger.EscribirLog(Any, "Error al intentar eliminar archivo no utilizado. [" + LocalName + "]");
                    return false;
                }

            }

            if (Program.DebugMode) System.Windows.Forms.MessageBox.Show("Rutina de limpieza finalizada");

            return true;

        }

    }
}
